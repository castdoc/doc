# doc repository

This repository is the production/live repository for https://doc.castsoftware.com.

A site called `doc` is configured in Cloudflare Pages and uses the `main`branch in the Hugo build process. You can view the results here: https://doc.castsoftware.com.
