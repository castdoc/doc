---
title: "Documentation migration status"
type: "status"
---

## Status as of 07 January 2024

### Technology Extension documentation

Technology Extension documentation (previously located in https://doc.castsoftware.com/display/TECHNOS) has been migrated as follows:

- the most recent release of each extension documentation is now located here: https://doc.castsoftware.com/technologies. This documentation will be updated going forward and is to be considered the reference.
- a static HTML export of the entire extension documentation (including older releases of extensions) as of 05 January 2024 is located here: https://doc.castsoftware.com/export/TECHNOS. This will not be updated and is retained only for archiving purposes.

### All other documentation

All other documentation is still available at https://doc-legacy.castsoftware.com. This documentation will be progressively moved to this new documentation system over the next few weeks.

### Search

Currently the search feature is an "offline" tool (a predefined list) and will be replaced soon by Google Programmable Search Engine.
