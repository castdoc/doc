---
title: CAST Documentation
description: Software, illuminated.
---

{{% blocks/cover title="CAST Documentation" image_anchor="top" height="full" %}}
{{% param description %}}
{.display-6}

<!-- 
Differences with 3 tile page:
- set "col-md-4" to "col-md-3" to get 4 equal columns of 25% each (col-md-4 will set 3 columns of 33.33..%)
- remove "float-xl-end" from first tile and "float-xl-start" from last tile so that they all centre equally
- set "col-md-3 mb-md-0 mb-3" on the last tile
-->

<!--<div class="row mt-md-8 mt-5 ">-->
<div class="row mt-md-8 mt-6 justify-content-center">
  <!--<div class="col-md-3 mb-md-0 mb-3 no-underline">-->
  <div class="col-md mb-3 no-underline">
    <a href="export/">
      <div class="card-home mx-auto" style="width: 200px; height: 200px;">
        <img class="mx-auto mt-3" src="./images/homepage/com.castsoftware.aip.png" width="75" height="75" alt="CAST Imaging v2">
        <div class="card-body">
          <p class="card-home-text" style="margin-top: -10px;">Imaging v2<br>Console v2<br>AIP Core 8.3</p>
        </div>
      </div>
     </a> 
  </div>
  <div class="col-md mb-3 no-underline">
    <a href="home/">
      <div class="card-home mx-auto" style="width: 200px; height: 200px;">
        <img class="mx-auto mt-3" src="./images/homepage/com.castsoftware.imaging.png" width="75" height="75" alt="CAST Imaging v3">
        <div class="card-body">
          <p class="card-home-text">Imaging v3</p>
        </div>
      </div>
     </a> 
  </div>   
  <div class="col-md mb-3 no-underline">
    <a href="imagingoncloud/">
      <div class="card-home mx-auto" style="width: 200px; height: 200px;">
        <img class="mx-auto mt-3" src="./images/homepage/com.castsoftware.imaging.cloud.png" width="112" height="80" alt="CAST Imaging on Cloud">
        <div class="card-body">
          <p class="card-home-text">Imaging on Cloud</p>
        </div>
      </div>
     </a> 
  </div>
  <div class="col-md mb-3 no-underline">
    <a href="https://doc.casthighlight.com" target="_blank" rel="noopener">
      <div class="card-home mx-auto" style="width: 200px; height: 200px;">
        <img class="mx-auto mt-3" src="./images/homepage/com.castsoftware.highlight.png" width="75" height="75" alt="CAST Highlight" >
        <div class="card-body">
          <p class="card-home-text">Highlight</p>
        </div>
      </div>
    </a> 
  </div>
  <div class="col-md mb-3 no-underline">
    <a href="https://appmarq.com" target="_blank" rel="noopener">
      <div class="card-home mx-auto" style="width: 200px; height: 200px;">
        <img class="mx-auto mt-3" src="./images/homepage/com.castsoftware.appmarq.png" width="75" height="75" alt="AppMarq">
        <div class="card-body">
          <p class="card-home-text">AppMarq</p>
        </div>
      </div>
    </a> 
  </div>
</div>

{{% /blocks/cover %}}