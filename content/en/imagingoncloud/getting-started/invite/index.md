---
title: "Invite new members"
linkTitle: "Invite new members"
type: "docs"
no_list: true
weight: 20
---

***

A user with the "administrator" role has permission to invite additional users to CAST Imaging on Cloud. The first user in any organization will automatically have the "administrator" role and can therefore grant access to other users. To invite additional users to an organization, the company administrator must follow these steps:

- Click **Members** in the left panel:

![](members1.jpg)

- Click the **Invite members** button (upper-right corner) and enter the email address of the users you want to invite:

![](members2.jpg)

- The invited users will receive an automated email from `noreply@castimaging.io`, welcoming them to CAST Imaging on Cloud on your behalf, with instructions on how to get started. These users will not be granted the administrator role.
