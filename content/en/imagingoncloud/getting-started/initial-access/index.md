---
title: "Initial access"
linkTitle: "Initial access"
type: "docs"
no_list: true
weight: 10
---

***

To access CAST Imaging on Cloud, request an account using the following link: https://www.castsoftware.com/cast-imaging-trial. A confirmation email will be sent to you containing instructions about how to:

- Create your account and access [CAST Imaging on Cloud](https://castimaging.io)
- Download, install and run **CAST Imaging Analyzer**

![](analyzer-download.jpg)

- Select your source code and run a profiling:

![](profiling.jpg)

- Run a deep analysis and upload your results:

![](run_analysis.jpg)

- Consult the results in CAST Imaging on Cloud:

![](cloud1.jpg)

![](cloud2.jpg)