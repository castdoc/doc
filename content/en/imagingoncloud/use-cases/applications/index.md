---
title: "Exploring applications with CAST Imaging on Cloud"
linkTitle: "Exploring applications"
type: "docs"
no_list: true
weight: 10
---

***

## Overview

CAST Imaging on Cloud provides architects and tech leads with a powerful visualization of an application's architecture, dependencies, and inner workings. By automatically mapping the source code, config files, database structure, it helps users quickly understand application structures, identify key components, and navigate through complex call graphs.

## Why Explore an application?

Understanding an application's architecture is critical for onboarding new developers, as it allows them to gain insights into the codebase without relying on outdated documentation. It also helps in assessing technical debt by identifying complex or problematic areas in the system. Furthermore, it plays a crucial role in planning changes or migrations, ensuring informed decision-making when modifying or modernizing applications.

## Key capabilities

CAST Imaging on Cloud offers a range of features designed to simplify application analysis and visualization. These capabilities help users gain deeper insights into their application's structure and dependencies, making it easier to manage and optimize software systems.

### Visualizing the Architecture

CAST Imaging transforms source code into an interactive map, allowing users to view all architectural layers and their interactions, examine dependencies between components, and identify database models and access patterns.

![](explore1.png)

This view gives anillustration of layers interaction for an ecommerce legacy application. Each concept represent a layer or a technical component of the software. Size of the component is given for each node thanks to the number of components visible as a badge. Interaction between components are visible thanks to dotted line based on existing static call between source code elements.

### Navigating the codebase

CAST Imaging provides multiple ways to explore an application's architecture and dependencies. Users can view code elements categorized by type through Object Type taxonomy, such as database tables, triggers, and Java classes. The interactive visualization allows users to drill down into specific functional areas to understand data flows and dependencies between components - for example, how database sequences connect to tables, or how Java classes interact with database objects through DAO layers. This hierarchical exploration helps teams quickly grasp both high-level architecture and detailed technical implementations

![](explore2.png)

### Using it as living documentation

Unlike static documentation, CAST Imaging allows users to save and share custom views, add tags and annotations for team collaboration, and maintain up-to-date insights as the application evolves.

![](explore3.png)

## How it works

1. Run the analyzer – download the Windows or Docker-based Analyzer and scan the application locally.
1. Upload analysis results – only the structural analysis results are uploaded, ensuring security.
1. Explore the application – use CAST Imaging's cloud interface to navigate and analyze the architecture.

## Conclusion

CAST Imaging on Cloud simplifies application exploration, making it easier for architects and tech leads to understand, document, and plan changes effectively. With its interactive visualization, teams can gain deep insights into their systems and enhance collaboration.