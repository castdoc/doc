---
title: "How to automate your code analyses"
linkTitle: "How to automate your code analyses"
type: "docs"
no_list: true
weight: 20
---

***

## Overview

The **CAST Imaging Analyzer** tool is downloaded and installed on your local machine and allows you to analyze your source code in "UI mode" before uploading your results to CAST Imaging on Cloud. Bundled with this UI tool is an equivalent **command line** tool that CAST provides to allow you to automate your analyses. Below is a description of the options available for this command line tool and an explanation of how to use it.

## Where can I get the command line tool?

The command line tool can be found on your local machine after running the **CAST Imaging Analyzer** installer, in the following location:

```text
%PROGRAMFILES%\CAST\CAST Imaging Analyzer\CAST-Imaging-CLI.exe
```

## How can I list all command options?

Use the following command:

```text
CAST-Imaging-CLI.exe -h
```

Then for each available option, you can run the `-h` command, for example:

```text
CAST-Imaging-CLI.exe run -h
```

## What can the command line tool do?

The tool has various operating modes/options, as described below:

| Mode/Option | Description |
|---|---|
| `run` | Run full analysis process on your chosen source code and upload the results to CAST Imaging on Cloud if you choose (this mode runs all other options listed below.) |
| `fastscan` | Run only the profiling step on your chosen source code and upload the results to CAST Imaging on Cloud if you choose. This is a source code "discovery" process where your source code will be scanned to determine the different languages, technologies and frameworks that are included, along with the number of files and the total number of code lines. This is not a full analysis. |
| `analysis` | Run only the analysis step on your chosen source code and upload the results to CAST Imaging on Cloud if you choose. This is a full deep analysis and requires that you have already run the `fastscan` step on its own. |
| `upload` | Runs the upload step to upload results of the `fastscan`and/or `analysis` steps to CAST Imaging on Cloud and requires that these steps have already been run. |

## What is the difference between online and offline mode?

The command line tool can be run in two different ways, **online** (with internet access) and **offline** (without internet access):

- **online**: All required analysis resources are automatically downloaded during the analysis. Results are automatically uploaded to your account in CAST Imaging on Cloud. This mode is the default and should be used where possible.
- **offline**: All required analysis resources must already exist on the local machine. Results are not uploaded to CAST Imaging on Cloud and are retained in the output folder. This mode is recommended only for those that are working in a secure environment without internet access and can be enabled using the `-of, --offline` flag. See [How to run code analyses in offline mode](../offline/).

## What flags are available?

| Flag | Description | Default value | Required? |
|---|---|---|:-:|
| `-h, --help` | Shows the help message and then exits. | - | :x: |
| `-n, --name` | Defines the name of the application. | A randomly generated name | :x: |
| `-o, --output` | Defines the folder where the results will be stored at the end of the process. | `%APPDATA%\CAST\CAST Imaging Analyzer\results\<application-name>` | :x: |
| `-s, --source` | Defines the folder where the application source code is stored. | - | :heavy_check_mark: |
| `-ak, --api-key` | The API key of the user that owns the application. An API key can be generated in CAST Imaging on Cloud by clicking the username in the upper right corner. | - | Required when uploading results to CAST Imaging on Cloud (online mode) |
| `-is, --imaging-server` | URL of CAST Imaging on Cloud. | `https://castimaging.io` | :x: |
| `-cl, --clean-logs` | Reduces the number of log messages that are output. | Disabled | :x: |
| `-nu, --no-upload` | Prevents any results from being uploaded to CAST Imaging on Cloud. Useful for testing the tool. | Disabled | :x: |
| `-nb, --no-browser` | Prevents the default browser on the local machine from opening and displaying the results in CAST Imaging on Cloud. | Disabled | :x: |
| `-of, --offline` | Forces the tool to function entirely without an internet connection and is recommended only for those that are working in a secure environment without internet access. Note that CAST extensions must already exist in `%PROGRAMDATA%\CAST\CAST-Imaging-CLI\carl\bin\extensions` or another location specified by `-sp, --storage-path`. | Disabled | :x: |
| `-sp, --storage-path` | Used with `-of, --offline`, defines the location of the CAST extensions on the local disk. If not specified, the default value is used. | `%PROGRAMDATA%\CAST\CAST-Imaging-CLI\carl\bin\extensions` | :x: |
| `-re, --rescan` | Defines the absolute path to `profiler\profiler-results.json` in the folder defined by the `-o, --output` flag from a previous `run` action and is used when you want to rerun the `run` action but do not want to reproduce all output files. For example when you want to define a new ignore patterns file. | - | :x: |
| `-ip, --ignore-patterns` | Defines the absolute path to a text file (.txt) containing a list of ignore patterns, defining specific files and folders that should be excluded from the analysis. The ignore patterns are defined using glob pattern matching (see [https://docs.oracle.com/javase/tutorial/essential/io/fileOps.html#glob](https://docs.oracle.com/javase/tutorial/essential/io/fileOps.html#glob)). For example:<ul><li><code>\*.txt</code> will exclude all files with the extension .txt</li><li><code>tests/</code> will exclude any folders named <code>tests</code> and everything inside them - e.g. <code>root_folder/tests</code>, <code>root_folder/another_folder/tests</code></li><li><code>*.Tests/</code> will exclude any folders whose name includes</li><li><code>.Tests</code> (for example <code>C:\Support\Delivery\Sample.Tests\sample\\</code>)</li><li>patterns starting with <code>/</code> will exclude starting only from the root folder. In other words, <code>/tests/</code> will exclude everything in the specific folder <code>root_folder/tests</code> but not <code>root_folder/another_folder/tests</code></li></ul>Note that if a file called <code>ignore-patterns.txt</code> is placed in the root of the folder defined by <code>-s, --source</code>, any patterns defined in this file will be automatically taken into account. This file does not need to be defined by <code>-ip, --ignore-patterns</code>. The use of <code>-ip, --ignore-patterns</code> overrides the <code>ignore-patterns.txt</code> file and any patterns defined in it are ignored. | - | :x: |
| `-es, --extend-server` | URL of an on premises CAST Extend server, i.e. for those that are not using https://extend.castsoftware.com. The option expects a URL, for example http://my-extend-server:8085. | https://extend.castsoftware.com | :x: |

## Run mode example

Run the profiling and analysis steps, store the results in a dedicated folder and then upload all results to CAST Imaging on Cloud (this is all actions run in one go):

```
CAST-Imaging-CLI.exe run -n "application1" -o "C:\CAST\cloud\application1\results1" -s "C:\CAST\source_code\application1" -ak "u9t2Dteq.1YYvvPCoMNp"
```

Run the profiling and analysis steps, store the results in a dedicated folder, exclude some files/folders and then upload all results to CAST Imaging on Cloud:

```

CAST-Imaging-CLI.exe run -n "application1" -o "C:\CAST\cloud\application1\results1" -s "C:\CAST\source_code\application1" -ak "u9t2Dteq.1YYvvPCoMNp" -ip "C:\CAST\cloud\application1\exclusions.txt"
```

Rerun the profiling and analysis steps on a previous run results, without reproducing all output files, store the results in a dedicated folder, exclude some files/folders and then upload all results to CAST Imaging on Cloud:

```
CAST-Imaging-CLI.exe run -n "application1" -o "C:\CAST\cloud\application1\results2" -s "C:\CAST\source_code\application1" -ak "u9t2Dteq.1YYvvPCoMNp" -re "C:\CAST\cloud\application1\results2\profiler\profiler-results.json" -ip "C:\CAST\cloud\application1\exclusions_new.txt"
```

## Profiling mode example

Run the profiling step only, store the results in a dedicated folder and then upload all results to CAST Imaging on Cloud:

```
CAST-Imaging-CLI.exe fastscan -n "application2" -o "C:\CAST\cloud\application2\results1" -s "C:\CAST\source_code\application2" -ak "u9t2Dteq.1YYvvPCoMNp"
```

Rerun the profiling step on a previous run results, without reproducing all output files, store the results in a dedicated folder, exclude some files/folders and then upload all results to CAST Imaging on Cloud:
```

CAST-Imaging-CLI.exe fastscan -n "application2" -o "C:\CAST\cloud\application2\results2" -s "C:\CAST\source_code\application2" -ak "u9t2Dteq.1YYvvPCoMNp" -re "C:\CAST\cloud\application2\results2\profiler\profiler-results.json" -ip "C:\CAST\cloud\application2\exclusions_new.txt"
```

## Analysis mode example

{{% alert title="Note" color="info" %}}Using the `analysis` mode requires that the `fastscan` mode has already been run.{{% /alert %}}

Run the analysis step only, store the results in a dedicated folder and then upload all results to CAST Imaging on Cloud:

```
CAST-Imaging-CLI.exe analysis -n "application2" -o "C:\CAST\cloud\application2\results1" -s "C:\CAST\source_code\application2" -ak "u9t2Dteq.1YYvvPCoMNp" -pr "C:\CAST\cloud\application2\results1\profiler\profiler-results.json"
```

## Upload mode example

Upload the results of previous `fastscan` and `analysis` mode actions:

```
CAST-Imaging-CLI.exe upload -n "application2" -o "C:\CAST\cloud\application2\results1" -s "C:\CAST\source_code\application2" "u9t2Dteq.1YYvvPCoMNp" -pr "C:\CAST\cloud\application2\results1\profiler\profiler-results.json" -cr "C:\CAST\cloud\application2\results1\carl\carl"
```

## Offline mode example

See [How to run code analyses in offline mode](../offline/).
