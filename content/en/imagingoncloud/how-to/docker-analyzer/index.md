---
title: "How to run code analyses using Docker"
linkTitle: "How to run code analyses using Docker"
type: "docs"
no_list: true
weight: 40
---

***

## Overview

Are you regularly using Docker and would you like to run the CAST Imaging Analyzer in a Docker container? Look no further, because this is possible! CAST provides the analyzer tool as an image on [Docker Hub](https://hub.docker.com/r/castimaging/imaging-docker-analyzer) that is regularly updated.

## How does it work?

The containerized CAST Imaging Analyzer is run direct from the command line on your machine and results are uploaded to CAST Imaging Cloud automatically.

Command line:

```text
docker run --rm --pull always -v {SOURCE_CODE_PATH}:/home/carl/extras/source -v {CACHE_PATH}:/home/carl/extras/extensions castimaging/imaging-docker-analyzer:latest -ak {API_KEY} -n {APP_NAME}
```

Where:

- `{SOURCE_CODE_PATH}` - is the path to the local folder containing your source code
- `{CACHE_PATH}` - is the path to a local folder where extensions will be downloaded and cached
- `{API_KEY}` - is your CAST Imaging Cloud API key
- `{APP_NAME}` - is your Application Name

Note that:

- `--pull always` - using `always` will ensure that the image is always updated to the latest version before the analysis is run (recommended)
- `--rm` will automatically delete the container when the analysis is complete, remove this parameter if you want to access the container after the analysis is complete.

For example:

```text
docker run --rm --pull always -v /home/CAST/app1:/home/carl/extras/source -v /home/CAST/extensions:/home/carl/extras/extensions castimaging/imaging-docker-analyzer:latest -ak NG8pzC3I.6R8EI5vBR3qaNZO -n APP1
```
