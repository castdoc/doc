---
title: "How to use the Maven plugin"
linkTitle: "How to use the Maven plugin"
type: "docs"
no_list: true
weight: 50
---

***

## Overview

`imaging-cloud-analyzer-maven-plugin` is a Maven plugin which will run the CAST Imaging Analyzer on the current project, manage any required third-party dependencies and uplaod the results direct to CAST Imaging on Cloud.

The plugin provides two options (known as "goals"):

- `fastscan`: Runs a quick scan of the current project.
- `run`: Executes a full analysis.

##  Requirements

- Maven 3.6.0 or higher
- Java 17 or higher
- ensure that `MAVEN_HOME` and `JAVA_HOME` environment variables are correctly set
- for Unix-based systems, ensure that the `mvn` and `docker` command is available in the terminal
- for Microsoft Windows, ensure that you have installed the **CAST Imaging Analyzer tool** on your machine

## Quick Start to run full analysis (run goal)

### Step 1 - Add the following configuration to your project's `pom.xml`

```xml
<build>
  <plugins>
    <plugin>
      <groupId>io.castimaging</groupId>
      <artifactId>imaging-cloud-analyzer-maven-plugin</artifactId>
      <version>1.0.2</version>
      <executions>
        <execution>
          <phase>package</phase>
          <goals>
            <goal>run</goal>
          </goals>
        </execution>
      </executions>
      <configuration>
        <apiKey>${YOUR_API_KEY}</apiKey>
        <outputPath>${PATH/TO/RESULT}</outputPath>
        <analyzerPath>${PATH/TO/INSTALL/ANALYZER}</analyzerPath> <!-- Needed for WINDOWS -->
        <imagingServerUrl>${URL/TO/imagingServer}</imagingServerUrl>
      </configuration>
    </plugin>
  </plugins>
</build>
```

### Step 2 - Run the plugin

```shell
cd /path/to/your/project
mvn clean package
```

### Step 3 - Check the results

- Check the result in the specified output path `${PATH/TO/RESULT}/${APPLICATION_NAME}`.
- Check the result in the CAST Imaging on Cloud dashboard: [https://castimaging.io](https://castimaging.io).

## Configuration parameters

- Required parameters:
  - `apiKey`: your API token. This is required to authenticate the plugin.
  - `outputPath`: Specifies where the analysis results will be saved (Microsoft Windows only).
  - `analyzerPath`: The path to the installed `CAST-Imaging-CLI.exe` tool - this is provided as part of the CAST Imaging Analyzer (Microsoft Windows only).

- Optional parameters:
  - `applicationName`: The name of the application being analyzed. If not provided, the default is your project's name.
  - `ignorePatternPath`: Path to a text file containing ignore patterns (one pattern per line) to exclude specific files or directories from the analysis.
  - `imagingServerUrl`: The URL of the CAST Imaging Cloud server. The default is `https://castimaging.io/`.
  
{{% alert color="info" %}}You can find out more about these options in [How to automate your code analyses](../automate).{{% /alert %}}

## Advanced usage

### Information

- This plugin will install some extensions and CLI tools in specific directories (defined by `<analyzerPath>`). Make sure you have the necessary permissions to write to these directories.
- If you're using **IntelliJ IDEA**, no additional configuration is needed — the IDE will handle everything automatically.
- For command-line usage, make sure `MAVEN_HOME` and `JAVA_HOME` are correctly set.

### Maven repository lookup logic

The plugin will search for the Maven repository in the following order:

1. `-Dmaven.repo.local` (if specified)
2. `~/.m2/settings.xml` (if configured)
3. `$MAVEN_HOME/conf/settings.xml` (if available)
4. `~/.m2/repository` (default)

### Plugin installation

The extensions will be installed in the repository directory:

```text
${LOCAL_REPOSITORY}/com/castsoftware/imaging-cloud-analyzer-maven-plugin/extensions/
```

All the dependencies will be collected and recorded in the followingfile:

```text
${OUTPUT_PATH}/${APPLICATION_NAME}/mr/jarList.txt
```

### Additional configuration

By default, the run goal is bound to the Maven package lifecycle phase. To explicitly declare the plugin goal in your `pom.xml`, add for example:

```xml
<executions>
  <execution>
    <phase>[compile,package,verify,install,etc.]</phase>
    <goals>
      <goal>[run,fastscan]</goal>
    </goals>
  </execution>
</executions>
```
