---
title: "How to use the GitHub Actions feature"
linkTitle: "How to use the GitHub Actions feature"
type: "docs"
no_list: true
weight: 30
---

***

## Overview

Is your application code stored in a **GitHub repository**? Did you know that CAST Imaging on Cloud interfaces with [GitHub Actions](https://github.com/features/actions) so that you can run the entire code analysis workflow whenever you push or merge to your desired branch? This means that you can fully automate the analysis and generation of results in CAST Imaging on Cloud direct from your GitHub repository.

## How does it work?

CAST Imaging on Cloud simply requires that your application source code is stored in a **GitHub repository** (public or private) and that you have access to [GitHub Actions](https://github.com/features/actions) in your GitHub account. Following that, a dedicated `.yml` file must be present in your repository, containing instructions that will define when the GitHub Actions job is run, what it does and where the profiling and analysis results should be uploaded to. The job will use a dedicated GitHub hosted runner to perform the required actions.

Using the GitHub Actions feature effectively replaces the CAST Imaging Analyzer tool that you would normally download to your local machine to perform the required actions.

## Prerequisites

- A CAST Imaging on Cloud account
- Source code stored in the `main` or `master` branch of a **GitHub repository** (public or private)
- Access to [GitHub Actions](https://github.com/features/actions) - all GitHub accounts (even those on the free tier) have access to GitHub Actions:

![](actions.jpg)

{{% alert color="info" %}}If the **Actions** tab is not displayed under the name of your repository on GitHub, it may be because Actions is disabled for the repository. For more information, see [Managing GitHub Actions settings for a repository](https://docs.github.com/en/repositories/managing-your-repositorys-settings-and-features/enabling-features-for-your-repository/managing-github-actions-settings-for-a-repository).{{% /alert %}}

### How does GitHub charge for GitHub Actions?

All GitHub account tiers provide a number of GitHub Actions "build" minutes per month (see [About billing for GitHub Actions](https://docs.github.com/en/billing/managing-billing-for-github-actions/about-billing-for-github-actions)):

- If you are using a **free** GitHub account, then the CAST Imaging on Cloud workflows will fail when all "free" minutes are used up and you will not be able to launch any new GitHub Action jobs until the build minutes are reset or you subscribe to a paid tier.
- If you are **paying** for a GitHub account, then you will be automatically billed for any additional "build" minutes (beyond those provided with your subscription).

It is not possible to predict how many "build" minutes your GitHub Actions workflow for CAST Imaging on Cloud will use, but the larger the application, the longer it will take to complete. You should therefore ensure that you monitor your GitHub Actions usage.

## How to onboard an application using the GitHub Actions workflow

### Step 1 - Create the application in CAST Imaging on Cloud

In your CAST Imaging on Cloud account, click **Add Application > Via GitHub Actions**:

![](add_app.jpg)

Name your application:

![](add_app2.jpg)

Then follow the instructions on screen:

![](add_app3.jpg)

### Step 2 - Configure the GitHub Actions repository secret in GitHub

In your GitHub account, navigate to the repository containing your application source code, then **Settings > Security > Secrets and variables > Actions**:

![](github1.jpg)

Create a new secret using the information provided to you in CAST Imaging on Cloud (this links your source code to the application in CAST Imaging on Cloud):

![](add_secret.jpg)

### Step 3 - Create a GitHub Actions .yml build file in your GitHub repository

In your GitHub account, navigate to the **Code** tab and create a GitHub Actions build file called `cast-imaging-analyzer.yml` in `.github/workflows` in the `main` or `master` branch:

![](code.jpg)

{{% alert color="info" %}}You can also commit files using your favourite Git client.{{% /alert %}}

Paste in the instructions provided to you in CAST Imaging on Cloud and commit your changes:

![](code2.jpg)

Committing this file will automatically trigger a GitHub Actions workflow build which you can see in the **Actions** tab:

![](action_progress.jpg)

This workflow build will run the profiling and analysis steps and then upload the results into CAST Imaging on Cloud. When the workflow has completed, you can consult the results as normal in CAST Imaging on Cloud.

## FAQ

### How do I trigger a new GitHub Actions workflow build?

The `cast-imaging-analyzer.yml` workflow build file is configured to trigger a new GitHub Actions workflow build whenever you push/merge into the `main` or `master` branch, therefore generating new results in CAST Imaging on Cloud:

```yaml
on:
  push:
    branches:
      - master
      - main
```

You can change this behaviour if necessary by modifying the syntax as described in [Events that trigger workflows](https://docs.github.com/en/actions/writing-workflows/choosing-when-your-workflow-runs/events-that-trigger-workflows).

### What if my source code is not in main or master?

If you do not store your source code in the `main` or `master` branch or you want to trigger a build from a specific branch, you can update the `cast-imaging-analyzer.yml` workflow build file as described in [Running your workflow only when a push to specific branches occurs](https://docs.github.com/en/actions/writing-workflows/choosing-when-your-workflow-runs/events-that-trigger-workflows#running-your-workflow-only-when-a-push-to-specific-branches-occurs).

### What if my GitHub Actions job exceeds the timeout?

By default all GitHub Actions jobs have a **360 minute** (6 hour) timeout - i.e. if the job exceeds this limit then it will be stopped by GitHub and will fail. This can occur when you are attempting to onboard a very large application with millions of lines of code. It is not possible to extend this 360 minute timeout when using the GitHub hosted runner, however, you can set up your own self-hosted runner and use it with the GitHub Action job - this runner will not be subject to the GitHub Actions timeout. Please [contact Technical Support](mailto:help@castimaging.io) <i class="fa-solid fa-envelope"></i> if you need advice about this.

### How do I redisplay the instructions in CAST Imaging on Cloud?

If you have navigated away from the instructions pop-up displayed in CAST Imaging on Cloud, you can redisplay using the **Actions > Configure analysis** option:

![](configure.jpg)