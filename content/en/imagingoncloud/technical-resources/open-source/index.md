---
title: "Open-source and third-party software used in CAST Imaging on Cloud"
linkTitle: "Open-source and third-party software"
type: "docs"
no_list: true
weight: 30
---

***

## Web platform - embedded components

- [Keycloak](https://www.keycloak.org/) 
- [Neo4j](https://neo4j.com/)
- [Linkurious Ogma](https://linkurious.com/ogma/)

## Web platform - dependencies

{{% fetch-remote-md url="https://il-public-files.s3.us-east-2.amazonaws.com/dependency_list.md" %}}

## Local analyzer - dependencies

{{% fetch-remote-md url="https://il-public-files.s3.us-east-2.amazonaws.com/dependency_list_analyzer.md" %}}
