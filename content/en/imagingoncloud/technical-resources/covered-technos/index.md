---
title: "Covered technologies for Imaging on Cloud"
linkTitle: "Covered technologies"
type: "docs"
no_list: true
weight: 10
---

***

The following technologies are supported in **CAST Imaging on Cloud**:

- **Java/JEE**  - Java and the main frameworks such as Hibernate, Spring ecosystem etc.
- **Microsoft .NET** - projects built in C# and ASP.NET + MVC, WCF, WPF frameworks
- **Web** - HTML, CSS, Javascript, Typescript and relevant frameworks such as React, Angular, Node.js
- **RDBMS** - support for the main SQL RDBMS and NoSQL
- **Mainframe** - Cobol, JCL, IMS, CICS, Schedulers (except PL1, EGL and RPG for IBM Mainframe zOS and IBM i (formerly System i, iSeries or AS/400))
- **Message Queues**

{{% alert color="info" %}}Looking for information about **CAST Imaging "on premises"**? See [Coverage overview for CAST Imaging](../../../technologies/coverage-overview/). {{% /alert %}}