---
title: "FAQ"
linkTitle: "FAQ"
type: "docs"
no_list: true
weight: 20
---

***

## Covered technologies

See [Covered technologies](../covered-technos/).

## Supported browsers

| Browser | Minimum supported release |
|---|---|
| Microsoft Edge | 44 and above |
| Mozilla Firefox | 95 and above |
| Google Chrome | most recent only |
| Safari | 12 and above |

## Supported platforms

Applies to the **CAST Imaging Analyzer** tool downloaded and installed on your local machine which analyzes your source code:

- Windows 10/11 64bit (for direct installation)
- Docker (provided as a Docker image)
