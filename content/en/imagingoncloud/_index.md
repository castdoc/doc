---
title: "Welcome to CAST Imaging on Cloud"
linkTitle: "Imaging on Cloud"
description: "CAST Imaging is the automatically created knowledge base of your application's inner workings. And it's in the cloud!"
type: "docs"
no_list: true
---

***

CAST Imaging is unlike any other code analyzer: it's like an MRI examining your custom software applications, enabling you to visualize and understand their intricate inner workings. You can use CAST Imaging to discover and document the makeup of an application, to assess the impact of changes you plan to make, or to identify opportunities to improve your application's performance, reliability, or security.

CAST Imaging runs a deep, semantic and contextual analysis of all artifacts comprising an application, identifies all its code elements and data structures, and maps out all their dependencies. CAST Imaging supports over 150 technologies, including Java/JEE, .NET, C#, Web, Mainframe, Web, SQL, NoSQL, and [more](technical-resources/covered-technos/).

## Getting started

- [Initial access](getting-started/initial-access/)
- [Invite new members](getting-started/invite/)

## How to

- [How to run code analyses in offline mode](how-to/offline/)
- [How to automate your code analyses](how-to/automate/)
- [How to use the GitHub Actions feature](how-to/github-actions/) <i class="fa-brands fa-github"></i>
- [How to run code analyses using Docker](how-to/docker-analyzer/) <i class="fa-brands fa-docker"></i>
- [How to use the Maven plugin](how-to/maven-plugin/)

## Use cases

- [Exploring applications](use-cases/applications)

## Release Notes

- [Analyzer Release Notes](release-notes/analyzer/)
- [Platform Release Notes](release-notes/platform/)

## Technical resources

- [Covered technologies](technical-resources/covered-technos/)
- [FAQ](technical-resources/faq/)
- [Open-source and third-party software used in CAST Imaging on Cloud](technical-resources/open-source/)
- [Contact Technical Support](mailto:help@castimaging.io) <i class="fa-solid fa-envelope"></i>

## Legal information

- [EULA](legal/eula/) <i class="fa-solid fa-file-pdf"></i>
- [Terms & Conditions](legal/terms-conditions/) <i class="fa-solid fa-file-pdf"></i>
