---
title: "CAST Imaging Documentation"
description: "Deep insights into your application's inner workings..."
type: "home"
---

<div class="mx-auto" style="max-width: 75%;!important">
	<p class="p-2 mt-3 text-center" style="background-color: #E3EDF2; border: 1px; border-style: solid; border-color: #A0C1D2; border-radius: 5px;"><i class="fa-solid fa-play"></i> Want to get up and running quickly on Microsoft Windows? See our <a href="../quickstart">Quickstart</a> guide.</p>
</div>

<div class="td-content">
	<h3>What would you like to do?</h3>
</div>

{{< cardpanehome >}}
{{% card header="[Install](../install/)" %}}
<i class="fa-solid fa-download"></i>
Detailed information about deploying CAST Imaging on the platform of your choice.
{{% /card %}}
<!-- Temporarily removed
{{% card header="Onboard" %}}
<i class="fa-solid fa-plus"></i>
How to analyze your application with CAST Imaging and achieve the results you want.
{{% /card %}}
-->
{{% card header="[Explore results](../explore-results/)" %}}
<i class="fa-solid fa-compass"></i>
Find out how to navigate your analysis results to investigate your application.
{{% /card %}}
{{% cardhome header="[Administer](../administer/)" %}}
<i class="fa-solid fa-gear"></i>
How to administer and update your CAST Imaging installation once it is up and running.
{{% /cardhome %}}
{{< /cardpanehome >}}

<div class="td-content">
	<h3>Reference</h3>
</div>

{{< cardpanehome >}}
{{% cardhomereference %}}
<i class="fa-solid fa-book"></i>&nbsp;[User Interface](../interface/)
{{% /cardhomereference %}}
{{% cardhomereference %}}
<i class="fa-solid fa-microchip"></i>&nbsp;[Technologies](../technologies/coverage-overview/)
{{% /cardhomereference %}}
{{% cardhomereference %}}
<i class="fa-solid fa-clipboard-list"></i>&nbsp;[Requirements](../requirements/)
{{% /cardhomereference %}}
{{% cardhomereference %}}
<i class="fa-solid fa-code-branch"></i>&nbsp;[Release Notes](../release-notes/)
{{% /cardhomereference %}}
{{% cardhomereferencelast %}}
<i class="fa-solid fa-scale-balanced"></i>&nbsp;[Legal](../legal/)
{{% /cardhomereferencelast %}}
{{< /cardpanehome >}}
