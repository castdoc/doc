---
title: "Search Configuration"
linkTitle: "Search Configuration"
type: "docs"
no_list: true
weight: 70
---

***

## Overview

The Search Configuration setting provides options to enhance the advanced search mechanism for your analysis results through the use of **object properties** as search entry points.

For example, in the image below, a filter has been added to return only objects that match a specific object property called `Number of code lines` and where the value of this property is above `100`:

![](results.jpg)

Then results can be further filtered on the object name itself:

![](results2.jpg)

The object properties available in the advanced search panel are configured in the **Search Configuration** panel. Additional properties can be added, and properties can be removed.

In a fresh installation of CAST Imaging (when no applications have been analyzed or imported), no properties are available for configuration. When the first application is analyzed or imported, properties will become visible in the **Search Configuration** panel and by default, some will be automatically made available to users via the advanced search panel:

![](panel.jpg)


- on the left hand side in the image above are the properties that are not yet visible to users when searching
- on the right hand side in the image above are the properties that are already available to users for search purposes

### Where do these properties come from?

The properties that can be configured for search purposes are calculated during the analysis process. A value for each available property is calculated by CAST for every object in the application and this is then stored with the rest of the application data. CAST Imaging has access to this properties data and it can be used for search purposes.

## Configuring properties for search purposes

To change the properties that are available to users:

- select the property / properties (you can also use the **Select All** option)
- then use the arrow selectors to move the selected properties from one side to the other

For example to move the `Halstead Program Length` property from the left to the right side to make it available to users, select it and then click the highlighted arrow:

![](configure.jpg)

The property is now located on the right and can be used by users when searching for objects:

![](halstead.jpg)

{{% alert color="info" %}}If you are searching for a specific property to enable for users, use the search boxes to narrow down the results:<br><br>![](narrow.jpg) {{% /alert %}}