---
title: "Managing Default Dynamic Links Rules"
linkTitle: "Default Dynamic Links Rules"
type: "docs"
no_list: true
weight: 25
---

## Overview

![](dynamic_links.jpg)

When an analysis is run, the authenticity of some links that have been identified between objects cannot be absolutely determined - these links are known as "dynamic links". Often these "unverified links" systematically occur in every analysis or have no meaning for the specific technology you are working with. To counter this, filter rules (defined in `.DLM.XML` files) can be configured to either validate (i.e. retain) or ignore (delete) these recurring dynamic links automatically when an analysis is run.

This option enables you to add Dynamic Links Rules (`.DLM.XML` files) at a global level (i.e. for all applications managed in CAST Imaging):

- the `.DLM.XML` file you upload will be stored in the `common-data\upload\dlm` folder on disk
- for all **new** applications that are onboarded, the `.DLM.XML` file will be automatically added to the Dynamic Links configuration panel under Config > Advanced and taken into account in the next analysis.
- for all **existing** applications, the `.DLM.XML` file will not be automatically taken into account. Instead Application Owners can re-use the rules at application level (in the Config > Advanced panel) if required.

{{% alert color="info" %}}No `.DLM.XML` files are provided by default.{{% /alert %}}

## How do I create Dynamic Links Rules?

See [Dynamic Links Rules](../../../../interface/analysis-config/config/advanced/dynamic-links/rules/).

## What happens if I want to edit a .DLM.XML file I have already uploaded?

You can edit the physical `.DLM.XML` file in `common-data\upload\dlm`. Changes are taken into account the next time a new analysis is run.