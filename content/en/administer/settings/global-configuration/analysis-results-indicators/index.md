---
title: "Managing Analysis Results Indicators"
linkTitle: "Analysis Results Indicators"
type: "docs"
no_list: false
weight: 20
---

## Overview

![](indicators.jpg)

The Analysis Results Indicators setting allows you to enable/disable specific indicators.

## What are Analysis Results Indicators?

Analysis Results Indicators are designed to provide basic information quickly so that the analysis/snapshot can be validated. Each indicator is given a score from 0 to 3 during the analysis. The lower the score, the more likely that an issue exists that needs to be investigated. Some example indicators are shown below:

- Artifacts in transactions
- Data entities used by transactions
- Incomplete transactions
- ...

The indicators are generated:

- during the analysis/snapshot in a dedicated step
- on-demand

> You can trigger alerts during an analysis based on indicator scores, see [here](../alert-settings/thresholds).

## How do I deactivate Analysis Results Indicators?

There are two methods:

- use the toggle switches on individual indicators
- make bulk changes using the tick boxes

![](indicators_update.jpg)

## Can I download the current state of each Analysis Results Indicator?

Use the cloud icon to download a CSV file containing a list of all indicators and their current state:

![](indicators_csv.jpg)