---
title: "Configuring source code delivery from a folder"
linkTitle: "Source Folder Location"
type: "docs"
no_list: true
weight: 50
---

## Overview

![](source_folder_location.jpg)

The Source Folder Location option allows you to configure a location on a disk from which you want to deliver source code. Configuring this option is in addition to the default option to deliver source code from a ZIP/archive file.

{{% alert color="info" %}}<ul><li>The Source Folder Location should only contain "raw" source code, and not ZIP/archive files.</li><li>You can configure the Source Code Folder location (only for Microsoft Windows in a single machine/standalone deployment scenario) during the [install process](../../../../install/global/windows/), via the <code>SOURCES_FOLDER_LOCATION</code> [variable](../../../../install/global/windows/variables/).</li></ul>{{% /alert %}}

## Why would I need to deliver from a disk location, rather than a ZIP file?

You could choose to deliver your application source code direct from a cloned Git repository located somewhere on your network, or your application source code may be very large which means creating a ZIP file is not possible.

## What path syntax is accepted?

| Type | Path Syntax |
|------|-------------|
| UNC network path | `\\server\source_code` |
| Local path | `C:\source_code`<br>`C:/source_code`<br>`D:\source_code\temp`<br>`D:/source_code/temp` |
| Mapped or subst drives | `S:\`<br>`T:\source_code` |

- each node must have read access to this path therefore CAST highly recommends using a shared network drive
- the login configured to run the various Windows Services must have read access to this path.
- if you have issues with regard to the accessibility of a mapped/subst drive, you can resolve it using the `map-drives.bat` file.

## How do I deliver code from a folder?

If you have configured the location, when onboarding a new application, use the `Select source path` option:

![](source_folder_location_example.jpg)

{{% alert color="info" %}}Only one folder in the designated path can be selected, so you must ensure that all source code is presented in one "root" folder (you can organise it however you need to underneath this root folder).{{% /alert %}}