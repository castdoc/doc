---
title: "Using Exclusion Templates"
linkTitle: "Exclusion Templates"
type: "docs"
no_list: false
weight: 30
---

## Overview

![](exclusion_pattern.jpg)

The Exclusion Templates setting is used to define templates containing specific source code exclusion patterns - these exclusion patterns define file extensions or paths/folders that should always be excluded from the CAST Imaging source code analysis process. For example, your application source code may contain files should never be analyzed because they provide no value or are temporary or technical files.

Defining these exclusion patterns in a template will ensure they are re-used across all applications managed in CAST Imaging.

> It is also possible to exclude source code at individual application level.

## How do I define exclusion patterns for a template?

- Click the `Add` button and give your template a name
- A template can contain multiple exclusion patterns - add them one by one, pressing ENTER to add the pattern

## Example exclusion patterns

- `*.txt` will exclude all files with the extension `.txt`
- `tests/` will exclude any folders named `tests` and everything inside them - e.g. `root_folder/tests`, `root_folder/another_folder/tests`
- `*.Tests/` will exclude any folders whose name includes `.Tests` (for example `C:\Support\Delivery\Sample.Tests\sample\`)
- patterns starting with `/` will exclude starting only from the root folder. In other words, `/tests/` will exclude everything in the specific folder `root_folder/tests` but not `root_folder/another_folder/tests`

## What pattern matching system is used?

CAST Imaging uses `glob patterns` (see https://docs.oracle.com/javase/tutorial/essential/io/fileOps.html#glob for examples of how this system works).

## What exclusions are provided in the default exclusion template?

- `target/` (path/folder)
- `temp/` (path/folder) 
- `tests/` (path/folder)
- `*test` (file)
- `tmp/` (path/folder)
- `.svn/` (path/folder)
- `.git/` (path/folder)