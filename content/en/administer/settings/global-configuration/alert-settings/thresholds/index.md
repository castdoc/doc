---
title: "Defining thresholds for alerts"
linkTitle: "Thresholds"
type: "docs"
no_list: true
weight: 30
---

## Overview

Use this panel to define custom thresholds for alerts based on percentages of unanalyzed files, i.e. files that have not been analyzed for whatever reason (ignored, syntax error, excluded etc.) and for alerts based on Analysis Results Indicators with a low score.

Default thresholds are set by CAST, but you can customize these (for example, you may be receiving too many alerts and so changing the threshold may help):

![](thresholds.jpg)

## Defining unanalyzed code thresholds

Alerts for unanalyzed files will be displayed as follows:

- % of all not analyzed files

![](thresholds_example2.jpg)

- % of not analyzed files except those excluded by exclusion rules

![](thresholds_example1.jpg)

## What about the Analyis Indicators Score option?

This option allows you to set a threshold for triggering alerts based on [Analysis Results Indicators](../../analysis-results-indicators). By default this is set to `0`, meaning that any Analysis Result Indicator that receives `0` stars will automatically trigger an alert. 

Possible values are `0`, `1` and `2`, matching the number of stars that an indicator is awarded during an analysis. Alerts for indicators will be displayed as follows:

![](indicator_low.jpg)

## Using the Request validation for all alerts option

This option (when enabled) will force users to review and validate any alerts that are displayed before the `Generate views` step can complete, using the following tick box:

![](validate_alerts.jpg)

The following behaviour is in force when the option is enabled:

- In all situations, the `Generate views` step will not be automatically run as part of an analysis, then:
    - If there are no alerts, the `Generate views` step must be triggered manually to complete the analysis.
    - If there are alerts, these alerts should be reviewed and then acknowledged by ticking the `Alerts have been reviewed and validated` check box. When this is done, the `Generate views` step must be triggered manually to complete the analysis.

## Why are these alerts important?

If the percentage thresholds you set are breached, then an alert will be triggered and you should investigate why (by examining the analysis log files) since a large percentage of unanalyzed files, or low analysi results indicator scores are an indication that there may be issues to deal with, which can negatively impact your analysis results.