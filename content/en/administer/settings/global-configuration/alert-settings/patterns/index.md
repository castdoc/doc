---
title: "Defining patterns for alerts"
linkTitle: "Patterns"
type: "docs"
no_list: true
weight: 10
---

## Overview

Use this panel to define alerts based on a string or a regular expression match on the content of the analysis log:

![](alerts_patterns.jpg)

## How do I create a pattern?

Use the `Add Pattern` button and then fill in the details of this pattern:

- Match Case: choose whether the pattern you have added should be case-sensitive or not.
- String/Regex: choose how CAST Imaging should interpret your pattern:
    - If the pattern is a pure alpha numeric string, you should choose `STRING`.
    - If the pattern is a regular expression, or contains regular expression type syntax, you should choose `REGEX`.

## What about the default patterns?

CAST Imaging provides a set of default patterns that provide alerts for many common messages that occur in analysis logs. You do not have to use these (i.e. they can be deleted), however CAST recommends that these are used as they provide valuable information and can help you "tune" your source code or analysis configuration.