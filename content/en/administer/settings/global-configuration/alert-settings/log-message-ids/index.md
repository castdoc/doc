---
title: "Defining Log Message IDs for alerts"
linkTitle: "Log Message IDs"
type: "docs"
no_list: true
weight: 20
---

## Overview

Use this panel to define alerts based on a specific Log Message ID encountered in the analysis log:

![](log_message_id.jpg)

When a Log Message ID is added to the list, whenever that specific Log Message is encountered during an analysis, a custom alert will be displayed (see [here](..) for more information about where alerts are displayed). In addition, each Log Message ID has a specific description and remediation: when the alert from this Log Message is displayed to the user, a `View Remedy` button is available to display these description and remediation entries to help understand why the Log Message has occurred and what can be done about it.

## How do I add a Log Message ID?

Click `Add Log Message ID`, tick the ID you require, then click `Add`:

![](add_log_message_ID.jpg)