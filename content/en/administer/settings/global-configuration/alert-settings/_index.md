---
title: "Managing CAST Imaging Alert Settings"
linkTitle: "Alert Settings"
type: "docs"
no_list: true
weight: 40
---

## Overview

The Alert Settings options are used to define "alerts" that will be displayed during an analysis based on the content of the analysis logs. For example, you may want CAST Imaging to display an alert when a specific log message is generated during an analysis, or you may want to be alerted when a certain percentage of your files have not been analyzed during an analysis.

There are three different alert settings that can be configured:

- [Patterns](patterns/): alerts based on a string or a regular expression match on the content of the analysis log
- [Log message IDs](log-message-ids/): alerts based on a specific Log Message ID encountered in the analysis log
- [Thresholds](thresholds/): alerts based on custom thresholds for percentages of not analyzed files

## Why are alerts useful?

Alerts are primarily used during the process of validating analysis results. An alert indicates that some kind of issue exists in the source code and therefore this can mean that your analysis results may not be as reliable as expected. For example, generating alerts for specific Log Message IDs can alert you to the fact that part of your source code is missing or has been skipped.

## Where are alerts displayed?

Alerts are displayed in various locations:

### Job Progress panel

![](display1.jpg)

### Overview panel

![](display2.jpg)

### Log window

![](display4.jpg)

## If there are multiple alerts are they grouped together?

When multiple occurrences of the same alert are triggered, these occurrences will be grouped and will give rise to one single alert:

![](multiple_alerts.jpg)

## Example

A [custom pattern](patterns) has been added as follows - this will trigger an alert every time the `JAVA124` log message occurs:

![](example1.jpg)

When an analysis is run and the message occurs, an alert is triggered and displayed in the locations listed previously, for example:

![](example2.jpg)