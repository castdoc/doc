---
title: "Configuring the Snapshot Strategy option"
linkTitle: "Snapshot Strategy"
type: "docs"
no_list: true
weight: 80
---

## Overview

![](snapshot_strategy.jpg)

The Snapshot Strategy option automatically manages snapshot retention for your applications. By default this setting is not configured and all snapshots are retained.

## Why should I limit the number of snapshots?

The primary reason is to ensure that disk space on the CAST Storage Service/PostgreSQL instance node responsible for your application is not totally consumed by snapshots. In addition, a large number of snapshots can impact performance.

## How do I set the snapshot retention?

Choose either of the available options:

### Control the number of snapshots

Use this option if you want to retain a specific number of snapshots for all applications.

| Option | Description |
|--------|-------------|
| Keep minimum X snapshot(s) | Configure a minimum number of snapshots to retain for any given application. |
| Keep maximum X snapshot(s) | configure a maximum number of snapshots to retain for any given application. |
| Delete the snapshots if they are older than X day(s) | Configure a retention policy that will delete any snapshot for any Application that is older than X number of days. |

All three options can be used in conjunction, in which case the order of priority is as follows:

- Keep minimum X snapshot(s) per application
- Delete the snapshots if they are older than X day(s)
- Keep maximum X snapshot(s) per application

Therefore priority is given to ensuring that the minimum number of snapshots is always retained (if this option is enabled), even if those snapshots might be older than the number of days specified in the option `Delete the snapshots if they are older than X day(s)`.

### Filter on snapshot name

Use this option to retain snapshots based on their name for all applications.

| Option | Description |
|--------|-------------|
| Remove snapshots with a name that contains | Remove or retain any snapshot whose name matches a string entered in the field to the right. |
| Keep only snapshots with a name that contains | As above, but will retain matching snapshots instead of removing them. |

## How do I schedule the snapshot retention?

When any of the retention options are configured, a scheduled task will be run every weekday at 2300 to identify if any snapshots must be deleted for an application. This task will first identify if there are snapshots to be removed and then launch a snapshot cleanup job if required, which will be responsible for removing the snapshots and cleaning any data linked to this snapshot in the CAST Storage Service/PostgreSQL instance.

If you want to set an alternative date/time/frequency for the scheduled task you can use the `Schedule` button:

![](schedule.jpg)

You can choose a date/time using the UI, or you can set a date/time using the custom option using a `cron` expression which requires six characters in this order:

- seconds (0-59)
- minutes (0-59)
- hours (0-23)
- date of the month (1-31)
- month (1-12 or JAN-DEC)
- days of the week (0-6 where Sunday=0 or 7, or SUN, MON, TUE, WED, THU, FRI, SAT, or range of days MON-FRI).

For example to change to running the job at 1am every Sunday, set:

```text
0 0 1 * * SUN
```