---
title: "Configuring the Delivery Strategy option"
linkTitle: "Delivery Strategy"
type: "docs"
no_list: true
weight: 90
---

## Overview

![](delivery_strategy.jpg)

The Delivery Strategy option is designed to prevent a new analysis or snapshot from being run if the delivered source code:

- has changed by X% - for example, this option can be used to detect a very large change in source code which could indicate a problem that has not been identified prior to delivery
- has not changed at all - i.e. this can avoid a wasteful re-analysis process if the source code has not changed.

By default this setting is not configured and  are retained.

> These option are only effective where an initial analysis has already been completed. Options will be ignored for the first analysis for an application.

## Why should I configure these options?

Analyses can sometimes take a long time to complete, therefore the goal is to prevent an analysis from running when it is not necessary to do so.

## When is the analysis stopped?

If either of the options are triggered, the code delivery process will be halted in the `Delivering version` step and a warning message will be logged. For example:

```text
INF: 2020-11-10 17:04:19: Actual percentage change in delivery report for added version is : 0.0 
INF: 2020-11-10 17:04:19: stop delivery as percentage change in delivery report for added version is : 0.0
```