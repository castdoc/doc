---
title: "Configuring the Job History Strategy option"
linkTitle: "Job History Strategy"
type: "docs"
no_list: true
weight: 60
---

## Overview

![](job_history1.jpg)

The Job History Strategy option is used to set a limit on the number of historic jobs that are retained by CAST Imaging. A job is any action that has been completed by any node.

By default this setting is not configured and all jobs are retained.

## Why should I limit the number of jobs?

The primary reason is to ensure good performance. Over time, and especially where there are multiple applications/nodes, the number of jobs (and their logs) stored by CAST Imaging can become very large. When this occurs, you may find that it takes a long time for a given node to be fully recognised in CAST Imaging if the node is restarted for whatever reason - this performance issue is caused by the large number of jobs (and their logs) that are stored by CAST Imaging and which need to be looped through whenever a node is restarted.

## Example

If there are 200 jobs retained (in one application) at the time the value is set, then the next time a job is actioned, CAST Imaging will remove all but 50 of the 200 jobs - i.e. the oldest jobs will be removed. Each time a new job is actioned the oldest job and its logs will be removed to meet the criteria you set.
