---
title: "Using the PDS Dump option"
linkTitle: "PDS Dump"
type: "docs"
no_list: true
weight: 100
---

> These settings are targetted at the [com.castsoftware.mainframe](../../../../technologies/mainframe/extensions/com.castsoftware.mainframe/) technology extension.

## Overview

![](pds_dump.jpg)

The PDS Dump settings configure CAST Imaging to recognise Mainframe source code (Cobol, JCL etc.) and other source code such as Perl or ColdFusion that may be provided as a `PDS` (Partitioned Data Set) dump. A PDS is a type of "library" containing elements known as "members" exported from a z/OS system (eg. Cobol programs, copybooks, JCL and other technologies such as Perl or ColdFusion etc.) Each member in the `PDS` is preceded by a banner containing the member's name (among other information) and is concatenated with other elements in text format.

CAST Imaging supports one type of member and one banner prefix per `PDS` dump file. If there are several types of members and if several banner prefixes are used for the same type of members, they must be delivered through multiple PDS Dump files. Each PDS Dump file that is recognised by CAST Imaging will be extracted: one file (to be analyzed) will be created per element in the PDS Dump file and these files are then analyzed when an analysis is run.

## What are the default settings?

Several PDS library extensions will be predefined: this will ensure that CAST Imaging is able to recognise PDS Dump files provided in the source code configured as shown below.

All entries will be configured with a banner prefix `VMEMBER NAME`, `left margin = 1` and `line maxLength = 80`:

| Library | File extension |
|---|---|
| COBX | .cob |
| CPYX | .cpy |
| JCLX | .jcl |
| PSBX | .psb |
| DBDX | .dbd |
| BMSX | .bms |

You can leave these predefined PDS types as is, or you can delete/edit as necessary.

## Glossary of terms

### Library extension

The file extension of the PDS Dump file, for example `COPYX`. Composite extensions are also supported, for example those containing dots such as `XXX.YYY`.

The following characters are not supported in this field: `\/:?"*<>|`

### Member file extension

This option enables you to choose the content (via a file extension) of the PDS Dump file that will be extracted - i.e. entering `.cpy` will ensure that only `Cobol Copybooks` will be extracted. Only one file extension is permitted.

### Banner prefix

Indicates the left hand part of the banner in the PDS Dump excluding the member name. This determines the start of each member - this is used by CAST Imaging to identify each member.

### Left margin

Indicates the line column(s) in which system characters are present. This column (or columns) are ignored during the extraction and are not transferred to file.

### Line maxLength

For each member line that will be extracted, this value indicates the line max length that will be retained during the extraction to file. Any characters that are located in the line beyond the line max length will be ignored during the extraction and packaging process and are not transferred to file.
