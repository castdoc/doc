---
title: "Configuring the Backup Strategy option"
linkTitle: "Backup Strategy"
type: "docs"
no_list: true
weight: 70
---

## Overview

![](backup_strategy.jpg)

The Backup Strategy option automatically manages application backup retention. Application backups are stored on the relevant node (i.e. the node responsible for your application) and can be managed in <span style="color:crimson">Applications - Application Details</span>. Backups can be run manually or automatically as part of the analysis.

By default this setting is not configured and all backups are retained.

## How do set the backup retention?

Use the options in combination or on their own:

### Delete the backups if they are older than X day(s)

When the chosen number of days has elapsed, all older backups available in the application backup storage location will be removed from disk the next time a new backup retention job is run. Disabling the option will disable the backup removal entirely - in this case, no backups are removed.

### Keep X backup(s) per application

When the chosen number of backups are available in the application backup storage location, the oldest backup will be removed from disk the next time a new backup retention job is run. In other words, the backup retention job will retain no more than the chosen number of backups for restoration at any one time. Disabling the option will disable the backup retention entirely - in this case, no backups are removed.

## Why should I limit the number of backups?

The primary reason is to ensure that disk space on the node responsible for your application is not totally consumed by backup files. Over time, and especially when backups are automated and analyses are frequent, space can be quickly consumed.

## When are the backups removed?

Backup retention jobs are run on the following schedule and cannot be changed:

- 30 minutes after CAST Imaging starts up, and then every day at the same time after that.
- every time a job is run that includes an automatic backup (i.e. an analysis).
