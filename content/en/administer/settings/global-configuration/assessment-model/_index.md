---
title: "Defining an Assessment Model strategy"
linkTitle: "Assessment Model Strategy"
type: "docs"
no_list: true
weight: 10
---

## Overview

![](am.jpg)

The Assessment Model Strategy setting provides two distinct settings:

- activating or deactivating the [Share Assessment Model](share)
- managing [how Assessment Models are handled during an update](update) to a new release of CAST Imaging Core, or a new release of an extension

## What are Assessment Models?

At the core of the measurement mechanisms that are used during an analysis is the Assessment Model: a set of rules, sizing/quality measures and distributions in a hierarchy of parent Technical Criteria and Health Factors that are used to grade an application and to provide information about any defects (code violations) in the application's source code. The set of rules, sizing/quality measures and distributions are predefined and preconfigured according to established best practices.

The "main" Assessment Model is provided in your node's installation of CAST Imaging Core. In addition, some CAST extensions also contain partial Assessment Models (known as "fragments"), which provide additional rules, sizing/quality measures and distributions specific to the technology targeted by the extension.