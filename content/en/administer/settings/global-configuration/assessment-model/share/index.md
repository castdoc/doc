---
title: "Using the Share Assessment Model feature"
linkTitle: "Share Assessment Model"
type: "docs"
no_list: true
weight: 10
---

## What is the purpose of this feature?

![](share.jpg)

The aim is to ensure that all applications managed in CAST Imaging automatically use the exact same [Assessment Model](../#what-are-assessment-models) settings - this can be very important for ensuring comparable results across all applications. The fact that the change only needs to be done once also reduces the time required to tune your configuration settings.

## How do I use the Share Assessment Model feature?

By default the feature is disabled, meaning that Assessment Model customization is performed on a per application basis: all users that have permission to interact with an application will be able to make changes to an application's Assessment Model, specifically for that application using the Assessment Model configuration [option](../../../../../interface/analysis-config/config/assessment-model):

![](am_share_standard.jpg)

Enabling the feature will change the behaviour as follows:

- Users with permission to interact with an application will no longer be able to customize an application's Assessment Model (the settings are visible but cannot be modified).
- Users with the Admin role will be able to customize an application's Assessment Model and these changes will be replicated immediately to all applications managed in CAST Imaging

> If you need to make specific customizations for specific applications, you should NOT enable this option.

### What is a typical workflow with the feature enabled?

- Login as a user with the Admin role and enable the `Share Assessment Model Settings` option. Now browse to the Assessment Model configuration page for any application (it does not matter which Application you choose) and make the changes you need to make, i.e. change a rule weight, criticity, or activate/deactivate a rule:

![](am_share1.jpg)

- Return to the Share Assessment Model Settings option and any changes you made will now be visible in the list. For example, in the following image, changes have been made to two rules:

![](am_share2.jpg)

- In the Assessment Model page for any application managed in CAST Imaging, an icon will be displayed indicating that changes have been made to the rule at "shared" level:

![](am_share3.jpg)

### How do I delete a shared change?

Use the delete option in the Share Assessment Model Settings option alongside the record you no longer require:

![](am_share3.jpg)

When a record is deleted:

- The icon indicating that changes have been made to the rule at "shared" level will be removed from the rule in the Assessment Model page.
- Deleting a shared customization does not mean that the original setting/option will be restored to each Assessment Model at application level - instead the change will remain as it was just before it was deleted at shared level.
- CAST Imaging does not retain the value for any option in an Assessment Model at application level prior to being customized as a shared record, therefore if you delete a shared record, there is no method to return the option to its previous value at application level.

#### Technical notes

- Changes are propagated to other application Assessment Models when:
    - you navigate to the Assessment Model page for a given application.
    - you run a snapshot or consolidate a snapshot for a given application.
- When a customization is present in the Share Assessment Model Settings option page, you can make additional changes in this page, or you can make changes at application level in the Assessment Model page.
- When a rule is activated or deactivated, it does not depend on the parent Technical Criterion to which it is linked. Therefore a change of this state when shared mode is active will be propagated to every other Technical Criterion in which the rule appears.