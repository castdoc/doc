---
title: "Using the CSS Optimize option"
linkTitle: "CSS Optimize"
type: "docs"
no_list: true
weight: 20
---

## Overview

![](css_optimize.jpg)

Over time and through continued use, the efficiency of your application's schemas (`*_MNGT`, `*_LOCAL`, `*_CENTRAL`, `general_measure`) may well start to degrade ("gaps" in table data, inefficient indexes etc.) - this degradation can significantly impact performance with the most visible impact seen in the performance of the CAST Dashboards. To counter this, CAST Imaging provides a feature (known as `CssOptimize`) that is run automatically to optimize the schemas stored in your CAST Storage Service/PostgreSQL instance - i.e. to clean up defects that have appeared over time.

## What optimization options are available?

The `CssOptimize` tool will perform various different optimization actions, all of which are standard PostgreSQL actions:

| Option | Description |
|---|---|
| Disabled | This option disables all automatic optimization actions. Only for use when troubleshooting - otherwise not recommended since this can have a significant impact on performance of CAST Dashboards. |
| Analyze (default action) | See https://www.postgresql.org/docs/current/sql-analyze.html. |
| Vacuum | See https://www.postgresql.org/docs/current/sql-vacuum.html. |
| Analyze and Vacuum | As above. |
| Full Vacuum | As above. |

## When is the optimization run?

This optimization is run automatically with the default `Analyze` action in the following situations:

- immediately on completion of a snapshot
- immediately on completion of a data upload to the Measurement schema

If you would like to change the default action (for example change to `Analyze` and `Vacuum`, or to disable the action entirely), choose the option you require in the drop down list.

The optimize actions can also be run manually:

- via the CAST Imaging UI:
    - See [here](../db-connection/) (for the Measurement schema).
    - See [here](../../../applications/) (for the Application schemas).
- using command line tools, see [here](../../../../db/cssadmin/).