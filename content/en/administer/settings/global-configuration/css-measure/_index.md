---
title: "Configuring CSS and Measurement Settings"
linkTitle: "CSS and Measurement Settings"
type: "docs"
no_list: false
weight: 55
---

The CSS and Measurement Settings panel governs the CAST Storage Service/PostgreSQL instances that are made available to your nodes for analysis results storage requirements.