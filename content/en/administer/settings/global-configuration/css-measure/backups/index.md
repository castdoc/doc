---
title: "Using the Backups option"
linkTitle: "Backups"
type: "docs"
no_list: true
weight: 30
draft: true
---

## Overview

![](backup_strategy.jpg)

Use this setting to create backups of your Measurement schema.

## Where is the backup stored?

Download the backup. The backup will be downloaded as a ZIP file containing a .cssbackup file. This can be restored using the CAST Storage Service tools. Measurement backups are physically stored on Nodes. When the backup is run, the least "busy" Node will be used to store the backup file in the following location. By default, this location is set to use the "backup" folder defined for the Nodes - see Configure AIP Node storage folder locations - optional - v. 1.x for more information. 

%PROGRAMDATA%\AipConsole\AipNode\backup\measurements\YYYY-MM-DD.zip


