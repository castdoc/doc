---
title: "Using the database connection list"
linkTitle: "Database Connection List"
type: "docs"
no_list: true
weight: 10
---

## Overview

![](db_list.jpg)

This panel lists all the CAST Storage Service/PostgreSQL instances that are available to your node instances. After installation, one instance will be listed (the instance configured during the installation process (Microsoft Windows) or provided automatically (Linux via Docker)) and this will be used to store all analysis results as well as persistence data.

## What do CSS and Measurement mean?

### CSS
When ticked, the CAST Storage Service/PostgreSQL instance is made available for your application's analysis results storage (i.e the three schemas `*_MNGT`, `*_LOCAL`, and `*_CENTRAL`). You can tick any number of CSS instances, but you must always keep at least one instance ticked so that when new applications are added, a CAST Storage Service/PostgreSQL instance can be assigned to that application. Making any changes requires that you use the `Save` button to save the changes.

When multiple instances exist and you have ticked CSS multiple times, when a new application is onboarded you will be offered this CSS instance (and any others that are ticked) as storage in [Advanced Platform Configuration section](../../../../../interface/analysis-config/overview/advanced-platform-config/) in the [Overview page](../../../../../interface/analysis-config/overview/):

![](css_offer.jpg)

See [Overview - Advanced Platform Configuration section](../../../../../interface/analysis-config/overview/advanced-platform-config/).

## Measurement

When ticked the CAST Storage Service/PostgreSQL instance will be used to host the Measurement schema (`general_measure`) for consolidating your analysis result data for use in the **CAST Management (Health) Dashboard**. The interface  uses a "radio" button configuration, ensuring that only one instance is always selected (only one instance can host the Measurement schema). Making any changes requires that you use the `Save` button to save the changes.

If you would like to change the instance used to store the Measurement schema you can do so - however, be aware of the impacts:

- Changing the target CAST Storage Service/PostgreSQL instance will cause a new Measurement Service schema (with the name specified in the `Measurement Schema name` field) to be created on the new target CAST Storage Service/PostgreSQL instance the next time an analysis is run. This new Measurement Service schema will then be used for all future actions where data is uploaded to the Measurement schema. Remember that any analysis result data stored in any previously used Measurement Service schema will no longer be available in the CAST Health Dashboard - however the schema will continue to exist on the CAST Storage Service/PostgreSQL instance.

## How do I add or edit a CSS/PostgreSQL instance?

Use the add/edit button as appropriate and fill in the fields as indicated below:

![](new_css.jpg)

| Field | Description |
|---|---|
| Host | The hostname/IP address of the target CAST Storage Service/PostgreSQL instance. This instance can either be installed on a local machine or remote machine (network access required). CAST recommends that you avoid using "localhost" even if the target CAST Storage Service/PostgreSQL is located on the same machine - this is particularly true where all components are hosted within a Docker environment on the same machine. |
| Port | The port number of the target CAST Storage Service/PostgreSQL instance. [CAST Storage Service 4.x](https://extend.castsoftware.com/#/extension?id=com.castsoftware.css&version=4.13.14) defaults to `2284`. |
| Username/Password | The credentials for the target CAST Storage Service/PostgreSQL instance. The default credentials for CAST Storage Service instances are `operator/CastAIP`. |
| Database name | The database name on your target CAST Storage Service/PostgreSQL instance. By default, `postgres` will be populated already. |
| Use SSL | Tick this option if your target CAST Storage Service/PostgreSQL instance is running in SSL encrypted mode. See [here](https://doc-legacy.castsoftware.com/display/STORAGE/SSL+encrypted+mode+configuration+for+CAST+Storage+Service+and+PostgreSQL). |

## How does the Measurement schema name field work?

This field defines the name of the Measurement Service schema. By default this is set to `general_measure`. Authorized characters for the name are as follows:

- `0-9`
- `A-Z`
- `_` (underscore)

A validation process is actioned and any unauthorized characters, such as `-`, `#` or `$` will be rejected.

If you would like to change the name of the Measurement schema be aware of the impacts:

- Changing the Measurement schema name will cause a new Measurement schema (with the name specified in the `Measurement Schema name` field) to be created on the target CAST Storage Service/PostgreSQL instance the next time an analysis is run. This new Measurement Service schema will then be used for all future actions where data is uploaded to the Measurement schema. Remember that any any analysis result data stored in any previously used Measurement Service schema will no longer be available in the CAST Health Dashboard - however the schema will continue to exist on the CAST Storage Service/PostgreSQL instance.

## What about the Optimize option?

![](optimize_button.jpg)

> This option targets the Measurement schema

Over time and through continued use, the efficiency of your Measurement schema may start to degrade ("gaps" in table data, inefficient indexes etc.) - this degradation can significantly impact performance with the most visible impact seen in the performance of the CAST Health Dashboard. To counter this, CAST provides a feature (known as `CssOptimize`) that can be run manually to optimize the Measurement schema stored on your CAST Storage Service/PostgreSQL instance(s) - i.e. to clean up defects that have appeared over time. You can choose one of four options described [here](../optimization). The optimization action will run immediately.

By default, CAST Imaging will run an automatic optimization of the Measurement schema using the `Analyze` action (this action can be changed/disabled [here](../optimization)) immediately on completion of a data upload to the Measure schema.