---
title: "Using the .NET Assemblies option"
linkTitle: ".NET Assemblies"
type: "docs"
no_list: true
weight: 120
---

> These settings are targeted at the [com.castsoftware.dotnet](../../../../technologies/dotnet/extensions/com.castsoftware.dotnet/) technology extension.

## Overview

![](net_assemblies.jpg)

If you choose not to deliver your application's external .NET assemblies (provided by third-parties, or your own custom assemblies) together with the application source code, this option allows you to tell CAST Imaging where these assemblies are located. During an analysis, CAST Imaging will search the folders you declared and find the assemblies required by your application source code.

The settings are "global", (i.e. for all applications managed in CAST Imaging), therefore CAST highly recommends placing required .NET assemblies in a common shared folder that can be accessed by all nodes.

## Are there any default paths?

No default paths are provided.
