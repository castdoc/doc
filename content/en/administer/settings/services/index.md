---
title: "Services"
linkTitle: "Services"
type: "docs"
no_list: true
weight: 40
---

## Overview

Provides technical information about the various services that are registered and interact with your CAST Imaging installation:

- Nodes
- CAST Imaging Viewer

## Nodes

For each node that is registered with your CAST Imaging installation, an entry will be listed. This typically takes the following format:

![](node-service.jpg)

### Name / URL / technical spec

![](name_url.jpg)

Name, URL and technical spec of the registered node. The name is determined automatically and consists of the server host name and the port that the node service is running on.

>Where the node service resides on the same machine as the CAST Imaging Console services, the node will be title `<node_name>:<port>`.

### Disk and database storage status

![](disk-database.jpg)

Shows:

- the status of the disk storage on the node. Rolling the mouse pointer over the icon will display the amount of free disk space in GB. The icon will be displayed in yellow when disk space is low.
- the status of all CAST Storage Service/PostgreSQL instances (whether it is "up" (green) or "down/unreachable" (red)) configured in CAST Imaging (see [CSS and Measurement](../global-configuration/css-measure/) settings). Rolling the mouse pointer over the icon will display the host name and port number of the instance.

### System logs

Use this option to download the node service logs (i.e. the `%PROGRAMDATA%\CAST\Imaging\CAST-Imaging-Analysis-Node\logs\analysis-node.log` file) for the current node.

## Imaging/ Dashboard

A simple entry to show the URL and port number of your results consultation services. If the service is uncontactable, it will be displayed in red:

![](not_available.jpg)