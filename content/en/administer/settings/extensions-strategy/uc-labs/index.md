---
title: "Using the Allow User Community/Labs setting"
linkTitle: "Allow User Community/Labs"
type: "docs"
no_list: true
weight: 40
---

## Overview

This option either allows or prevents the installation of User Community or Labs extensions. By default the option is enabled, meaning that User Community or Labs extensions will be listed in the Extensions Strategy page and will be available for installation in the Available panel at application level.

![](uclab.jpg)

## Why is this setting enabled out of the box?

Some User Community/Labs extensions can enhance your analysis results and therefore CAST specifically enables this option to allow this type of extension to be installed.

## What happens when the Allow User Community/Labs setting is enabled/disabled?

When you enable/disable the option you will be prompted whether you want to apply the same setting to all extensions via the `Force all extensions to this option` tick box:

![](uclab_prompt.jpg)

Ticking this option will ensure that ALL individual extensions are ALSO set to the same setting (either enabled or disabled), overriding any settings set at the extension level.

## Can I see which extensions are User Community/Labs extensions?

Use the filter column:

![](uclab_filter.jpg)

## Notes

- When the option is disabled, all User Community and Labs extensions will be blacklisted/denied preventing them from being installed:

![](uclab_deny.jpg)

- If a User Community or labs extension is set to Force Install prior to disabling the `Allow User Community/Labs` option, the extension will remain in the Force Install list and will NOT be blacklisted/deny listed.