---
title: "Using the Blacklist/Deny setting"
linkTitle: "Blacklist/Deny"
type: "docs"
no_list: true
weight: 60
---

## Overview

CAST Imaging makes it possible to blacklist/deny a specific extension. This action prevents the extension from being made available for installation, even if the extension has been set to [Force Install](../force-install):

![](deny_list.jpg)

## When should I use this option?

One example could be a situation where a User Community/Labs extension generates results that conflict with an official CAST "product" extension.

## Can I see which extensions are blacklisted/denied?

Use the filter column:

![](deny_list_filter.jpg)