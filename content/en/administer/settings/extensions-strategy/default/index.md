---
title: "What are the default extension strategy settings?"
linkTitle: "Default settings"
type: "docs"
no_list: true
weight: 10
---

## General settings

![](general.jpg)

### Auto update

Disabled - i.e. extensions will not update when a new release becomes available, therefore maintaining stability of your analysis results. This puts the onus on you to ensure that you update your extensions where necessary to benefit from bug fixes or feature improvements.

See [Using the extension Auto Update setting](../auto-update).

### Extension stability level

`LTS` (Long Term Support) and `Funcrel` (Functional Release) permitted, Beta and Alpha extensions are blocked and will not be made available for installation whether manually or automatically.

See [Using the extension Stability Level setting](../stability).

### Allow User Community/Labs

Enabled. This allows extensions created by the wider CAST community to be made available for installation whether manually or automatically.

See [Using the Allow User Community/Labs setting](../uc-labs).

## Force installed extensions

![](force_install.jpg)

A set of extensions will be listed in the force install list by default (and also after update to a new release of CAST Imaging). This means that these extensions will be installed for all Applications managed within CAST Imaging Console during the next analysis:

- [Application Information](https://extend.castsoftware.com/#/extension?id=com.castsoftware.applicationinformation&version=latest)
- [Automatic Links Validator](https://extend.castsoftware.com/#/extension?id=com.castsoftware.automaticlinksvalidator&version=latest)
- [Console Information for Imaging](https://extend.castsoftware.com/#/extension?id=com.castsoftware.consoleinformation&version=latest)
- [Core Metrics](https://extend.castsoftware.com/#/extension?id=com.castsoftware.coremetrics&version=latest)
- [Highlight to MRI](https://extend.castsoftware.com/#/extension?id=com.castsoftware.highlight2mri&version=latest) - ONLY when the entries in [CAST Highlight Settings](../../system-settings/highlight/) have been populated
- [ISO-5055 Index](https://extend.castsoftware.com/#/extension?id=com.castsoftware.omg-ascqm-index&version=latest)
- KB Portable Root Paths (internal extension)
- [Module Definition Types](https://extend.castsoftware.com/#/extension?id=com.castsoftware.moduledefinitiontypes&version=latest)
- [Quality Standards Mapping](https://extend.castsoftware.com/#/extension?id=com.castsoftware.qualitystandards&version=latest)

It is not mandatory to use any of these extensions (they can be disabled), but they provide additional insight and information and CAST highly recommends that they are ALL left enabled - indeed some features in CAST Imaging will not be available if the force installed extensions are disabled. Some additional notes:

- If you are using CAST Extend local server and you intend to use these extensions (i.e. leave them listed in the table) you must ensure that you update your service to include these extensions.
- The Auto Update option at extension level will be disabled by default - this means that extension will not update automatically unless you either:
    - enable Auto Update at global level
    - manually enable Auto Update at extension level by moving the slider to the enabled position alongside the extension

{{% alert color="info" %}} See [Using the Force Install setting](../force-install).{{% /alert %}}