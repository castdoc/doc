---
title: "Using the extension Auto Update setting"
linkTitle: "Auto Update"
type: "docs"
no_list: true
weight: 20
---

## Overview

There are two Auto Update settings available for use:

### Global level (all extensions)

![](auto_update.jpg)

When enabled (the default position is disabled), this option provides a global (i.e. for all Applications managed in CAST Imaging) method of ensuring that all Applications will always use the latest release for all "included extensions" - i.e. those required for analysis, including `Force Installed` extensions.

CAST Imaging will also respect the setting applied via the extension Stability Level slider (see [Stability Level](../stability)) - in other words, if you only permit `LTS` or `funcrel` releases via the slider, the "latest extension" will be the most recent `LTS` or `funcrel` release - any other more recent `alpha` or `beta` releases will be ignored.

### Extension level

![](auto_update_extensions.jpg)

This option functions only for individual extensions.

## When should I enable the Auto Update option?

The Auto Update option at global and extension level is deliberately disabled by default and this is specifically to maintain stability of your analysis results: new releases of extensions will contain bug fixes and new features, which can impact any future analysis results.

The choice to enable this option is therefore a balance between the need to maintain result stability and the ability to quickly take advantage (without having to specifically do anything) of new features and bug fixes that are regularly published by CAST.

## When are new extensions installed when Auto Update is enabled?

When CAST Imaging detects that an extension update is required (i.e. a new release is available) an icon will be placed in the Included Extensions panel to highlight that an update is available:

![](requires_update.jpg)

The update itself will be applied when a new analysis is actioned, in a dedicated job step:

![](update_job.jpg)

## What happens when the Auto Update option is enabled/disabled at global level?

When you enable/disable the global level option, you will be prompted whether you want to apply the same setting to all extensions via the `Force all extensions to this option` tick box:

![](auto_update_prompt.jpg)

Ticking this option will ensure that ALL individual extensions are ALSO set to the same setting (either enabled or disabled), overriding any settings set at the extension level. Do not enable the tick box if you prefer to retain the individual Auto Update setting that you have manually applied:

![](auto_update_all.jpg)

## Can I see which extensions are set to Auto Update?

Use the filter column:

![](auto_update_filter.jpg)