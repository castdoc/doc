---
title: "Using the extension Stability Level setting"
linkTitle: "Stability Level"
type: "docs"
no_list: true
weight: 30
---

## Overview

![](stab_level.jpg)

This option controls which type of extensions can be installed, whether manually or automatically:

- LTS (or longterm: Long Term Support)
- Funcrel (i.e.: Functional Release)
- Beta
- Alpha

For example, it may not be desirable to allow the use of extensions that are in `Funcrel`, `Beta` or `Alpha` status in a "production" analysis. Therefore the slider can be adjusted to allow only `LTS` extensions.

## Which setting should I choose?

By default, only `LTS` and `Funcrel` extensions are permitted. This is to ensure that extensions that are only in the `Beta` or `Alpha` phase of their development are not installed since these may contain issues or features that are undesirable.

You are free to choose any range of extension release type you need.

>The Force Install setting overrides the Stability Level setting: if you have set a specific `Alpha` release of an extension to Force Install, but you have set the Stability Level to `LTS` and `Funcrel` extensions, the specific `Alpha` release of the extension will still be installed.

## Where can I see the impact of this setting?

### Within the Extensions Strategy list

All lists of extensions will update and follow the chosen Stability Level, for example:

![](stab_level_1.jpg)

### Within the Available Extensions panel

When looking at the Available Extensions panel, only extensions whose version status matches the strategy chosen at global level will be available for selection - in the image below, `Funcrel`, `Beta` and `Alpha` extensions are no longer available since the strategy has been changed to `LTS` only:

![](stab_level_2.jpg)