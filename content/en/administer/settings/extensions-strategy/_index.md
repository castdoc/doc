---
title: "Extensions Strategy"
linkTitle: "Extensions Strategy"
type: "docs"
no_list: false
weight: 80
---

The Extensions Strategy setting provides various options for managing at a global level (i.e. for all Applications managed in CAST Imaging) the extensions that are used in each Application analysis. The main objective of these options is to ensure that the extensions that are available for use in an analysis by an Application owner or Analysis Manager can be controlled - for example:

- allow/prevent the use of specific extensions depending on their status (LTS, funcrel, beta, alpha etc.)
- allow only specific extensions and specific versions of extensions to be used in all analyses - a white list
- force the installation/use of specific extensions in all analyses

The settings can be seen as controlling mechanisms to prevent or grant access to any specific official or custom extensions that have been published by CAST. It is not mandatory to make any changes to the default options, however, CAST highly recommends that you evaluate the correct strategy for your own environment before you start the onboarding process.
