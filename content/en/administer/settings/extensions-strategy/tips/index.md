---
title: "General tips and rules for extension installation"
linkTitle: "Tips and rules"
type: "docs"
no_list: true
weight: 70
---

## Extension download and storage

Extensions are managed from the [Extensions Strategy panel](..) but it is the node responsible for your application that will:

- physically download the extension
- store the extension on its local disk in `%PROGRAMDATA%\CAST\CAST\Extensions`

## Release numbers

The release number of extensions used for your application will be determined the first time you run an analysis on this application. The most recent release of the extension will be downloaded (respecting the [stability level](../stability) setting) if the extension does not already exist on the node (in `%PROGRAMDATA%\CAST\CAST\Extensions`) responsible for your application. If the extension already exists on the node, then it is this specific release of the extension that will be used unless [Auto Update](../auto-update) is enabled or unless a specific release number is manually selected (either in the Extensions Strategy administration panel or at Application level in the Included/Available extensions screen).

## Extension installation failing at "install extensions"

- If you find that your analyses are failing at the "install extensions" step due to CAST Imaging automatically requesting an extension that does not meet the strategy requirements (i.e. it is an `alpha` or `beta` where only `LTS` or `funcrel` extensions are allowed), you can [blacklist/deny](../black-list) the extension to prevent CAST Imaging from automatically attempting to install it. Note that this may mean your analysis results are compromised where the extension provides specific functionality required for your source code.

## Priority levels

The following priority levels are applied to extensions settings - settings at the top have priority:

![](priority.jpg)