---
title: "Using the Force Install setting"
linkTitle: "Force Install"
type: "docs"
no_list: true
weight: 50
---

## Overview

The `Force Install` option is available for all extensions, at extension level. It allows you to force CAST Imaging to ALWAYS install a specific release of a specific extension in all of your applications. Set the release you would like and then enable the Force Install toggle:

![](force_install_ext.jpg)

CAST sets some extensions to `Force Install` by default and with the latest release (at the time when your deployment was installed) in order to provide additional features and behaviour - see [Force installed extensions](../default/#force-installed-extensions).

## What is the aim of this option?

This option has two principal aims:

- to ensure that every application you onboard gets a specific release of an extension, thereby ensuring analysis result stability.
- to ensure that extensions providing specific behaviour or features are always installed in every application you onboard.

## Can I see which extensions are set to Force Install?

Use the filter column:

![](force_install_filter.jpg)