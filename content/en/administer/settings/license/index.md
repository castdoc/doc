---
title: "Manage your license keys"
linkTitle: "License keys"
type: "docs"
no_list: true
weight: 50
---

## Global CAST license key

When a fresh installation of CAST Imaging is initialized, a global license key is referenced (this is obtained from [CAST Support](https://help.castsoftware.com/hc/en-us)). This license key is displayed in this panel.

The license key grants a license for all nodes managed in CAST Imaging Console and restricts usage in various different ways as determined by your contract with CAST, for example:

- By the number of Contributing Developers (CDs)
- By the expiry date

If you have a new key, use the interface to edit and add your new license key.
