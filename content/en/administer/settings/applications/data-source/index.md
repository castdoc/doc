---
title: "Managing data source configuration"
linkTitle: "Managing data source configuration"
type: "docs"
no_list: true
weight: 20
---

***

## Overview

The data source configuration option is specifically for application view results that have been imported directly into CAST Imaging without having been analyzed in the current instance. These results will have been generated in previous releases of CAST Imaging Viewer or another instance of CAST Imaging (using the `etl-automation` [tool](https://doc.castsoftware.com/export/IMAGING/Automation+tool+export+and+import+process)) and therefore there is no record of where the source code corresponding to the results is stored (i.e. which analysis schema on which **CAST Storage Service/PostgreSQL instance**). This means that the application's source code cannot be displayed as part of the results in the [Right panel](../../../../interface/viewer/right-panel/):

![](no_source_code.jpg)

As such, the data source configuration option provides a way to link the imported results with the application's source code stored in a remote CAST Storage Service/PostgreSQL instance.

{{% alert color=info %}}<ul><li>Applications that have been fully analyzed in CAST Imaging will have their source code available by default and therefore this option is not available, nor necessary.</li><li>See also <a href="../../../../administer/performance/source-code/">Source code does not display</a> to troubleshoot issues with source code display.</li></ul>{{% /alert %}}

## How do I access the data source configuration option?

Select the application you want to configure and then use the **Actions** menu:

![](access.jpg)

{{% alert color=info %}}The **View data source** option is only available for applications that have been imported and therefore have the dedicated import icon:<br><br>![](icon.jpg){{% /alert %}}

## How do I add a new data source?

To add a new data source, the **CAST Storage Service/PostgreSQL instance** on which the source code is located (i.e. the instance on which the application was originally analyzed) must already have been declared in CAST Imaging using the [CSS and Measurement Settings](../../global-configuration/css-measure/) option:

![](add_css.jpg)

Then click the **View data source** menu option as describe above and click **Connect to a database**:

![](connect.jpg)

Then choose and confirm the **CAST Storage Service/PostgreSQL instance** and the **schema** that corresponds to the imported application where the source code is stored:

![](choose.jpg)

Finally check that you can access the source code of an object using the results viewer:

![](code_working.jpg)