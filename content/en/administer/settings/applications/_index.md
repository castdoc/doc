---
title: "Applications"
linkTitle: "Applications"
type: "docs"
no_list: false
weight: 10
---

***

## Overview

The Applications panel lists all applications available in CAST Imaging and provides various technical options, including:

- manage domains
- manage CAST Imaging Core update
- manage data source for source code viewing for imported applications
- manage advisor for viewer
- manage backups
- manage nodes
- manage log debug options

![](applications.jpg)

## Column headings

| Heading | Description |
|---|---|
| Application name | Display name for the application. |
| Domain | The domain the application belongs to (if any). See also [Working with domains](domains/). |
| Created by | The user that created or imported the application. |
| Core version | The CAST Imaging Core release used to analyze the application. Where no version number is visible, the application has been imported in results form instead. |
| Core update available | If a release number is listed, this indicates that the CAST Imaging Core release used to analyze the application is out-of-date and that a new release is available. See [Using the CAST Imaging Core update option](update/). |
| Import version | Indicates the internal engine release number used to generate the results. |
| Import duration | Indicates the length of time required to generate the results. |
| Actions | Contextual menu containing various options. See [Actions](#actions). |

## Icons

![](icons.jpg)

The icons available above the list of applications indicate the following:

| Icon | Description |
|---|---|
| ![](delete_icon.jpg) | [Delete](delete/) the application. |
| ![](domains_icon.jpg) | Assign the application to a specific domain. See [Working with domains](domains/). |
| ![](upgrade_icon.jpg) | When active, allows you to update CAST Imaging Core (used to analyze the application) to a new release. See [Using the CAST Imaging Core update option](update/). |
| ![](advisor_icon.jpg) | Allows you to [upgrade](upgrade-advisor/) the Advisor feature to take advantage of improved rules. |
| ![](job_icon.jpg) | When blue, this icon indicates that a result generation or application result import job is currently in progress. Clicking the icon will reveal the job in progress and any that might be queued:<br><br>![](queue.jpg) |

## Actions

### Icons

![](action_icons.jpg)

Icons will indicate both:

- in-progress jobs
- completed jobs - a flag coloured depending on the job outcome:
  - Green - completed without issue
  - Red - job has failed
  - Yellow - job has been stopped

Clicking any of the icons will display the current on-going job logs, or the most recent if the job is completed.

{{% alert color="info" %}}Both analysis jobs and result import jobs will have status flags.{{% /alert %}}

### Contextual menu

![](menu.jpg)

| Menu option | Description |
|---|---|
| View details | |
| View data source | Add a connection to a CAST Storage Service/PostgreSQL instance specifically to view source code for applications that have been imported. See [Managing data source configuration](data-source/). |
| Change domain | Assign the application to a specific domain. See [Working with domains](domains/). |
| Update application | When active, allows you to update CAST Imaging Core (used to analyze the application) to a new release. See [Using the CAST Imaging Core update option](update/). |
| Optimize | Run various database optimization options for the schemas related to the current application (`*_MNGT`, `*_LOCAL`, `*_CENTRAL`, `general_measure`). These options are also available in the **Global Configuration** administration options, see [Optimization](../global-configuration/css-measure/optimization/). |
| Download import logs | Downloads all logs associated with the process of generating results post analysis. A file called "logs" is downloaded which can be opened with a text editor. |
| Rename / Delete | Rename or [Delete](delete/) an existing application. |
