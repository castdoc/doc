---
title: "Delete an application"
linkTitle: "Delete an application"
type: "docs"
no_list: true
weight: 50
---

***

Deleting an application will remove all trace of the application from CAST Imaging: the Application's delivery / deploy folders and all Application schemas stored on the CAST Storage Service/PostgreSQL instance will be deleted. This is a process that cannot be reversed, so only use it if you are sure.

To delete an application use the **Delete** option in the action menu:

![](delete_app.jpg)

{{% alert color="info"%}}<ul><li>You can bulk delete applications by selecting those you want to delete via the checkbox on the left.</li><li>Single applications can also be deleted from the <a href="../../../../interface/landing-page/">Landing page</a> for users with appropriate permissions.</li></ul>{{% /alert %}}

The following prompt will be displayed:

![](delete_prompt.jpg)

- If you want to retain the application schemas (`*_MNGT`, `*_LOCAL`, `*_CENTRAL`,) in their target CAST Storage Service/PostgreSQL instance, ensure you tick the **Keep data in CSS** option.
- If you want to retain any backup data on disk related to the application (see [Backups](../details/backups)), ensure you tick the **Keep backup files/data** option.
