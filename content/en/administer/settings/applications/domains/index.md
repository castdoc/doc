---
title: "Working with domains"
linkTitle: "Working with domains"
type: "docs"
no_list: true
weight: 30
---

## Overview

![](domains.jpg)

A domain is a method of grouping applications together primarily so that they can be assigned to specific profiles (see [User Permissions](../../user-permissions)) in order to grant access to the applications in the domain.

By default, no domains are provided and all new applications will not belong to any domain (they will appear under `No Domain`).

## How do I create a domain?

Use the `Add new domain` button highlighted in yellow at the top of the page.

## How do I assign an application to a domain?

Use the `Change Domain` option:

![](change_domain.jpg)

Or use the icon on the toolbar:

![](change_domain2.jpg)

## How do I assign a domain to a user/group profile?

See [User Permissions](../../user-permissions).

## Can I filter on domains to improve visibility?

Yes, in the main application landing page:

![](domains1.jpg)

And in the current **Admin Settings** page as shown in the image at the top of the page.