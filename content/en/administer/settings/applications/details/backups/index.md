---
title: "Managing application backups"
linkTitle: "Managing application backups"
type: "docs"
no_list: true
weight: 20
---

***

## Overview

![](backups.jpg)

Use this section to:

- manage any application backups that have already been created
- run a new backup

## How do I access this option?

Use the `View Details` option in the main Applications list:

![](view_details.jpg)

## How do I perform a manual backup?

Use the icon highlighted in the image at the top of this page and choose your backup options:

![](backup_options.jpg)

## What is backed up?

- the application's associated schemas `*_MNGT`, `*_LOCAL`, and `*_CENTRAL` (will be backed up in `.cssbackup` format)
- the application's **Delivery** folder will be backed up in uncompressed format, also including:
    - exclusion patterns, source path, objective choices etc. in XML format
    - [Analysis Results Indicator](../../../global-configuration/analysis-results-indicators/) records
    - all fast scan data

## Where is the backup stored?

CAST Imaging Console will temporarily store data on disk before transferring the successful backup data to the default storage location.

This temporary storage is located on the node responsible for the application that is being backed up. This location is governed by the `CAST_CURRENT_USER_TEMP_PATH` variable defined by CAST Imaging Core in the `%PROGRAMFILES%\CAST\8.x\CastGlobalSettings.ini` file.

Successful backups is then stored in the following location by default:

```text
\common-data\backup\<application_name>\<appname>_YYYY-MM-DD.zip`
```

## Can I download a copy of the backup?

Use the download icon:

![](download_backup.jpg)

## How do I restore a backup?

Use the restore icon:

![](restore_backup.jpg)

### What is restored?

- the Application's associated schemas (`*_MNGT`, `*_LOCAL`, and `*_CENTRAL`) will be restored  (i.e. existing schemas are deleted and replaced by those in the backup)
- the Application's associated Delivery folder will be restored (i.e. the existing Delivery folder will be deleted and replaced by the data in the backup)
- options within the Application, for example exclusion patterns, source path, objective choices
- any new scan created after the backup was created will be deleted
- any other backups created after the restored backup was created will be deleted
- the Measurement schema will be synchronised using the data in the restored schemas
- all fast scan data is restored

### Post-restore recommendations

CAST highly recommends that you action the following post-restore to ensure that the restored application has all elements in place:

- Run a new scan (analysis) of your application with the existing source code, then compute structural flaws and generate views.