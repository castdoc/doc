---
title: "Managing a node"
linkTitle: "Managing a node"
type: "docs"
no_list: true
weight: 50
---

***

## Overview

A node is the "analysis engine service" that is responsible for analyzing your application. This screen enables you to set a specific node for your application.

## Default behavior

CAST Imaging will automatically function as follows when there are **multiple nodes** available (when only one node is available, for example in a single machine installation, this screen is irrelevant):

- Applications are not tied to a specific node
- Nodes are considered to be stateless
- CAST Imaging will automatically operate in load-balancing mode where nodes are chosen from the pool of available nodes to perform the next job in this order:
    - the node running the most recent release of CAST Imaging Core will be chosen first, above all others
    - if all nodes are running the same release of CAST Imaging Core, then the least used node instance is selected
    - therefore, by default, this section will display **Any** as follows, indicating that any node can be used to process the application, provided that the release of CAST Imaging Core running on the node is equal to the release of CAST Imaging Core used for your application for previous actions:

![](node1.jpg)

## How do I set a specific node for my application?

If you need to set a specific node for your application (for example where your application requires specific configuration, such as RAM or CPU), use the drop down list to select the node:

![](node2.jpg)

{{% alert color="info" %}}<ul><li>CAST Imaging will ONLY offer nodes running the same release of CAST Imaging Core used for your application for previous actions: nodes running older releases of CAST Imaging Core will not be offered for selection.</li><li>When you select a specific node it is not possible to change the node again in the future. This is to ensure result consistency.</li></ul>{{% /alert %}}

## Why can't I select a different node?

If you have multiple nodes but you cannot select a different one using this drop down list (i.e. it is disabled), this means that a dedicated node has already been set in the [Overview page](../../../../../interface/analysis-config/overview/) under the [Advanced Platform Configuration section](../../../../../interface/analysis-config/overview/advanced-platform-config/). In this situation, the node that has been selected will be displayed:

![](node_different.jpg)

