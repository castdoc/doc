---
title: "Using the debug options"
linkTitle: "Using the debug options"
type: "docs"
no_list: true
weight: 40
---

***

## Overview

The **Debug** options are application specific and provide various options that can help debug issues that you may encounter during the analysis process. Typically these options should be deactivated as they can impact performance.

## Show SQL

When activated, this option will render SQL queries used by CAST Imaging in all log files. Please do not activate this options unless you are requested to do so by CAST Support as part of a troubleshooting exercise.

## Activate AMT Memory Profiling

Please do not activate this option unless you are requested to do so by CAST Support as part of a troubleshooting exercise.

## Include debug messages	

When activated, this option will enable detailed log messages. The option will return to the disabled position automatically immediately after the next analysis job has completed. This is so that debug messages are only made available in the analysis log file when the are required: if the option is left enabled for all analyses, analysis log messages can become very large.

![](debug1.jpg)

![](debug2.jpg)
