---
title: "Using the database schemas option"
linkTitle: "Using the database schemas option"
type: "docs"
no_list: true
weight: 30
---

***

This section lists the schemas associated with the current application. These will be named as follows:

- Management schema - `<application_name>_mngt`
- Analysis schema - `<application_name>_local`
- Dashboard schema - `<application_name>_central`

The **CAST Storage Service/PostgreSQL instance** on which the schemas are stored will also be displayed in the top bar:

![](schemas.jpg)

You may occasionally see schema names starting with `uuid_` listed in the **Name** column. This can occur in the following situations:

- When characters such as `_` (underscore) and `.` (full stop/period) are used in the corresponding application name, these characters are NOT authorized for use in schema names.
- If schemas already exist on the target CAST Storage Service/PostgreSQL instance with the same name.