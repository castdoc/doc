---
title: "Managing Application details"
linkTitle: "Details"
type: "docs"
no_list: false
weight: 10
---

***

This section explains the **Details** section, providing information about specific applications. This option can be accessed by selecting the application then using the **Actions** menu:

![](access.jpg)

Each option in the **Details** section is explained in the pages listed below:

![](details.jpg)
