---
title: "Named Application license key"
linkTitle: "Named Application license key"
type: "docs"
no_list: true
weight: 10
---

***

This section is only available when a **Named Application** license key scope has been chosen during the initial configuration of the instance:

![](license.jpg)

This panel enables you to update/replace the license key - for example if the license is expiring and you have a new replacement license key. To do so, enter the replacement license key in the **License Key** field and then click **Update**.

{{% alert color="info" %}}Note that the replacement license key MUST contain the SAME application name as the previous license.{{% /alert %}}
