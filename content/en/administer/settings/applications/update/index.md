---
title: "Using the CAST Imaging Core update option"
linkTitle: "Update CAST Imaging Core"
type: "docs"
no_list: true
weight: 40
---

***

## Overview

![](upgrade.jpg)

The Update option allows you to perform an Application/schema update on one or multiple applications.

By default the icon is disabled until you select an application that is eligible for update, and will then enable as shown in the image above.

{{% alert title="Warning" color="warning" %}}
An update cannot be reversed.
{{% /alert %}}

## When is an application eligible for update?

When the CAST Imaging Core release running on the node is older than the CAST Imaging Core release available on ANY node managed in CAST Imaging.

## How do I start a CAST Imaging Core update?

Use the icon highlighted in the image at the top of this page, or use the `Upgrade Application` option:

![](upgrade_action.jpg)

## What happens when I start an update?

A warning is displayed before the update action is started. You can also choose the target CAST Imaging Core release - this drop down box will list all the Core releases available on all nodes managed in CAST Imaging that are NEWER than the CAST Imaging Core release used for the current application.

## What actions does CAST Imaging perform during an application update?

- An automatic backup of the application's result schemas (`_MNGT`, `_CENTRAL`, `_LOCAL`) and its **Delivery** folder is performed automatically - see [Backups](../../../administer/settings/applications/details/backups/) for details.
- An update of the **application schemas** and the **Delivery** folder to the new CAST Imaging Core release.

### Automatic backup: use of temporary disk space

When an automatic backup is run as part of the update process, CAST Imaging will temporarily store data on disk before transferring the successful backup data to the default storage location described in [Backups](../../../administer/settings/applications/details/backups/). This temporary storage is located on the Node responsible for the application that is being backed up, by default set to:

```text
Microsoft Windows
%TEMP%\CAST\CAST\$CAST_MAJOR_VERSION$.$CAST_MINOR_VERSION$\CAST_Tmp_backups

Linux via Docker - located within the node container
/tmp/CAST/CAST/$CAST_MAJOR_VERSION$.$CAST_MINOR_VERSION$/CAST_Tmp_backups

```

This location is governed by the `CAST_CURRENT_USER_TEMP_PATH` variable defined by CAST Imaging Core on the node in question in the following file:
 
```text
Microsoft Windows
%PROGRAMFILES%\CAST\<release>\CastGlobalSettings.ini
 
Linux via Docker - located within the node container
/home/carl/caip/CastGlobalSettings.ini
```
 
Therefore you should ensure that the node has appropriate free disk space before starting the update. The automatic backup can be disabled, by editing the following file (this file is never overwritten in an update process):

```text
Microsoft Windows
%PROGRAMDATA%\CAST\Imaging\CAST-Imaging-Analysis-Node\application-default.yml

Linux via Docker - located within the node container
/home/carl/data_aip_node_v3/application-default.yml
```

Copy the following into the file - the line `enabled: false` disables the automatic backup:

```yaml
backup:
   aipcore:
      upgrade:
          # Set this option to false in order to prevent backup to be performed during the AIP Core upgrade. Default to true.
          enabled: false
```

Save the file and then restart the Node so that the change is taken into account.

{{% alert color="info" %}}The backup process is performed by the node that is running the application / schema update, therefore if you have multiple nodes and want to disable the automatic backup for all of them, you must repeat the process described above on all nodes.{{% /alert %}}

