---
title: "AI Settings"
linkTitle: "AI Settings"
type: "docs"
no_list: true
weight: 55
---

***

{{% alert color="warning" title="Important" %}}By entering an API key for an AI provider, CAST Imaging users will be able to send snippets of source code to OpenAI / Azure OpenAI / Ollama AI / Google Vertex AI, via the CAST Imaging interface. CAST Imaging does not provide any specific governance of this, therefore you must ensure that your organization's internal policies around AI authorize this usage.{{% /alert %}}

## Overview

CAST Imaging integrates with AI technology to provide an automated AI driven explanation of objects materialized during an application analysis. You can find out more details about using AI services in [Leverage AI services](../../../explore-results/ai/).

CAST supports the following AI providers:

- [OpenAI](https://openai.com/)
- [Azure OpenAI](https://azure.microsoft.com/en-in/products/cognitive-services/openai-service)
- [Ollama AI](https://ollama.com/) - local or cloud deployments
- [Google Vertex AI](https://cloud.google.com/vertex-ai)

This feature is available only when the required provider is configured and when the relevant feature is enabled - see below:

![](ai_features.jpg)

## Configuring a provider

Ensure you have generated the required configuration data from your chosen provider [see below](#obtaining-an-api-key), choose the provider in the drop down and enter the key, then click **Update**:

![](key.jpg)

The following is required:

| Provider | Required |
|---|---|
| OpenAI | <ul><li><strong>API key</strong></li><li>Access to <code>api.openai.com</code> (port 443).</li></ul> |
| Azure OpenAI | <ul><li><strong>API Key</strong></li><li><strong>Endpoint URL</strong>: usually this will take the form <code>https://<app_name>.openai.azure.com</code>.</li><li><strong>Deployment Name</strong>: usually this will take the form <code><app_name></code>.</li></ul>CAST recommends using the **gpt-35-turbo** or **gpt-4** models. |
| Ollama AI | <ul><li><strong>Endpoint URL</strong>: usually this will take the form <code>http://<local_server>:11434</code>.</li><li><strong>Model Name</strong>: use the following URL to determine your chosen model name: <code>http://local_server:11434/api/tags</code> - the minimum model required is <a href="https://ollama.com/library/llama3.3:70b-instruct-q8_0">llama3.3:70b-instruct-q8_0</a>.</li></ul> |
| Vertex AI | <ul><li><strong>Model Name</strong>: this will take the form <code><model_name></code> - CAST highly recommends the <a href="https://deepmind.google/technologies/gemini/pro/">Gemini-1.5 Pro</a> model.</li><li><strong>Location</strong>: usually this will be set to <code>us-central1</code> but this may depend on your Google deployment location.</li><li><strong>Service Account Details</strong>: this is your IAM/Admin service account key exported to <code>.json</code> format.</li></ul> |

{{% alert color="info" %}}An internet connection is required when inputting the configuration details for third-party cloud providers.{{% /alert %}}

## Enabling AI features

When a valid API key or configuration data is defined, the various AI feature toggles will become available. Use the check boxes to enable the features you require:

![](toggles.jpg)

| Option | Description |
|---|---|
| **Explain code with AI** | Enables right click contextual menu options when consulting results and working at Object level - with options providing explanations directly in the source code:<br><br>![](explain_ai.jpg) |
| **Assistant chatbot** | Enables the **Ask me anything** feature within the **Imaging Assistant** located in the lower right corner when consulting results (note that the **Summarize with AI** option will remain disabled - it is enabled via the **AI Summary** option explained below):<br><br>![](chatbot.jpg) |
| **AI Summary** | Enables two features that can generate an AI driven explanation of items in your application, displayed in a [Post-It](../../../interface/viewer/right-panel/post-its/):<br><br><ul><li><strong>Summarize with AI</strong>: generates an AI-driven explanation of items in the <strong>Transaction</strong>, <strong>Data Call Graph</strong> and <strong>Module</strong> scopes, saving to a <strong>Post-It</strong> available via a tab in the <strong>Imaging Assistant</strong>:<br><br><img src="summarize_with_ai.jpg"><br><br></li><li><strong>Transaction Summary</strong>: generates an AI driven explanation of a given transaction, available via the <strong>Customize my results</strong> option. See also <a href="../../../explore-results/customize/transaction-summary/">Using the Transaction Summary feature</a>.<br><br><img src="transaction_summary.jpg"> |

{{% alert color="info" %}}You can find out more details about using AI in [Leverage AI services](../../../explore-results/ai/).{{% /alert %}}

## Ensure your source code is accessible

The AI-driven features are all based on the **source code** of objects in your application, therefore, CAST Imaging must be able to display source code. 

- For applications that have been fully analyzed with CAST Imaging, source code will be available by default in the [right panel](../../../interface/viewer/right-panel/source-code/). You do not need to do anything else.

- For applications whose results have been imported into CAST Imaging (i.e. the application has not been fully analyzed), you will need to ensure that a datasource is configured. See [Managing data source configuration](../applications/data-source/)

## Obtaining an API key

### OpenAI

- Browse to https://platform.openai.com/login?launch and click SIGN UP (if you do not have a OpenAI account) or LOG IN (If you have already signed up)
- Choose the type of key you require:

![](choose.jpg)

- Browse to your profile using the user menu in the top right:

![](profile.jpg)

- Click the **User API keys** tab and then select **View project API keys**:

![](profile2.jpg)

- Finally click **Create new secret key**:

![](create_api.jpg)

### Azure OpenAI

The following is required to obtain an Azure OpenAI API key:

- An Azure subscription - [Create one for free](https://azure.microsoft.com/free/cognitive-services?azure-portal=true).
- Access granted to Azure OpenAI in the desired Azure subscription. - Currently, access to this service is granted only by application. You can apply for access to Azure OpenAI by completing the form at https://aka.ms/oai/access.
- An Azure OpenAI Service resource with either the `gpt-35-turbo` or the `gpt-4` models deployed. For more information about model deployment, see the resource deployment guide.

See https://azure.microsoft.com/en-gb/products/ai-services/openai-service/ for more information.