---
title: "Settings"
linkTitle: "Settings"
type: "docs"
no_list: false
weight: 10
---

***

The **Settings** panel is available to users that have the `Admin` profile, or a custom profile with the `Administrator` role (see [User Permissions](user-permissions/)). Available settings include:

- [Extensions Strategy](extensions-strategy)
- [User Permissions](user-permissions)
- [Application Management](applications)
- etc.

Access the panel using the icon in the top right corner:

![](settings.jpg)
