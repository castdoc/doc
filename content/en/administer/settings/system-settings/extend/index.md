---
title: "CAST Extend Settings"
linkTitle: "CAST Extend Settings"
type: "docs"
no_list: true
weight: 20
---

## Overview

The CAST Extend Settings control the integration with [CAST Extend](https://extend.castsoftware.com). CAST Extend is CAST's own repository of extensions for use during analyses and access to this for managing your extension is provided in the UI. When a fresh installation of CAST Imaging is initialized, CAST Extend settings are referenced: these settings are displayed in this panel.

You can therefore use the panel to update the settings for CAST Extend, i.e. if you want to change the user name, or if you want to start using [Extend local Server](../../../../install/extend-local/).

## Settings

![](extend.jpg)

### Extend website

This mode configures each node to connect to [https://extend.castsoftware.com](CAST's publicly available extension server) over the internet on `port 443` via `TCP`. The `CAST Extend URL` field will be automatically populated with the https://extend.castsoftware.com in read-only mode (i.e. the URL cannot be changed). Your `CAST Extend API key` as input during the installation process (this can be generated in the CAST Extend UI) will also be displayed.

{{% alert color="info" %}}If you do not have a CAST Extend account, you can register, for free, using the following URL: [https://extend.castsoftware.com/#/register](https://extend.castsoftware.com/#/register).{{% /alert %}}

### Extend Local Server

This mode configures each node to connect to an on-premises deployment of [Extend Local Server](../../../../install/extend-local/), therefore avoiding a connection to CAST's publicly available extension server over the internet (in other words this option is aimed at those who cannot access internet resources from the CAST Imaging installation):

![](info.jpg)

#### API key

The API Key will have been generated during the installation of `Extend Local Server` and is displayed in the final summary screen of the installer on Microsoft Windows:

![](local_api_key.jpg)

Or by running `docker logs cast_extend_proxy` on Linux via Docker:

![](logs.jpg)

Alternatively you can find the API key (on the line `APIKEY`) in the following locations on the machine on which Extend Local Server is installed:

```text
Microsoft Windows: %PROGRAMDATA%\CAST\Extend\config.proxy.json
Linux via Docker: /shared/extend_data/config.proxy.json
```

#### Synchronize Extensions

![](sync_extensions.jpg)

This button is displayed when the connection settings are input and saved. This option will force Extend Local Server (when running in `online mode`) to fetch the extension "manifest" - i.e. information that CAST Imaging requires in order to request the correct extension:

- available extensions
- available releases
- technology to extension mappings

This process is also automated (every two hours).

###  Allow CAST to automatically collect anonymous statistical data

In order to provide an estimate of the time required to fully analyze a given application version, CAST needs to collect anonymous data relating to the volume of files to be analyzed, the performance/specification of the analysis server and the actual time taken to perform analyses and generate snapshots. The data collected will be stored by CAST on its CAST Extend server and will not contain any information that would allow the identification of any customer related information or application. The data stored by CAST is purely statistical and is related only to the size and duration of analyses and snapshot generation (an example of the data collected by CAST is available for download in .json format [here](example.json)). The richer the database of information we hold, the more accurate the estimation will be.

{{% alert color="warning" %}}By default, this option is **enabled** in the initial start-up wizard – in other words you are automatically opted-in and can disable the option at any time in this panel.{{% /alert %}}