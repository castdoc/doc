---
title: "CAST Highlight Settings"
linkTitle: "CAST Highlight Settings"
type: "docs"
no_list: true
weight: 10
---

## Overview

The CAST Highlight Settings are provided for customers that are using the [com.castsoftware.highlight2mri](../../../../technologies/multi/com.castsoftware.highlight2mri/) extension to inject CAST Highlight results as properties of objects generated during an application analysis. By linking directly to your CAST Highlight account, your CAST Highlight SCA vulnerabilities can be downloaded direct and injected into the analysis results, which is then reused in CAST Imaging. You can find out more information about when to fill in these fields in the documentation linked above.

> - Using the integration with CAST Highlight via the [com.castsoftware.highlight2mri](../../../../technologies/multi/com.castsoftware.highlight2mri/) extension requires a live internet connection to the URL defined in the Highlight URL field.
> - The CAST Highlight data is made available in CAST Imaging Viewer.
> - It is NOT necessary to configure these settings if you are using the [com.castsoftware.highlight2mri](../../../../technologies/multi/com.castsoftware.highlight2mri/) extension to generate CloudReady information for consumption in CAST Imaging Viewer.

## Settings

![](highlight.jpg)

###  Highlight URL

The URL to your CAST Highlight instance, either public (https://rpa.casthighlight.com) or private.

### Company ID / Client ID

These entries can be found in your CAST Highlight user profile. Note that `Client ID` is optional, while `Company ID` is required. If you do not supply the `Client ID` value, then CAST Imaging is not able to know when the supplied CAST Highlight token expires and will therefore not generate a system alert about this (i.e. the token could expire and CAST Imaging will not know).

### Token

The `Highlight Token` - this can be also be found in your CAST Highlight user profile but must be manually enabled by CAST Highlight Support first. Note that a token is valid for a finite duration and will expire. When it has expired and if you supply the `Client ID` value (see above), a system alert is generated to warn you.

### Create application 

Enabled by default and will automatically create the corresponding application in CAST Highlight if it does not already exist there.

### Save

Saves the entries that you have added (a check is actioned to ensure that the entries are valid). In addition, saving the entries will ensure that the [com.castsoftware.highlight2mri](../../../../technologies/multi/com.castsoftware.highlight2mri/) extension is automatically added to the [Force Install](../../extensions-strategy/force-install) list - in other words the extension will be automatically installed in all existing applications when the next analysis is run and all new applications by default.