---
title: "Proxy Settings"
linkTitle: "Proxy Settings"
type: "docs"
no_list: true
weight: 30
---

## Overview

Proxy settings allow you to configure CAST Imaging (and all nodes) to interact with your organization's internal proxy server (such as (Squid)[https://www.squid-cache.org/]). When a fresh installation of CAST Imaging is initialized, proxy settings are referenced: these settings are displayed in this panel.

You can therefore use the panel to update the proxy settings, or if you want to completely disable the use of a proxy.

### Settings

![](proxy.jpg)

{{% alert color="info"%}}<ul><li>When the proxy option is changed, CAST Imaging will attempt to connect to CAST Extend or CAST Extend local server - if it cannot connect, then an error will occur and it will not be possible to change the option.</li><li>If your proxy system intercepts <strong>HTTPS</strong> connections, you may need to ensure that the HTTPS certificate for your proxy system is imported into the trusted <code>cacerts</code> Java keystore on each Node machine. This can be actioned with the Java <code>keytool</code> (supplied with your Java installation on the node), for example using the following command:<br><br><code>keytool -importcert -trustcacerts -file [path_to_cert_file] -alias [alias] -keystore %JAVA_HOME%\lib\security\cacerts</code></li></ul>{{% /alert %}}

### No proxy

Default setting. No proxy required.

### Manual proxy configuration

Configure these settings if you need CAST Imaging to interact with your on-premises proxy server:

#### Host / Port / Credentials

Enter the IP address/fully qualified domain name, port number (where applicable) and credentials of the proxy server you need to use.

#### Excluded Address

The value of this property is a list of hosts, separated by the semicolon `;`  character. In addition, the wildcard character `*` can be used for pattern matching. For example `*.foo.com;localhost` will indicate that every host in the `foo.com` domain and `localhost` should be accessed directly even if a proxy server is configured.

CAST highly recommends excluding the following URLs when you are using the Manual Proxy option:

- <span style="color:DeepPink">CAST Dashboard URL defined in Dashboard Integration</span>
- If your nodes are configured to use [CAST Extend local server](../extend/) and the nodes are also configured to pass all outgoing connections through a proxy, then you may need to also whitelist the IP address/fully qualified domain name of the machine on which [CAST Extend local server](../extend/) is installed in order to route connections correctly.