---
title: "User Permissions - an example scenario"
linkTitle: "Example scenario"
type: "docs"
no_list: true
weight: 10
---

***

## Overview

This page provides an explanation of how to manage user permissions in a complex (but more realistic) deployment scenario. In this example we have a large team of users who all need to use CAST Imaging in different ways:

- some only need to onboard and analyze applications
- some only need to consume the results for a limited set of applications
- some need to perform both activities

## Scenario

In our hypothetical example, the set of applications are organized into the following [domains](../../applications/domains/):

- DOM-A
  - HR Management
  - HR - B
- DOM-B
  - DeclareExpress
  - SoldMonster
  - Pet Clinic
- DOM-C
  - AdviseMe
  - Recitax
  - CreateShop 

### User activity

#### Operators team

A first team of users (aka the **"Operators"**) is set up to produce information on all the applications and provide it to the other users (aka the **"Consumers"**). They will need to analyze the source code and tune the analysis engine. They will also need to review the results, within the Viewer and the Dashboards, to validate the analyses.

#### Consumer teams

Two **"Consumers"** teams (Group-A and Group-B) are authorized to access the information produced by the **"Operators"**. They need to access both the Viewer and the Dashboards but they do not need to perform analyses. Moreover, they should only be authorized to access a limited list of applications.

#### App to App team

Lastly, a specific team exists that only needs to access to the **App to App Dependencies view** to study how the various applications interact and to prepare the project plans.

## Defining the user permissions via custom profiles

In addition to the default `ADMIN` profile role, specific custom profiles will need to be created so that the permissions outlined previously can be put into action.

Each of the required custom profiles is described in the table below: 

| Custom user profile name | Description | Assigned role(s) | Assigned Applications/Domains |
|---|---|---|---|
| Operators | With this profile, users can analyze existing applications belonging to domains DOM-A, DOM-B, and DOM-C.<br><br>To ensure that they can also create new applications, you must also associate default profile called `Application Creator` to the user(s)/group(s). | <ul><li>Application Owner</li></ul> | <ul><li>Any new applications that are onboarded or imported.</li><li>Domains:<ul><li>DOM-A</li><li>DOM-B</li><li>DOM-C</li></ul></li></ul>I.e. all applications and domains. |
| Group-A | Users with this profile can only view results for applications belonging to the domain DOM-A, within the Viewer and the Dashboards. | <ul><li>Exclusion Manager</li><li>Quality Automation Manager</li><li>App-to-App Dependencies</li><li>Custom Aggregation Access</li><li>Services Views Access</li><li>Source Code Access</li></ul> | Domain: DOM-A |
| Group-B | Users with this profile can only view results for applications belonging to the domain DOM-B, within the Viewer and the Dashboards. | <ul><li>Exclusion Manager</li><li>Quality Automation Manager</li><li>App-to-App Dependencies</li><li>Custom Aggregation Access</li><li>Services Views Access</li><li>Source Code Access</li></ul> | Domain DOM-B |
| App-Interaction | Users with this profile can only access the "App to App Dependencies view" to study the interactions between all the applications that have been added to the system. | <ul><li>App-To-App Dependencies</li></ul>Note that this profile corresponds to the default profile "APP TO APP OBSERVER" which can be used instead. | All applications |

Screenshots showing this configuration can be seen below:

![](permissions1.png)

![](permissions2.png)

## Illustration

### Operators team

Members of the "operators" team:

- can perform almost all avalailable actions, except those reserved for administrators.
- can access all existing applications and add new:

![](operators1.png)

- are focused on performing application analyses.
- can view results, to validate the analyses.
- can access the Engineering dashboard and the Management dashboard.
 
![](operators2.png)

- can see all the applications in the "App to App Dependencies view".

![](operators3.png)

### Consumers teams

Members of the two "consumers" teams:

- can only view results for the applications they are authorized to access.
- can only access the applications in the specific domains they have been assigned to: DOM-A (Group-A) and DOM-B (Group-B): they can consume information via the Viewer and the Dashboards:

![](consumers1.png)

- can only see the applications they are authorized to access in the "App to App Dependencies view":

![](consumers3.png)

- can only interact with the applications they are authorized to access in the "Architecture Studio":

![](consumers2.png)

### App-Interaction team

This specific team is only authorized to view the dependencies existing between the applications in the system. They cannot drill down into any application:

![](interaction1.png)