---
title: "User Permissions"
linkTitle: "User Permissions"
type: "docs"
no_list: true
weight: 30
---

***

## Overview

CAST Imaging uses a system of roles and profiles to assign access and action permissions to individual users or groups of users. Roles and applications/domains are assigned to specific profiles and then profiles are assigned to users or groups.

Permissions granted via a role allow the user to perform specific actions and access specific data within CAST Imaging, either for one application or domain, or multiple applications/domains.

{{% alert title="Note" color="info" %}}<ul><li>A user must be assigned a profile (either directly or via a group) before using CAST Imaging - login is not possible otherwise.</li><li>All predefined profiles (except <code>Admin</code> which has access to all applications/domains) do not, by default, have access to any applications or domains, therefore you will need to grant access either to all applications/domains or specific applications/domains before users will be able to view any data.</li><li>You can view an example configuration in <a href="example/">User Permissions - an example scenario</a></li></ul>{{% /alert %}}

## What default predefined profiles are available and what roles do they include?

Default profiles are managed in the `Profiles` tab where you can also define your own custom profiles. Default predefined profiles cannot be removed and are flagged with a "padlock":

![](profiles.jpg)

| Profile Name  | Roles included |
|---|---|
| ADMIN | `Administrator`<br><br> This profile is granted to the default local user called "admin" that is available out of the box. It implicitly also includes all available roles. |
| APP TO APP OBSERVER | Access to the **App to App Dependencies view** at application level when consulting analysis results (no other area of CAST Imaging will be available) with the following roles:<br><br>`App-To-App Dependencies`<br><br>Access is granted to all applications automatically. |
| APPLICATION CREATOR | Access to onboarding (i.e. allows new applications to be added or imported), analysis and configuration features with the following roles:<br><br>- `Application Owner`<br><br>This implicitly includes all of the following roles:<br><br>- `Exclusion Manager`<br>- `Quality Automation Manager`<br>- `Quality Manager`<br>- `App-To-App Dependencies` <br>- `Manage Bulk Import`<br>- `Manage Custom Object Types`<br>- `Manage Level 5 View`<br>- `Custom Aggregation Access`<br>- `Services Views Access`<br>- `Source Code Access`<br>- `Generate Automated Modules`<br><br>Users with this profile will have the `Application Owner` role over their own applications.<br><br>Specific Applications/Domains cannot be assigned to this profile. |
| APPLICATION ARCHITECT | Access to result consumption features in CAST Imaging Viewer and Dashboards with the following roles:<br><br>- `Exclusion Manager`<br>- `Quality Automation Manager`<br>- `Quality Manager`<br>- `App-To-App Dependencies`<br>- `Manage Bulk Import`<br>- `Manage Custom Object Types`<br>- `Manage Level 5 View`<br>- `Custom Aggregation Access`<br>- `Services Views Access`<br>- `Source Code Access`<br>- `Transaction Summary`<br>- `Generate Automated Modules`<br><br>Specific Applications/Domains can be assigned to this profile. |
| APPLICATION USER | Access to result consumption features in CAST Imaging Viewer and Dashboards with the following roles:<br><br>- `Exclusion Manager`<br>- `Quality Automation Manager`<br>- `Quality Manager`<br>- `Access App-To-App Dependencies`<br>- `Manage Custom Object Types`<br>- `Custom Aggregation Access`<br>- `Services Views Access`<br>- `Source Code Access`<br><br>Specific Applications/Domains can be assigned to this profile. |
| APPLICATION GUEST | Access to result consumption features in CAST Imaging Viewer and Dashboards with the following roles:<br><br>- `Viewer Read Only`<br><br>Specific Applications/Domains can be assigned to this profile. |

## What default roles are available and what permissions do they grant?

| Role Name | Permissions granted  |
|---|---|
| `Administrator` | Grants access to all admin resources and any aspect of any application, including results in CAST Imaging Viewer and CAST Dashboards.<br><br>Cannot be combined with another role in the same profile and applies to all applications. |
| `Application Owner` | Grants access to application analysis, configuration and consumption in CAST Imaging Viewer.<br><br>Always has permission for the owned application, therefore if a user is assigned two different profiles on the same application and one profile contains a Viewer Read Only role, this will not impact the application where the user has the Application Owner role.<br><br>Does not, on its own, grant the right to add or import applications, to do so the `All Applications/domains` permission must also be granted in the profile.<br><br>Cannot be combined with another role in the same profile and applies to all applications.|
| `Viewer Read Only` | Grants permission to view analysis results in CAST Dashboards and CAST Imaging Viewer without any edit permissions on any features.<br><br>Cannot be combined with another role in the same profile.<br><br>Cannot be combined with another role in the same profile and applies to all applications.|
| `App-To-App Dependencies` | Grants access to App to App Dependencies View in CAST Imaging Viewer. |
| `Custom Aggregation Access` | Grants permission to create custom aggregation views in CAST Imaging Viewer. |
| `Cypher Search Access` | Grants permission to run cypher queries in CAST Imaging Viewer. |
| `Exclusion Manager` | Grants permission to add and remove objects from the Exclusion List. |
| `Generate Automated Module` | Grants permission to access the Module Assistant tab under "Customize your results" and run the "Generate modules" action. |
| `Manage Bulk Import` | Grants permissions to bulk import tags and annotations for use in CAST Imaging Viewer. |
| `Manage Custom Object Types` | Grants permissions to create custom Object Types in Level 5 in CAST Imaging Viewer. |
| `Manage Level 5 View` | Grants permission to to hide nodes in Level 5 in CAST Imaging Viewer. |
| `Quality Automation Manager` | Grants permission to add and remove objects from the Education list in the CAST Engineering Dashboard. |
| `Quality Manager` | Grants permission to add and remove objects from the Action Plan and to use the Action Plan Recommendation features in the CAST Engineering Dashboard. |
| `Services Views Access` | Grants permission to access "Aggregated by" services view and services if applicable for an application in CAST Imaging Viewer. |
| `Source Code Access` | Grants permission to view source code in CAST Imaging Viewer and CAST Dashboards. |
| `Transaction Summary` | Grants permission to view the AI Transaction summary feature in CAST Imaging Viewer. |

## How do I assign permissions to my users or groups or users?

Permissions are assigned to users or groups of users via a profile using the `Users` tab. You can assign any of the default profiles, or you can create your own custom profiles using the `Profiles` tab:

![](users.jpg)

{{% alert title="Note" color="info" %}}Multiple profiles can be assigned to a user or group: in this situation, the most permissive roles within each profile will take priority.{{% /alert %}}

## How do I create a new custom profile?

Use the `Profiles` tab > `Add New Profile` option:

![](add_new_profile.jpg)

Then assign your required roles and/or applications/domains to your new profile:

![](add_new_profile1.jpg)

{{% alert title="Note" color="info" %}}If you assign the `Administrator`, `Application Owner` or `Viewer Read Only` roles, no other roles can be assigned:<br><br>![](add_new_profile3.jpg){{% /alert %}}

## How do the "Applications/Domains" options work?

Within a profile (custom or predefined) you can restrict access based on applications and/or domains. By default only the `Admin` profile has access to all applications and all domains, therefore you must explicitly grant access using this method otherwise users will not be able to view any data when using the predefined profiles.

![](restrict-app-domain.jpg)

![](restrict-app-domain1.jpg)

## Example scenarios

### Create a custom profile that allows applications to be added or imported

- Grant the `Application Owner` role
- Grant the `All applications/domains` permission

![](custom_app_creator.jpg)
