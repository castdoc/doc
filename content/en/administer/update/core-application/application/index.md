---
title: "In-place application update"
linkTitle: "Application"
type: "docs"
no_list: true
weight: 20
---

***

## Overview

When you have updated CAST Imaging Core on your analysis node(s) (see [In-place core update](../core)) the next step is to perform an application update to the new release of CAST Imaging Core. This update process is performed in the CAST Imaging UI, using the [Applications](../../../settings/applications/) panel - see also [Update](../../../settings/applications/update/).

{{% alert color="info" %}}This step is not applicable to applications whose results were originally imported into CAST Imaging from CSV - i.e. where an analysis is not possible.{{% /alert %}}

## What actions does CAST Imaging perform during an application update?

- An automatic backup of the Application's Management, Analysis and Dashboard database schemas and its **Delivery** folder is performed automatically - see [Backups](../../../settings/applications/details/backups/) for details.
- An update of the **application schemas** and the **Delivery** folder to the new CAST Imaging Core release.

## What happens if the update fails?

If the update fails for any reason, an automatic roll back of the application update (schemas and delivery folder) will be performed using the backup taken automatically at the start of the update process.

## What about extensions?

As a general rule, any extensions installed during the most recent analysis will NOT be automatically updated, even if a more recent version of an extension is available on [CAST Extend](https://extend.castsoftware.com) or in [Extend Local Server](../../../../install/extend-local/).

## Update process

All actions listed below are performed in the UI of your CAST Imaging instance:

- Login to CAST Imaging with a user that has the default `Admin` profile or a custom profile with the `Administrator` role - see [User Permissions](../../../settings/user-permissions/).
- Determine the [Assessment Model upgrade strategy](../../../settings/global-configuration/assessment-model/upgrade/) - in the majority of scenarios leave the setting at the default **Preserve Assessment Model but enable new rules**.
- Browse to the [Applications](../../../settings/applications/) panel - the **Core upgrade available** column will indicate all applications that need to be updated (those whose results were originally imported into CAST Imaging from CSV will not be eligible). Perform the application update by selecting an application then clicking the icon highlighted below:

![](upgrade_action.jpg)

{{% alert color="info"%}}You can update multiple applications in one go by ticking all applications you want to update and then clicking the **Update** icon.{{% /alert %}}

- When the update is complete, check the **Core version** column to ensure that it is showing the new release:

![](update_check.jpg)

- Finally, run a new analysis on the freshly upgraded application - this will also generate and make available new results (structural flaws and views):

![](analysis1.jpg)

![](analysis2.jpg)

![](analysis3.jpg)

{{% alert color="info" %}}CAST recommends generating structural flaws and views so that your results are also updated. Using the **Run Analysis** button includes these actions.{{% /alert %}}
