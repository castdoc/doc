---
title: "In-place core and application update"
linkTitle: "Core and Application"
type: "docs"
no_list: true
---

***

## Overview

This guide provides instructions for running an in-place update to a new release of **CAST Imaging Core** ([com.castsoftware.imaging.core](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.core&version=latest)) for each Node and the subsequent update of your **applications** to this new Core release. Technically, this means and update of:

- **CAST Imaging Core** installed on each Node (or single Node for a single machine installation scenario)
- **Applications** managed by a Node where CAST Imaging Core has been updated to a new release:
  - corresponding result data **schemas** stored on a CAST Storage Service / PostgreSQL instance
  - associated **delivery** folder
  - associated **assessment model**

## Supported update paths

| Source | Target | Supported? | Notes |
|:-:|:-:|:-:|---|
| 8.4.0 | 8.4.1 | :white_check_mark: | - |

## Requirements

- Local administrator privileges:
  - **Microsoft Windows**: ability to `Run as administrator`
  - **Linux via Docker**: user in the `sudoers` list
- All existing services should be left running during the update.
- In a distributed installation scenario with multiple Nodes, it is not obligatory to update CAST Imaging Core on all Nodes within the deployment.

## What are the impacts of upgrading on existing result data?

Each CAST Imaging Core release and each new extension release provides new features and updates which improve the value of the platform and justify an upgrade. However, there are a number of changes or improvements which can impact your existing measurement results/grades:

- New or improved rules to perform deeper analysis
- Updates to the Assessment Model, e.g. changes to rule weights, severity or thresholds. This can be mitigated by using the "Preserve assessment model" option during the upgrade
- Improvements to the language analysis system, e.g. more fine-grained detection of objects or links
- Extended automatic discovery of files included in the analysis
- Bug fixes to improve the precision of results
- And, unfortunately, a new release may also introduce new bugs which may impact the results until they are discovered and removed

CAST highly recommends consulting the release notes for [CAST Imaging Core 8.4.x](../../../release-notes/com.castsoftware.imaging.core/) to determine any changes that have been introduced.

## Upgrade process

{{< cardpanehome >}}
{{% cardhomereference header="**Step 1**" %}}
<img class="no-border" src="com.castsoftware.aip.png">&nbsp;[Imaging Core](core/)
{{% /cardhomereference %}}
{{% cardhomereference header="**Step 2**" %}}
<i class="fa-solid fa-wallet"></i>&nbsp;[Application](application/)
{{% /cardhomereference %}}
{{< /cardpanehome >}}
