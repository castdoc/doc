---
title: "In-place core update"
linkTitle: "Core"
type: "docs"
no_list: true
weight: 10
---

*** 

## Overview

CAST regularly publishes new releases to CAST Imaging Core ([com.castsoftware.imaging.core](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.core&version=latest)) - the analysis engine used by your Nodes. These releases provide new features and updates which improve the value of the platform. In order to benefit from them, you will need to **install** the new release of CAST Imaging Core on your analysis node(s) to replace the previous releases. This process is described below.

## Microsoft Windows

The process is similar to a fresh installation, as described in [Install CAST Imaging Core for Microsoft Windows](../../../../install/global/windows/imaging-core/):

- On each node machine that you want to update, download and unzip the required release of [com.castsoftware.imaging.core](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.core&version=latest).
- Execute the `setup.bat` file with elevated permissions (right click, `Run as administrator`) at the root of the unzipped files and follow the interactive installer.
- The installer will detect the existing release installed on the machine - ensure that the **Upgrade** option is selected in the installer wizard:

![](upgrade.jpg)

When completed, **restart** the Microsoft Windows **CAST Imaging Analysis Node** service to ensure that the new release of CAST Imaging Core is taken into account:

![](restart.jpg)

{{% alert color="info" %}}In an update scenario, the installer will NOT install the .NET Framework even if it is not present on the target machine. In this scenario, it is the responsibility of the end-user to install the required .NET Framework.{{% /alert %}}

## Linux via Docker

CAST Imaging Core is provided as a Docker image which also includes the **CAST Imaging Analysis Node Service**. CAST Imaging Core therefore cannot be downloaded and installed manually as is the case for Microsoft Windows.

Instead, to update to a new release of CAST Imaging Core, you will need to perform an [in-place component update](../../component/) to a new release of CAST Imaging, for example 3.1 to 3.2, which will ensure that you receive the new CAST Imaging Core image.

The following table lists CAST Imaging releases and the release of CAST Imaging Core they include:

| CAST Imaging | CAST Imaging Core |
|:-:|:-:|
| 3.2.0-funcrel | 8.4.1 |
| 3.1.0-funcrel | 8.4.0 |
| 3.0.0-funcrel | 8.4.0 |

## How do I know the update has been successful?

Login to CAST Imaging with a user that has the default `Admin` profile or a custom profile with the `Administrator` role - see [User Permissions](../../../settings/user-permissions/). Browse to the [Services](../../../settings/services/) panel and locate the node you just updated. The CAST Imaging Core release installed on the node will be listed - check that this is what you expect:

![](success.jpg)

## What next?

When CAST Imaging Core has been updated on your analysis node(s) you must then perform an application update using the CAST Imaging UI, see [In-place application update](../application/).
