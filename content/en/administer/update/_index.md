---
title: "Update"
linkTitle: "Update"
type: "docs"
no_list: true
---

***

{{< cardpanehome >}}
{{% cardhomereference header="**Component update**" %}}
Instructions for performing an in-place update of your **existing installation of CAST Imaging** to a new release. See [In-place component update](component/).
{{% /cardhomereference %}}
{{% cardhomereference header="**Core and application update**" %}}
Instructions for performing an in-place update to a new release of **CAST Imaging Core** for each Node and the subsequent update of your **applications** to this new Core release. See [In-place core and application update](core-application/).
{{% /cardhomereference %}}
{{< /cardpanehome >}}
