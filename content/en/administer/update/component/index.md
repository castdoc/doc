---
title: "In-place component update"
linkTitle: "Component"
type: "docs"
no_list: true
---

***

## Overview

This guide provides instructions for running an in-place update to a new release for the following installed items:

- **Microsoft Windows** deployment of [com.castsoftware.imaging.console](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.console&version=latest): on one single machine or via a distributed deployment
- **Linux via Docker** deployment of [com.castsoftware.imaging.all.docker](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.all.docker&version=latest): on one single machine or via a distributed deployment

This process will update the following installed components and retain all existing data (applications, results, etc.):
- `imaging-services`
- `imaging-viewer`
- `analysis-node`

## Supported update paths

| Source | Target | Supported? | Notes |
|:-:|:-:|:-:|---|
| 3.1.0-funcrel | 3.2.0-funcrel | Partially | Supported for installation on **Microsoft Windows**. Note 3.2.0-funcrel is not currently available for Linux via Docker. |
| 3.0.0-funcrel | Any 3.x release | Partially | Supported for installation on **Microsoft Windows**. Not supported for installations on **Linux via Docker** - a fresh installation of the new release is required. |

## Requirements

- Local administrator privileges:
  - **Microsoft Windows**: ability to `Run as administrator`
  - **Linux via Docker**: user in the `sudoers` list
- For Microsoft Windows, all existing services should be left running during the update.
- For Linux for Docker, CAST recommends stopping the containers before pulling new images.
- In a distributed deployment scenario:
  - all components MUST be updated to the same new release (i.e. you cannot update just the `analysis-node` component or just the `imaging-services` component)
  - you can update components in any order i.e. the `analysis-node` component can be updated before `imaging-services` or `imaging-viewer` component
- It is not possible to change the TCP port number of any service, nor any `HOSTNAME_xxx` [installation variables](../../../install/global/windows/variables/) as part of the update process.

{{% alert color="info" %}}For Microsoft Windows, CAST recommends disabling **Quick Edit** mode in your command (CMD) window before running the update. This mode (if enabled) will pause the batch script if there is any interaction with the command window (e.g. a mouse click) during the process and will not re-start the script until a key is pressed:<br><br>![](quickedit.jpg){{% /alert %}}

## Microsoft Windows deployment

Download the latest release of the [installer](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.console&version=latest) and unzip it anywhere on your local disk. You will need to download/unzip the installation media on each machine where a component is to be updated. Locate the relevant `*.conf` file at the root of the unzipped files and open with a text editor. For example:

- **single machine installation**: edit the `config-all.conf` file.
- **distributed installation**: edit the `config-imaging-services.conf`, `config-imaging-viewer.conf` or `config-analysis-node.conf` file to match the component installed on the current machine

Modify the appropriate configuration file to ensure that **all parameters EXACTLY match those that were used in the same file when installing the previous release**. You can find an explanation of the parameters in [Microsoft Windows installation variables](../../../install/global/windows/variables/).

To run the in-place update, on the appropriate machine open a CMD window with elevated permissions (right click, `Run as administrator`) and execute the appropriate command from the root of the unzipped files, depending on the component(s) you are updating:

{{< tabpane text=false >}}
  {{< tab header="Single machine" >}}
  cast-imaging-update.bat package=all{{< /tab >}}
  {{< tab header="Distributed deployment" >}}
  cast-imaging-update.bat package=imaging-services
  cast-imaging-update.bat package=imaging-viewer
  cast-imaging-update.bat package=analysis-node{{< /tab >}}
{{< /tabpane >}}

The update process will then proceed. All existing data will be preserved. On completion you should check that:

- all Microsoft Windows services are up and running (the screenshot below is for a single machine installation):

![](../../../install/global/windows/windows_services.jpg)

- all services within the CAST Imaging [Services](../../settings/services/) "admin" panel are showing in green:

![](check_services.jpg)

## Linux via Docker deployment 

{{% alert color="info" %}}<ul><li>3.2.0-funcrel is not currently available for Linux via Docker.</li><li>Updates from 3.0.0-funcrel to 3.1.0-funcrel are <strong>not supported</strong>. The previous installation should be removed and a fresh installation of the new release should be run.</li>{{% /alert %}}

<!--
Download the latest release of the [installer](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.all.docker&version=latest) and unzip it anywhere on your local disk (see [Installation on Linux via Docker](../../../install/global/docker/) for more information about how to use `cUrl` to download the installer). You will need to download/unzip the installation media on each machine where a component is to be updated.

The update process differs for each component:

### imaging-viewer

First stop all `imaging-viewer` related containers by running the following command from the existing installation folder:

```bash
cd /home/CAST/imaging-viewer/
./imaging -s stop
```

Now run the following command from the newly unzipped installation media folder:

```bash
./imagingsetup -d [custom_path] -i update
```

Where:
- `-d` is the path on disk where the `imaging-viewer` data and configuration files are stored from the previous installation, typically this will be in `/home/CAST/imaging-viewer/`

This command will:

- install the configuration files from the new release to the same path where the files from the previous installation already exist - the installer will merge the existing and new files, preserving any customization that has been made
- pull brand new `imaging-viewer` images
- re-use the existing neo4j data
- restart the containers

Check the status of the various containers - there should be *four* in total specific to the `imaging-viewer` component (`server`, `open_ai_manager`, `etl`, `neo4j`):

```bash
docker ps
```

Finally check that the component is "functioning" in the UI using the [Services](../../settings/services/) panel:

![](check_ui_linux.jpg)


<!--
## `imaging-console`

Locate the `.env` file at the root of the unzipped files and open it in a text editor. Modify this to ensure that the following parameters EXACTLY match those that were used in the same file when installing the previous release:

```text
HOST_HOSTNAME=imaging.corp.domain.com
DELIVERY_FOLDER=\\shared\console\delivery
DEPLOY_FOLDER=\\shared\console\deploy
SHARED_FOLDER=\\shared\console\common-data
```
>If you modified any other parameters such as port numbers, these must also be changed to match your previous installation.

Now run the following command from the root of the unzipped files to pull the new CAST Imaging Console component images from Docker Hub and start the containers:

```text
docker-compose up -d
```
Check the status of the various services - there should be *six* in total:

```text
docker ps
```

A successful action will result in a functioning CAST Imaging Console running the chosen release. All existing data will be preserved.
-->
