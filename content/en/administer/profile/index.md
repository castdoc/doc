---
title: "Profile"
linkTitle: "Profile"
type: "docs"
no_list: true
weight: 15
---

***

## Overview

The **Profile** panel is available to all users. Available settings include:

- Define your name
- Define your email address to receive notifications
- Generate an API key for use with third party applications or during a migration from v2
  
Access the panel using the icon in the top right corner:

![](profile.jpg)

## Generating an API key

When third-party applications need to interact with the CAST Imaging (or you need to migrate from V2) you can generate an API key to access the required resources securely. Traditional user/password credentials are deemed a security risk and can also expire causing recurring problems when the resources are used for automation purposes. An API key solves these issues and CAST recommends using one wherever possible.

Note that:

- An API key is generated on a per-user basis
- Only one key can exist for each user
- When a new key is generated, the old one instantly becomes invalid
- The User Details section must be populated with information before the **Generate** button will become active

### How do I use an API key?

Ensure that you add an `X-API-KEY` attribute in the request header which contains your API key. For example when using `curl`:

```bash
curl -H "X-API-KEY: 06935MrA.G7LStH3Ck9sgeHBYokzIjTdv2DTbicsT" http://imaging.corp.domain.com/rest
```

## User Details

The values for all fields are automatically taken from CAST Imaging's [authentication](../../install/authentication) system, if they exist there. Synchronization occurs in both directions - i.e. a manual update in the CAST Imaging UI will update the same value in the authentication system and vice-versa:

*Authentication system:*

![](details1.jpg)

*CAST Imaging UI*

![](details2.jpg)

{{% alert color="info" %}}<ul><li>All three fields in this section must be populated in order to generate an <strong>API key</strong>.</li><li>If you are using <a href="../../install/authentication/ldap/">LDAP</a> or <a href="../../install/authentication/saml/">SAML</a> authentication, user details are taken directly from these systems (if they have been filled in). Therefore any manual updates made in the CAST Imaging UI may be <strong>overwritten</strong> when the LDAP/SAML user database is synchronized with the CAST Imaging authentication system.</li><li>Currently the <strong>I want to receive email notifications</strong> option does not function.</ul>{{% /alert %}}