---
title: "Configure a custom language display"
linkTitle: "Configure a custom language display"
type: "docs"
no_list: true
---

***

{{% alert color="info" %}}This option does NOT update language found in your results data - only labels, buttons and other items in the UI are displayed in the chosen language.{{% /alert %}}

## Overview

Out of the box, CAST Imaging offers two languages for the display of labels and other UI items via the **Preferred Language** option in the [Preferences](..) panel:

- English (en_US) - default
- Chinese (zh_CN)

Instructions below explain how to add your own custom language.

![](ch_example.jpg)

## Step 1 - Translate and rename default file

Locate the `en.json` file on the machine running the `imaging-viewer` component in your CAST Imaging installation:

(this file contains all the UI label text in English):

{{< tabpane text=false >}}
  {{< tab header="Microsoft Windows" >}}%PROGRAMFILES%\CAST\Imaging\CAST-Imaging-Viewer\nginx\html\locales\{{< /tab >}}
  {{< tab header="Linux via Docker" >}}Within the Docker container "server", in /opt/imaging/web/dist/locales/{{< /tab >}}
{{< /tabpane >}}

Copy this file somewhere else on your local machine so that you can modify it for your own custom language. Use an auto translate service such as https://translate.i18next.com/ which is designed to accept `.json` files as input.

Now rename the file to match your custom language, for example `fr.json` for French.

## Step 2 - Add your custom language to your install

Locate the `app-config.js` file in your CAST Imaging installation:

{{< tabpane text=false >}}
  {{< tab header="Microsoft Windows" >}}%PROGRAMFILES%\CAST\Imaging\CAST-Imaging-Viewer\nginx\html\{{< /tab >}}
  {{< tab header="Linux via Docker" >}}On the local disk: /home/CAST/imaging-viewer/server/nginx/html/{{< /tab >}}
{{< /tabpane >}}

Find the following line and add in your custom language (French in this example):

{{< tabpane text=false >}}
  {{< tab header="Microsoft Windows" >}}languages: [{ 'Chinese': 'ch' },{ 'French': 'fr' }], // Add new languages e.g., [{LanguageName: languageCode}, {'': '' }]{{< /tab >}}
  {{< tab header="Linux via Docker" >}}language: 'English_en','French_fr', // Add new languages e.g., 'LanguageName_languageCode'{{< /tab >}}
{{< /tabpane >}}

Where:
- `LanguageName` (e.g., 'French') is the name of the language as it will appear in the UI - this must match the locale used in the `languageCode` entry.
- `languageCode` (e.g., 'fr') represents the language. This must match the name of the corresponding `.json` file, e.g. `fr.json`.

## Step 3 - (Linux via Docker only) Add the required files to the installation and mount them

Create a new folder called `locales` in the following location and then copy the translated `.json` file (e.g. `fr.json`) into the newly created folder:

```text
/home/CAST/imaging-viewer/server/nginx/html/
```

Now edit the `docker-compose.yml` file located here:

```python
/home/CAST/imaging-viewer/
```

Find the `server` section in the file and add in the following under `volumes` where `lang.json` matches the translated `.json` file (e.g. `fr.json`):

```text
./server/nginx/html/locales/<lang.json>:/opt/imaging/web/dist/locales/<lang.json>
```

For example:

```yaml
  server:
    user: root
    container_name: server
    image: castimaging/viewer:${SERVER_IMAGE_TAG}
    ports:
    - ${SERVER_PORT:-9000}:9000
    - ${HTTP_PORT:-8083}:80
    - ${HTTPS_PORT:-443}:443
    networks:
    - imaging_system
    restart: always
    volumes:
    - ./server:/opt/imaging/config
    - ./logs:/opt/imaging/imaging-service/logs
    - ./neo4j/csv:/opt/imaging/imaging-service/upload
    - ./server/nginx/html/locales/fr.json:/opt/imaging/web/dist/locales/fr.json
```

## Step 4 - Enable the custom language in the UI

Finally, restart the following services/containers to ensure your changes are taken into account:

| Microsoft Windows service | Linux via Docker container |
|---|---|
| <ul><li>CAST Imaging Viewer Frontend</li></ul> | <ul><li>server</li></ul> |

You may also need to empty your browser cache before the new language option appears:

![](french.jpg)