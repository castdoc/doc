---
title: "Viewer preferences"
linkTitle: "Viewer preferences"
type: "docs"
no_list: true
weight: 20
---

***

## Overview

The **Preferences** panel is available to users that have [user profiles](../settings/user-permissions/) that allow result consumption since all options impact the exploration of results.  Available settings include:

- Results landing page behaviour
- Preferred UI language
- Preferred graph layout
- etc.

Access the panel using the icon in the top right corner:

![](preferences.jpg)

Each option is described below.

{{% alert color="info" %}}The options are per user and are persistent across user sessions.{{% /alert %}}

## Landing Page

Choose the preferred default application landing page:

- **Welcome page**: the default setting out of the box providing you with a dedicated "Welcome page" for your application showing details about the application (total number of objects, LOC value, number of transactions etc.) and provides various to load tailored views of the items in the application (e.g. : all Database Tables, all Modules etc.):

![](welcome.jpg)

- **Imaging menu**: this option will direct you to the application detail (i.e. default level 5 node display) whenever application results are accessed, therefore bypassing the Welcome page.

## Preferred Language
Select a system wide language for the the UI (specific to result consultation). The following language options are available:

- English (en_US) - default
- Chinese (zh_CN)

For example:

![](custom-lang/ch_example.jpg)

{{% alert color="info" %}}<ul><li>This option does NOT update language found in your results data - only labels, buttons and other items in the UI are displayed in the chosen language.</li><li>You can configure your own custom language display - see <a href="custom-lang/">Configure a custom language display</a>.</li></ul>{{% /alert %}}

## Preferred Language for "Explain with OpenAI"

Select the language in which you prefer to view the AI code explanation (see [Leverage AI services](../../explore-results/ai/)) The following language options are available:

- English - default
- Chinese
- Italian
- German
- French

## Level Drill Mode

This option governs how the double-click drill down action behaves for the Level views:

- **Children only (default)**: Only direct children of the selected node will be displayed at Objects level. Note that if child objects can be "grouped by communities" then this layout will always be used, i.e. the direct children of the selected node will be grouped together by community.
- **Children + caller/callee**: Direct children of the selected node and any objects calling or called by the selected node will be displayed at **Objects level**. Note that if the child objects and their caller/callees can be "grouped by communities" then this layout will always be used, i.e. the direct children of the selected node and their caller/callees will be grouped together by community.

## Preferred graph layout

Defines the preferred default graph layout mode all scopes and views. This option will be reflected in the view itself:

![](action_menu.jpg)

| Item | Description |
|---|---|
| `Automatic` | The default option for all scopes and views, but can be overridden:<ul><li>For most scopes the default arrangement will be `Sequential`.</li><li>`Force` will be used when drilling down on nodes or links from levels to Objects level.</li><li>Application to Application view will use `Force` layout by default.</li><li>Imaging Advisor and other flows from Welcome page use `Force`.</li><li>Saved views will maintain custom positioned nodes.</li><li>Any additional interactions: Focus, Expand All, Add Callers, and Add Callees, Linked Objects etc., will maintain the user-positioned nodes and apply layout only to newly added nodes. |
| `Sequential` | Positions nodes and objects in sequence horizontally:<br>![](sequential.jpg) |
| `Force` | Positions nodes and objects so that all the links are of more or less equal length and there are as few crossing links as possible, by assigning forces among the set of links and objects/nodes, based on their relative positions:<br>![](force.jpg) |
| `Hierarchical` | Positions nodes and objects in sequence vertically:<br>![](hierarchical.jpg) |
| `Grid` | Positions nodes and objects in a grid:<br>![](grid.jpg) |

## Object Limit / Edge Limit

A progressive rendering mechanism exists so that very large and complex displays can load in a satisfactory period of time. When accessing a node/object (double click) which contains more than 1000 child objects, or more than 2000 edges/links, the progressive rendering system is invoked in order to avoid situations where the rendering process locks the browser. Portions of the full graph are displayed progressively in steps and when this occurs, a **Stop Layout** message will be displayed allowing users to stop the progressive loading.

The option therefore allows you to modify the thresholds at which this progressive rendering system is invoked, if required. The browser cache may need to be emptied and reloaded in order to view changes to the thresholds.

## Notify objects count

Sets the threshold (default = 10000) at which a popup is displayed when:

- drilling down on a node in any view
- or switching to the Objects level

The popup warns that the resulting view contains a large number of objects - i.e. above the threshold - and that the view may take some time to fully load. Various options are then offered. For example:

![](notify1.jpg)

![](notify2.jpg)

- **Download** is displayed when clicking an object at Objects level, specifically for large Transactions and Data Call Graph items
- **Proceed** is displayed when clicking a node in Level 5.
- Alternatively you can click the **Search Objects** button to search for and display specific objects from the resulting view.

## Object Name Limit

Limits the length (in number of characters) of object names displayed in the view (default = 40). This is to improve the display in the view (long object names will crowd the view). Object names longer than 40 characters are trimmed using "...", to view the full name, hover the cursor over the object:

![](trimmed.png)

## Application walkthrough (animation)

Toggles the application walkthrough feature. When enabled (default position), an animation is displayed when switching between levels and down to objects level.

## Welcome Guide

Toggles the Welcome Guide feature. When enabled (default position), navigating from a tile in the Welcome page will automatically display an explanation of the selected tailored view in a pop-up:

![](guide.jpg)

## Objects export

Toggles the display of the **Download** button in the [global search](../../explore-results/search/global/) box allowing you to download to `.CSV` file all the matching search results which have a tick in the checkbox.

![](../../explore-results/search/global/download.jpg)

## Notify for Saving Views

Toggles the display of a popup when moving away from a view (i.e. logout/change application) to notify you that any changes you have made will be lost (default = enabled):

![](notify.jpg)

## Advanced Search Configurations

Set default persistent search options for the [global search](../../explore-results/search/global/):

![](search.jpg)

{{% alert color=info %}}These options are explained in [global search](../../explore-results/search/global/#what-about-other-search-options).{{% /alert %}}

## Download sample tutorial

Links directly to the CAST Extend entry for the sample tutorial application, allowing you to download this if necessary: https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.tutorial&version=latest.