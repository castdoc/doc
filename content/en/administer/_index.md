---
title: "Administer"
linkTitle: "Administer"
type: "docs"
no_list: false
---

***

Information about administering CAST Imaging once it is installed.