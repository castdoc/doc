---
title: "Database"
linkTitle: "Database"
type: "docs"
no_list: false
---

***

CAST Imaging [requires](../../requirements/db/) a CAST Storage Service PosgreSQL RDBMS to store both the data generated during analyses and persistence data (settings/properties etc.). This section provides some hints and tips about managing this.