---
title: "How can I replace my CSS/PostgreSQL instance?"
linkTitle: "Replace your CSS/PostgreSQL instance"
type: "docs"
no_list: true
---

***

## Overview

This document describes how to replace an existing CAST Storage Service(CSS)/PostgreSQL instance that has been associated and used with your CAST Imaging installation, with a new instance - for example, a situation where you want to retire an existing instance.

This process involves backing up all databases/schemas used by CAST Imaging and moving them to the new replacement instance and then updating your CAST Imaging installation to use the new instance.

{{% alert color="info" %}}This document assumes that the new replacement CAST Storage Service/PostgreSQL instance is already installed and functioning and that CAST Imaging is installed direct on Microsoft Windows.{{% /alert %}}

### Which databases/schemas are involved?

Not all databases/schemas may need moving - this depends on how you have deployed CAST Imaging. For example the instance you want to retire may only be used to store some schemas/databases, in which case you need to identify which items are involved, as listed below:

#### Databases

- `keycloak_v3`

#### Schemas

- application schemas (per application):
    - `<application_name>_local`
    - `<application_name>_central`
    - `<application_name>_mngt`
- CAST Health Dashboard schema:
    - `general_measure` (or other custom name)
- persistence data schemas
    - `admin_center`
    - `analysis_node`    

## Step 1 - Backup and restore your Application schemas

Use the `CombinedTransfer.bat` batch file provided with CAST Imaging Core on any of your nodes, located here:

```text
%PROGRAMFILES%\CAST\<release>\CSSAdmin\CSSUpgrade\
```

This tool is described in more detail [here](https://doc.castsoftware.com/export/STORAGE/Moving+existing+CAST+schemas#MovingexistingCASTschemas-Method1:usingtheCombinedTransfer.batfile).

{{% alert color="info" %}}This tool will only backup and restore application schemas (`<application_name>_local`,`<application_name>_central`,`<application_name>_mngt`).{{% /alert %}}

## Step 2 - Backup and restore other schemas

Use the `CSSBackup.exe` and `CSSRestore.exe` tools provided with CAST Imaging Core on any of your nodes, located here:

```text
%PROGRAMFILES%\CAST\<release>\CSSAdmin\
```

These are command line tools that can be used to backup and restore (to a different instance) any of the following schemas:

- `general_measure`
- `admin_center`
- `analysis_node` 

These tools are descibed in more detail [here](../cssadmin/).

## Step 3 - Backup and restore the keycloak_v3 database

Use PostgreSQL's freely available tools to perform the backup and restore of the `keycloak_v3` database:
- [pgAdmin](https://www.pgadmin.org/download/) UI tool. See https://www.pgadmin.org/docs/pgadmin4/latest/backup_and_restore.html for more information.
- `pg_dump` - see https://www.postgresql.org/docs/current/app-pgdump.html for more information.

## Step 4 - Stop all CAST Imaging services

Use the Microsoft Windows Services control panel to stop all CAST Imaging related services:

![Stop services](stop_services.jpg)

## Step 5 - Update the analysis_node schema

On the new CAST Storage Service/PostgreSQL instance, run the following query against the table `database_connection_settings` located in the `analysis_node` schema to update the host name of your CAST Storage Service/PostgreSQL instance (i.e swap the existing host name with the new host name) so that the node(s) are aware of where the application schemas are located. Update `new_host_name` to match the host name/IP address of the replacement CAST Storage Service/PostgreSQL instance:

```sql
UPDATE analysis_node.database_connection_settings
set host = 'new_host_name' where database_name='postgres';
```
{{% alert color="info" %}}This query assumes that your application schemas are stored in the `postgres` database.{{% /alert %}}

## Step 6 - Update references to the keycloak_v3 database

Locate the `keycloak.conf` file in `%PROGRAMFILES%\CAST\Imaging\CAST-Imaging-SSO\conf` and open with a text editor. Edit the following lines to match the details of the replacement CAST Storage Service/PostgreSQL instance:

```text
# The username of the database user.
db-username=operator

# The password of the database user.
db-password=CastAIP

# The full database JDBC URL. If not provided, a default URL is set based on the selected database vendor.
db-url=jdbc:postgresql://<host_name>:<port>/keycloak_v3
```

## Step 7 - Restart all CAST Imaging services

Use the Microsoft Windows Services control panel to restart all CAST Imaging related services.

## Step 8 - Add the new CAST Storage Service/PostgreSQL instance into CAST Imaging Console

Add your replacement CAST Storage Service/PostgreSQL instance to CAST Imaging Console via `Settings > Global Configurations > CSS and Measurement Settings`. Enable at a minimum the `CSS` option and also the `Measurement` option (if you are storing the `general_measure` on the replacement CAST Storage Service/PostgreSQL instance):

![Add new instance](add_db.jpg)

## Step 9 - Final check - run analysis

As a final check, run a new analysis on an application whose schemas you have moved from the old to the new CAST Storage Service/PostgreSQL instance:

![New analysis](new_scan.jpg)

## Step 10 - Remove existing CAST Storage Service/PostgreSQL instance

If the new analysis is successful, you can now safely remove the existing CAST Storage Service/PostgreSQL instance from CAST Imaging Console:

![Remove existing instance](remove_css.jpg)
