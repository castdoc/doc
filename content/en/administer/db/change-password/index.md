---
title: "How can I change the operator and guest passwords?"
linkTitle: "Change the operator and guest passwords"
type: "docs"
no_list: true
---

***

## Overview

When a CAST Storage Service or PostgreSQL instance is installed, two custom users/roles are available for use with CAST Imaging: `operator`/`CastAIP` and `guest`/`WelcomeToAIP`. An additional user `postgres` is provided out of the box in all situations.

If you would like to change the passwords for these users, there are various methods. CAST recommends either using **PgAdmin** (which is bundled with CAST Storage Service) or use a **SQL query**. Both are explained below.

## Step 1 - Change the password

### Using PgAdmin

Right click the user and choose **Properties**. Then click the **Definition** tab and enter the new password in the **Password** field. Click **Save** to action the changes.

![](pgadmin.jpg)

### Using a SQL query

Use the [ALTER USER](https://www.postgresql.org/docs/current/sql-alteruser.html) statement, where `username` is the name of the user and `password` is the new password:

```sql
ALTER USER username   
WITH PASSWORD 'password';
```

## Step 2 - Update CAST Imaging to use the new password

Information about the CAST Storage Service/PostgreSQL instances available to CAST Imaging (including the user credentials) is stored in the schema `analysis_node` in a table called `database_connection_properties`. This schema will be stored on the default CAST Storage Service defined during the CAST Imaging installation on **Microsoft Windows**, or in the PostgreSQL instance provided with CAST Imaging on **Linux via Docker**.

These stored user password will need updating after changing user password as described above in Step 1. To do so, CAST highly recommends that you use the CAST Imaging admin UI:

![](UI1.jpg)

![](UI2.jpg)

## Step 3 - Update standalone CAST Dashboards

If you are using standalone CAST Dashboards with CAST Imaging, you will need to update their configuration as well using the following file:

```text
WAR
%CATALINA_HOME%\webapps\<dashboard>\WEB-INF\classes\application.properties

Installed JAR
Microsoft Windows: %PROGRAMDATA%\CAST\Dashboards\<folder>\application.properties
Linux: /root/ProgramData/CAST/Dashboards
```

Change the <password> to match the new password on the following line (there may be multiple CAST Storage Service/PostgreSQL instances referenced):

```text
restapi.datasource[0].password=<password>
```

Restart the CAST Dashboard to ensure the change is taken into account.