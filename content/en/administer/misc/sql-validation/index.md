---
title: "Enabling SQL validation"
linkTitle: "Enabling SQL validation"
type: "docs"
no_list: true
---

***

{{% alert title="Note" color="info" %}}Valid only for Microsoft Windows deployments.{{% /alert %}}

## Overview

Out of the box SQL validation (to check for syntax errors) is disabled for the following features:

- **Creating query jobs** (see [Update Application schema](../../../interface/analysis-config/config/advanced/update-schema))
- **Creating Explicit Filter queries for User Defined Modules** (see [User Defined Modules](../../../interface/analysis-config/config/modules/udm))

Therefore, when you save the job or explicit filter, no check is made to ensure that the SQL is valid. This could result in a job failure when the job is run, if the SQL is not valid. If you need to enable SQL validation, you can do so via a configuration file.

## Step 1 - Edit the configuration file

Edit the following file in a text editor -  this file is available on EACH Node, therefore if you have more than one Node, you will need to make this change on all Nodes:

```text
%PROGRAMDATA%\CAST\Imaging\CAST-Imaging-Analysis-Node\application-default.yml
```

Paste the following lines in to the file (`sql` should be at the root) and ensure the `enabled` parameter is set to `true`:

```text
sql:
  validation:
    # this property to enable or disable the sql validation of query for modules explicit filter or update KB tool
    enabled: true
```

Save the file when you have completed the changes.

{{% alert title="Note" color="info" %}}This configuration is present by default in the `%PROGRAMDATA%\CAST\Imaging\CAST-Imaging-Analysis-Node\application-default.yml` file, however, you should always add configuration to the `application-default.yml` for customization purposes since it overrides the content of `application.yml` and the file is never overwritten during an update of the Node.{{% /alert %}}

## Step 2 - Apply configuration changes

Restart the Node Windows Service to ensure all changes are taken into account.
