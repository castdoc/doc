---
title: "Miscellaneous"
linkTitle: "Miscellaneous"
type: "docs"
no_list: false
---

***

This section provides instructions for enabling/disabling various items in the CAST Imaging that are not exposed in the UI.