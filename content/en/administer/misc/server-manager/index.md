---
title: "Using CAST-ServerManager.jar"
linkTitle: "Using CAST-ServerManager.jar"
type: "docs"
no_list: true
---

***

{{% alert title="Note" color="info" %}}Valid for both Microsoft Windows and Linux via Docker deployments.{{% /alert %}}

{{% alert title="Note" color="warning" %}}This tool requires advanced knowledge about CAST Imaging and should not be used unless instructed to by CAST Support.{{% /alert %}}

## Overview

**CAST-ServerManager.jar** is a command line tool provided with CAST Imaging Core 8.4.x ([com.castsoftware.imaging.core](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.core&version=latest)).

## What does it do?

The tool is primarily used by CAST Imaging to perform various database related activities, such as:

- creating/updating application schemas
- managing records of extensions used with applications

It can also be used in command line mode by end-users to perform the same type of actions. These actions are considered "advanced" and should only be attempted if you are certain you understand what you are doing, or you have been instructed to by CAST Support.

## Where can I find the tool?

| Operating System | Location |
|---|---|
| Microsoft Windows | `%PROGRAMFILES%\CAST\8.4\CAST-ServerManager.jar` |
| Linux via Docker | In the `analysis-node` container: `/home/carl/caip/CAST-ServerManager.jar` |

## How do I run the tool?

The tool is exclusively run via the command line, for example:

```text
java.exe -jar CAST-ServerManager.jar -action [options]
```

## Command line options

### Install

Installs new application schemas:

```text
java.exe -jar CAST-ServerManager.jar -action Install -url <ConnectionURL> -schema <Schema> -schemaKind "<SchemaKind>" -user <User> -password <Password> -logFilePath "<LogFilePath>"
```

Where:

- `-url` is a JDBC connection URL with the syntax `jdbc:postgresql://[Host]:[Port]/[Database]`:
  - `[Host]`: the FQDN or the IP address of the CAST Storage Service/PostgreSQL instance
  - `[Port]`: the CAST Storage Service/PostgreSQL instance port number (`2284` for CAST Storage Service 4.x)
  - `[Database]`: the database name (usually `postgres`)
- `-schema` is the name of the schema(s) to be created (depending on the value chosen for the `-schemaKind` parameter). This is a prefix which will be automatically converted to the full schema name by adding the following suffixes:
  - `_local`
  - `_mngt`
  - `_central`
  - `_measure`
- `-schemaKind` is one of the following:
  - `Combined` (default, if not set on the command line, installs Management Base, Knowledge Base and Central Base)
  - `Management Base` (single schema)
  - `Knowledge Base` (single schema)
  - `Central Base` (single schema)
  - `Measurement Base` (single schema)
- `-user` is the CAST Storage Service/PostgreSQL login (usually `operator`)
- `-password` is the CAST Storage Service/PostgreSQL password to match `-user`
- `-logFilePath` is a path to a text output file

#### Example

Install a "triplet" of application schemas called `application1_mngt`, `application1_central`, `application1_local`:

```text
java.exe -jar CAST-ServerManager.jar -action Install -url jdbc:postgresql://css.corp.company.com:2284/postgres -schema application1 -schemaKind "Combined" -user operator -password mypassword -logFilePath "C:\temp\log.log"
```

### Upgrade

Updates the target application schema(s) to a new release of CAST Imaging Core ([com.castsoftware.imaging.core](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.core&version=latest)):

```text
java.exe -jar CAST-ServerManager.jar -action Upgrade -url <ConnectionURL> -schema <Schema> -schemaKind "<SchemaKind>" -user <User> -password <Password> -logFilePath "<LogFilePath>" -assesmentModelMigrationMode "<MigrationMode>"
```

Where:

- `-schema` is the name of the schema(s) to update. If `-schemaKind` is set to `Combined` (or omitted entirely) then you must reference the name of the Management Base (`_mngt`), this will also update the associated `_local` and `_central` schemas.
- `-assesmentModelMigrationMode` is one of the following, see [Defining the Assessment Model Upgrade Strategy](../../settings/global-configuration/assessment-model/upgrade) for more information:
  - `Replace by new Assessment Model` (default, if not set on the command line)
  - `Activate new rules`
  - `Disable new rules`
  - `Leave unchanged`

#### Example

Update a "triplet" of application schemas called `application1_mngt`, `application1_central`, `application1_local` to the new release of CAST Imaging Core:

```text
java.exe -jar CAST-ServerManager.jar -action Upgrade -url jdbc:postgresql://css.corp.company.com:2284/postgres -schema application1_mngt -schemaKind "Combined" -user operator -password mypassword -logFilePath "C:\temp\log.log"
```

### Manage Extensions

Install, update or remove extensions associated to the target application schema(s). Extensions to be managed are specified in a `.txt` file:

```text
java.exe -jar CAST-ServerManager.jar -action "Manage Extensions" -url <ConnectionURL> -schema <Schema> -schemaKind "<SchemaKind>" -user <User> -password <Password> -logFilePath "<LogFilePath>" -extensions "<ExtensionFilePath>"
```

Where:

- `-schema` is the name of the schema(s) to work with. If `-schemaKind` is set to `Combined` (or omitted entirely) then you must reference the name of the Management Base (`_mngt`), this will also reference the associated `_local` and `_central` schemas.
- `-extensions` is a path to a text file containing a list of the extensions (using the extension ID) to manage. To update the extension to a new release, specify the target release number, and to remove an extension, use `remove`. Extensions that will remain unchanged do not need to be referenced. For example, removing two extensions and updating two to new releases:

```text
com.castsoftware.JEE-MavenHttp=remove
com.castsoftware.shell=remove
com.castsoftware.jquery=2.3.2-funcrel
com.castsoftware.automaticlinksvalidator=1.0.9-funcrel
```

#### Dependency Strategy

The default strategy to select the best version of a dependent extension listed in the target extension's `.nuspec` file, is to take the maximum available release that matches any release constraints.

It is possible to change the strategy in order to take the minimum available release by adding the `-minimalDependency` option to the command line.

#### Example

Update two extensions to new releases and remove two extensions for a "triplet" of application schemas called `application1_mngt`, `application1_central`, `application1_local`, where the `extensions.txt` file contains the same as above:

```text
java.exe -jar CAST-ServerManager.jar -action "Manage Extensions" -url jdbc:postgresql://css.corp.company.com:2284/postgres -schema application1_mngt -schemaKind "Combined" -user operator -password mypassword -logFilePath "C:\temp\log.log" -extensions "C:\temp\extensions.txt"
```

### Refresh

"Refreshes" the target application schema(s), i.e. it rebuilds the indexes, the stored procedures, and performs various repair operations on the existing data.

```text
java.exe -jar CAST-ServerManager.jar -action Refresh -url <ConnectionURL> -schema <Schema> -schemaKind "<SchemaKind>" -user <User> -password <Password> -logFilePath "<LogFilePath>"
```

Where:

- `-schema` is the name of the schema(s) to work with. If `-schemaKind` is set to `Combined` (or omitted entirely) then you must reference the name of the Management Base (`_mngt`), this will also reference the associated `_local` and `_central` schemas.

#### Example

Refresh a "triplet" of application schemas called `application1_mngt`, `application1_central`, `application1_local`:

```text
java.exe -jar CAST-ServerManager.jar -action Refresh -url jdbc:postgresql://css.corp.company.com:2284/postgres -schema application1_mngt -schemaKind "Combined" -user operator -password mypassword -logFilePath "C:\temp\log.log"
```

### Remove Locks

Removes the various locks that may be put on the schemas associated to an existing application (this unlock action is not available in the UI):

```text
java.exe -jar CAST-ServerManager.jar -action "Remove Locks" -url <ConnectionURL> -schema <Schema> -schemaKind "<SchemaKind>" -user <User> -password <Password> -logFilePath "<LogFilePath>"
```

Where:

- `-schema` is the name of the schema(s) to work with. If `-schemaKind` is set to `Combined` (or omitted entirely) then you must reference the name of the Management Base (`_mngt`), this will also reference the associated `_local` and `_central` schemas.

#### Example

Remove locks on a "triplet" of application schemas called `application1_mngt`, `application1_central`, `application1_local`:

```text
java.exe -jar CAST-ServerManager.jar -action "Remove Locks" -url jdbc:postgresql://css.corp.company.com:2284/postgres -schema application1_mngt -schemaKind "Combined" -user operator -password mypassword -logFilePath "C:\temp\log.log"
```

### Check Packages

Checks the consistency of the `.PCK` files available in the CAST Imaging Core distribution, optionally complemented with a list of extensions. It does not require to be connected to a triplet of schemas, nor a single schema:

```text
java.exe -jar CAST-ServerManager.jar -action "Check Packages" -logFilePath "<LogFilePath>" -extensions "<ExtensionFilePath>"
```

#### Example

```text
java.exe -jar CAST-ServerManager.jar -action "Check Packages" -logFilePath "C:\temp\log.log" -extensions "C:\temp\extensions.txt"
```

## Additional options

For all the above command lines, it is possible to add special options, to be used in specific situations:

- -`legacyPath "<LegacyDirectoryPath>"`: force the use of a specific CAST Imaging Core distribution.
- `-logtime-`: remove the date prefix on each log comment
- `-v`: verbose log mode

## Error codes

Error codes are different under Microsoft Windows and Linux, because they are stored as long integers under Microsoft Windows, while as single bytes under Linux.

| Windows Code | Linux Code | Name | Comment |
|:---:|:---:|---|---|
| 2000 | 208 | CONNECTIONERRORCODE | Unable to connect to the server |
| 3000 | 184 | ERROR_CODE_ACTION_PARSE_ERROR | Unable to parse the action |
| 3001 | 185 | ERROR_CODE_CREATE_SCHEMA | Unable to create a schema |
| 3002 | 186 | ERROR_CODE_RECONNECT | Unable to reconnect to the schema after schema(s) creation |
| 3003 | 187 | ERROR_CODE_COLLECT_EXTENSIONS | Unable to collect the extensions |
| 3004 | 188 | ERROR_CODE_LOAD_PACKAGES | Unable to load the packages |
| 3005 | 189 | ERROR_CODE_COLLECT_SCHEMAS | Unable to collect the schemas to modify |
| 3006 | 190 | ERROR_CODE_MODIFY_SCHEMAS | Unable to modify the collected schemas |
| 3007 | 191 | ERROR_CODE_SET_LOCKS | Unable to set the locks |
| 3008 | 192 | ERROR_CODE_REMOVE_LOCKS | Unable to remove the locks |
| 3009 | 193 | ERROR_CODE_CHECK_PACKAGES | Some packages are invalid |
