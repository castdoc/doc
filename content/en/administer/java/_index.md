---
title: "Java"
linkTitle: "Java"
type: "docs"
no_list: false
---

***

A Java JDK is [required](../../requirements/software/) on Microsoft Windows machines where CAST Imaging is installed. This section provides some hints and tips about managing this.