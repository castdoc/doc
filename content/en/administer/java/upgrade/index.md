---
title: "Post Java JDK upgrade steps"
linkTitle: "Post Java JDK upgrade steps"
type: "docs"
no_list: true
---

***

{{% alert title="Note" color="info" %}}Valid only for Microsoft Windows deployments.{{% /alert %}}

## Overview

After upgrading the Java JDK (for example to a new service pack or even a new minor or major release) installed on your machine or machines used by CAST Imaging, there are some additional steps required, specifically for your CAST Imaging installation. These steps are described below.

## Update your %JAVA_HOME% system environment variable

Ensure that your `%JAVA_HOME%` system environment variable points to the new location of your Java JDK installation by using the Microsoft Windows built in Environment Variable editor which can be accessed by searching for **"Edit the system environment variables"**:

![](env.jpg)

## Update CAST Imaging Microsoft Windows services using Java

The following Microsoft Windows services require Java to be present on the local machine and were installed with a hardcoded path to the Java JDK:

- **CAST Imaging Analysis Node** > this service may exist on more than one machine if you have installed CAST Imaging in "distributed" mode
- **CAST Imaging Console Service**
- **CAST Imaging Dashboard Service**

As such, these services will fail to start after an upgrade to your Java JDK and a reboot of your machine. To resolve this follow the steps below.

### Step 1 - Stop the impacted services

Use the Microsoft Windows services control panel to stop the impacted services:

![](stop.jpg)

### Step 2 - Uninstall the services using the batch files provided by CAST

Run the following batch files with elevated permissions (right click, `Run as administrator`):

```text
%PROGRAMFILES%\CAST\Imaging\CAST-Imaging-Analysis-Node\tools\aip-node-service-uninstall.bat
%PROGRAMFILES%\CAST\Imaging\CAST-Imaging-Console\tools\aip-gateway-service-uninstall.bat
%PROGRAMFILES%\CAST\Imaging\CAST-Imaging-Dashboards\dashboard-service-uninstall.bat
```

### Step 3 - Modify the service install batch files

Edit the following files to update the path to your upgraded Java JDK:

```text
%PROGRAMFILES%\CAST\Imaging\CAST-Imaging-Analysis-Node\tools\aip-node-service-install.bat
%PROGRAMFILES%\CAST\Imaging\CAST-Imaging-Console\tools\aip-gateway-service-install.bat
%PROGRAMFILES%\CAST\Imaging\CAST-Imaging-Dashboards\dashboard-service-install.bat
```

For example, for `%PROGRAMFILES%\CAST\Imaging\CAST-Imaging-Analysis-Node\tools\aip-node-service-install.bat` change the path on the following line:

![](path.jpg)

### Step 4 - Re-install the Microsoft Windows services

Run the following batch files with elevated permissions (right click, `Run as administrator`) to re-install the services with the correct Java path:

```text
%PROGRAMFILES%\CAST\Imaging\CAST-Imaging-Analysis-Node\tools\aip-node-service-install.bat
%PROGRAMFILES%\CAST\Imaging\CAST-Imaging-Console\tools\aip-gateway-service-install.bat
%PROGRAMFILES%\CAST\Imaging\CAST-Imaging-Dashboards\dashboard-service-install.bat
```

### Step 5 - Ensure the services are started

Use the Microsoft Windows services control panel to check that the impacted services are running - if they are not, start them up.
