---
title: "Changing Neo4j transaction log rotation period"
linkTitle: "Transaction log rotation"
type: "docs"
no_list: true
---

***

## Overview

[Neo4j](https://neo4j.com/product/neo4j-graph-database/) - the graph database system used to power the CAST Imaging results - keeps track of all write operations to each database to ensure data consistency and enable recovery. This information is stored in files known as transactional logs located here: 

```text
Microsoft Windows
%PROGRAMDATA%\CAST\Imaging\CAST-Imaging-Viewer\Neo4jData\transactions\neo4j

Linux via Docker:
/home/CAST/imaging-viewer/neo4j/neo4j5_data/transactions
```

Out of the box CAST Imaging is configured to store transactional logs as follows:

- Max size of the current transactional log file: `250MB` (when the file reaches this size it is archived and a new file is started)
- Max number of transactional log files retained on disk: `10` (when the total number of files reaches 10, the next new transactional log file created will cause the oldest archive to be deleted from disk).

This means that the total size of the transactional log files should not exceed `2.5GB`. However if you are facing disk space issues due to the size of the transaction log files, you can change the default behaviour as described below.

{{< alert color="info" >}}You can find out more about the transactional log files and their rotation settings in https://neo4j.com/docs/operations-manual/current/database-internals/transaction-logs/{{< /alert >}}

## Step 1 - Edit the neo4j.conf file

Locate the following file depending on your operating system:

{{< tabpane text=false >}}
  {{< tab header="Microsoft Windows" >}}%PROGRAMDATA\CAST\Imaging\CAST-Imaging-Viewer\setup-config\neo4j\neo4j.conf{{< /tab >}}
  {{< tab header="Linux via Docker" >}}/home/CAST/imaging-viewer/neo4j/configuration/neo4j.conf{{< /tab >}}
{{< /tabpane >}}

Edit the file, update the following entries as required by your environment and then save the file:

```bash
db.tx_log.rotation.retention_policy=10 files
db.tx_log.rotation.size=250M
```

## Step 2 - Run the update script (Microsoft Windows only)

Run the following command (from a CMD window with elevated permissions (right click, `Run as administrator`)) to ensure that the service is updated with the new configuration:

```text
%PROGRAMFILES%\CAST\Imaging\CAST-Imaging-Viewer\neo4j\bin neo4j windows-service update
```

## Step 3 - Restart services

Finally, restart the following services/containers to ensure your changes are taken into account:

| Microsoft Windows service | Linux via Docker container |
|---|---|
| <ul><li>CAST Imaging Viewer Neo4j Graph DB</li></ul> | <ul><li>neo4j</li></ul> |