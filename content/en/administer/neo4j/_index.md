---
title: "Working with Neo4j"
linkTitle: "Neo4j"
type: "docs"
no_list: false
---

***

This section provides instructions for configuring the default settings provided in [Neo4j](https://neo4j.com/product/neo4j-graph-database/) - the graph database system used to power the CAST Imaging results.
