---
title: "Optimizing Neo4j memory allocation"
linkTitle: "Memory optimization"
type: "docs"
no_list: true
---

***

## Overview

Out of the box [Neo4j](https://neo4j.com/product/neo4j-graph-database/) - the graph database system used to power the CAST Imaging results - is set to use **8GB of RAM memory** (JVM heap, initial and maximum).

This configuration is a value that is designed to work in most situations, however, it may not be enough for your use case. In general, a sure sign that you need to modify the amount of RAM memory allocated to Neo4j is when objects fail to load in the results - particularly if there are a large number of objects to display - this is because the query used to fetch the objects for display is not completing before a timeout occurs. This timeout will manifest itself as a 504 (gateway timeout) error in the browser. Allocating additional RAM to Neo4j can help reduce the time required to complete the query.

{{< alert color="info" >}}You can read more about Neo4j memory usage in [https://neo4j.com/docs/operations-manual/current/performance/memory-configuration](https://neo4j.com/docs/operations-manual/current/performance/memory-configuration).{{< /alert >}}

## Step 1 - Edit the neo4j.conf file

Locate the following file depending on your operating system:

{{< tabpane text=false >}}
  {{< tab header="Microsoft Windows" >}}%PROGRAMDATA\CAST\Imaging\CAST-Imaging-Viewer\setup-config\neo4j\neo4j.conf{{< /tab >}}
  {{< tab header="Linux via Docker" >}}/home/CAST/imaging-viewer/neo4j/configuration/neo4j.conf{{< /tab >}}
{{< /tabpane >}}

Edit the file, update the following entries as required by your environment and then save the file:

```bash
dbms.memory.heap.initial_size=8G
dbms.memory.heap.max_size=8G
```

## Step 2 - Run the update script (Microsoft Windows only)

Run the following command (from a CMD window with elevated permissions (right click, `Run as administrator`)) to ensure that the service is updated with the new configuration:

```text
%PROGRAMFILES%\CAST\Imaging\CAST-Imaging-Viewer\neo4j\bin neo4j windows-service update
```

## Step 3 - Restart services

Finally, restart the following services/containers to ensure your changes are taken into account:

| Microsoft Windows service | Linux via Docker container |
|---|---|
| <ul><li>CAST Imaging Viewer Neo4j Graph DB</li></ul> | <ul><li>neo4j</li></ul> |