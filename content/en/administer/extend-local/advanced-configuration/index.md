---
title: "Advanced configuration"
linkTitle: "Advanced configuration"
type: "docs"
no_list: true
weight: 30
---

***

## Overview

Extend Local server includes a text based configuration file that can be used to change some advanced configuration settings that are not exposed in the UI. This text file is located here:

```text
Microsoft Windows: %PROGRAMDATA%\CAST\Extend\config.proxy.json
Linux via Docker: /shared/extend_data/config.proxy.json
```

You will need to restart Extend Local Server to ensure any changes listed below are taken into account.

## Telemetry

When configuring CAST Imaging to use Extend Local Server an option called **Allow CAST to collect anonymous statistical data automatically** is available (see [CAST Extend settings](../../settings/system-settings/extend/)). This option allows you to choose whether to send anonymous statistical data to CAST:

![](anon.jpg)

If you want to block this data at Extend Local Server level you can do so by setting the `AIPC` > `ANALYSIS_REPORT` option in the `config.proxy.json` file to `false` (this will override the setting available in the UI):

```text
  "TELEMETRY": {
    "AIPC": {
      "ANALYSIS_REPORT": false
    },
    "EXTEND": {
      "DETERMINATOR": true
    }
  },
```
{{% alert color="warning" %}}You should leave the setting `EXTEND` > `DETERMINATOR` at `true` unless you are instructed to change this by CAST Support.{{% /alert %}}

## Extension icon synchronization

When extensions are displayed in the CAST Imaging UI (see [Extensions](../../../interface/analysis-config/extensions/)), ordinarily an icon will be used to distinguish the extension:

![](icons.jpg)

These icons are stored in a remote GitHub repository and are downloaded by Extend Local Server when the extension is synchronized. Therefore if your environment is heavily firewalled and you want to prevent the download of these files, then you can do so by setting the `SYNCHRONIZATION` > `ICONS` > `SKIP_DOWNLOAD` option in the `config.proxy.json` file to `true`:

```text
  "SYNCHRONIZATION": {
    "ICONS": {
      "SKIP_DOWNLOAD": true,
      "EXTENSIONS": {}
    },
    ...
```

You can alternatively prevent the download of icons for specific extensions using the `SYNCHRONIZATION` > `ICONS` > `EXTENSIONS` option (this will only function when `SKIP_DOWNLOAD` is set to `false`):

```text
  "SYNCHRONIZATION": {
    "ICONS": {
      "SKIP_DOWNLOAD": false,
      "EXTENSIONS": {
        "com.castsoftware.html5": true,
		"com.castsoftware.sqlanalyzer": true,
		"com.castsoftware.jee": true
      }
    },
    ...
```

## Extension synchronization blacklisting

If you want to blacklist the synchronization of a specific extension entirely (i.e. so it is never downloaded), use the `SYNCHRONIZATION` > `EXTENSIONS` option, adding the extension ID and setting it to `true`:

```text
  "SYNCHRONIZATION": {
    "ICONS": {
      "SKIP_DOWNLOAD": false,
      "EXTENSIONS": {}
    },
    "EXTENSIONS": {}
  }
```

For example:

```text
  "SYNCHRONIZATION": {
    "ICONS": {
      "SKIP_DOWNLOAD": false,
      "EXTENSIONS": {}
    },
    "EXTENSIONS": {
		"com.castsoftware.html5": true,
		"com.castsoftware.sqlanalyzer": true,
		"com.castsoftware.jee": true
    }
  },
```

## Hostname update

If the hostname of the server hosting the Extend Local Server is changed, then you should update the `PUBLIC_URL` entry to match the new hostname of the server hosting the service:

```text
{
  ...
  "PUBLIC_URL": "http://my_server:8085/",
  ...
}
```  

In addition, you will need to update the **CAST Extend URL** option with the new hostname, as configured in CAST Imaging:

![](hostname.jpg)
