---
title: "Update Extend Local Server to a new release"
linkTitle: "Update Extend Local Server"
type: "docs"
no_list: true
weight: 40
---

***

## Overview

Instructions for updating your installation of Extend Local Server to a new release on the same machine. The process involves:

- backing up any extensions already present/downloaded and configuration files
- uninstalling the existing instance
- downloading the new release
- installing the new release

{{% alert color="info" %}}If you want to install the new instance on a new machine, follow as below but perform steps 3 - 7 on the new machine.{{% /alert %}}

## Microsoft Windows

### Step 1 - Stop the existing instance

Use the Microsoft Windows Services control panel to stop the instance:

![](stop.jpg)

### Step 2 - Backup existing items

Make a copy of the following folder and files and save them to a different location. The folder contains all the extensions that you have already manually uploaded into Extend Local Server or have been downloaded automatically:

```text
%PROGRAMDATA%\CAST\Extend\packages
```

{{% alert color="info" %}}If you have modified the `config.proxy.json` file (for example as described in [Advanced configuration](../advanced-configuration/) or [Manifest synchronization](../synchronization/)) you will need to note down any changes you have made and re-apply them in the new instance. It is not possible to backup and restore this file to a new instance.{{% /alert %}}

### Step 3 - Uninstall the existing instance

Use the Microsoft Windows add/remove applications control panel to fully uninstall Extend Local Server.

### Step 4 - Download and install the new release

See the instructions in [Install Extend Local Server](../../../install/extend-local/). Ensure you complete the **Post install setup** step.

### Step 5 - Stop newly installed Extend Local Server

Use the Microsoft Windows Services control panel to stop the new instance.

### Step 6 - Copy the existing data to the new release

Copy the content of the items that you backed up in Step 2 into the following path, overwriting if prompted:

```text
%PROGRAMDATA%\CAST\Extend\packages
```

### Step 7 - Restart newly installed instance

Restart the new installation of Extend Local Server using the Microsoft Windows Services control panel.