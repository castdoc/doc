---
title: "Manifest synchronization"
linkTitle: "Manifest synchronization"
type: "docs"
no_list: true
weight: 20
---

***

## Overview

When Extend Local Server is configured in **"online"** mode (see [install Extend Local Server](../../../install/extend-local/)) i.e. with a connection to CAST's Extend ecosystem (https://extend.castsoftware.com), it will automatically fetch what is know as the "manifest" on a regular basis. This is information CAST Imaging requires in order to request the correct extension from Extend Local Server, for example:
    - available extensions
    - available releases
    - technology to extension mappings

In a default installation, synchronization is performed immediately after installation (once the CAST Extend user credentials have been input) and then automatically **every two hours** (at the time the server is first started), **Monday to Friday**.

It is also possible to perform a manual synchronization via the UI, or to change the synchronization interval should you need to. See below.

## How do I force a manual synchronization in the UI?

You can manually force a synchronization (for example a new extension has been released which is required immediately), from the Extend Local Server UI accessible using the following URL:

```text
http://<FQDN/ip_address>:<port>/ui/admin?<unique_guid>
```

The `<unique_guid>` can be found in the following files (the `ADMI` entry):

```text
Microsoft Windows: %PROGRAMDATA%\CAST\Extend\config.proxy.json
Linux via Docker: /shared/extend_data/config.proxy.json
```

When the button is pressed, the synchronize action starts (1):

![](synchro1.jpg)

Clicking the icon marked (2) above will show the progress of the synchronization process:

![](synchro2.jpg)

You can cancel an in-progress synchronization using the cancel button highlighted below (2). When a synchronization is cancelled, anything that has already been synchronized will remain, i.e. a rollback is not actioned, it is simply stopped.

![](synchro3.jpg)

## How do I change the default synchronization interval?

The synchronize action is performed by default automatically every two hours, Monday to Friday. If you would like to change this schedule (for example to include weekends or to reduce the frequency), locate the `SYNC_INTERVAL` entry in the `config.proxy.json` file, located here:

```text
Microsoft Windows: %PROGRAMDATA%\CAST\Extend\config.proxy.json
Linux via Docker: /shared/extend_data/config.proxy.json
```

`NULL` is the default schedule, equal to a synchronization every two hours, Monday - Friday. To modify it, use [cron tab syntax](https://www.adminschoice.com/crontab-quick-reference) within quote marks. See the examples below.

{{% alert color="info" %}}<ul><li>CAST does not recommend scheduling a synchronization at a more regular frequency than every two hours.</li><li>CAST does not usually perform manifest changes at weekends therefore there is no need to schedule synchronization on Saturdays and Sundays.</li><li>If a newly released extension is required immediately, use the manual <strong>Synchronization</strong> button.</li></ul>{{% /alert %}}


### Once per day at 0500 (5am), Monday - Friday

```text
{
    ...
    "SYNC_INTERVAL": "0 05 * * 1-5"
}
```

### Every 6 hours, on the hour, Monday - Friday

```text
{
    ...
    "SYNC_INTERVAL": 0 0-23/6 * * 1-5"
}
```

You will need to restart Extend Local Server in order for the changes to be applied.

## How do I manage synchronization failures?

A manifest synchronization may fail to fully synchronize at some point, perhpas due to transient network issues for example. By default Extend Local Server is configured to retry the synchronization automatically three times at intervals of 3, 5 and 10 seconds respectively. If this retry configuration is too restrictive, you can change it using the the `RETRY_POLICY` entry in the `config.proxy.json` file, located here:

```text
Microsoft Windows: %PROGRAMDATA%\CAST\Extend\config.proxy.json
Linux via Docker: /shared/extend_data/config.proxy.json
```

E.g.

```text
  "RETRY_POLICY": {
    "COUNT": 3,
    "INTERVAL": [
      3,
      5,
      10
    ]
  }
```

Where:

- `COUNT` is the number of retries that will be attempted
- `INTERVAL` is the interval in seconds between each retry attempt

You will need to restart Extend Local Server in order for the changes to be applied.