---
title: "Update Extend Local Server with new content"
linkTitle: "Update Extend Local Server with new content"
type: "docs"
no_list: true
weight: 10
---

***

## Overview

Extend Local Server is delivered "empty" without any preconfigured extensions and in **online mode** where a connection to CAST's publicly available "Extend" ecosystem (https://extend.castsoftware.com) is available, extensions will be automatically downloaded from CAST Extend when requested therefore there is nothing further to do. However, if you are using Extend Local Server in **offline mode** (i.e. in a secure environment without a connection to https://extend.castsoftware.com) you will need to populate Extend Local Server with specific extensions manually.

The process of manually populating Extend Local Server is as follows:

- Create a new bundle (an `.extarchive` file) containing the extensions you require using the [ExtendCli](extend-cli/) tool.
- Upload the bundle to update Extend Local Server either using the UI or using the API and curl.

When CAST Extend local server is updated with a new bundle of extensions and extensions have already been added, a "merge" is actioned, rather than a "delete and add". This means that no extensions are ever removed, i.e.:

- Any extensions that already exist in Extend Local Server but do not exist in the new bundle will remain as they are in Extend Local Server
- Any extensions that already exist in Extend Local Server at a given release number will remain as they are in Extend Local Server when:
    - an older release of the same extension is added to the bundle (both releases will be available for use)
    - a newer release of the same extension is added to the bundle (both releases will be available for use)
    - an older and a newer release of the same extension are added to the bundle (all three release will be available for use)
    - the same release of the extension is added to the bundle

When an `.extarchive` is uploaded, the packages are stored by default in:

```text
Microsoft Windows: %PROGRAMDATA%\CAST\Extend\packages
Linux via Docker: /shared/extend_data
```
## Update process

### Step 1 - Create the bundle (.extarchive file)

Creating the bundle (`.extarchive` file) involves using a command line tool called [ExtendCli](extend-cli/) which is available either as a standalone [download from CAST Extend](https://extend.castsoftware.com/#/extension?id=com.castsoftware.aip.extendcli&version=latest), or directly on a Node (it is bundled with the Node services). This tool can be run on any machine which has:

-  a connection to CAST Extend (https://extend.castsoftware.com)
-  network access to Extend Local Server

An API key for an account on CAST Extend is also required:

![](apikey.jpg)

The tool will download the required extensions and save them into a bundle (`.extarchive` file) ready for upload to Extend Local Server.

{{% alert color="info" %}}You can find out more about ExtendCli [here](extend-cli/).{{% /alert %}}

{{% alert color="warning" %}}Currently it is not possible to run ExtendCli on a Linux machine.{{% /alert %}}

The following commands will download and pack the latest release of all **"product"** extensions (i.e. those that are officially supported by CAST), specifically for use on a deployment of **CAST Imaging v3 for Microsoft Windows**:

```text
ExtendCli.exe config set api-key <CAST_EXTEND_API_KEY>
ExtendCli.exe bundle clear
ExtendCli.exe bundle template add product
Extendcli.exe bundle pack -o <FOLDER_PATH>
```

If you want to download and pack **"labs"** and **"community"** extensions, you can use the following additional commands:

```text
ExtendCli.exe bundle template add labs
ExtendCli.exe bundle template add community
```

Or alternatively, you can add specific extensions:

```text
ExtendCli.exe config set api-key <CAST_EXTEND_API_KEY>
ExtendCli.exe bundle clear
ExtendCli.exe bundle add com.castsoftware.html5
ExtendCli.exe bundle add com.castsoftware.jee
ExtendCli.exe bundle add com.castsoftware.dotnet
Extendcli.exe bundle pack -o <Folder path>
```

If CAST Imaging is deployed on **Linux via Docker**, you will need to modify the `pack` command to add `-p (--platform)` (this will force the tool to fetch only extensions that are compatible with Linux) and `-i (--ignore)` (this will suppress any warnings about extensions that cannot be resolved because they are not supported in a Linux environment):

```text
ExtendCli.exe config set api-key <CAST_EXTEND_API_KEY>
ExtendCli.exe bundle clear
ExtendCli.exe bundle template add product
Extendcli.exe bundle pack -o <FOLDER_PATH> -p linux_x64 -i
```

If you want to download and pack the latest release of all **"product"** extensions (i.e. those that are officially supported by CAST), specifically for use on a deployment of **CAST Imaging/Console v2 for Microsoft Windows** and exclude those extensions that are not compatible with CAST Imaging/Console v2 (such as the 2.x releases of `com.castsoftware.dotnet`, `com.castsoftware.jee` and `com.castsoftware.mainframe`), run the following commands, where `8.3.x` matches the release of Core you are using:

```text
ExtendCli.exe config set api-key <CAST_EXTEND_API_KEY>
ExtendCli.exe bundle clear
Extendcli.exe bundle pack -a 8.3.x -o <FOLDER_PATH>
```

### Step 2 - Upload the bundle (.extarchive file) to Extend Local Server

There are two ways to do this:

#### Via the Extend Local Server UI

Use the **Upload** option in the Extend Local Server UI, which can be accessed using the URL:

```text
http://<FQDN/ip_address>:<port>/ui/admin?<unique_guid>
```

The `<unique_guid>` can be found in the following files (the `ADMI` entry):

```text
Microsoft Windows: %PROGRAMDATA%\CAST\Extend\config.proxy.json
Linux via Docker: /shared/extend_data/config.proxy.json
```

![](upload.jpg)

#### Via the Extend Local Server built in API and curl

Use the following command line with [curl](https://curl.se/):

```text
curl -H "x-cxproxy-apikey:<API_KEY>" -F "data=@<path_to_file>.extarchive" http://<FQDN/ip_address>:<port>/api/synchronization/bundle/upload
```

Where:

- `-H` defines an extra header to include in the request when sending HTTP to a server. In the case of Extend Local Server, this is used to send the API_KEY value to allow access. See https://curl.se/docs/manpage.html#-H.
- `<API_KEY>` grants access to Extend Local Server. The `<API_KEY>` can be found in the following files:
```text
Microsoft Windows: %PROGRAMDATA%\CAST\Extend\config.proxy.json
Linux via Docker: /shared/extend_data/config.proxy.json
```
- `-F` forces curl to POST data using the **Content-Type multipart/form-data**. See https://curl.se/docs/manpage.html#-F.
- `<path_to_file>.extarchive` the path to the `.extarchive` bundle file created in Step 1.
- `<FQDN/ip_address>:<port>` the Extend Local Server URL
- `-V` (not listed above) can be added where you need to debug an issue during the upload process. CAST does not recommend using this option for normal usage.

For example:

```text
curl -H "x-cxproxy-apikey:<API_KEY>" -F "data=@D:\temp\CastArchive_133717405612771244.extarchive" http://extendlocal.corp.domain.com:8085/api/synchronization/bundle/upload
```

### Step 3 - Refresh the extensions cache

The final step in the update process is to ensure that the CAST Imaging extension cache (a dynamic record of all available extensions) is refreshed. If you do not do this, you may find that CAST Imaging does not immediately offer your uploaded extension(s) in the **Available** tab of the [Extensions panel](../../../interface/analysis-config/extensions/).

To do so, ensure you restart all Node services that are currently configured in CAST Imaging.
