---
title: "Using ExtendCli"
linkTitle: "Using ExtendCli"
type: "docs"
no_list: true
---

***

## Overview

[Extend Local Server](../) is delivered "empty" without any preconfigured extensions and in **offline mode** (i.e. in a secure environment without a connection to https://extend.castsoftware.com) you will need to populate Extend Local Server with extensions manually. In addition, you may want to update Extend Local Server to add new extensions or to include more recent releases of extensions. **ExtendCli** is a command line tool designed to perform this job and is explained in detail below.

{{% alert color="info"%}}<ul><li>ExtendCli is currently only available for Microsoft Windows deployments.</li><li>All dependent extensions of a given extension will be included when using ExtendCli.</li></ul>{{% /alert %}}

## Where can I find ExtendCli?

ExtendCli is provided in two ways:

- As a separate downloadable tool, available from CAST Extend: https://extend.castsoftware.com/#/extension?id=com.castsoftware.aip.extendcli&version=latest - CAST highly recommends that you use this tool to benefit from the most [recent updates and changes](../../../../release-notes/com.castsoftware.aip.extendcli).
- On each Node in the following location:

```text
%PROGRAMFILES%\CAST\Imaging\CAST-Imaging-Analysis-Node\bin\ExtendCli\ExtendCli.exe
```

## Using ExtendCli
ExtendCli is provided as an executable (ExtendCli.exe) and must be used in command line mode. To do so, either:

- open a command prompt, and run the tool from where the downloaded tool has been unzipped
- create a re-usable batch script file

The required command line is as follows:

```text
ExtendCli.exe [command][options]
```

Note that before using ExtendCli with the `bundle`, `download` and `install` commands, you MUST first set the following two values with the `config` command:

```text
ExtendCli.exe config set extend-url https://extend.castsoftware.com
ExtendCli.exe config set api-key <your_api_key>
```

See the examples below.

### bundle
Technically, a bundle is a `.json` file in which information about the required extensions, any dependencies and other selection options are stored. The default value of "bundle" is `default.json`. You can create as many "bundles" as necessary. Available options are listed below:

You can determine the `<extension_id>` of a given extension using the [CAST Extend website](https://extend.castsoftware.com):

![](extension_id.jpg)

| Option | Description | Command line | Comment | Example of command line |
|---|---|---|---|---|
| `add` | Add extensions to the active bundle. | `ExtendCli.exe bundle add <extension id> [options]` | When no option is provided, the most recent stable release of the extension is taken.<br><br>Note that [options] can be cumulative. | `ExtendCli.exe bundle add com.castsoftware.reactjs` |
|  |  | `ExtendCli.exe bundle add <extension id> -d`<br><br>`ExtendCli.exe bundle add <extension id> --skip-dependencies` | Skip extension dependencies. | `ExtendCli.exe bundle add com.castsoftware.reactjs -d` |
|  |  | `ExtendCli.exe bundle add <extension id> -s`<br><br>`ExtendCli.exe bundle add <extension id> --latest-stable` | Add most recent stable version (LTS or Funcrel). | `ExtendCli.exe bundle add com.castsoftware.reactjs -s ` |
|  |  | `ExtendCli.exe bundle add <extension id> -a`<br><br>`ExtendCli.exe bundle add <extension id> --absolute-latest` | Add absolute latest version (including alpha and beta releases). | `ExtendCli.exe bundle add com.castsoftware.reactjs -a` |
|  |  | `ExtendCli.exe bundle add <extension id> -f`<br><br>`ExtendCli.exe bundle add <extension id> --latest-funcrel` | Add latest funcrel release. | `ExtendCli.exe bundle add com.castsoftware.reactjs --latest-funcrel` |
|  |  | `ExtendCli.exe bundle add <extension id> -l`<br><br>`ExtendCli.exe bundle add <extension id> --latest-lts` | Add latest LTS release. | `ExtendCli.exe bundle add com.castsoftware.reactjs --latest-lts` |
|  |  | `ExtendCli.exe bundle add <extension id> -v`<br><br>`ExtendCli.exe bundle add <extension id> --version` | Add a specific release of the extension. | `ExtendCli.exe bundle add com.castsoftware.reactjs --version 1.1.0-funcrel` |
| `clear` | Clear your bundle. | `ExtendCli.exe bundle clear` | - | `ExtendCli.exe bundle clear` |
| `download` | Download extensions in the current bundle as .nupkg files | `ExtendCli.exe bundle download [options]` | - | `ExtendCli.exe bundle download` |
|  |  | `ExtendCli.exe bundle download -o / --outdir "<path>` | In < 1.1.0-funcrel, the specified folder must already exist. In ≥ 1.1.0-funcrel, the folder will be created if it does not exist. | `ExtendCli.exe bundle download -o "C:\temp"` |
|  |  | `ExtendCli.exe bundle download -a / --aip-version` | Download the extension version/release compatible with a specific release of CAST Imaging Core. | `ExtendCli.exe bundle download --aip-version 8.3.53` |
|  |  | `ExtendCli.exe bundle download -e / --extensionStrategy <alpha\|beta\|funcrel\|lts>` | Download the extensions matching a specific release category. For example, using "beta" will also include extensions that are in "funcrel" and "lts", but not extensions that are in "alpha". | `ExtendCli.exe bundle download -o "C:\temp" -e beta` |
| `export` | Export the content of the bundle into another .json file. | `ExtendCli.exe bundle export <file path>` | - | `ExtendCli.exe bundle export "C:\mybundle.json"` |
| `install` | Install all extensions described in the bundle to the default extensions folder or an optional custom destination folder. | `ExtendCli.exe bundle install -d "<destination file path>"` | - | `ExtendCli.exe bundle install "C:\my_path"` |
| `import` | Import the content of an existing exported .json file into your bundle. | `ExtendCli.exe bundle import "<file path>"` | - | `ExtendCli.exe bundle import "C:\mybundle.json"` |
| `list` | List the extension versions/releases in your bundle. | `ExtendCli.exe bundle list` | - | `ExtendCli.exe bundle list` |
|  |  | `ExtendCli.exe bundle list -a / --aip-version` | List the extensions version/release compatible with a specific release of CAST Imaging Core. | `ExtendCli.exe bundle list --aip-version 8.3.53` |
|  |  | `ExtendCli.exe bundle list -e / --extensionStrategy <alpha\|beta\|funcrel\|lts>` | List the extensions matching a specific release category. For example, using "beta" will also include extensions that are in "funcrel" and "lts", but not extensions that are in "alpha". | `ExtendCli.exe bundle list -e beta` |
| `pack` | Create an `.extarchive` file from your bundle that can be imported into CAST Extend Offline. | `ExtendCli.exe bundle pack [options]` | - | `ExtendCli.exe bundle pack` |
|  |  | `ExtendCli.exe bundle pack -o "<path>"` | Target folder where the archive will be saved. This folder must already exist. | `ExtendCli.exe bundle pack -o "C:\Temp\Bundles"` |
|  |  | `ExtendCli.exe bundle pack -d` | Only downloads missing extensions, will not create an archive. | `ExtendCli.exe bundle pack -d` |
|  |  | `ExtendCli.exe bundle pack -a / --aip-version <core_release>` | Pack extensions compatible with a specific release of CAST Imaging Core. This is useful when you want to explicitly avoid downloading extensions that are not compatible with Linux, i.e. you can set the release number to 8.3.something which will exclude new releases of extensions specific to 8.4.x. | `ExtendCli.exe bundle pack -a 8.3.53` |
|  |  | `ExtendCli.exe bundle pack -p / --platform linux_x64 -i / --ignore` | Pack extensions compatible with Linux only. Extensions only compatible with Microsoft Windows will be ignored. `-i` is "ignore" and will suppress any warnings about extensions that are not compatible with Linux. | `ExtendCli.exe bundle pack -p linux_x64 -i` |
|  |  | `ExtendCli.exe bundle pack -e \| --extensionStrategy <alpha\|beta\|funcrel\|lts>` | Pack the extensions matching a specific release category. For example, using "beta" will also include extensions that are in "funcrel" and "lts", but not extensions that are in "alpha". | `ExtendCli.exe bundle pack -e beta` |
| template | Manage predefined bundles. | `ExtendCli.exe bundle template <command><options>` | Manage predefined templates available in CAST Extend and include them in your extension. | - |
|  |  | `ExtendCli.exe bundle template add <product/community/labs/template_id>` | Allows the addition of the absolute latest version (including alpha and beta releases) of all official / community / labs / or specific predefined bundle of extensions into a template bundle. Available in ≥ 1.1.0-funcrel.  | `ExtendCli.exe bundle template add product`<br><br>`ExtendCli.exe bundle template add community`<br><br>`ExtendCli.exe bundle template add labs`<br><br>`ExtendCli.exe bundle template add 14` |
|  |  | `ExtendCli.exe bundle template add <product/community/labs/template_id> -s --latest-stable`<br><br>`ExtendCli.exe bundle template add <product/community/labs/template_id> -a --absolute-latest`<br><br>`ExtendCli.exe bundle template add <product/community/labs/template_id> -f --latest-funcrel`<br><br>`ExtendCli.exe bundle template add <product/community/labs/template_id> -l --latest-lts` | Allows the addition of all official / community / labs / or specific predefined bundle of extensions into a template bundle AND allows you to specify the release of extensions you require:<br><br>`-s --latest-stable` - Add most recent stable version (LTS or Funcrel)<br><br>`-a --absolute-latest` - Add absolute latest version (including alpha and beta releases) - equivalent to not passing any version option at all (i.e. the default).<br><br>`-f --latest-funcrel` - Add latest funcrel release.<br><br>`-l --latest-lts` - Add latest LTS release.<br><br>Available in ≥ 3.0.0-funcrel.  | `ExtendCli.exe bundle template add product --latest-stable`<br><br>`ExtendCli.exe bundle template add community --absolute-latest`<br><br>`ExtendCli.exe bundle template add labs --latest-funcrel`<br><br>`ExtendCli.exe bundle template add 14  --latest-lts` |
|  |  | `ExtendCli.exe bundle template list` | Lists the seven predefined bundles:<br><br>Id = 1 → All product extensions (note that this is deprecated from ≥ 1.1.0-funcrel - you should use "product" instead)<br><br>Id = 4 → Microsoft .NET<br><br>Id= 5 → Mobile<br><br>Id = 6 → Web<br><br>Id = 14 → Data<br><br>Id = 16 → Java / JEE<br><br>Id = 17 → Security (this bundle provides support for Security analysis for all technologies)<br><br>Id = product (from ≥ 1.1.0-funcrel)<br><br>Id = labs (from ≥ 1.1.0-funcrel)<br><br>Id = community (from ≥ 1.1.0-funcrel) | `ExtendCli.exe bundle template list` |
|  |  | `ExtendCli.exe bundle template content <template_id>` | Displays the content of one predefined bundle. | `ExtendCli.exe bundle template content 17` |
| `extract` | Extract an `.extarchive` file and optionally install the resulting extensions. | `ExtendCli.exe bundle extract [options] <archivePath>` | Extract an .extarchive file and place the contents in the default `%PROGRAMDATA%\CAST\CAST\Extensions` folder. | `ExtendCli.exe bundle extract C:\temp\my_bundle.extarchive` |
|  |  | `ExtendCli.exe bundle extract -d <path> <archivePath>` | Extract an `.extarchive` file and place the contents in a custom folder. | `ExtendCli.exe bundle extract -d C:\CAST\extensions  C:\temp\my_bundle.extarchive` |
|  |  | `ExtendCli.exe bundle extract  -i <archivePath>` | Extract an `.extarchive` file, place the contents in the default `%PROGRAMDATA%\CAST\CAST\Extensions` folder and install them ready for use with CAST. | `ExtendCli.exe bundle extract -i C:\temp\my_bundle.extarchive` |
|  |  | `ExtendCli.exe bundle extract -d <path> -i <archivePath>` | Extract an `.extarchive` file, place them in a custom folder and install them ready for use with CAST. |` ExtendCli.exe bundle extract -d C:\CAST\extensions -i C:\temp\my_bundle.extarchive` |
| remove | Remove individual extensions from a bundle. | `ExtendCli.exe bundle remove [...package-ids]` | - | `ExtendCli.exe bundle remove com.castsoftware.reactjs` |

### config

The config command configures the parameters to allow the tool to connect to CAST Extend over the internet to fetch the required extensions (extensions) for the bundle:

| Option | Description | Command line | Comment | Example of command line |
|---|---|---|---|---|
| `set` | Setup the CAST Extend (online) connection parameters. | `ExtendCli.exe config set <api-key>` | `<api-key>` is specific to a login for CAST Extend (online). You can find this in your profile.<br><br>Note that this command only needs to be used once (unless you decide to use a different api-key for an alternative CAST Extend (online) login).  | `extendCli.exe config set api-key w9876xyz-d999-32ii-9fb0-0123c5` |
|  |  | `ExtendCli.exe config set [extend-url]` | `[extend-url]` refers to the URL to use for CAST Extend (online). When no option is provided, the default value is used: https://extend.castsoftware.com.<br><br>Note that this command only needs to be used once (unless you decide to use a different URL). | `extendCli.exe config set extend-url "your URL"` |
|  |  | `ExtendCli.exe config set [storage-dir]` | `[storage-dir]` refers to the location where extension packages will be downloaded to when using the download option. When no option is provided, the default value is used:<br><br>     `%PROGRAMDATA%\CAST\ExtendCLI\packages` <br><br>If ExtendCli is being run using the executable provided with a Node, then the default storage location is set to:<br><br>       `C:\ProgramData\CAST\Imaging\CAST-Imaging-Analysis-Node\ExtendCliConfig\packages` | `extendCli.exe config set storage-dir C:\CAST\temp` |
|  |  | `ExtendCli.exe config set [proxy] <proxy_address>` | Where possible `ExtendCli.exe config proxy <proxy_address> [-options]` introduced in 3.0.0 should be used instead.<br><br>`[proxy]` is optional. It should be used:<br><br><ul><li>when your network environment uses a proxy for internet access.</li><li>when the machine you are running ExtendCli on does not have operating system level proxy settings enabled.</li></ul>The syntax is as follows:<br><br>`ExtendCli.exe config set proxy <protocol://[user:password@]host[:port]>`<br><br>Note that if you are using the Use setup script option (1), CAST also recommends enabling the Automatically detect settings option (2):<br>       <br>![](proxy1.jpg) | `extendCli.exe config set proxy http://proxy_user:proxy_password@proxy.corp.com:3128` |
| `proxy` | Configure a connection via a proxy (available in ≥ 3.0.0) | `ExtendCli.exe config proxy <proxy_address> [-options]` | This option should be used instead of `ExtendCli.exe config set [proxy]` where possible.<br><br><ul><li>when your network environment uses a proxy for internet access.</li><li>and when the machine you are running ExtendCli on does not have operating system level proxy settings enabled.<br><br>The syntax is as follows:<br><br>`ExtendCli.exe config proxy <protocol://host[:port]> [-options]`<br><br>The following options are available:<br><br><ul><li>`-b \| --bypass-on-local` > Do not use the configured proxy settings for local requests.</li><li>>`-c \| --default-credentials` > Use the current Microsoft Windows account to authenticate with the proxy server.</li><li>`-i \| --bypass` > Specifies one single address that will not use the proxy. Can be used multiple times in a command.</li><li>`-u \| --username` > Specifies the username to authenticate with the proxy server if authentication is required.</li><li>`-p \| --password` > Specifies the password for the username used to authenticate with the proxy server if authentication is required.</li><li`>-d \| --domain` > Specifies the domain for domain restricted proxy servers.</li><li>`-r \| --reset` > Will reset any previously configured proxy settings, i.e. when using ExtendCli commands in a batch file or consecutively in a CMD window.</li></ul>Note that if you are using the Use setup script option (1), CAST also recommends enabling the Automatically detect settings option (2) as shown in the image above. | `ExtendCli.exe config proxy http://proxy.corp.com:3128`<br><br>`ExtendCli.exe config proxy http://proxy.corp.com:3128 -c`<br><br>`ExtendCli.exe config proxy http://proxy.corp.com:3128 -u my_user -p my_password`<br><br>`ExtendCli.exe config proxy http://proxy.corp.com:3128 -u my_user -p my_password -i http://some.address.com -i http://some.other.address.com -i *:8080 -i https://*:*` |

### download

This option allows you to download an extension in `.nupkg ` format. The package will NOT be downloaded to `%PROGRAMDATA%\CAST\CAST\Extensions`, instead the package will be downloaded to the following default locations:

`%PROGRAMDATA%\CAST\ExtendCLI\packages`

If ExtendCli is being run using the executable provided with a Node, then the default storage location is set to:

`%PROGRAMDATA%\CAST\Imaging\CAST-Imaging-Analysis-Node\ExtendCliConfig\packages`

The download folder can be set to an alternative location using the `config set storage-dir` option.

| Option | Description | Command line | Example of command line |
|---|---|---|---|
| `-d \| --skip-dependencies` | This option allows you to download an extension without any dependencies. CAST only recommends using this option if you understand the implication - an extension will not function correctly without its dependencies. | `ExtendCli.exe download -d <extension id>`<br><br>`ExtendCli.exe download --skip-dependencies <extension id>` | `ExtendCli.exe download -d com.castsoftware.reactjs` |
| `-s \| --latest-stable <true\|false>` | This option allows you to configure whether to download the most recent stable release of an extension (LTS or funcrel).  The default behaviour (i.e. when the option is omitted) the action is set to `true` and the `latest-stable` release will be downloaded. Explicitly add the option and set it to `false` to ensure that the latest stable release is not downloaded. | `ExtendCli.exe download -s false <extension id`><br><br>`ExtendCli.exe download --latest-stable false <extension id>` | `ExtendCli.exe download -s false com.castsoftware.reactjs` |
| `-a \| --absolute-latest` | This option allows you to download the most recent release of an extension including alpha and beta releases and all dependencies. | `ExtendCli.exe download -a <extension id>`<br><br>`ExtendCli.exe download --absolute-latest <extension id>` | `ExtendCli.exe download -a com.castsoftware.reactjs` |
| `-f \| --latest-funcrel` | This option allows you to download the most recent funcrel release of an extension and all dependencies. | `ExtendCli.exe download -f <extension id>`<br><br>`ExtendCli.exe download --latest-funcrel <extension id>` | `ExtendCli.exe download -f com.castsoftware.reactjs` |
| `-l \| --latest-lts` | This option allows you to download the most recent LTS release of an extension and all dependencies. | `ExtendCli.exe download -l <extension id>`<br><br>`ExtendCli.exe download --latest-lts <extension id>` | `ExtendCli.exe download -l com.castsoftware.reactjs` |
| `-v \| --version` | This option allows you to download a specific release of an extension and all dependencies. | `ExtendCli.exe download -v <release><extension id>`<br><br>`ExtendCli.exe download --version <release><extension id>` | `ExtendCli.exe download -v 1.1.4-funcrel com.castsoftware.reactjs` |

### install

This option allows you to download an extension from CAST Extend and store it on the local machine in `%PROGRAMDATA%\CAST\CAST\Extensions` (or an alternative location if you have modified the `CAST_PLUGINS_ROOT_PATH` option in the `CastGlobalSettings.ini` file).

| Option | Description |
|---|---|
| `<extensionId>` | This option allows you to specify a particular extension for download using its ID - this is a mandatory option. The ID of a CAST extension always takes the form `com.castsoftware.<name>`, without any version numbers. Note that the most recent versions of an extension's dependencies will always be downloaded alongside the extension, unless you are using the `[--ignore-dependencies]` option - see below. |
| `[--version <version>]` | This option allows you to specify a specific version of the extension for install using its version number. This option is not mandatory. When not included, the most recent version of the extension you require will always be downloaded. |
| `[--ignore-dependencies]` | This option allows you to download an extension without any dependencies. CAST only recommends using this option if you understand the implication - an extension will not function correctly without its dependencies. This option is not mandatory. When not included, the most recent versions of an extension's dependencies will always be downloaded. |
| `[--username <username>]` / `[--password <password>]` | When connecting to a server that requires authentication (for example https://extend.castsoftware.com:443/api) use these two options to specify your credentials. You will require an account on the remote server to authenticate when using CAST Extend. See https://extend.castsoftware.com/#/register to register for an account on CAST Extend. |

#### SHA256 data integrity checks

{{% alert color="info" %}}Available in ≥ 3.1.0.{{% /alert %}}

An integrity check is only performed if the source server generates a hash code. When using the `install` command, ExtendCLI will automatically verify each extension using a unique SHA256 hash code generated by the source server (whether this is CAST's official Extend server, or an Extend Local server) to ensure the integrity of the downloaded data::

- If the integrity check passes, then the extension will be installed without issue
- If the integrity check fails, then an error is thrown and the downloaded extension is deleted and will not be installed. In this scenario, you should attempt the install procedure again - if the integrity check fails again, please contact CAST Support.

### uninstall

Use this option to specify an extension that you have downloaded to your machine and that you now want to remove from your machine:

| Option | Description |
|---|---|
| `<extensionId>` | This option allows you to specify a particular extension for removal using its ID - this is a mandatory option. The ID of a CAST extension always takes the form `com.castsoftware.<name>`, without any version numbers. |
| `[--version <version>] `| This option allows you to specify a specific version of the extension for uninstall using its version number. This option is not mandatory. When not included, the most recent version of the extension you require will always be deleted from your machine. |

{{% alert color="info" %}}<ul><li>Removing an extension will only delete the extension files on the local machine. It does not remove the extension from any CAST databases/schemas that it has been installed to.</li><li>Removing an extension only removes the specific extension. Any dependencies are left on the machine as other extensions may require them.</li></ul>{{% /alert %}}

## Examples

### Set the CAST Extend (online) connection properties

Set the CAST Extend URL:

```text
ExtendCli.exe config set extend-url "Your URL"
```

Set the API key:

```text
ExtendCli.exe config set api-key w9876xyz-d999-32ii-9fb0-0123cde45
```

### Define one extension to package

The extension and any dependencies will be added to the list of extensions in the bundle with the following command:

```text
ExtendCli.exe bundle add com.castsoftware.typescript
```

You can also use certain `[options]` as follows:

```text
ExtendCli.exe bundle add com.castsoftware.typescript [options]
```

These options can be cumulative, for instance to download the absolute latest release (including alpha/beta) or the latest funcrel release or the latest LTS, use the following:

```text
ExtendCli.exe bundle add com.castsoftware.typescript -a -f -l
```

To download the absolute latest release (including alpha/beta) or the latest LTS, use the following:

```text
ExtendCli.exe bundle add com.castsoftware.typescript -a -l
```

### Add extensions from predefined bundles

The `add template `command allows you to add extensions to your bundle that belong to a specific predefined bundle (predefined in CAST Extend (online)). The following command will add template number 14 to the bundle:

```text
ExtendCli.exe bundle template add 14
```

The following commands will add all "product", "user community" and "labs" extensions:

```text
ExtendCli.exe bundle template add product
ExtendCli.exe bundle template add community
ExtendCli.exe bundle template add labs
```

### Save the bundle

Once you have defined the content of your bundle using the `bundle add` or `template add` commands, you can save it to a `.json` file, so that you can use the same bundle in the future:

```text
ExtendCli.exe bundle export "C:\PRODUCT\Extend\ExtendCli\save_test\mylastbundle"
```

### Generate the package

When you generate the package, it will create the `.extarchive` file `<last package name|default package name>.extarchive` which can then be uploaded into your Extend Local Server. You must define a folder where the archive file will be placed and the folder must exist already:

```text
ExtendCli.exe bundle pack -o "C:\PRODUCT\Extend\My package"
```

Navigate to the folder you have defined as output (in the above example the path is `C:\PRODUCT\Extend\My package`) - it will contain the generated `.extarchive` file that you can upload into your Extend Local Server by following the steps described [Update Extend Local Server with new content](../).

If you are packing extensions to use with a Linux deployment of CAST Imaging, use the following command instead:

```text
Extendcli.exe bundle pack -o <FOLDER_PATH> -p linux_x64 -f
```

## Advanced configuration settings - appsettings.json

Some additional advanced settings can be configured using the following file:

```text
Downloadable tool:
<extend_cli_unzip_folder>\appsettings.json

Provided with the Node:
%PROGRAMFILES%\CAST\Imaging\CAST-Imaging-Analysis-Node\bin\ExtendCli\appsettings.json
```

### Timeout

```text
"Timeout": 10800
```

This setting is specifically for those with very slow network connections. The value is in seconds (the default value is 10800, or 3 hrs) and should only be increased if you are facing timeouts when fetching data from CAST Extend, i.e. when the total time required for CAST Extend to build the bundle and then for ExtendCli to download it exceeds 3 hrs. CAST does NOT recommend decreasing this value.

### SettingsPath

```text
"SettingsPath": null
```

This setting is set by default to `%PROGRAMDATA%\CAST\Imaging\CAST-imaging-analysis-node\ExtendCliConfig` and defines where the current instance of ExtendCli will store configuration settings and cache files. If you need to run multiple instances of ExtendCli on one machine, and you want to ensure clear separation of data between each instance, you can define a custom path for the configuration data using this option. For example to set the path to `D:\CAST\temp\ExtendCli`:

```text
"SettingsPath": "D:\\CAST\\temp\\ExtendCli"
```

Paths must conform to the following syntax:

- Enclosed in double quotes
- Absolute path (not relative)
- Back slashes are required (forward slashes not accepted) and must be escaped with a back slash
