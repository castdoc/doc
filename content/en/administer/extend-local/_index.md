---
title: "Extend Local Server"
linkTitle: "Extend Local Server"
type: "docs"
no_list: false
---

***

This section provides instructions and information about managing the **Extend Local Server** component, an on-premises solution that acts as an intermediary placed between CAST Imaging and CAST's publicly available "Extend" ecosystem (https://extend.castsoftware.com). You can find out more about this component in [Install Extend Local Server](../../install/extend-local/).