---
title: "Source code does not display"
linkTitle: "Source code does not display"
type: "docs"
no_list: true
---

***

## Overview

CAST Imaging will automatically display the source code of items in your application (see [Right panel - Source code](../../../interface/viewer/right-panel/source-code)) except where you have imported application results direct into CAST Imaging using the **Import an application view** option which requires you to define a [data-source](../../settings/applications/data-source):

![](import.jpg)

In all other circumstances, there may an issue that needs resolving. Some possible explanations are provided below.

## I have analyzed my application / configured a data-source, but I still cannot see source code for some items

Some objects/nodes in CAST Imaging do not contain any source code and a message is displayed to explain this:

![](no_code.jpg)

This can occur for a variety of reasons, for example:

- Nodes in **levels 1 - 5** do not have associated source code since these nodes are "groups" of nodes/objects
- At object level, objects that are categorized as **"external"**, i.e. third-party libraries, will not show any source code:

![](no_code_external.jpg)

- At object level, when the object is not a specific object but rather an object grouping. In this situation, the grouped items must be expanded in order to see the constituent objects and their source code:

![](no_code_grouped.jpg)

- When the object is an "Unknown Program" - this is seen mostly with Mainframe or SQL applications when the analyzer creates a placeholder for programs found that are not present in the delivered source code:

![](no_code_uknown.jpg)

## No specific error in the UI but no source code displayed

If you are attempting to view the source code of an item but you cannot and there is no specific error in the UI, you may have fallen foul of an internal threshold value that **limits the amount of source code that can be displayed in the browser** to improve performance. This issue (if it has occurred) is logged in the Node log file located on the Node that was used to fetch the source code from the database storage system:

```text
Microsoft Windows: %PROGRAMDATA%\CAST\Imaging\CAST-Imaging-Analysis-Node\logs\analysis-node.log

Linux via Docker: Use "docker logs analysis-node" to view the container logs
```

{{% alert color="info" %}}In a **distributed deployment** with more than one node, you may need to search the log file of each node to verify whether the error has occurred. By default, CAST Imaging functions in load balancing mode, therefore a random node is chosen to perform jobs such as fetching the source code.{{% /alert %}}

The error will look like this:

```text
2024-05-15 09:12:13.289  INFO 5200 --- [nio-9980-exec-5] o.apache.coyote.http11.Http11Processor   : Error parsing HTTP request header
 Note: further occurrences of HTTP request parsing errors will be logged at DEBUG level.

java.lang.IllegalArgumentException: Request header is too large
```

This error indicates that CAST Imaging was unable to display the source code because the source code of the object in question is too large and exceeds the size authorized in the API call between the CAST Imaging UI and the back-end database. It is possible to update the authorized file size to allow the source code of larger objects to be displayed. This is described below.

### Resolution

Edit the `application.yml` located here:

```text
Microsoft Windows: %PROGRAMDATA%\CAST\Imaging\CAST-Imaging-Analysis-Node

Linux via Docker: Use the "docker exec it analysis-node sh" command to enter the container in shell, then move to the "data_aip_node_v3" folder where you will find the file
```

Modify the following line to increase the header size to 1MB (from the default 32KB) - note that you may need to increase this value further if a value of 1MB does not resolve the error:

```yml
server:
  #Max total header size for HTTP requests
  max-http-header-size=1MB
```

Save the file and then restart the node service/container to ensure the changes are taken into account.

{{% alert color="info" %}}The application.yml file will be overwritten in an in-place update of CAST Imaging to a new release. To avoid this, you can instead add customization to the `application-default.yml` file which is never overwritten.{{% /alert %}}
