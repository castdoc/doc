---
title: "Timeouts while consulting results"
linkTitle: "Timeouts while consulting results"
type: "docs"
no_list: true
---

***

## Overview

If you are experiencing timeouts or errors while attempting to view or work with your analysis results, especially for very large applications, this is likely due to the way in which your application is configured with regard to **modules** (the internal grouping of objects used by CAST Imaging). If your application contains **only one single module** then all the objects in your application are contained within that module. When CAST Imaging attempts to fetch results for certain features that rely on modules, it needs to fetch all the objects in the module: when there is only one module, this process can take a long time to complete and will sometimes fail due to exceeding an internal timeout threshold.

This issue usually manifests itself with the following UI error `Failed to get graph data` or `Failed to get Transaction list`:

![](failed1.jpg)
![](failed2.jpg)

Equally, the browser may simply "hang" and become unresponsive. On other occasions no error is returned and the UI simply fails to display the requested items, for example when looking at Cloud Maturity results no objects are displayed in the right panel:

![](empty.jpg)

{{% alert color="info" %}}By default CAST Imaging will always configure one module per application known as the **Full Content** module - you can check this in the [module configuration](../../../interface/analysis-config/config/modules/) panel.{{% /alert %}}

## Resolution

To resolve this issue, there are various things you can try:

- ensure that sufficient memory is granted to Neo4j - see [Optimizing Neo4j memory allocation](../../neo4j/memory/)
- switch from **Force** to **Sequential** object display
- modify the **timeout** threshold

### Switch from Force to Sequential object display

Simply switching from **Force** to **Sequential** object display in the UI can improve loading times. To do so, use the [Graph layout tool](../../../interface/viewer/view/actions/) option:

![](sequential.jpg)

You can also set **Sequential** as the default display method using the [Viewer Preferences](../../viewer-prefs/):

![](viewer_prefs.jpg)

### Modify the timeout threshold

#### Step 1 - Update imagingservice.json / app.config

On the machine on which your `imaging-viewer` service is installed, perform the following steps:

Locate the following file depending on your operating system:

{{< tabpane text=false >}}
  {{< tab header="Microsoft Windows" >}}%PROGRAMFILES%\CAST\Imaging\CAST-Imaging-Viewer\setup-config\imagingservice\imagingservice.json{{< /tab >}}
  {{< tab header="Linux via Docker" >}}/home/CAST/imaging-viewer/server/app.config{{< /tab >}}
{{< /tabpane >}}

Edit the file and add a new line `"LOGGER_TIMEOUT":` with the time out value listed in seconds. If the line already exists, increase the value to your required setting. For example to set the time out to two minutes (120 seconds) add the following and then save the file. The example below is for Microsoft Windows, but the same is true for Docker via Linux:

```json
{
    "APP_CONFIG": "C:\\ProgramData\\CAST\\Imaging\\CAST-Imaging-Viewer\\setup-config\\imagingservice\\app-config.json",
    "PORT": "9010",
    "NEO4J_USERNAME": "neo4j",
    "NEO4J_PASSWORD": "imaging",
    "NEO4J_BOLT_ENDPOINT": "127.0.0.1:7697",
    "NEO4J_IMPORT_PATH": "C:\\ProgramData\\CAST\\Imaging\\CAST-Imaging-Viewer\\Neo4jData\\import",
    "TOKEN": "D5ED6A406775FC71B8D2A978883E8ED4",
    "LOG_PATH": "C:\\Program Files\\CAST\\Imaging\\CAST-Imaging-Viewer\\logs\\",
    "LOG_LEVEL": "info",
    "AUDIT_TRAIL": true,
    "LOGGER_TIMEOUT": 120,
    "EUREKA_PORT": "8098",
    "EUREKA_HOST": "localhost",
    "SERVICE_HOST": "localhost",
    "SERVICE_NAME": "IMAGING",
    "FE_PORT": 8093
}
```

#### Step 2 - Update proxy.conf

Locate the following file depending on your operating system:

{{< tabpane text=false >}}
  {{< tab header="Microsoft Windows" >}}%PROGRAMFILES%\CAST\Imaging\CAST-Imaging-Viewer\nginx\proxy.conf{{< /tab >}}
  {{< tab header="Linux via Docker" >}}/home/CAST/imaging-viewer/server/nginx/conf/proxy.conf{{< /tab >}}
{{< /tabpane >}}

Edit the file and add a new line `proxy_read_timeout` with the time out value listed in seconds. If the line already exists, increase the value to your required setting. For example to set the time out to two minutes (120 seconds) add the following and then save the file:

```json
proxy_redirect     off;
proxy_set_header   Host $host;
proxy_set_header   X-Real-IP $remote_addr;
proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
proxy_set_header   X-Forwarded-Host $server_name;
proxy_read_timeout 120s;
```

#### Step 3 - Restart services

Finally, restart the following services/containers to ensure your changes are taken into account:

| Microsoft Windows | Linux via Docker |
|---|---|
| <ul><li>CAST Imaging Viewer Backend</li><li>CAST Imaging Viewer Frontend</li></ul> | <ul><li>server</li></ul> |
