---
title: "API"
linkTitle: "API"
type: "docs"
no_list: false
---

***

CAST Imaging provides API access to both results and analysis configuration/management.  Choose the API you would like to know more about: