---
title: "imaging-services API"
linkTitle: "imaging-services API"
type: "docs"
no_list: true
---

***

## Overview

The `imaging-services` API provides access to the **analysis configuration and management** features provided in CAST Imaging.

### Base URL

All provided endpoints imply they are accessed using the standard CAST Imaging access URL as follows:

```text
http://<machine>:<port>
```

Where:

- `<machine>` is the fully qualified domain name of the machine where your `imaging-services` component is installed.
- `<port` is the port number used for the machine where your `imaging-services` component is installed, the default is `8090`.

### Authentication - API key

To use the API, you will need to be authenticated to CAST Imaging. To do so you should generate an **API key** in the CAST Imaging UI - you can obtain this by clicking the user icon in the top right corner of the screen and selecting [Profile](../../profile/). When using the API, you will need pass the API key in the request header, for example:

```text
X-API-KEY: <api-key>
```
### Request body / response type

All requests will expect `"application/json"` when a request body is expected. However, this documentation will be using a sort of **pseudo json**, with the expected type for a variable, and an indication if any key is optional (they are otherwise required).

Most requests will return a response type `"application/json"` unless otherwise specified. The following psuedo Methods are used:

| Method | Description |
|---|---|
| `anyOf(...)` | This indicates that the value in the request/response for the provided key must be the same as any of the values in the list. In case this is surrounded by square brackets, [anyOf()], this means that the value of the key should be an array that can only contain values from the list passed to `anyOf`. |
| `guid()` | This indicates that the value must be a GUID matching the pattern `[0-9A-Fa-f]{8}-([0-9A-Fa-f]{4}-){3}[0-9A-Fa-f]{12})`. |
| `{guid}` | Same as `guid()`, but is used in the API endpoint path, to identify a resource using this GUID (for example an application GUID). |
| `max()` | A max value that will fail the request if it is exceeded. |
| `empty()` | In a response section, this means that the endpoint's response will have an empty body. |
| `optional()` | Indicates that this value may or may not be present in the response (the key may not be present). |

## APIs

### /api/settings/license

**GET** | Retrieve the license information as it is stored in the Imaging Node DB

**Status**: 200 (OK)

**Response**: 

```json
{
  "license": "MyLicense;TestValue",
  "licensedTo": "TestValue",
  "licenceType": anyOf("unknown", "aip", "efp", "sec", "imaging", "insight"),
  "licenseKind": anyOf("unkown", "legacy_aip", "scope_cd", "scope_app", "named_user"),
  "expirationDate": "2024-12-18"
}
```

**PUT** | Update the license

**Request**:

```json
{
  "license": "MyNewLicense;TestValue"
}
```

**Status**: 200 (OK), 412 (PRECONDITION_FAILED)- if the license is invalid.

**Response**: 

```json
{
  "license":"MyLicense;TestValue",
  "licensedTo": "TestValue",
  "licenceType": anyOf("unknown", "aip", "efp", "sec", "imaging", "insight"),
  "licenseKind": anyOf("unkown", "legacy_aip", "scope_cd", "scope_app", "named_user"),
  "expirationDate": "2024-12-18"
}
```

### /api/settings/extend

**GET** | Get the information about the Extend configuration

**Status**: 200 (OK)

**Response**: 

```json
{
  "enableSendReport": false,
  "extendApikey": "apiKey",
  "extendMode": "extend_online",
  "extendOffline": false,
  "extendOnline": true,
  "extendProxy": false,
  "extendUrl": "<https://extend.castsoftware.com>"
}
```

**PUT** | Update the Extend configuration

**Request**: 

```json
{
  "extendUrl": "<http://extend.castsoftware.com>",
  "extendUsername": "",
  "extendPassword": "",
  "enableSendReport": true,
  "extendApikey": "",
  "extendProxyMode": anyOf("auto", "offline")
}
```

**Status**: 200 (OK), 412 (PRECONDITION_FAILED): if access to Extend with provided URL and/or credentials failed.

**Response**: `empty()`

{{% alert color="info" %}}<ul><li>Either provide the username/password or the API Key, Imaging Node will test access using either, and remove the username/password, only storing the API Key instead.</li><li>Extend mode will be automatically defined based on the provided <code>extendUrl</code>.</li></ul>{{% /alert %}}

### /api/settings/sources-folder

**GET** | Get the current sources folder configuration on the Imaging Node machine.

**Status**: 200 (OK)

**Response**: 

```json
{
  "data": "/path/to/an/imaging/node/folder"
}
```

**PUT** | Set the current sources folder configuration on the Imaging Node machine

**Request**:

```json
{
  "data": "/path/to/an/imaging/node/folder"
}
```

**Status**: 200 (OK), 412 (PRECONDITION_FAILED): if the provided path is not valid, i.e. doesn't exist or has the wrong permissions.

**Response**: `empty()`

**DELETE** | Unsets the current source folder configuration

**Status**: 200 (OK)

**Response**: `empty()`

### /api/applications

**GET** | Retrieve the list of applications

**Response**: 

```json
{
  "applications": [
    {
      "guid": guid(),
      "name": "NameOfApplication",
      "title": "DisplayNameOfApplication",
      "currentVersionGuid": guid(),
      "latestVersionGuid": guid(),
      "managed": true,
      "schemaPrefix": "myapp_",
      "caipVersion": "8.3.59",
      "domain": optional("myDomain"),
      "inPlaceMode": true,
      "imagingTenant": "string",
      "targetNode": optional("myNode"),
      "snapshotDone": true,
      "imagingDone": true,
      "onboarded": true,
      "missingDependencies": true,
      "movingToFastScan": true,
      "movingToFastScanAnalysisPending": true,
      "analysisDuration": 0,
      "versionName": "myVersion",
      "versionTitle": "displayNameOfVersion",
      "versionGuid": guid(),
      "versionStatus": anyOf("opened", "purged", "delivering", "delivered", "discovered", "scanned", "accepted", "updating_extensions", "analysis_ongoing", "analysis_done", "analysis_failed", "preparing_analysis_data", "analyzed", "analysis_data_prepared", "processing_imaging", "imaging_processed", "snapshot_ongoing", "snapshot_done", "fully_analyzed", "validated", "publishing"),
      "lastActionDate": "2024-12-18T09:25:02.089Z"
    }
  ],
  "versionStatusCounts": {
    "scanned": 0,
    "analyzed": 0
  },
  "totalApplications": 1,
  "legacyApplicationCount": 0,
  "onboardedApplicationCount": 1
}
```

**Status**: 200 (OK)

### /api/applications/guid()

**GET** | Get the details of an application with the given GUID

**Response**: 

```json
{
  "guid": "string",
  "createdDate": "2024-12-18T09:25:51.477Z",
  "modifiedDate": "2024-12-18T09:25:51.477Z",
  "name": "string",
  "title": "string",
  "currentVersionGuid": "string",
  "latestVersionGuid": "string",
  "managed": true,
  "schemaPrefix": "string",
  "caipVersion": "string",
  "domain": "string",
  "inPlaceMode": true,
  "imagingTenant": "string",
  "analysisDuration": 0,
  "targetNode": "string",
  "onboarded": true,
  "missingDependencies": true,
  "movingToFastScan": true,
  "movingToFastScanAnalysisPending": true,
  "caipUpgradeAvailable": "string",
  "migrationStatus": "string",
  "unsupportedTechnos": "string",
  "recentMigrationChangeLogDate": "2024-12-18T09:25:51.477Z",
  "version": {
    "guid": "string",
    "name": "string",
    "title": "string",
    "status": "opened",
    "versionDate": "2024-12-18T09:25:51.477Z",
    "currentVersion": true,
    "configurationChanged": true,
    "acModelConfigurationChanged": true,
    "dataConfigurationChanged": true,
    "sourcesConfigurationChanged": true,
    "analysisUnitStatusChanged": true,
    "acModelsSynchronized": true,
    "dmtIgnore": "string",
    "previousVersion": "string",
    "deliveryConfiguration": {
      "guid": "string",
      "ignorePatterns": [
        "string"
      ],
      "exclusionRules": [
        {
          "rule": "string",
          "enabled": true
        }
      ]
    },
    "extensionAutoConfigEnabled": true,
    "sourcePath": "string",
    "objectives": [
      "global_risk"
    ],
    "analysisDataPrepared": true,
    "imagingDone": true
  },
  "onboardedNotAnalyzed": true
}
```

**Status**: 200 (OK), 404 (Not Found): if the application with the given GUID couldn't be found.

### /api/applications/{guid}/security-dataflow/jee

**GET** | Get the status of JEE Security Dataflow (enabled/disabled)

**Status**: 200 (OK), 404 (Not Found)

**Response**: 

```json
{
    "data": true
}
```

**PUT** | Update the status of the JEE Security Dataflow (enabled/disable)

**Request**:

```json
{
	"data": true
}
```

**Status**: 200 (OK), 404 (Not Found)

### /api/applications/{guid}/security-dataflow/dotnet

**GET** | Get the status of .NET Security Dataflow (enabled/disabled)

**Status**: 200 (OK), 404 (Not Found)

**Response**: 

```json
{
	"data": true
}
```

**PUT** | Update the status of .NET Security Dataflow (enabled/disabled)

**Request**:

```json
{
	"data": true
}
```

**Status**: 200 (OK), 404 (Not Found)

### /api/applications/{guid}/overview

**GET** | Get the overview information for an application

**Status**: 200 (OK), 404 (Not Found): if no application was found with the given GUID

**Response**: 

```python
{
  "versionCount" : 0,
  "lastSnapshot" : {
    "architectureChecked" : true,
    "versionTitle" : "versionTitle",
    "aepCount" : 4,
    "invalidTransactionCount" : 3,
    "title" : "title",
    "versionName" : "versionName",
    "linesOfCode" : 7,
    "afpCount" : 2,
    "efpCount" : 7,
    "name" : "name",
    "validTransactionCount" : 9,
    "enhancementMeasure" : "enhancementMeasure",
    "securityDataflowChecked" : true
  },
  "notAnalysedVersionCount" : 6,
  "lastAnalysis" : {
    "duration" : 5,
    "versionTitle" : "versionTitle",
    "dynamicLinkCount" : 5,
    "versionName" : "versionName",
    "notReviewedDynamicLinkCount" : 2
  },
  "snapshotCount" : 1
}
```

### /api/applications/{guid}/technologies

**GET** | Get the list of technologies for an application

**Status**: 200 (OK), 404 (Not Found)

**Response**: 

```json
[ "sql", "ua" ]
```

### /api/applications/{guid}/license

**GET** | Get License information for an application

**Status**: 200 (OK), 404 (Not Found)

**Response**: 

```json
{
  "license" : "mylicense;myCompany",
  "licenceType": anyOf("unknown", "aip", "efp", "sec", "imaging", "insight"),
  "licenseKind": anyOf("unkown", "legacy_aip", "scope_cd", "scope_app", "named_user"),
  "licensedTo" : "myCompany",
  "expirationDate" : "2000-01-23"
}
```

**PUT** | Update the license for an application

**Request**: 

```json
{
	"license": "String"
}
```

**Status**: 200 (OK), 404 (Not Found): application with given GUID was not found, 412 (PRECONDITION_FAILED): the license is either invalid, expired, not a scoped application license or the name of the application doesn't match the license

**Response**: `empty()`

### /api/applications/{guid}/onboarding

**GET** | Get onboarding information for an application

**Status**: 200 (OK), 404 (Not Found): application with given GUID was not found

**Response**: 

```json
{
  "estimatedSnapshotTime" : 3,
  "databaseName" : "databaseName",
  "estimatedAnalysisTime" : 9,
  "versionName" : "versionName",
  "versionGuid" : "versionGuid",
  "targetNode" : "targetNode",
  "profilerScanned" : true,
  "estimatedImagingTime" : 2,
  "caipVersion" : "caipVersion",
  "nbFiles" : 0,
  "nbLoc" : 1,
  "applicationSize" : 6,
  "alertsValidated" : true,
  "configurationDone" : true,
  "sourcePath" : "sourcePath",
  "discoveryDone" : true,
  "estimatedTime" : 7,
  "createdDate" : "2000-01-23T04:56:07.000+00:00",
  "totalNbLoc" : 2,
  "estimatedWeight" : "estimatedWeight",
  "totalNbFiles" : 5,
  "guid" : "guid",
  "deliveryConfigGuid" : "deliveryConfigGuid",
  "objectives" : [ "global_risk", "global_risk" ],
  "totalApplicationSize" : 5,
  "status" : "unknown"
}
```

### /api/applications/{guid}/onboarding/initial-counts

**GET** | Get the initial count of elements from the application, separated by categories

**Status**: 200 (OK), 404 (Not Found): application with given GUID was not found

**Response**: 

```json
{
  "data": [
    {
      "excludedCount": 0,
      "includedCount": 1,
      "technology": "Eclipse Project"
    },
    {
      "excludedCount": 0,
      "includedCount": 9,
      "technology": "Java Libraries"
    },
    {
      "excludedCount": 0,
      "includedCount": 1,
      "technology": "Java Web App Information"
    },
    {
      "excludedCount": 0,
      "includedCount": 1,
      "technology": "Tag Library Descriptor File"
    }
  ],
  "markup": [
    {
      "excludedCount": 0,
      "includedCount": 1,
      "technology": "HTML"
    },
    {
      "excludedCount": 0,
      "includedCount": 2,
      "technology": "JSF Tiles Definition"
    }
  ],
  "programming": [
    {
      "excludedCount": 0,
      "includedCount": 13,
      "technology": "Java"
    },
    {
      "excludedCount": 0,
      "includedCount": 15,
      "technology": "Java Server Pages"
    }
  ],
  "prose": [
    {
      "excludedCount": 0,
      "includedCount": 1,
      "technology": "Text"
    }
  ],
  "unknown": [
    {
      "excludedCount": 0,
      "includedCount": 3,
      "technology": ".prefs"
    }
  ]
}
```

### /api/applications/{guid}/onboarding/file-tree

**GET** | Returns a tree of the elements for the onboarded application

**Status**: 200 (OK), 404 (Not Found): Application with given GUID was not found

**Response**: 

```json
{
    "DB": null,
    "J2EE":
    {
        ".settings": null,
        "build":
        {
            "classes": null
        },
        "src":
        {
            "com":
            {
                "cast":
                {
                    "monster_event":
                    {
                        "base":
                        {
                            "dao": null,
                            "model": null
                        }
                    }
                }
            },
            "META-INF": null
        },
        "WebContent":
        {
            "jsp": null,
            "META-INF": null,
            "WEB-INF":
            {
                "lib": null
            }
        }
    }
}
```

### /api/applications/{guid}/onboarding/initial-files-list

**GET** | Get a list of the files for the onboarded application

**Status**: 200 (OK), 404 (Not Found): application with given GUID was not found

**Response**: 

```json
[
  {
    "excluded": false,
    "exclusionPattern": null,
    "relativePath": "\\DB\\VASTCreateTable.4.uax"
  },
  {
    "excluded": false,
    "exclusionPattern": null,
    "relativePath": "\\J2EE\\.settings\\org.eclipse.jdt.core.prefs"
  },
  {
    "excluded": false,
    "exclusionPattern": null,
    "relativePath": "\\DB\\VASTCreateIndex.3.src"
  }
]
```

### /api/applications/{guid}/onboarding/d2-architecture

**GET** | Get the D2 Architecture image in SVG Format. This endpoint returns a downloadable file, using `content-disposition: attachment`.

**Status**: 200 (OK), 404 (Not Found): application with given GUID was not found

**Content-Type**: `image/svg+xml`

**Content-Disposition**: `attachment`

### /api/applications/{guid}/onboarding/d2-technotree

**GET** | Get the D2 Technology Tree

**Status**: 200 (OK), 404 (Not Found): application with given GUID was not found

**Response**: 

```json
{
  "technoPathIcon": {
    "[JEE]": "java",
    "[JEE].[Spring]": "spring",
    "[JEE].[Spring].[Spring Data]": "spring",
    "[XML]": null
  },
  "technoRoleMap": [
    {
      "iconNames": [
        "jsp"
      ],
      "name": "Java Server Pages",
      "roles": [
        "presentation",
        "language"
      ],
      "technologyPaths": [
        "[JEE]"
      ],
      "type": "extension"
    },
    {
      "name": "@Entity",
      "roles": [
        "annotation",
        "persistence:orm"
      ],
      "technologyPaths": [
        "[JEE].[Spring].[Spring Data]"
      ],
      "type": "annotation"
    },
    {
      "iconNames": [
        "xml"
      ],
      "name": "XML",
      "roles": [
        "language"
      ],
      "technologyPaths": [
        "[XML]"
      ],
      "type": "extension"
    }
  ]
}
```

### /api/applications/{guid}/onboarding/composition

**GET** | Retrieve a list of the elements found in the application, with detail regarding the number of files found as well as information for UI rendering

**Status**: 200 (OK), 404 (Not Found): application with given GUID was not found

**Response**:

```json
[ {
    "color": "rgb(176, 114, 1)",
    "extension": ".jav",
    "isBinary": false,
    "language": "Java",
    "nbBytes": 40243,
    "nbFiles": 13,
    "nbLocs": 1523,
    "nbNonUTF8Files": 0,
    "primaryExtension": ".java",
    "subExtensions": [],
    "type": "programming"
  }, {
    "color": "rgb(194, 105, 105)",
    "extension": ".castextraction",
    "isBinary": false,
    "language": "CASTExtraction",
    "nbBytes": 102724,
    "nbFiles": 34,
    "nbLocs": 1811,
    "nbNonUTF8Files": 0,
    "primaryExtension": ".uax",
    "subExtensions": [
      ".src",
      ".uaxdirectory"
    ],
    "type": "data"
  } ]
```

### /api/applications/{guid}/onboarding/techno-counts

**GET** | Returns a list of all technologies and their associated counts of files, as well as whether they are supported by CAST or not

**Status**: 200 (OK), 404 (Not Found): application with given GUID was not found

**Response**: 

```json
[ {
  "nbBytes" : 1,
  "color" : "color",
  "nbFiles" : 0,
  "nbNonUtf8Files" : 5,
  "available" : true,
  "nbLocs" : 6,
  "technology" : "technology",
  "ratioFilesUtf8" : 5.637376656633329,
  "status" : "not_supported"
}, {
  "nbBytes" : 1,
  "color" : "color",
  "nbFiles" : 0,
  "nbNonUtf8Files" : 5,
  "available" : true,
  "nbLocs" : 6,
  "technology" : "technology",
  "ratioFilesUtf8" : 5.637376656633329,
  "status" : "not_supported"
} ]
```

### /api/applications/{guid}/analysis-report?versionGuid=${guid}

**GET** | Retrieve the analysis report for the given Application GUID and Version GUID (provided as a query parameter)

**Response**: 

```json
[
    {
        "fileInfo": {
            "castExtension": "com.castsoftware.html5",
            "extensions": [
                "jsp"
            ],
            "primaryExtension": "jsp",
            "technoType": "programming",
            "technology": "Java Server Pages"
        },
        "nbAnalyzedFiles": 30,
        "nbExcludedFiles": 0,
        "nbFoundFiles": 0,
        "nbPartiallyAnalyzedFiles": 0,
        "nbProcessedFiles": 15,
        "nbSkippedFiles": 0,
        "nbUnanalyzedFiles": 0,
        "nbUnresolvedFiles": 0
    },
    {
        "fileInfo": {
            "castExtension": "com.castsoftware.jee",
            "extensions": [
                "java"
            ],
            "primaryExtension": "java",
            "technoType": "programming",
            "technology": "Java"
        },
        "nbAnalyzedFiles": 13,
        "nbExcludedFiles": 0,
        "nbFoundFiles": 0,
        "nbPartiallyAnalyzedFiles": 0,
        "nbProcessedFiles": 13,
        "nbSkippedFiles": 0,
        "nbUnanalyzedFiles": 0,
        "nbUnresolvedFiles": 0
    },
    {
        "fileInfo": {
            "castExtension": "com.castsoftware.html5",
            "extensions": [
                "html"
            ],
            "primaryExtension": "html",
            "technoType": "markup",
            "technology": "HTML"
        },
        "nbAnalyzedFiles": 2,
        "nbExcludedFiles": 0,
        "nbFoundFiles": 0,
        "nbPartiallyAnalyzedFiles": 0,
        "nbProcessedFiles": 1,
        "nbSkippedFiles": 0,
        "nbUnanalyzedFiles": 0,
        "nbUnresolvedFiles": 0
    }
]
```

### /api/applications/{guid}/analysis-report/log-info?versionGuid={guid}

**GET** | Retrieve the log information for a given analysis-report; i.e. gives some job details as well as the main log associated with it

**Response**: 

```json
{
    "jobActionLabelType": [
        "analyze",
        "publish"
    ],
    "jobExecutionGuid": "7ea51a7b-8092-4d5d-857c-c46af7248db1",
    "logTask": "analyze",
    "logType": "MAIN_LOG",
    "subLogName": "0-analyze"
}
```

### /api/applications/{guid}/versions

**GET** | Get the list of versions for a given application

**Response**: 

```json
[
  {
    "acModelConfigurationChanged": false,
    "acModelsSynchronized": true,
    "analysisDataPrepared": true,
    "analysisUnitStatusChanged": false,
    "configurationChanged": false,
    "currentVersion": true,
    "dataConfigurationChanged": false,
    "extensionAutoConfigEnabled": true,
    "guid": "105b279d-f765-4b90-a0f4-976128389779",
    "imagingDone": false,
    "name": "My Version",
    "sourcePath": "upload:840_monster/main_sources",
    "sourcesConfigurationChanged": false,
    "status": "analyzed",
    "title": "My Version",
    "versionDate": "2024-10-15T15:20:00.000Z"
  }
]
```

### /api/applications/{guid}/versions/{guid}/architecture

**GET** | Returns a dot graph, representing the architecture of the application

**Response**: 

```json
{
  "discovered": "digraph G {\n  rankdir=LR;\n  graph [fontsize=10 fontname=\"Verdana\" compound=true];\n    // framework:hibernate\n  subgraph cluster_Java {label = \"Java\"; Java [comment=\"count=13\"]; subgraph cluster_JavaPersistence { label=\"Java Persistence\"; Hibernate;}} subgraph cluster_Storage {label=\"Storage\"; SQL [comment=\"count=0\"];} Java -\u003E Hibernate -\u003E SQL;\n// framework:jpa\n  subgraph cluster_Java {label = \"Java\"; Java [comment=\"count=13\"]; subgraph cluster_JavaPersistence { label=\"Java Persistence\"; JPA;}} subgraph cluster_Storage {label=\"Storage\"; SQL [comment=\"count=0\"];} Java -\u003E JPA; JPA -\u003E SQL;\n// language:html\n  subgraph cluster_Web {label=\"Web\"; HTML [comment=\"count=1\"];} \n// language:java\n  Java [comment=\"count=13\"];\n}"
}
```

### /api/applications/{guid}/versions/{guid}/detailed-frameworks-per-classification

**GET** | Retrieve a map of categories containing a list of framework found during analysis

**Response**: 

```json
{
  "storage/sql/client": [
    {
      "available": true,
      "framework": "jpa",
      "status": "supported_by_cast"
    },
    {
      "available": true,
      "framework": "hibernate",
      "status": "supported_by_cast"
    }
  ]
}
```

### /api/applications/{guid}/versions/{guid}/estimations

**GET** | Retrieve the estimations for the duration of various jobs for the given application and version

**Response**: 

```json
{
  "estimationDto": {
    "analyzeEstimation": 181.18,
    "imagingEstimation": 155.81,
    "snapshotEstimation": 187.87
  },
  "versionProcessHistoryDto": {
    "analysisDuration": 242743,
    "analysisStartTime": "2024-10-15T15:38:08.346Z",
    "currentVersion": true,
    "snapshotDuration": 159426,
    "snapshotStartTime": "2024-10-15T15:42:11.096Z"
  }
}
```

### /api/applications/{guid}/versions/{guid}/extensions

**GET** | Retrieve the list of extensions for the given application and version

**Response**: 

```json
{
  "autoConfigEnabled": true,
  "extensions": [
  {
    "author": "CAST Product",
    "autoUpdateEnabled": null,
    "available": true,
    "blacklisted": null,
    "description": "This extension provides system level rules for several of the main technologies supported by CAST AIP. These rules are provided as an addition to the rules already present for each of these technologies.",
    "forceInstalled": null,
    "iconUrl": "<https://raw.githubusercontent.com/CAST-Extend/resources/master/com.castsoftware.systemlevelrules.png>",
    "id": "com.castsoftware.systemlevelrules",
    "installed": true,
    "installedVersion": "1.1.4-funcrel",
    "latestVersion": true,
    "lockedVersion": "1.1.4-funcrel",
    "minAipVersion": "8.3.0",
    "modifiedDate": null,
    "origin": "discovered",
    "prerelease": true,
    "serverSide": true,
    "supported": false,
    "title": "System Level Rules",
    "updateAvailable": false,
    "updateCandidate": "1.1.4-funcrel",
    "version": "1.1.4-funcrel"
  },
  {
    "author": "CAST Product",
    "autoUpdateEnabled": null,
    "available": true,
    "blacklisted": null,
    "description": "The extension provides support for Data Privacy policy on data sensitivity indicators (like GDRP and PCI-DSS) and support for column's access links with the SQL Analyzer.",
    "forceInstalled": null,
    "iconUrl": "<https://raw.githubusercontent.com/CAST-Extend/resources/master/com.castsoftware.datacolumnaccess.png>",
    "id": "com.castsoftware.datacolumnaccess",
    "installed": false,
    "installedVersion": null,
    "latestVersion": true,
    "lockedVersion": "2.2.2-funcrel",
    "minAipVersion": "8.0.0",
    "modifiedDate": null,
    "origin": "custom",
    "prerelease": true,
    "serverSide": true,
    "supported": false,
    "title": "RDMS Data Sensitivity and Column Access",
    "updateAvailable": false,
    "updateCandidate": "2.2.3-funcrel",
    "version": "2.2.3-funcrel"
  }]
}
```

**PUT**

**Request**: 

```json
{
	"id": "",
	"version": "",
	"origin": anyOf("shipped", "discovered", "force_install", "custom", "installed")
	"installed": false,
	"installedVersion": "",
	"available": false,
	"supported": false,
	"autoUpdateEnabled": false,
	"serverSide": false
}
```

### /api/applications/{guid}/versions/{guid}/extensions/installed

**GET** | Retrieve the list of installed extensions for the given application and version

**Response**:

```json
{
  "autoConfigEnabled": true,
  "extensions": [
  {
    "author": "CAST Product",
    "autoUpdateEnabled": null,
    "available": true,
    "blacklisted": null,
    "description": "This extension provides system level rules for several of the main technologies supported by CAST AIP. These rules are provided as an addition to the rules already present for each of these technologies.",
    "forceInstalled": null,
    "iconUrl": "<https://raw.githubusercontent.com/CAST-Extend/resources/master/com.castsoftware.systemlevelrules.png>",
    "id": "com.castsoftware.systemlevelrules",
    "installed": true,
    "installedVersion": "1.1.4-funcrel",
    "latestVersion": true,
    "lockedVersion": "1.1.4-funcrel",
    "minAipVersion": "8.3.0",
    "modifiedDate": null,
    "origin": "discovered",
    "prerelease": true,
    "serverSide": true,
    "supported": false,
    "title": "System Level Rules",
    "updateAvailable": false,
    "updateCandidate": "1.1.4-funcrel",
    "version": "1.1.4-funcrel"
  },
  {
    "author": "CAST Product",
    "autoUpdateEnabled": null,
    "available": false,
    "blacklisted": null,
    "description": "Support for discovering database files based projects.",
    "forceInstalled": null,
    "iconUrl": "<https://raw.githubusercontent.com/CAST-Extend/resources/master/com.castsoftware.dmtsqlfilediscoverer.png>",
    "id": "com.castsoftware.dmtsqlfilediscoverer",
    "installed": true,
    "installedVersion": "1.0.5",
    "latestVersion": true,
    "lockedVersion": "1.0.5",
    "minAipVersion": "8.3.17",
    "modifiedDate": null,
    "origin": "installed",
    "prerelease": false,
    "serverSide": true,
    "supported": false,
    "title": "Database File Discoverer",
    "updateAvailable": false,
    "updateCandidate": "1.0.5",
    "version": "1.0.5"
  }]
}
```

### /api/applications/{guid}/snapshots

**GET** | Retrieve the list of snapshots for the given application

**Response**: 

```json
[
  {
    "application": {
      "analysisDuration": 242710,
      "caipVersion": "8.4.0",
      "createdDate": "2024-10-15T15:20:08.269Z",
      "currentVersionGuid": "105b279d-f765-4b90-a0f4-976128389779",
      "domain": null,
      "guid": "e881541d-1df7-4249-af96-362329510863",
      "imagingTenant": null,
      "inPlaceMode": true,
      "latestVersionGuid": "105b279d-f765-4b90-a0f4-976128389779",
      "managed": true,
      "missingDependencies": false,
      "modifiedDate": "2024-10-16T08:50:29.553Z",
      "movingToFastScan": false,
      "movingToFastScanAnalysisPending": false,
      "name": "840_monster",
      "onboarded": true,
      "onboardedNotAnalyzed": false,
      "schemaPrefix": "uuid_e881541d_1df7_4249_af96_362329510863",
      "targetNode": null,
      "title": "840_monster"
    },
    "captureDate": "2024-10-15T15:38:00.000Z",
    "createdDate": "2024-10-15T15:44:48.668Z",
    "guid": "60a1bfdc-3443-4df5-8606-3b73ccd3114e",
    "modifiedDate": "2024-10-15T15:44:48.747Z",
    "name": "Snapshot-202410151738",
    "title": "Snapshot-202410151738",
    "versionName": "My Version"
  },
  {
    "application": {
      "analysisDuration": 242710,
      "caipVersion": "8.4.0",
      "createdDate": "2024-10-15T15:20:08.269Z",
      "currentVersionGuid": "105b279d-f765-4b90-a0f4-976128389779",
      "domain": null,
      "guid": "e881541d-1df7-4249-af96-362329510863",
      "imagingTenant": null,
      "inPlaceMode": true,
      "latestVersionGuid": "105b279d-f765-4b90-a0f4-976128389779",
      "managed": true,
      "missingDependencies": false,
      "modifiedDate": "2024-10-16T08:50:29.553Z",
      "movingToFastScan": false,
      "movingToFastScanAnalysisPending": false,
      "name": "840_monster",
      "onboarded": true,
      "onboardedNotAnalyzed": false,
      "schemaPrefix": "uuid_e881541d_1df7_4249_af96_362329510863",
      "targetNode": null,
      "title": "840_monster"
    },
    "captureDate": "2024-10-15T15:21:00.000Z",
    "createdDate": "2024-10-15T15:35:53.788Z",
    "guid": "10773487-61a2-4c5e-866a-71e95193484a",
    "modifiedDate": "2024-10-15T15:35:54.053Z",
    "name": "Snapshot-202410151721",
    "title": "Snapshot-202410151721",
    "versionName": "My Version"
  }
]
```

### /api/applications/{guid}/snapshots/{guid}/indicators

**GET** | Retrieve indicators for the given application and snapshot

**Response**: 

```json
[
  {
    "alertMinThreshold": 0,
    "category": "Transactions",
    "description": "Percentage of incomplete transactions (that do not access any data entity and end point).\n\tAssociated details contain transactions that does not access any data entity or end point.",
    "fileName": "fp_empty_transactions.csv",
    "id": "fp_empty_transactions",
    "justification": null,
    "name": "Incomplete transactions",
    "remedy": "Review data entities and associated transaction configuration rules.\nInvestigate the incomplete transactions and justify if they are valid incomplete or not.\nIf possible, add custom end points to make the incomplete transactions valid. ",
    "status": 2,
    "thresholds": {
      "thresholds": [
        {
          "conditions": [
            "\u003C=10"
          ],
          "id": "3"
        },
        {
          "conditions": [
            "\u003E10 and \u003C=30"
          ],
          "id": "2"
        },
        {
          "conditions": [
            "\u003E30 and \u003C=40"
          ],
          "id": "1"
        }
      ]
    },
    "type": "percentage",
    "value": 12.5
  },
  {
    "alertMinThreshold": 0,
    "category": "Transactions",
    "description": "Percentage of data entities accessed by transactions.\n\tAssociated details contain list of data entities not accessed by any transaction. ",
    "fileName": "fp_data_used.csv",
    "id": "fp_data_used",
    "justification": null,
    "name": "Data entities used by transactions",
    "remedy": "Review the source code delivery with regards to the transaction configuration.\nCheck missing links and validate with application team if it is normal these data entities are not accessed by any transactions.",
    "status": 2,
    "thresholds": {
      "thresholds": [
        {
          "conditions": [
            "\u003E=90"
          ],
          "id": "3"
        },
        {
          "conditions": [
            "\u003E=70 and \u003C90"
          ],
          "id": "2"
        },
        {
          "conditions": [
            "\u003E=50 and \u003C70"
          ],
          "id": "1"
        }
      ]
    },
    "type": "percentage",
    "value": 87.5
  }
]
```

**PUT**

**Request**: 

```json
{
	"id": "",
	"justification": ""
}
```

### /api/applications/{guid}/module-options

**GET** | Retrieve the module configuration for the given application

**Response**: 

```json
{
  "emptyModulesIgnored": true,
  "generationType": "full_content",
  "modules": [
    {
      "generationType": "full_content",
      "guid": "8c01cb4b-928e-42fa-86b9-40e9a86b35b7",
      "name": "840_monster full content",
      "description": "description"
    }
  ]
}
```

### /api/applications/{guid}/dependencies

**GET** | Get the dependencies for the given application

**Response**: 

```json
[
  {
    "guid": "ae273ba1-49b2-49a8-8bbf-7f322f0c0005",
    "origin": "custom",
    "source": {
      "technology": "jee",
      "type": "Techno"
    },
    "target": {
      "name": "Main_sources-Database",
      "technology": "ua",
      "type": "Analysis Unit"
    }
  }
]
```

**POST**

**Request**: 

```json
{
  "sourceTechnologyName": "jee",
  "sourceAnalysisUnitGuid": "",
  "targetTechnologyName": "mainframe",
  "targetAnalysisUnitGuid": ""
}
```

**Response**: 

```json
{
  "origin" : "default",
  "guid" : guid(),
  "source" : {
    "name" : "name",
    "technology" : "technology",
    "type" : "type"
  }
}
```

### /api/applications/{guid}/technologies/jee

**GET** | Get the technology configuration for JEE for the given application

**Response**: 

```json
{
  "classpaths": [],
  "configuration": {
    "clientFileExtensions": "*.htc;*.htm;*.html;*.js;*.vbs",
    "ejbUsage": "N/A",
    "enableWebAnalysisUsage": true,
    "hibernateUsage": "latest",
    "javaFileExtensions": "*.java;*.sqlj",
    "javaVersion": "latest",
    "jspFileExtensions": "*.jsp;*.xhtml",
    "servletJspStandardVersion": "latest",
    "springUsage": "latest",
    "strutsUsage": "latest",
    "wbsUsage": "N/A",
    "xmlFileExtensions": "*.wsdd;*.wsdl;*.xml"
  }
}
```

### /api/applications/{guid}/technologies/jee/analysis-units

**GET** | Get the list of JEE analysis units for the given application

**Response**: 

```json
[
  {
    "active": true,
    "guid": "c04a4c31-c22b-4f2d-99b2-0eb23f1a9542",
    "name": "Monster_Events_JSP",
    "projectPath": "upload:840_monster/main_sources/J2EE/.project",
    "technology": "jee",
    "type": "default"
  }
]
```

### /api/applications/{guid}/technologies/ua

**GET**

**Response**: `empty()`

### /api/applications/{guid}/technologies/ua/analysis-units

**GET** | Get the list of analysis units for Universal Analyzer Technologies

**Response**: 

```json
[
  {
    "active": true,
    "guid": "fccbb3fc-41c0-4b7b-9aa9-8a3fa9093848",
    "name": "HTML5",
    "projectPath": "upload:840_monster/main_sources",
    "technology": "ua",
    "type": "default"
  },
  {
    "active": true,
    "guid": "47400a6b-bfab-4bda-8feb-4cc70fa4314f",
    "name": "Main_sources-Database",
    "projectPath": "upload:840_monster/main_sources",
    "technology": "ua",
    "type": "default"
  }
]
```

**POST**

### /api/applications/{guid}/packages

**GET** | Get the list of packages in the application

**Response**: 

```json
[
  {
    "guid": "c6669f85-e9a8-4440-886a-cfe45fb94aa5",
    "name": "main_sources"
  }
]
```

### /api/applications/{guid}/languages

**GET** | Get the list of languages for the given application

**Response**: 

```json
[
  {
    "internalName": "HTML5",
    "name": "HTML5/Javascript"
  },
  {
    "internalName": "SQLScript",
    "name": "SQL"
  }
]
```

### /api/jobs/{guid}

**GET** | Get information about a given job from its GUID

**Response**: 

```json
{
  "createdDate": "2024-10-15T15:38:08.323Z",
  "currentPhase": "publish",
  "currentStep": "upload_application",
  "endTime": "2024-10-15T15:45:06.407Z",
  "guid": "7ea51a7b-8092-4d5d-857c-c46af7248db1",
  "jobActionLabelType": [
    "analyze",
    "publish"
  ],
  "jobParameters": {
    "analysisEstimation": "216",
    "appGuid": "e881541d-1df7-4249-af96-362329510863",
    "appName": "840_monster",
    "appTitle": "840_monster",
    "backupApplication": "false",
    "deleteVersion": "false",
    "deliveryConfigGuid": "d23c2bd4-8fa8-44f7-82ce-361c72245ea4",
    "diskSpace": "510743040000",
    "endStep": "snapshot",
    "errorRecoveryInfo": "no_step",
    "extensionAutoConfigEnabled": "true",
    "freeMemory": "11006730240",
    "imagingEstimation": "160",
    "inPlaceMode": "true",
    "includeFastScan": "false",
    "jobStepsEnabled": "analyze;snapshot;snapshot_indicator;upload_application",
    "mergeSteps": "true",
    "onboardingGuid": "99534eba-5544-407b-b15a-5e1ee086d25c",
    "processImaging": "true",
    "releaseDate": "202410151720",
    "resumeDeepAnalysis": "false",
    "runAnalysis": "true",
    "saveSourceFolderZip": "true",
    "snapshotCaptureDate": "202410151738",
    "snapshotEstimation": "172",
    "snapshotGuid": "60a1bfdc-3443-4df5-8606-3b73ccd3114e",
    "snapshotName": "Snapshot-202410151738",
    "snapshotTitle": "Snapshot-202410151738",
    "sourcePath": "C:\\imaging-node\\data\\shared\\upload\\840_monster\\main_sources",
    "startStep": "analyze",
    "takeSnapshot": "true",
    "updateExtensionsNeeded": "false",
    "uploadApplication": "true",
    "versionGuid": "105b279d-f765-4b90-a0f4-976128389779",
    "versionName": "My Version",
    "versionTitle": "My Version"
  },
  "jobPhaseExecutions": [
    {
      "duration": 242743,
      "endTime": "2024-10-15T15:42:11.090Z",
      "jobStepExecutions": [
        {
          "alreadyExecutedStep": false,
          "duration": 242743,
          "endTime": "2024-10-15T15:42:11.090Z",
          "logFolderPath": "file:///C:/CAST/standalone/data/shared/job-logs/7ea51a7b-8092-4d5d-857c-c46af7248db1/analyze/",
          "logPath": "C:\\imaging-node\\data\\shared\\job-logs\\7ea51a7b-8092-4d5d-857c-c46af7248db1\\analyze\\0-analyze.log",
          "phaseType": "analyze",
          "startTime": "2024-10-15T15:38:08.346Z",
          "stepType": "analyze"
        }
      ],
      "phaseType": "analyze",
      "startTime": "2024-10-15T15:38:08.346Z"
    },
    {
      "duration": 175301,
      "endTime": "2024-10-15T15:45:06.397Z",
      "jobStepExecutions": [
        {
          "alreadyExecutedStep": false,
          "duration": 159426,
          "endTime": "2024-10-15T15:44:50.522Z",
          "logFolderPath": "file:///C:/CAST/standalone/data/shared/job-logs/7ea51a7b-8092-4d5d-857c-c46af7248db1/snapshot/",
          "logPath": "C:\\imaging-node\\data\\shared\\job-logs\\7ea51a7b-8092-4d5d-857c-c46af7248db1\\snapshot\\0-snapshot.log",
          "phaseType": "publish",
          "startTime": "2024-10-15T15:42:11.096Z",
          "stepType": "snapshot"
        },
        {
          "alreadyExecutedStep": false,
          "duration": 945,
          "endTime": "2024-10-15T15:44:51.476Z",
          "logFolderPath": "file:///C:/CAST/standalone/data/shared/job-logs/7ea51a7b-8092-4d5d-857c-c46af7248db1/snapshot_indicator/",
          "logPath": "C:\\imaging-node\\data\\shared\\job-logs\\7ea51a7b-8092-4d5d-857c-c46af7248db1\\snapshot_indicator\\0-snapshot-indicator.log",
          "phaseType": "publish",
          "startTime": "2024-10-15T15:44:50.530Z",
          "stepType": "snapshot_indicator"
        },
        {
          "alreadyExecutedStep": false,
          "duration": 14915,
          "endTime": "2024-10-15T15:45:06.397Z",
          "logFolderPath": "file:///C:/CAST/standalone/data/shared/job-logs/7ea51a7b-8092-4d5d-857c-c46af7248db1/upload_application/",
          "logPath": "C:\\imaging-node\\data\\shared\\job-logs\\7ea51a7b-8092-4d5d-857c-c46af7248db1\\upload_application\\0-upload-application.log",
          "phaseType": "publish",
          "startTime": "2024-10-15T15:44:51.482Z",
          "stepType": "upload_application"
        }
      ],
      "phaseType": "publish",
      "startTime": "2024-10-15T15:42:11.096Z"
    }
  ],
  "jobStepExecutions": [
    {
      "alreadyExecutedStep": false,
      "duration": 242743,
      "endTime": "2024-10-15T15:42:11.090Z",
      "logFolderPath": "file:///C:/CAST/standalone/data/shared/job-logs/7ea51a7b-8092-4d5d-857c-c46af7248db1/analyze/",
      "logPath": "C:\\imaging-node\\data\\shared\\job-logs\\7ea51a7b-8092-4d5d-857c-c46af7248db1\\analyze\\0-analyze.log",
      "phaseType": "analyze",
      "startTime": "2024-10-15T15:38:08.346Z",
      "stepType": "analyze"
    },
    {
      "alreadyExecutedStep": false,
      "duration": 159426,
      "endTime": "2024-10-15T15:44:50.522Z",
      "logFolderPath": "file:///C:/CAST/standalone/data/shared/job-logs/7ea51a7b-8092-4d5d-857c-c46af7248db1/snapshot/",
      "logPath": "C:\\imaging-node\\data\\shared\\job-logs\\7ea51a7b-8092-4d5d-857c-c46af7248db1\\snapshot\\0-snapshot.log",
      "phaseType": "publish",
      "startTime": "2024-10-15T15:42:11.096Z",
      "stepType": "snapshot"
    },
    {
      "alreadyExecutedStep": false,
      "duration": 945,
      "endTime": "2024-10-15T15:44:51.476Z",
      "logFolderPath": "file:///C:/CAST/standalone/data/shared/job-logs/7ea51a7b-8092-4d5d-857c-c46af7248db1/snapshot_indicator/",
      "logPath": "C:\\imaging-node\\data\\shared\\job-logs\\7ea51a7b-8092-4d5d-857c-c46af7248db1\\snapshot_indicator\\0-snapshot-indicator.log",
      "phaseType": "publish",
      "startTime": "2024-10-15T15:44:50.530Z",
      "stepType": "snapshot_indicator"
    },
    {
      "alreadyExecutedStep": false,
      "duration": 14915,
      "endTime": "2024-10-15T15:45:06.397Z",
      "logFolderPath": "file:///C:/CAST/standalone/data/shared/job-logs/7ea51a7b-8092-4d5d-857c-c46af7248db1/upload_application/",
      "logPath": "C:\\imaging-node\\data\\shared\\job-logs\\7ea51a7b-8092-4d5d-857c-c46af7248db1\\upload_application\\0-upload-application.log",
      "phaseType": "publish",
      "startTime": "2024-10-15T15:44:51.482Z",
      "stepType": "upload_application"
    }
  ],
  "jobType": "deep_analysis",
  "modifiedDate": "2024-10-15T15:45:06.410Z",
  "startTime": "2024-10-15T15:38:08.343Z",
  "startedBy": "admin",
  "startedOn": "aipnodeapp",
  "state": "completed"
}
```

### /api/jobs/{guid}/logs/download

**GET** | Download a zip file containing all the logs of the given job

**Content-Type**: `binary/octet-stream`

**Content-Disposition**: `attachment;filename=file.zip`

**Response**: Zip binary

### /api/jobs/{guid}/steps/{stepType}/logs/download

**GET** | Download a zip file containing all the logs of the given Step for the job

{{% alert color="info" %}}The `stepType` parameter must be a `stepType` from the list of `jobExecutionSteps` job.{{% /alert %}}

**Content-Type**: `binary/octet-stream`

**Content-Disposition**: `attachment;filename=file.zip`

**Response**: Zip binary

### /api/jobs/alerts/{guid}

**GET** | Get the alerts for the given job GUID

**Response**: 

```json
{
  "alerts": [
    {
      "alertType": "LOG_MANAGER",
      "description": "The JEE analyzer does not know how to handle the source file. This may occur if the Java version in the analysis configuration is incorrect. Otherwise, this warning may be related to new syntax that is not handled by the analyzer or a bug in the analyzer itself.",
      "detailsUrl": null,
      "level": "WARNING",
      "matchedAlert": "JAVA044",
      "messages": [
        {
          "file": "14-jeeanalyzer",
          "lineNumber": 597,
          "message": "JAVA044: Syntax not  recognized :",
          "messageTime": "2024-10-15T15:39:40.889Z"
        }
      ],
      "phaseType": "analyze",
      "remediation": "Please check that the correct Java version is set in the analysis configuration. Otherwise, please contact CAST Support if the syntax is valid.",
      "stepName": "analyze",
      "title": "Syntax not recognized"
    },
    {
      "alertType": "INDICATOR_ALERT",
      "description": "Analysis indicators are computed at the end of an analysis and after a snapshot has been taken. They are related to particular results and their score provides insights on the accuracy and the quality of these results.",
      "detailsUrl": null,
      "level": "WARNING",
      "matchedAlert": null,
      "messages": [
        {
          "file": null,
          "lineNumber": 0,
          "message": "Following indicators returned a low score:\nDynamic Links reviewed: 0.0%\nTechnologies not interacting with others: 1.0\nAEFP/AEP variation: 54.72%",
          "messageTime": "2024-10-15T15:44:51.465Z"
        }
      ],
      "phaseType": "publish",
      "remediation": "Go to the Analysis Results Indicators section of the Overview page. \nSee the remedy action that is proposed with each Analysis Indicator to fix the issue a low score denotes.",
      "stepName": "snapshot_indicator",
      "title": "Analysis indicator with a low score"
    }
  ],
  "size": 2
}
```
