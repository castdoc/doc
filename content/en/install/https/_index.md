---
title: "HTTPS configuration"
linkTitle: "HTTPS configuration"
type: "docs"
weight: 60
no_list: true
---

***

Out of the box CAST Imaging is deployed using **HTTP** - in other words, end-users will connect to CAST Imaging over an insecure HTTP connection. **HTTPS** connections (via TLS) add an extra layer of browsing security, ensuring that data passed through CAST Imaging has been encrypted and is unreadable by third-parties. In addition, an **HTTPS** connection is a prerequisite for configuring [SAML](../authentication/saml/) authentication.

{{< cardpanehome >}}
{{% cardhomereference %}}
<i class="fa-brands fa-windows"></i>&nbsp;[Microsoft Windows](windows/)
{{% /cardhomereference %}}
{{% cardhomereference %}}
<i class="fa-brands fa-docker"></i>/<i class="fa-brands fa-linux"></i>&nbsp;[Linux via Docker](docker/)
{{% /cardhomereference %}}
{{< /cardpanehome >}}
