---
title: "HTTPS configuration for Linux via Docker"
linkTitle: "Linux via Docker"
type: "docs"
weight: 20
---

***

## Overview

Configuring **HTTPS** for Linux via Docker installations involves the use of an [NGINX web server](https://nginx.org/) in **reverse proxy mode** placed in front of the various CAST Imaging components:

- NGINX can be installed on a dedicated machine or on an existing machine where CAST Imaging Docker containers are already running.
- All requests for CAST Imaging v3 services will be handled by NGINX and forwarded to the relevant service.
- The public/private key pair is installed on NGINX.
- Some minor changes are required to CAST Imaging Docker containers, detailed in the instructions below.

## Step 1 - generate public/private keys

A **public** (e.g.: `public_key.crt`) and **private** (e.g. `private_key.key`) key pair in `PEM` format is required by NGINX and CAST highly recommends that you avoid using self-signed certificates - instead you should use certificates signed by a CA (Certificate Authority).

To generate a **private** key and a **CSR** (certificate signing request) which can be sent to a Certificate Authority, run the following command on the machine where NGINX is installed, with elevated permissions (see also [https://docs.openssl.org/1.1.1/man1/req/](https://docs.openssl.org/1.1.1/man1/req/)): 

```
openssl req -new -newkey rsa:2048 -nodes -keyout private_key.key -out csr.csr
```
 
Where: 
- `req` - indicates that we want a CSR
- `-new` - generates a brand new CSR 
- `-newkey` - generates a new private key
- `rsa:2048` - generates a 2048-bit RSA private key
- `-nodes` - no DES, meaning do not encrypt the private key in a PKCS#12 file
- `-keyout` - defines the name of the private key file
- `-out` - defines the name of the CSR file

You can of course, change any of the parameters to match your own requirements, these are simply suggestions. Ensure that when prompted you fill in the correct FQDN (fully qualified domain name) matching your host machine: this will be used by end users to access CAST Imaging. Retain the `private_key.key` file and use the contents of the `csr.csr` file to generate the `public_key.crt` at your chosen Certificate Authority.

## Step 2 - Install NGINX

Install NGINX: this is beyond the scope of this document, instead please consult the following third-party documentation: [https://nginx.org/en/docs/install.html](https://nginx.org/en/docs/install.html).

## Step 3 - Configure NGINX

When NGINX is installed, you now need to configure it. There are two parts to this process:

- configure the HTTPS element
- configure the reverse proxy

### Step 3a - Configure HTTPS

{{% alert color="info" %}}See also [Configuring HTTPS servers](https://nginx.org/en/docs/http/configuring_https_servers.html) for more information about HTTPS setup with NGINX.{{% /alert %}}

Out of the box, NGINX contains one "vHost" called "default", serving various default files on port 80. CAST recommends creating a new vHost and using this specifically for the HTTPS requirements. To add a new vHost, create a new empty file in the following location on disk, in our example the file is named with the machine's FQDN **imaging.corp.domain.com**:

```text
nano /etc/nginx/sites-available/imaging.corp.domain.com
```

Paste in the following `server` block configuration to define the HTTPS protocol and then save the file:

```text
server {
    listen 443 ssl;
    server_name imaging.corp.domain.com;
    server_tokens off;
    ssl_certificate path/to/public_key.crt;
    ssl_certificate_key path/to/private_key.key;
    ssl_protocols TLSv1.2;
    ssl_ciphers !EDH:!RC4:!ADH:!DSS:HIGH:+AES128:+AES256-SHA256:+AES128-
SHA256:+SHA:!3DES:!NULL:!aNULL:!eNULL;
    ssl_prefer_server_ciphers on;
    ssl_ecdh_curve secp384r1;

    location / {

    }
}
```

Where: 
- `listen 443 ssl` - indicates that we want NGINX to listen on all ipv4 addresses on the server, on port 443 
- `server_name` - indicates the FQDN on which NGINX will listen for incoming connections
- `server_tokens off` - disables emitting NGINX version numbers on error pages and in the "`Server`" response header field.
- `ssl_certificate` / `ssl_certificate_key` - paths to the `public_key.crt` and `private_key.ke`y files on the local disk
- `ssl_protocols` - specifies the protocol to use, a minimum of `TLSv1.2` is recommended
- `ssl_ciphers` - sets the SSL ciphers to use. This is only a recommendation and you should choose your own ciphers
- `ssl_prefer_server_ciphers` - specifies that server ciphers should be preferred over client ciphers
- `ssl_ecdh_curve` - specifies a curve for ECDHE ciphers (if you use them)

### Step 3b - Configure reverse proxy

The next step involves configuring NGINX to function in reverse proxy mode, i.e. to accept incoming connections and forward them to the relevant CAST Imaging services.

Edit the vHost configuration file (the same one you created in the previous step), add the following parameters to the `location / {}` block and then save the file:

```text
location / {
	proxy_http_version 1.1;
	proxy_buffering off;
	proxy_cache off;
    proxy_pass http://imagingv3_VM.corp.castsoftware.com:8090/;
	proxy_redirect off;
    proxy_set_header Connection '';
	proxy_set_header Host $host;
	proxy_set_header X-Forwarded-Proto $scheme;
	proxy_set_header X-Real-IP $remote_addr;
	proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    chunked_transfer_encoding off;
	client_max_body_size 100M;
}
```

Where:

- `proxy_http_version` - sets the HTTP protocol version for proxying, use 1.1
- `proxy_buffering` - enables or disables buffering of responses from the proxied server - set to `off`
- `proxy_pass` - sets the protocol and address/port of the proxied server - set this to the machine on which the CAST Imaging v3 `imaging-services` component is installed, using port `8090` (unless you have customized it). The address can be specified as a domain name or IP address
- `proxy_cache` - defines a shared memory zone used for caching. - set to `off`
- `proxy_set_header` - allows redefining or appending fields to the request header passed to the proxied server - see [https://nginx.org/en/docs/http/ngx_http_proxy_module.html#proxy_set_header](https://nginx.org/en/docs/http/ngx_http_proxy_module.html#proxy_set_header) for more information
- `chunked_transfer_encoding` - allows disabling chunked transfer encoding in HTTP/1.1 - set to `off`
- `client_max_body_size` - sets the maximum allowed size of the client request body. This limit determines the maximum file size that will be accepted: if your users upload ZIP archive files during the application onboarding, set this to an appropriate limit to accept the file

Additionally, you will also need to add various `proxy_set_header` parameters to the `server {}` block created previously:

```text
server {
    ...
    ...
    ...

    proxy_set_header X-Forwarded-For $proxy_protocol_addr; #to forward the original client's IP address
    proxy_set_header X-Forwarded-Proto $scheme; #to forward the original protocol (HTTP or HTTPS)
    proxy_set_header Host $host; #to forward the original host requested by the client

}    
```

{{% alert color="info" %}}Don't forget to ensure that an unhindered path exists between the machine on which NGINX is installed and the machine running the CAST Imaging v3 `imaging-services` component, e.g. appropriate firewall rules/VLAN configuration is in place.{{% /alert %}}

## Step 4 - Update imaging-services SSO (Keycloak) component

To ensure that the `imaging-services` SSO (Keycloak) component functions correctly behind the NGINX reverse proxy, you need to add some entries to the following files:

```text
/cast_imaging_services/docker-compose.yml
/cast_imaging_services/.env
```

These files will be located on disk in the folder created when you unzipped the CAST Imaging v3 Docker installation media (see [Installation on Linux via Docker](../../global/docker/)).

### docker-compose.yml

First add the following lines to the `sso-service:environment` block in the file and then save it:

```yml
KC_HOSTNAME_ADMIN_URL: ${KC_HOSTNAME}
KEYCLOAK_FRONTEND_URL: ${KC_HOSTNAME}
KC_HOSTNAME_STRICT_HTTPS: 'true'
PROXY_ADDRESS_FORWARDING: 'true'
KC_PROXY: 'edge'
```

For example:

![](yml1.jpg)

Next, add the following line to the `auth-service:environment` block in the same file and then save it:

```yml
NGINX_HOST=${NGINX_HOST}
```

For example:

![](yml2.jpg)

### .env

Add the following lines to the end of the file and then save it (where `<my_fqdn>` is the FQDN defined for the parameter `server_name` in the `server` block of the NGINX vHost you created earlier):

```yml
KC_HOSTNAME=https://<my_fqdn>/auth/
NGINX_HOST=https://<my_fqdn>
```

For example:

![](yml3.jpg)

## Step 5 - Restart imaging-services containers

To ensure the changes you have made are taken into account, you should restart the `imaging-services` containers. To do so, issue the following command from the `/cast_imaging_services` folder located on disk in the folder created when you unzipped the CAST Imaging v3 Docker installation media:

```text
docker-compose up -d
```

This command will rebuild only the images/containers with the changes you have implemented.

## Step 6 - Check access and HTTPS

Check that you can access CAST Imaging over HTTPS from the FQDN you declared in the NGINX vHost, e.g. `https://imaging.corp.domain.com`.