---
title: "HTTPS configuration for Microsoft Windows"
linkTitle: "Microsoft Windows"
type: "docs"
weight: 10
---

***

## Overview

Configuring **HTTPS** for Microsoft Windows installations involves modifying various CAST Imaging services that are part of the `imaging-services` component, therefore, all configuration should be actioned on the machine on which this component is installed. The services in question are:

- CAST Imaging SSO Service
- CAST Imaging Gateway Service
- CAST Imaging Authentication Service

In addition, a persistence data schema needs updating.

## Step 1 - generate public/private keys

A **public** (e.g.: `public_key.crt`) and **private** (e.g. `private_key.key`) key pair is required and CAST highly recommends that you avoid using self-signed certificates - instead you should use certificates signed by a CA (Certificate Authority). One certificate key pair can be used for both CAST Services.

To generate a **private** key and a **CSR** (certificate signing request) which can be sent to a Certificate Authority, run the following command on the host machine (see also https://docs.openssl.org/1.1.1/man1/req/): 

```
openssl req -new -newkey rsa:2048 -nodes -keyout private_key.key -out csr.csr
```

Where: 
- `req` - indicates that we want a CSR
- `-new` - generates a brand new CSR 
- `-newkey` - generates a new private key
- `rsa:2048` - generates a 2048-bit RSA mathematical private key
- `-nodes` - no DES, meaning do not encrypt the private key in a PKCS#12 file
- `-keyout` - defines the name of the private key file
- `-out` - defines the name of the CSR file

Retain the `private_key.key` file and use the contents of the `csr.csr` file to generate the `public_key.crt` at your chosen CA.

{{% alert title="Note" color="info" %}}<ul><li>OpenSSL (<a href="https://www.openssl.org/">https://www.openssl.org/</a>) is not shipped with Microsoft Windows, therefore you will need to obtain this tool first. If you have a recent release of <code>git</code> installed on the machine, <code>openssl.exe</code> is provided here: <code>%PROGRAMFILES%\Git\usr\bin</code>.</li><li>OpenSSL generates private keys and CSRs in <code>PEM</code> container format by default.</li><li>Ensure that when prompted you fill in the correct FQDN (fully qualified domain name) matching your host machine.</li></ul>{{% /alert %}}

## Step 2 - Configure CAST Imaging SSO Service

This service can be configured to load the required key pair using files in `PEM` container format or from a `Java Keystore`. When both alternatives are configured, the `PEM` container format takes precedence over the `Java Keystore`. This documentation provides instructions using keys in `PEM` container format.

### Copy key pair to correct location

Copy your key pair (e.g.: `public_key.crt` and `private_key.key`) to the following location:

```
%PROGRAMFILES%\CAST\Imaging\CAST-Imaging-SSO\conf\
```

### Edit keycloak.conf file

Open the following file in a text editor:

```
%PROGRAMFILES%\CAST\Imaging\CAST-Imaging-SSO\conf\keycloak.conf
```

Make the following changes:

- Change the line `http-port=8096` to `https-port=8096`
- Uncomment the line starting `#https-certificate-file` and modify it to point to your `public_key.crt` file, e.g.: `https-certificate-file=${kc.home.dir}conf/public_key.crt`
- Uncomment the line starting `#https-certificate-key-file` and modify it to point to your `private_key.key` file, e.g.: `https-certificate-key-file=${kc.home.dir}conf/private_key.key`

```text
https-port=8096
...
...
...
# HTTP

# The file path to a server certificate or certificate chain in PEM format.
#https-certificate-file=${kc.home.dir}conf/public_key.crt

# The file path to a private key in PEM format.
#https-certificate-key-file=${kc.home.dir}conf/private_key.key
...
...
...
```

Finally, save and close the file.

## Step 3 - Configure CAST Imaging Gateway Service

This service requires that the key pair are stored in a `Java Keystore` (`gateway.jks`). You can re-use the same key pair that you generated in Step 1.

### Convert key pair into PKCS#12 format keystore

Private `.key` files cannot be stored directly in a Java Keystore, therefore you will need to first convert/combine the `public_key.crt` and `private_key.key` files into `PKCS#12` format keystore, which can then be imported into the Java Keystore. To do so, run the following command to generate the `.P12` PKCS#12 format keystore (see also https://www.openssl.org/docs/man1.1.1/man1/openssl-pkcs12.html):

```
openssl pkcs12 -export -in public_key.crt -inkey private_key.key -name gateway -out gateway.p12
```

Where: 
- `pkcs12` - indicates the format of the output file
- `-export` - indicates that we require the creation of a file
- `-in` - defines the name of the public key file
- `-inkey` - defines the name of the private key file
- `-name` - defines a "friendly" name for the combined key pair
- `-out` - defines the name of the resulting .p12 file

{{% alert title="Note" color="info" %}}Running this command will request that you create a password for the `gateway.p12` file. Make a note of the password you choose.{{% /alert %}}

### Import PKCS#12 format keystore into Java format keystore

Run the following command using the `Java keytool` tool. This will import the `gateway.p12` keystore into the Java format keystore:

```
keytool -importkeystore -srckeystore gateway.p12 -destkeystore gateway.jks -srcstoretype PKCS12
```

{{% alert title="Note" color="info" %}}<ul><li>The Java Keytool tool(https://docs.oracle.com/en/java/javase/17/docs/specs/man/keytool.html) is shipped with your Java installation.</li><li>Running this command will request that you create a password for the <code>gateway.jks</code> keystore. Make a note of the password you choose. In addition you will be prompted to enter the password for the <code>gateway.p12</code> keystore you created previously.</li></ul>{{% /alert %}}

### Copy Java keystore to correct location

Copy your Java keystore (e.g.: `gateway.jks`) to the following location:

```
%PROGRAMFILES%\CAST\Imaging\CAST-Imaging-gateway\
```

### Edit application.yml file

Open the following file in a text editor:

```
%PROGRAMFILES%\CAST\Imaging\CAST-Imaging-gateway\application.yml
```

Edit the existing `server` entry to add in the required HTTPS configuration and change the `service, auth` line to https, for example:

```
server:
    forward-headers-strategy: framework
    port: 443
    ssl:
        enabled: true
        key-alias: gateway
        key-store: gateway.jks
        key-store-type: JKS
        key-store-password: <keystore password>
        key-password: <key password>
    service:
        admin-center: http://localhost:8098
        auth: https://localhost:8096
    tomcat:
        remote:
        remote-ip-header: x-forwarded-host
```

Where:
- `server, port` - set this to `443` to ensure your end-users do not need to append a port number to the CAST Imaging access URL
- `server, ssl, enabled` - set this to `true`
- `server, ssl, key-alias` - set this to the friendly name configured when running the OpenSSL tool to convert the key pair into the `PKCS#12` format keystore via the `-name` command
- `server, ssl, key-store-type` - set this to JKS
- `server, ssl, key-store-password` - set this to the password you created when running the Java keytool tool to import the PKCS#12 format keystore into Java format keystore
- `server, ssl, enabled` - set this to the password you created when running the OpenSSL tool to convert the key pair into the `PKCS#12` format keystore
- `server, service, auth` - change the existing `http` protocol to `https` (this line refers to the CAST Imaging SSO Service which is now using HTTPS)

Finally, save and close the file.

## Step 4 - Configure CAST Imaging Authentication Service

Open the following file in a text editor:

```
%PROGRAMFILES%\CAST\Imaging\CAST-Imaging-auth-service\application.yml
```

Find the existing `server` entry and modify the following
-  `service, auth` entry to `https` (this line refers to the CAST Imaging SSO Service which is now using HTTPS)
-  `service, nginx` entry to the fully qualified domain name of the current machine with `https://` protocol

```text
server:
  forward-headers-strategy: framework
  port: 8092
  service:
    admin-center: http://localhost:8098
    auth: https://localhost:8096
    gatewayPort: ${PORT_GATEWAY:8090}
    nginx: https://<FQDN>
  tomcat:
    remoteip:
      remote-ip-header: x-forwarded-host
```

Finally, save and close the file.

## Step 5 - Modify the admin_center schema

Using pgAdmin, connect to the CAST Storage Service/PostgreSQL instance on which the `admin_center` schema is located (this schema is created during the [installation](../global) process).

Locate the `properties` table in the `admin_center` schema and then locate the `keycloak.uri` entry:

![](keycloak_URI.jpg)

The `Value` column contains the URI used to access the **CAST Imaging SSO Service** - you need to change this to ensure that HTTPS is used and remove the port number (assuming you have set the CAST Imaging Gateway Service to use port `443` in Step 3):

![](keycloak_URI_saved.jpg)

Ensure the new value is saved.

## Step 6 - Restart all services and test access

To ensure that all changes are taken in to account, restart all CAST Imaging services on your machine. If you have remote nodes and/or remote CAST Imaging Viewer services on separate machines, these should also be restarted. You can then test access using the secure HTTPS protocol, for example using the machine's FQDN (fully qualified domain name):

```
https://imaging.corp.domain.com
```

## Step 7 - Install additional node(s)

If you need to install an additional node component AFTER having enabled HTTPS, you should ensure that you edit the `SECURED_CONNECTION` installation variable in the `config-analysis-node.conf` file and set it to `true`. This is to ensure that the health check that is actioned at the end of the component installation process to check that the component is "up" and running will use HTTPS. See also [Microsoft Windows installation variables](../global/windows/variables).

## Troubleshooting

### CAST Imaging Gateway Service fails to start when using self-signed certificates

If you have used self-signed `.key` and `.crt` files, you may find that the CAST Imaging Gateway Service fails to start with the error:

```
javax.net.ssl.SSLHandshakeException: PKIX path building failed: sun.security.provider.certpath.SunCertPathBuilderException: unable to find valid certification path to requested target
```

This generally indicates that the Java runtime installed on the local machine cannot verify the self-signed certificate - in other words that the self-signed certificate cannot be found in the java `cacerts` file. This is why CAST highly recommends using certificates that are signed by a CA (certificate authority) instead, as this avoids this problem. To workaround this issue, you need to import your `.crt` file into the Java JRE cacerts file, using keytool:

```
keytool -importcert -trustcacerts -file [path_to_cert_file] -alias [alias] -keystore %JAVA_HOME%\lib\security\cacerts
```