---
title: "Extend Local Server"
linkTitle: "Extend Local Server"
type: "docs"
weight: 70
no_list: true
---

***

## Overview

Do you need to install CAST Imaging in a **secure environment** with **no or very restricted internet access** but still need the full functionality offered by CAST Imaging to download extensions whenever required? If so, **Extend Local Server** could provide what you need:

- an on-premises solution that acts as an intermediary placed between CAST Imaging and CAST's publicly available "Extend" ecosystem (https://extend.castsoftware.com)
- can be used in **"online"** (has a direct connection to https://extend.castsoftware.com and will download any extension requested by CAST Imaging) or **"offline"** mode (no connection to https://extend.castsoftware.com and must be manually populated with extensions)

![](extend_local.jpg)

## How does it work?

### Online mode

Online mode is most suited where CAST Imaging has been deployed in a secure environment, but a connection to CAST's Extend system (https://extend.castsoftware.com) is available. In this scenario, Extend Local Server acts as a proxy to CAST Extend and therefore firewall rules or other mechanisms such as VLANs can be used to allow only Extend Local Server to have access to the internet (and by extension to CAST Extend). Therefore, Extend Local Server:

- will connect every two hours to fetch the extension "manifest" from CAST Extend - i.e. information CAST Imaging requires in order to request the correct extension, for example:
    - available extensions
    - available releases
    - technology to extension mappings
- will download extensions from CAST Extend only if required: download is only triggered when extensions are not already present in the Extend Local Server storage. Installation of required extensions for an analysis will therefore be much faster when extensions are already available in CAST Extend Local Server storage.

![](extend_local_flow.jpg)

### Offline mode

When a connection to CAST's Extend system (https://extend.castsoftware.com) is unavailable (**offline mode**) i.e. secure environment with no internet access, expired maintenance date or not reachable due to network issues:

- if the required extensions are already present in Extend Local Server storage (already downloaded previously from CAST Extend, or manually uploaded), CAST Imaging will be able to continue the analysis process without interruption.
- if the required extensions are NOT present in Extend Local server, then the analysis will fail at the "install extensions" or "update extensions" steps. In this situation required extension will need to be manually uploaded to Extend Local Server.

See also: [Update Extend Local Server with new content](../../administer/extend-local/manual-content-update/).

## Requirements

Extend Local Server is a standalone component and can therefore be installed wherever convenient in your local environment, e.g. on a dedicated machine, or on a machine already used by other CAST Imaging components. It should only be installed once in your environment.

When using a dedicated machine, CAST recommends following the general [hardware](../../requirements/hardware/) and [software](../../requirements/software/) requirements but note that the component **does not** require:

- a powerful CPU nor significant amounts of RAM (it will typically consume around 100MB RAM when running)
- a Java JRE/JDK

Other requirements:

- Extend Local Server runs as a service on **TCP port 8085** (this can be customized). You may therefore need to adjust firewall rules to allow incoming connections to this port
- a minimum of **2GB** free disk space
- a **CAST Extend account** whether running in online or offline mode (you can register here: https://extend.castsoftware.com/#/register)
- in online mode, access to https://extend.castsoftware.com on **port 443** via **TCP**
- the following CAST components will require access to Extend Local Server:
    - analysis-node
    - imaging-services
- if the CAST component accessing Extend Local Server is configured to pass all outgoing connections through a proxy then you may need to whitelist the IP address/host name of the machine running Extend Local Server in order to route connections correctly.

{{% alert color="info"%}}Node.js (v. 10.15.3) is shipped and installed with Extend Local Server.{{% /alert %}}

## What's new?

See the [release notes](../../release-notes/com.castsoftware.aip.extendproxy/).

## Installation

### Microsoft Windows

Download the latest release of Extend Local Server from https://extend.castsoftware.com/#/extension?id=com.castsoftware.aip.extendproxy&version=latest - this is an `.exe` file.

Execute the downloaded `.exe` file and follow the installation wizard. You will be prompted to choose installation folders for executables and for data, as well as the TCP port number on which the server will run (default is port 8085).

On completion, a summary screen is displayed - take note of the information provided (note that this information is also stored in `%PROGRAMFILES%\CAST\Extend\readme.txt`):

- **API key** - this will be required when configuring CAST Imaging to use Extend Local Server.
- **Extend Local Server Administration Center URL** - this URL will be opened in the default browser and allows you to manage the instance.

![](extend_summary.jpg)

#### Silent install

It is possible to run the installer in "silent mode" using the following command line:

```text
com.castsoftware.aip.extendproxy.<release_number>.exe /SP- /VERYSILENT /SUPPRESSMSGBOXES /LOG="<filename>.log" /LOADINF="<filename>.inf"
```

Where:

- `<filename>.log` allows you to specify a path/filename to use for the installation log file (if the file already exists it will be overwritten)
- `<filename>.inf` instructs the installer to load the required settings from the specified file (this file should be defined as listed below):

```text
[Setup]
Lang=english
Dir=C:\Program Files\CAST\Extend
Group=CAST Extend
NoIcons=0
Tasks=
ServerName=my_server_hostname
ServerPort=my_port_number
ExtendMail=my_extend_email
ExtendPassword=my_extend_password
DataDir=C:\ProgramData\CAST\Extend
```

Where:

- `Dir=` the path to the installation location. CAST recommends installing to C:\Program Files\CAST\Extend, but you are free to change this to another location if necessary.
- `ServerName=` the host name of the machine on which you are installing the service
- `ServerPort=` the port number on which the service will run. CAST recommends using `8085`, but you are free to change this if necessary.
- `ExtendMail=` enter your CAST Extend account email address
- `ExtendPassword=` enter your CAST Extend account password corresponding to the email address you entered for `ExtendMail=`

#### What is installed?

On completion of a Microsoft Windows installation, all files related to Extend Local Server are stored in:

```text
%PROGRAMFILES%\CAST\Extend
%PROGRAMDATA%\CAST\Extend
```

A Windows Service called **CAST_ExtendProxy** will be created and set to start automatically. You may need to adjust firewall rules to allow incoming connections on the listening port. Extend Local Server runs under the following executables:

```text
%PROGRAMFILES%\CAST\Extend\tools\nssm.exe
%PROGRAMFILES%\CAST\Extend\NodeJS\node.exe
```

### Linux via Docker

Run the following command to download the image from Docker Hub, create the container and set the required options:

```text
docker run -d --name cast_extendproxy -p <PORT>:8085 -u 0 -v <DATA_FOLDER>:/opt/cast_extend_proxy/data -e host_port=<PORT> -e host_name=<HOST_NAME> -e LOGDEBUG=off castimaging/extend-proxy:latest
```

Where:

- `<PORT>` is the port on which you want to run Extend Local Server - CAST recommends setting this to **8085**.
- `<DATA_FOLDER>` is a folder on the local machine which will be used to persist the configuration data and any extensions that require storing.
- `<HOST_NAME>` is your machine's FQDN (which you can obtain by running `hostname -f`) or IP address.

For example:

```text
docker run -d --name cast_extend_proxy -p 8085:8085 -u 0 -v /shared/extend_data:/opt/cast_extend_proxy/data -e host_port=8085 -e host_name=extend.corp.domain.com -e LOGDEBUG=off castimaging/extend-proxy:latest
```
![](docker.jpg)

When the container is up and running (check using the` docker ps` command) run the following command:

```text
docker logs cast_extend_proxy
```

This will show the following information which you should note down:

 - **API key** - this will be required when configuring CAST Imaging to use Extend Local Server.
- **Extend Local Server Administration Center URL** - this URL allows you to manage the instance.

For example:

![](logs.jpg)

## Post install setup

### Step 1 - Configure Extend Local Server settings

When Extend Local Server has been installed, browse to the **admin URL** (this is provided as part of the installation process as outlined already) preferably from a different machine on your local network. For **example**:

```text
http://<FQDN/ip_address>:<port>/ui/admin?<unique_guid>
```

The `<unique_guid>` can also be found in the following files (the `ADMI` entry):

```text
Microsoft Windows: %PROGRAMDATA%\CAST\Extend\config.proxy.json
Linux via Docker: /shared/extend_data/config.proxy.json
```

If Extend Local Server is functioning, the **Administration Center** will be displayed. You should now make some initial choices about how Extend Local Server functions:

![](admin_center.jpg)

You will need to choose between **online** and **offline** mode:

#### Online mode

In **online** mode, the following additional information is required:

- **CAST Extend credentials**
- **Proxy settings (optional)**: If your organization requires internal systems to use a proxy caching server for all connections, you can use this option to configure Extend Local server to use a proxy server for all external communication. Enable the option and then enter the URL of your proxy server in the format `protocol://[user:password@]host[:port]`.

If you choose **online mode** saving the settings will initiate a synchronization with CAST Extend (fetching the manifest), which can be seen in the **Updates** panel:

![](sync.jpg)

#### Onffline mode

In **offline** mode, no information is required, but you will need to populate the instance with extensions, see [Update Extend Local Server with new content](../../administer/extend-local/manual-content-update/).

### Step 2 - Configure CAST Imaging to use Extend Local Server

As part of the [global install](../global/) process for CAST Imaging, you will be prompted to configure your **CAST Extend settings** - this is where you can choose to use Extend Local Server rather than CAST Extend:

![](extend_install.jpg)

Alternatively if CAST Imaging is already running and you have configured it to use CAST Extend, you can now switch to Extend Local Server using the [CAST Extend Settings](../../administer/settings/system-settings/extend/) option accessed via the [Settings](../../administer/settings/) panel.

In both cases, you will need the following information:

- **Extend Local Server URL**: `http://<FQDN/ip_address>:<port>`
- **Extend Local Server API key**: this is generated during the installation process and can be found in the following files (the `APIKEY` entry):

```text
Microsoft Windows: %PROGRAMDATA%\CAST\Extend\config.proxy.json
Linux via Docker: /shared/extend_data/config.proxy.json
```

![](extend_install2.jpg)

## FAQ

<details>
  <summary>How do I upload extensions to Extend Local Server when I'm using it in offline mode?</summary>
  <br>See <a href="../../administer/extend-local/manual-content-update/">Update Extend Local Server with new content</a>.
</details>

<br>

<details>
  <summary>How do I update my Extend Local Server to a new release?</summary>
  <br>See <a href="../../administer/extend-local/update/">Update Extend Local Server to a new release</a>.
</details>

<br>

<details>
  <summary>Can I perform a manual manifest synchronization when I'm using it in online mode?</summary>
  <br>See <a href="../../administer/extend-local/synchronization/">Manifest synchronization</a>.
</details>

<br>

<details>
  <summary>Can I change the default manifest synchronization interval when I'm using it in online mode?</summary>
  <br>See <a href="../../administer/extend-local/synchronization/">Manifest synchronization</a>.
</details>

<br>

<details>
  <summary>Can I blacklist the download of specific extensions when I'm using it online mode?</summary>
  <br>See <a href="../../administer/extend-local/advanced-configuration/#extension-synchronization-blacklisting">Extension synchronization blacklisting</a>.
</details>

<br>

<details>
  <summary>Can I update the hostname of Extend Local Server after it has been installed?</summary>
  <br>See <a href="../../administer/extend-local/advanced-configuration/#hostname-update">Hostname update</a>.
</details>

