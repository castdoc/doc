---
title: "Authentication - Local"
linkTitle: "Local"
type: "docs"
weight: 10
no_list: true
---

## Overview

This section describes how to manage **local authentication** for CAST Imaging. This authentication mechanism is built into CAST Imaging and is enabled by default with one user called `admin` (password `admin`) made available to set up CAST Imaging initially. This user also has the `ADMIN` [profile](../../../../administer/settings/user-permissions/) which permits access to everything.

{{% alert title="Note" color="info" %}}**Local authentication** is a simple authentication mechanism that uses users and groups defined within the authentication system. This mechanism is not recommended for enterprise deployments where you need to manage many users or groups, instead CAST recommends using [LDAP](../ldap) or [SAML Single sign-on](../saml).{{% /alert %}}

## How do I log in to the authentication management system provided with CAST Imaging?

See [Authentication](../).

## How do I change the password for the default admin user?

Ensure you are working in the `aip-realm` realm:

- click the `Users` option on the left
- find the `admin` user in the list (it should be the only one)
- click the "pencil" icon in the `User label` column and update the password:

![](change_password.jpg)

## How do I add an additional local user?

Ensure you are working in the `aip-realm` realm:

- click the `Users` option on the left
- click the `Add user` button:

![](adduser.jpg)

- at a minimum, define the username:

![](adduser2.jpg)

- and then define the password:

![](adduser3.jpg)

## How do I create local groups?

Local groups allow you to group together multiple users and then apply permissions to the group, rather than to individual users. Firstly, ensure you are working in the `aip-realm` realm:

- then click the `Groups` option on the left
- then click the `Create group` button and enter a name for the group

![](groups_local.jpg)

 - add your **local users** to the newly created group by clicking the group:

![](group_click.jpg)

- then add the users to your group:

![](group_click2.jpg)

## How do I assign permissions to my local users and groups?

As soon as a local user or group is created, it will be visible in the CAST Imaging [User Permissions](../../../../administer/settings/user-permissions) interface (accessing this requires a login that has the `ADMIN` profile). Use this interface to grant permissions to your users and/or groups:

![](user_permissions.jpg)
