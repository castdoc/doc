---
title: "Authentication - LDAP"
linkTitle: "LDAP"
type: "docs"
weight: 20
no_list: true
---

{{% alert title="Note" color="info" %}}Enabling and configuring a new authentication mode will NOT disable any existing authentication modes you may have configured or that may have been provided out of the box.{{% /alert %}}

## Overview

This section describes how to set up and configure CAST Imaging to allow authentication using your on premises LDAP authentication system.

## FAQ

### How should the account used to access the LDAP system be configured?

CAST recommends creating a dedicated LDAP "service" account to be used exclusively for configuring access between CAST Imaging and your LDAP authentication system. This service account should be a member of an LDAP group dedicated to CAST (i.e. only users required to access CAST Imaging should be added to this group).

### What happens when users required to use CAST Imaging are added to the LDAP group dedicated to CAST, does the admin need to perform a synchronization in CAST Imaging?

CAST recommends that users are imported into CAST Imaging's authentication system (into its own database), otherwise some functionality is lost (typically API Key access to CAST Imaging because the API key needs to be stored with the user in CAST Imaging's authentication system database). When users are configured to be imported into CAST Imaging's authentication system database, this action is only performed the very first time a user logs into CAST Imaging. As such, CAST also recommends that a periodic synchronization is configured in CAST Imaging's authentication system. This can be done by enabling the `Periodic Full Sync` option - see [below](#synchronization-settings).

### What happens if a user password changes in LDAP?

All authentication is performed against the LDAP database, therefore if the user changes his/her password in the LDAP authentication system, then the change is immediate when attempting to access CAST Imaging (they will need to use the new password).

### What happens if a user is deleted from LDAP?
If the user is deleted from the LDAP authentication system then that user will no longer be able to login to CAST Imaging (because all authentication is performed against the LDAP database) - this occurs immediately. The user's details will remain in CAST Imaging's authentication system database until the next synchronization, after which, it will be removed automatically.

## LDAP configuration process

### Step 1 - Configure LDAP settings

Log in to the authentication management system provided with CAST Imaging as described in [Authentication](../). Then:

- ensure you are working in the `aip-realm` realm
- click the `User federation` option on the left
- then choose `Add LDAP providers`:

![](step1.jpg)

Fill in as follows (see also [https://www.keycloak.org/docs/latest/server_admin/#_ldap](https://www.keycloak.org/docs/latest/server_admin/#_ldap)). Only **required** options are documented below. When complete, click the `Save` button at the bottom of the list of options.

#### General options

| Field | Description |
|---|---|
| UI display name | A simple display name. Leave to the default setting. |
| Vendor | Choose your LDAP type, typically this will be `Active Directory`. |

#### Connection and authentication settings

| Field | Description |
|---|---|
| Connection URL | Enter the URL for your remote LDAP server. For example use a FQDN (fully qualified domain name), host name, IP address etc:<br><br><ul><li>`ldap://<my-ldap-server-hostname>.corp.domain.com`</li><li>`ldap://<my-ldap-server-hostname>`</li><li>`ldap://<ip-address>`</li></ul> |
| Bind type | Type of authentication used in the LDAP bind operation. Typically you should leave this set to `simple`. |
| Bind DN | Enter the `DN` (Distinguished Name) of the LDAP service account that will be used for the LDAP bind operation. |
| Bind credentials | Enter the password for the LDAP service account that will be used for the LDAP bind operation. |

{{% alert title="Note" color="info" %}}Use the `Test connection` and `Test authentication` buttons to validate your settings.{{% /alert %}}

#### LDAP searching and updating

| Field | Description |
|---|---|
| Edit mode | This option determines how the authentication system will handle the remote LDAP database. If you have chosen to import your LDAP users as recommended (see [Synchronization settings](#synchronization-settings)), then you must set this option to either `WRITABLE` or `UNSYNCED`. CAST recommends choosing `UNSYNCED`, which ensures that no changes will be made to the remote LDAP database.<br><br>If you have chosen not to import your LDAP users into the authentication system, you should set this to `READ_ONLY`, which ensures that no changes will be made to the remote LDAP database. |
| Users DN | The DN (Distinguished Name) of the Organisational Unit (OU) in your LDAP database in which your users are stored. For example:<br><br>![](usersdn.jpg)|
| Username LDAP attribute | Populated automatically based on the type of LDAP database you have selected in the `Vendor` field, but can be customized if required. This is the name of the LDAP database attribute, which (when users are imported) will be mapped as the username in the authentication system database. If users are not imported, this is what users will use to log in to the CAST product:<br><br><ul><li>For many LDAP server vendors this can be 'uid'.</li><li>For Active Directory this can be 'sAMAccountName' or 'cn'.</li></ul> |
| RDN LDAP attribute | Populated automatically based on the type of LDAP database you have selected in the `Vendor` field, but can be customized if required. This is the name of the LDAP database attribute, which is used as the RDN (top attribute) of a typical user Distinguished Name. Usually this is the same as `Username LDAP attribute`, however it is not a required attribute in the LDAP database. For example for Active Directory, sometimes `cn` is used as the RDN attribute when the username attribute might be `sAMAccountName`. |
| UUID LDAP attribute | Populated automatically based on the type of LDAP database you have selected in the `Vendor` field, but can be customized if required. This is the name of the LDAP database attribute, which is used as a unique object identifier (UUID) for objects in the LDAP database. For many LDAP server vendors, it is `entryUUID`; however some are different. For example for Active Directory it should be `objectGUID`. If your LDAP server does not support the notion of UUID, you can use any other attribute that is supposed to be unique among LDAP users in tree. For example `uid` or `entryDN`. |
| User object classes | Populated automatically based on the type of LDAP database you have selected in the Vendor field, but can be customized if required. These are all the values of the LDAP database 'objectClass' attribute for users in the LDAP database, separated with a comma. For example:<br><br>![](userobjectclass.jpg)<br><br><ul><li>When you have chosen to import LDAP users into the authentication system as recommended (see [Synchronization settings](#synchronization-settings)), users will be imported if at least one of the values in this field is defined in the LDAP database `objectClass` attribute for a given user.</li><li>If `Sync Registrations` is enabled (not recommended - see [Synchronization settings](#synchronization-settings)), users created directly in the authentication system will be written to the LDAP database with all the values entered in this field.</li></ul> |

#### Synchronization settings

| Field | Description |
|---|---|
| Import users | This option allows you to import your LDAP users into the authentication system database stored on your CAST Storage Service/PostgreSQL instance. CAST recommends that this option is set to `ON` otherwise the following functionality will be lost:<br><br><ul><li>if you need to generate an API Key to access CAST Imaging, the API Key is stored alongside the user details in the authentication system database and therefore, the LDAP user needs to have been imported already.</li><li>if you need to assign permissions to individual LDAP users, rather than, or as well as, via LDAP groups.</li></ul>When set to `ON`, the first time a user logs in, the LDAP provider imports the LDAP user details into the authentication system database and validates the LDAP password. This first time a user logs in is the only time that the user will be imported. Therefore, if you click the `Users` menu option, you will only see the LDAP users that have authenticated at least once.<br><br>The authentication system imports users this way, so that a single user login operation does not trigger an import of the entire LDAP user database and cause performance issues. Note that by default the user's details will only be imported once when they first login, therefore if users are updated in the LDAP database, these details are not automatically synchronized to the authentication system database unless you either:<br><br><ul><li>manually trigger the import of the entire LDAP database using the `Sync all users` option:<br><br>![](syncall.jpg)</li><br><li>or enable the `Periodic Full Sync` option:<br><br>![](fullsync.jpg) |
| Sync Registrations | This option determines whether users created directly in the authentication system will be synchronized back to the remote LDAP database. CAST recommends that this should be set to `OFF` since the LDAP database should be the only reference. |
| Periodic Full Sync | CAST recommends that you set this option to `ON` to ensure that any changes in the LDAP database are automatically synchronized back to the authentication system database. |
| Full sync period | Available when `Periodic Full Sync` is enabled. Set the interval in seconds, for example `86400` to perform a sync every 24hrs. |

### Step 2 - Configure LDAP group settings

If you need to grant CAST Imaging [permissions](../../../../administer/settings/user-permissions/) to LDAP groups (this is highly recommended) rather than, or as well as, individual LDAP users, you will need to configure a mapper specifically to synchronize your LDAP groups and make them available in CAST Imaging. To do so click the `Mappers` tab in your `LDAP User Federation setting` and click `Add mapper`:

![](add_mapper.jpg)

Fill in as follows:

| Field | Description |
|---|---|
| Name | This is simply a display name - enter as required. |
| Mapper Type | Must be set to `group-ldap-mapper`. |
| LDAP Groups DN | This the DN (Distinguished Name) of the Organisational Unit (OU) in your LDAP database in which your groups are stored. For example:<br><br>![](usersdn.jpg) |
| Group Name LDAP Attribute | Populated automatically, but can be customized if required. This is the name of the LDAP database attribute, which is used in group objects for the name and RDN of the group. In the vast majority of cases, this will be `cn` (Common Name) - typically group objects may have a Distinguished Name such as `cn=Group1,ou=groups,dc=example,dc=org`. For example:<br><br>![](groupname.jpg) |
| Group Object Classes | Populated automatically, but can be customized if required. These are all the values of the LDAP database `objectClass` attribute for your groups in the LDAP database, separated with a comma. Groups will be identified if at least one of the values in this field is defined in the LDAP database `objectClass` attribute for a given group. In the following example groups will be imported into the authentication system database if at least one of the values in this field is defined in the LDAP database 'objectClass' attribute for a given group:<br><br>![](groupobj.jpg) |
| Preserve Group Inheritance | This option determines whether your LDAP group inheritance organisation is mirrored in the authentication system database. In other words, if you have nested groups, you should ensure this option is set to `ON`. If set to `OFF`, all groups will be mirrored in the authentication system database in "flat mode", i.e. all at one level, ignoring the nested organisation. |
| Ignore Missing Groups | This option should always be set to `ON`. |
| Membership LDAP Attribute | Populated automatically, but can be customized if required. This is the name of the LDAP database attribute, which is used for group membership mappings. Usually this will be `member`. However if the `Membership Attribute Type` option (see below) is set to `UID` then `Membership LDAP Attribute` could be `memberUid`. |
| Membership Attribute Type | Select either `UID` or `DN`:<br><br><ul><li>DN means that the LDAP group has its members declared in the form of their full Distinguished Name (DN). For example `member: cn=john,ou=users,dc=example,dc=com`.</li><li>UID means that the LDAP group has its members declared in the form of pure user uids. For example `memberUid: john`.</li></ul> |
| Membership User LDAP Attribute  | Populated automatically, but can be customized if required. This option is ignored if the `Membership Attribute Type` option (see above) is set to `DN`.<br><br>This is the name of the LDAP database attribute for the user, which is used for membership mappings. Usually it will be `uid` . For example if `Membership User LDAP Attribute` is set to is `uid` and the LDAP group has `memberUid: john`, then it is expected that that particular LDAP user will have the attribute `uid: john`. |
| LDAP Filter | Additional LDAP Filter for filtering searched groups. Leave this empty if you don't need an additional filter. If you do need to filter, make sure that the value starts with '(' and ends with ')' |
| Mode | This option determines how the authentication system will handle groups:<br><br><ul><li><code>LDAP_ONLY</code> means that all group mappings are retrieved from both the LDAP database and the authentication system database (if any exist there). Any changes made within the authentication system will then be saved back into the LDAP database.</li><li><code>READ_ONLY</code> is a read-only mode where group mappings are retrieved from both the LDAP database and the authentication system database (if any exist there). Any changes made within the authentication system will NOT be saved back into the LDAP database.</li><li><code>IMPORT</code> (recommended setting) is a read-only mode where group mappings are retrieved from LDAP only (at the time when user is read from the LDAP database) and then the groups are saved to the authentication system database.</li></ul> |
| User Groups Retrieve Strategy | This option determines how groups should be retrieved from the LDAP database:<br><br><ul><li><code>LOAD_GROUPS_BY_MEMBER_ATTRIBUTE</code> means that groups will be retrieved by sending an LDAP query to retrieve all groups where "member" is our user<li><code>GET_GROUPS_FROM_USER_MEMBEROF_ATTRIBUTE</code> means that groups will be retrieved via the <code>memberOf</code> attribute of our user. Or from the other attribute specified by <code>Member-Of LDAP Attribute</code>.<li><code>LOAD_GROUPS_BY_MEMBER_ATTRIBUTE_RECURSIVELY</code> is applicable only in Active Directory and it means that groups will be retrieved recursively via the <code>LDAP_MATCHING_RULE_IN_CHAIN</code> extension. CAST recommends this option so that group membership is retrieved at the time the user first logs in to CAST Imaging.</li></ul>Note that if a user already exists in the authentication system database (i.e the user has already logged in before the groups mapper was setup), then the group membership details will not be updated for the given user. In this case, the user needs to be deleted from the authentication system database (using the `Users` option in the left panel) - the group membership details will be retrieved when the user next logs in. |
| Member-Of LDAP Attribute | Populated automatically, but can be customized if required. This option is ignored if the `User Groups Retrieve Strategy` option (see above) is set to anything other than `GET_GROUPS_FROM_USER_MEMBEROF_ATTRIBUTE`. It specifies the name of the LDAP database attribute on the LDAP user, which contains the groups, which the user is member of. Usually it will be `memberOf` and which is also the default value. |
| Drop non-existing groups during sync | If this option is set to `ON` then during retrieval of groups from the LDAP database, only those groups created directly in the authentication system which also exist in the LDAP database will be retained. All others will be deleted. CAST recommends setting this option to `ON` if you only care about groups set in your LDAP database. |
| Groups Path | This is the path where the LDAP groups will be created in the authentication system database and is set by default to `/` (i.e. the root). Root means that the groups will be created under this default entry. |

When complete, click the `Save` button at the bottom of the list of options and then action the `Sync LDAP groups to Keycloak`option:

![](syncgroups.jpg)

You can check whether the groups have been successfully retrieved using the following interface:

![](groups.jpg)

### Step 3 - Assign admin permissions to at least one LDAP user

By default, LDAP users/groups will not have any permissions assigned to them, so although users can login to CAST Imaging, they will not be able to make any changes. Therefore at least one LDAP user (or group) will need to be granted the `ADMIN` profile. CAST recommends using the [local authentication](../local) mechanism to do so. This mechanism will still be active and therefore you can log in to CAST Imaging using the default `admin / admin` credentials and assign the `ADMIN` profile to an LDAP user (or group) using the [User Permissions](../../../../administer/settings/user-permissions/) option.

Finally test that the LDAP user can successfully log in to CAST Imaging and has permission to access the administration settings in CAST Imaging:

![](admin.jpg)

### Step 4 - Assign permissions to other LDAP users

All further permission configuration should always be actioned in CAST Imaging itself, using the [User Permissions](../../../../administer/settings/user-permissions/) option.

### Step 5 - Disable all existing authentication methods

CAST highly recommends that you now disable all existing authentication methods to prevent users accessing CAST Imaging via a "back door" log in. In most cases this will involve disabling or deleting the [Local Authentication](../local/) users/groups provided out-of-the-box and any that you have created yourself.

To do so:

- temporarily disable the LDAP authentication mechanism (this is necessary otherwise "local" users/groups will not be visible)

![](disable_ldap.jpg)

- disable or delete the local users/groups to prevent them being used:

![](../disable.jpg)

- finally re-enable the LDAP authentication mechanism.
