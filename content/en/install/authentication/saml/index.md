---
title: "Authentication - SAML"
linkTitle: "SAML"
type: "docs"
weight: 30
no_list: true
---

{{% alert title="Note" color="info" %}}Enabling and configuring a new authentication mode will NOT disable any existing authentication modes you may have configured or that may have been provided out of the box.{{% /alert %}}

## Overview

This section describes how to set up and configure CAST Imaging to allow authentication using your on premises SAML authentication system.

## Requirements

Configuring CAST Imaging to use SAML Single Sign-on requires that you have already set up and configured [SSL](../../https/) (access via HTTPS).

## Step 1 - Obtain metadata

Log in to the authentication management system provided with CAST Imaging as described in [Authentication](../). Then:

- Ensure you are working in the `aip-realm` realm
- Click the `Realm settings` option on the left
- Then click `SAML 2.0 Identity Provider Metadata` in the `General` tab:

![](step1.jpg)

- The metadata will be opened in a new tab in your browser - save the contents as a file called `metadat.xml`.
- Send this file your organization's IT administrator, in order that they can register CAST Imaging in the SAML IDP server. Once this step is complete, the IT administrator will provide you with an **IDP metadata URL** which is required for the next step.

## Step 2 - Configure SAML settings

To configure SAML now that you have the **IDP metadata URL**:

- Ensure you are working in the `aip-realm` realm
- Click the `Identity providers` option on the left
- Then click `SAML v2.0`:

![](step2.jpg)

- Choose an `Alias` if you want to customize the provider settings
- Enter the **IDP metadata URL** into the `SAML entity descriptor` field.
- Click `Add`:

![](step2a.jpg)

- If the **IDP metadata URL** is correct, all settings will be automatically populated

## Step 3 - Add custom mappers

Mappers are required to match the attributes from the SAML response with the mandatory user attributes in CAST Imaging, i.e. `firstName`, `lastName`, `email`, so that this information will be automatically populated when a user logs in.

- Open the **IDP metadata URL** in a browser - the XML content will be displayed.
- Search for the mandatory user attribute name for example `email` will look similar to this:

```xml
<auth:ClaimType Uri="http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress" Optional="true"
				xmlns:auth="http://docs.oasis-open.org/wsfed/authorization/200706">
				<auth:DisplayName>E-Mail Address</auth:DisplayName>
				<auth:Description>The e-mail address of the user</auth:Description>
</auth:ClaimType>
```

- Copy the `ClaimType Uri` value (in the example above this is `http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress`)
- Now ensure you are working in the `aip-realm` realm
- Click the `Identity providers` option on the left
- then click `Mappers` in your SAML configuration:
- then click `Add mapper`:

![](step3.jpg)

Fill in the fields as follows:

- Name: `email` (or to match the attribute you are mapping, e.g  `firstName` / `lastName`)
- Sync mode override: `Inherit` (default)
- Mapper type: `Attribute Importer`
- Attribute Name: paste in the `ClaimType Uri` found previously
- Name Format: `ATTRIBUTE_FORMAT_BASIC` (default)
- User Attribute Name: `email` (or to match the attribute you are mapping, e.g  `firstName` /  `lastName`)

![](step3a.jpg)

- Click **Save** and repeat the same process for the `firstName` / `lastName` attributes.

Now after a login, the mandatory fields will be imported from the SAML response.

## Step 4 - Assign admin permissions to at least one SAML user

By default, SAML users/groups will not have any permissions assigned to them, so although users can login to CAST Imaging, they will not be able to make any changes. Therefore at least one user (or group) will need to be granted the `ADMIN` profile. CAST recommends using the [local authentication](../local) mechanism to do so. This mechanism will still be active and therefore you can log in to CAST Imaging using the default `admin / admin` credentials and assign the `ADMIN` profile to a SAML user (or group) using the [User Permissions](../../../../administer/settings/user-permissions/) option.

Finally test that the SAML user can successfully log in to CAST Imaging using their company email address and that they have permission to access the administration settings in CAST Imaging:

![](../ldap/admin.jpg)

## Step 5 - Assign permissions to other SAML users

All further permission configuration should always be actioned in CAST Imaging itself, using the [User Permissions](../../../../administer/settings/user-permissions/) option.

## Step 6 - Test login

Ensure that you can login to CAST Imaging using SAML:

![](login.jpg)

## Step 7 - Disable all existing authentication methods

CAST highly recommends that you now disable all existing authentication methods to prevent users accessing CAST Imaging via a "back door" log in. In most cases this will involve disabling or deleting the [Local Authentication](../local/) users/groups provided out-of-the-box and any that you have created yourself.

To do so:

- temporarily disable the SAML authentication mechanism (this is necessary otherwise "local" users/groups will not be visible)

![](disable_saml.jpg)

- disable or delete the local users/groups to prevent them being used:

![](../disable.jpg)

- finally re-enable the SAML authentication mechanism.

## Troubleshooting

### Identity provider is not displayed on login

If you do not see the option to login to CAST Imaging with the SAML identity provider, you will need to change the theme as follows:

- Ensure you are working in the `aip-realm` realm
- Click the `Realm settings` option on the left
- Then click `Themes`
- Choose `keycloak` for the `Login theme`:

![](theme.jpg)
