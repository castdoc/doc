---
title: "Authentication"
linkTitle: "Authentication"
type: "docs"
weight: 50
no_list: true
---

***

## Overview

Out-of-the-box, CAST Imaging is configured to use [Local Authentication](local/) via a simple username/password system provided by third-party component called **Keycloak** (see [below](#about-keycloak) for more information). Default login credentials are provided (`admin/admin`) with the global `ADMIN` [profile](../../../administer/settings/user-permissions/) so that installation can be set up initially.

To administer authentication, use the following url and login with the default credentials which are specific to the authentication system (`admin/admin`):

```text
http(s)://PUBLIC_URL:8090/auth 
```
{{% alert title="Note" color="info" %}}These credentials are specific to the authentication system and can be changed using the following URL: `http://PUBLIC_URL:8090/auth/realms/master/account/#/security/signingin`{{% /alert %}}

CAST recommends configuring CAST Imaging to use your on-premises enterprise authentication system such as **LDAP** or **SAML Single Sign-on**, as detailed below.

{{< cardpanehome >}}
{{% cardhomereference %}}
<img class="no-border" src="local.png">&nbsp;[Local Authentication](local/)
{{% /cardhomereference %}}
{{% cardhomereference %}}
<img class="no-border" src="ldap.png">&nbsp;[LDAP](ldap/)
{{% /cardhomereference %}}
{{% cardhomereference %}}
<img class="no-border" src="saml.png">&nbsp;[SAML Single Sign-on](saml/)
{{% /cardhomereference %}}
{{< /cardpanehome >}}

## About Keycloak

CAST uses a third-party component called [Keycloak](https://keycloak.org) to handle authentication for CAST Imaging. This component is installed automatically during the [install process](../). Some technical information about Keycloak follows:

### Product support

CAST supports this third-party component in the sense that tickets are created, and temporary or permanent fixes will be provided.

### Version upgrades

When a permanent fix is required, CAST provides the latest corrections published by Keycloak.

### Error management

CAST handles Keycloak errors when they are reported via support tickets.

### Configuration and parameterization issues

As mentioned previously, CAST handles configuration issues when support tickets are created.

### Software and security patching

When patching is required, CAST provides patches sourced from Keycloak.
