---
title: "Database server"
linkTitle: "Database server"
type: "docs"
weight: 40
no_list: false
---

***

CAST Imaging requires an RDBMS component to store both the data generated during analyses and persistence data (settings/properties etc.). CAST supports only **PostgreSQL** as follows: 

- **CAST Storage Service** - a PostgreSQL instance packaged and provided by CAST for installation on Microsoft Windows with its own custom installer and pre-configured settings.
- **PostgreSQL** - a standard installation direct on Linux, or through Docker, of an official PostgreSQL release.

There can be one or multiple instances of this component in a deployment. Multiple instances can be installed in order to load-balance your analysis and data storage requirements.

{{% alert color="info" %}}<ul><li>See also <a href="../../requirements/db/">Database requirements</a>.</li><li>CAST provides one single instance of the database component automatically as a container when installing CAST Imaging on <a href="../global/docker/">Linux via Docker</a>. When installing CAST Imaging on <a href="../global/windows/">Microsoft Windows</a>, this component must be obtained and installed separately. Additional remote instances can also be deployed in both cases.</li></ul>{{% /alert %}}

See the installation instructions below.