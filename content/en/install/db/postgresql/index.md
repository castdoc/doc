---
title: "PostgreSQL for Linux"
linkTitle: "PostgreSQL for Linux"
type: "docs"
weight: 20
no_list: true
---

***

## Overview

CAST does not provide a PostgreSQL installer for Linux environments, however, CAST recommends installing the equivalent PostgreSQL version on a Linux server to take advantage of the superior performance over CAST Storage Service [installed on Microsoft Windows](../css/).

Links to the official PostgreSQL installation instructions for the various different Linux distributions are listed below.

{{% alert color="info" %}}<ul><li>Your chosen Linux distribution may include a specific release of PostgreSQL by default but that may not be the latest available release nor one that is supported by CAST (see <a href="../../../requirements/db/">What are the database requirements?</a>). You can instead use the official PostgreSQL repository for your distribution's package manager system to install the specific release you need.</li><li>When installing CAST Imaging on <a href="../../global/docker/">Linux via Docker</a>, CAST provides a database instance as a Docker image. By default, this instance will be used by CAST Imaging for both analysis data and persistence data storage needs. However, you are free to install additional database instances, for example to separate the storage of persistence data and analysis data on two separate instances.</li></ul>{{% /alert %}}

## Instructions

| Distribution | Instructions |
|---|---|
| Ubuntu via APT | https://www.postgresql.org/download/linux/ubuntu/ |
| Debian via APT | https://www.postgresql.org/download/linux/debian/ |
| Red Hat (RHEL/Rocky/Alma/CentOS) via Yum or DNF | https://www.postgresql.org/download/linux/redhat/ |
