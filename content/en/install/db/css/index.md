---
title: "CAST Storage Service for Microsoft Windows"
linkTitle: "CAST Storage Service"
type: "docs"
weight: 10
no_list: true
---

***

## Overview

Information about how to install CAST Storage Service for Microsoft Windows.

## Requirements

CAST Storage Service is a standalone component and can therefore be installed wherever convenient in your local environment, e.g. on a dedicated machine, or on a machine already used by other CAST Imaging components. It can be installed as many times as necessary in your environment to load balance your analysis requirements.

When using a dedicated machine, CAST recommends following the general [hardware](../../requirements/hardware/) and [software](../../requirements/software/) requirements but note that the component **does not** require:

- a Java JRE/JDK

Other requirements:

- CAST Storage Service runs as a Microsoft Windows service on **TCP port 2284** (this can be customized in teh installation wizard). You may therefore need to adjust firewall rules to allow incoming connections to this port
- a minimum of **2GB** free disk space

### Characters allowed in the installation path

Generally speaking, the installation path must only contain ASCII characters or characters of the language (or language group) of your Microsoft Windows operating system. More precisely, all characters in the installation path must be present in the system Microsoft Windows code page of your workstation. The system Microsoft Windows code page is determined by the system locale. The system locale is defined in:

**Control Panel > Region > Administrative tab > Language for non-Unicode programs**.

This language defaults to the language of the Microsoft Windows operating system and can be changed, if needed.

As an example, on any western European Windows installation, the system code page is `Win-1252`; on central European Windows installations, the code page is `Win-1250`, and on a Turkish Windows installation, the code page is `Win-1254`. The exact value of the code page is stored in the Windows registry at the following location:

```text
HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Nls\CodePage
```

The value in the key `ACP` indicates the current system Windows code page (also called `ANSI Code Page`). If your installation path contains any character not present in the system Windows code page (e.g. a cyrillic character on a French Microsoft Windows installation), then you may encounter issues using the CAST Storage Service.

## What's new?

See the [release notes](../../../release-notes/com.castsoftware.css/).

## Interactive installation procedure

Download and unzip the latest release of com.castsoftware.css from [https://extend.castsoftware.com/#/extension?id=com.castsoftware.css&version=latest](https://extend.castsoftware.com/#/extension?id=com.castsoftware.css&version=latest).

Execute the `setup.bat` file with elevated permissions (right click, `Run as administrator`) at the root of the unzipped files and follow the interactive installer. Leave all options at their default where possible.

## Silent installation process

This method runs the installation in CLI mode. You will need to run the Command Prompt (CMD) with the `Run as Administrator` option, or, if you are running the script in a batch file, you may need to run the batch file with administrator rights.

- On the machine on which you want to install the CAST Storage Service, open the Command Prompt (CMD). Change the folder and/or directory and/or drive to that containing the CAST Storage Service `setup.bat` file (this is located in the root folder that is created from the CAST Storage Service zip file)
- Enter the following commands in the CMD window (or place them in a batch file if you prefer), changing the various options to suit your installation:

```bash
set SILENT=/s
setup.bat /qn INSTALLDIR=\"<INSTALLPATH_CSS>\" CSSPORT=\"<CSS_PORT>\" DBDATADIR=\"<INSTALLPATH_CSSDATA>\"
```

Where:

- `INSTALLDIR` is the full path to the location on disk where you want to install the CAST Storage Service binaries. The default location chosen by the GUI installer is: `%PROGRAMFILES%\CAST\CASTStorageService<version>`.
- `CSSPORT`	is the port number on which you want to run the CAST Storage Service, typically this is `2284`.
- `DBDATADIR` is the full path to the location on disk where you want to store the CAST Storage Service data. The default location chosen by the GUI installer is: `%PROGRAMFILES%\CAST\CASTStorageService<version>\db_data`.

## In-place updates

The CAST Storage Service installer does not support an in place update of an existing CAST Storage Service installed on the current machine. In all situations you will need to move the existing schemas from the previous release to the new release, see **Moving existing CAST schemas for more information** Note:

- If you have an older release of CAST Storage Service installed and want to install a new **major release** (for example you have CAST Storage Service 3.0.x and you would like to install 4.13.x), then you can install the two instances on the same machine provided that you ensure the installation path(`%PROGRAMFILES%\CAST\CastStorageService<version>`), data path (`%PROGRAMFILES%\CAST\CastStorageService<version>\db_data`) and listening port (typically 2284) are different.
- If you have an older release of CAST Storage Service installed and you would like to install a new **maintenance release** (for example you have CAST Storage Service 4.13.0 and you would like to install 4.13.10), then the two cannot be installed on the same machine. In this situation, you will need to:
  - backup all existing schemas
  - uninstall the existing instance
  - install the new instance on the same or a different machine (in this case you will need to update references to the new machine in CAST Imaging)
  - restore the schemas

## What is installed?

### Files

On completion of the installation, all files related to CAST Imaging Core are stored in:

```text
%PROGRAMFILES%\CAST\CastStorageService<version>
```

### Microsoft Windows Service

A Windows Service called `CastStorageService4` will be created and set to start automatically, running the on chosen TCP port (`2284` by default). You may need to adjust firewall rules to allow incoming connections on the listening port. CAST Storage Service runs under the following executables:

```text
%PROGRAMFILES%\CAST\CastStorageService<version>\bin\postgres.exe
%PROGRAMFILES%\CAST\CastStorageService<version>\bin\pg_ctl.exe
```

### Third-party software

If the corresponding item is already installed, then the installer will detect it and will skip the installation.

- Visual C++ 64-bit Redistributable Packages for Visual Studio 2015-2019
- Visual C++ 32-bit Redistributable Packages for Visual Studio 2015-2019
- pgAdmin4 6.19 (PostgreSQL database management console executable available at `%PROGRAMFILES%\CAST\CastStorageService4\pgadmin 4\bin\pgAdmin4.exe`)

### Default database users/roles and passwords

When a CAST Storage Service is installed, two additional users/roles are created (`operator` and `guest`) by default as listed below. An additional user (`postgres`) is present out-of-the box:


| User/role | Default password | Purpose |
|---|---|---|
| `operator` | CastAIP | This user/role is designed to be used for all interaction with CAST Imaging. It has full read and write access to all the schemas stored on the CAST Storage Service with the following privileges:<br><br>![](operator.jpg)<br><br>See Changing the default operator and guest passwords for CAST Storage Service if you would like to change the default password. |
| `guest` | WelcomeToAIP | This user/role is designed to be used exclusively by third-party tools requiring simple read-access to the schemas stored on the CAST Storage Service with the following privileges:<br><br>![](guest.jpg)<br><br>This user/role is not used by any CAST Imaging applications. We highly recommend that if you are using this user/role with CAST Imaging, that you stop doing so and switch to the default `operator` user.<br><br>See Changing the default operator and guest passwords for CAST Storage Service if you would like to change the default password. |
| `postgres` | - | The postgres user/role is created by default with the privileges as listed in the images below:<br><br>![](postgres.jpg)<br><br>If you are concerned about security, you should disable logins for this user/role, and/or change the default password:<br><br>**Disable login**<br><br>Using PgAdmin (which is bundled with CAST Storage Service):<br><br><ul><li>right click the <code>postgres</code> user and select <strong>Properties</strong></li><li>then in the <strong>Privileges</strong> tab disable the login by moving the <strong>Can login?</strong> slider to <strong>NO</strong></li><li>then, save the changes:</li></ul>![](postgres2.jpg)<br><br><strong>Change the password</strong><br><br>Using PgAdmin (which is bundled with CAST Storage Service):<br><br><ul><li>right click the <code>postgres</code> user and select <strong>Properties</strong></li><li>then in the <strong>Definition</strong> tab enter the new password</li><li>then, save the changes:</li></ul>![](postgres3.jpg)<br><br>Or use a SQL query:<br><br>```ALTER USER postgres```<br>```WITH PASSWORD 'password';``` |

## What next?

See [Post installation information and action items](../post-install/).