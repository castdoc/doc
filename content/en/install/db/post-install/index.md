---
title: "Post installation action items"
linkTitle: "Post installation action items"
type: "docs"
weight: 30
no_list: true
---

***

## Overview

When your CAST Storage Service or PostgreSQL instance has been installed, various changes must be actioned before you start using the instance. These are listed below.

## Configure network access via pg_hba.conf

Out of the box CAST Storage Service and PostgreSQL instances are configured to allow incoming connections on IPv4 **from anywhere** via the following line in the `pg_hba.conf` file:

```text
host   all   all   0.0.0.0/0   scram-sha-256
```

Therefore you may wish to secure your instance using the `pg_hba.conf` file to restrict access. There are many ways to do this which will entirely depend on your own environment and organization - for example you may wish to allow access only from certain IP addresses/hosts, or specific PostgreSQL users. The `pg_hba.conf` file is explained in more detail here: https://www.postgresql.org/docs/current/auth-pg-hba-conf.html. Any changes to this file require that the instance is restarted before they are taken into account.

The `pg_hba.conf` can be found in the following locations, although this may differ for installations in Linux environments:

```text
CAST Storage Service
%PROGRAMFILES%\CAST\CastStorageService<version>\db_data\pg_hba.conf

Linux
/etc/postgresql/<version>/main/pg_hab.conf
```

{{% alert color="info" %}}In a Linux environment, use `find / -name "pg_hba.conf"` to recursively search all folders to find this file.{{% /alert%}}

## Configure the instance via postgresql.conf

The `postgresql.conf` file contains various parameters that will configure your instance to function in a specific way. If you are using CAST Storage Service, CAST has modified some of the parameters in this file to ensure optimal performance, however, if you have installed a release of PostgreSQL, you must modify the parameters in this file as shown below:

| Parameters | Required value | Comments |
|---|---|---|
| `listen_addresses` | '*' | Instead of localhost by default. Enables connectivity from other machines. |
| `port` | 2284 |  |
| `max_connections` | 300 |  |
| `fsync` | off |  |
| `synchronous_commit` | off |  |
| `full_page_writes` | off |  |
| `commit_delay` | 10 |  |
| `checkpoint_completion_target` | 0.9 |  |
| `cursor_tuple_fraction` | 1.0 |  |
| `log_checkpoints` | on |  |
| `log_destination` | 'stderr' |  |
| `logging_collector` | on |  |
| `log_line_prefix` | '%t [%p]: [%l-1] ' | Don't forget the space before final quote mark. |
| `log_temp_files` | 1024kB |  |
| `log_autovacuum_min_duration` | 1000ms |  |
| `autovacuum_vacuum_cost_limit` | 200 |  |
| `bytea_output` | 'escape' |  |
| `datestyle` | 'iso, mdy' | Ensure that this option is active (it may already be active). |
| `lc_messages` | 'C' |  |
| `lc_monetary` | 'C' |  |
| `lc_numeric` | 'C' |  |
| `lc_time` | 'C' |  |
| `max_locks_per_transaction` | 4096 | Higher value than 64 by default. |
| `standard_conforming_strings` | on |  |

In addition, CAST recommends configuring other parameters in the `postgresql.conf` file to match the RAM assigned to the host machine. To do so, use [PGTune](https://pgtune.leopard.in.ua/) on the host machine to identify the correct values and then modify the following parameters in `postgresql.conf` with the values provided by PGTune:

- `effective_cache_size`
- `min_wal_size`
- `max_wal_size`
- `wal_buffers`
- `shared_buffers`
- `maintenance_work_mem`

When running PGTune, choose the following:

- Set the `DB version` to the installed version
- Set the `DB Type` to `Mixed type of application`
- Set the `Number of connections` to 300
- Some of the values suggested by PGTune are somewhat low. Therefore, please calculate the following postgresql.conf parameters as follows:
  - `shared_buffers` - value should be 25% of available RAM with a max of 8GB
  - `maintenance_work_mem` - 512MB

The `postgresql.conf` can be found in the following locations, although this may differ for installations in Linux environments:

```text
CAST Storage Service
%PROGRAMFILES%\CAST\CastStorageService<version>\db_data\postgresql.conf

Linux
/etc/postgresql/<version>/main/postgresql.conf
```

{{% alert color="info" %}}In a Linux environment, use `find / -name "postgresql.conf"` to recursively search all folders to find this file.{{% /alert%}}

## Create users

By default CAST Storage Service and PostgreSQL includes a system account user named `postgres` with the role `postgres`. CAST Storage Service provided by CAST includes two additional custom users as follows:

| Username | Password | Permissions | Notes |
|---|---|---|---|
| `operator` | CastAIP | `SUPERUSER` | - |
| `guest` | WelcomeToAIP | - | Note that this user is not used by CAST Imaging and is designed for third-party read-only access. |

If you would like to create these users on your PostgreSQL instance, use the following commands:

```sql
psql
*create user operator with SUPERUSER password 'CastAIP';
*create user guest with password 'WelcomeToAIP';
*grant postgres to operator;
```

You are also free to create your own users and then use them with CAST Imaging if you prefer - you need to have a minimum of one user with the `SUPERUSER` permission and then grant the `postgres` role to this user. For example:

```sql
psql
*create user my_user with SUPERUSER password 'my_password';
*grant postgres to my_user;
```

If you prefer to create custom users that do NOT require the `SUPERUSER` permission this is also possible. For example, this script creates a role `my_role `with the password `my_password` that can login. The role has not been granted the `SUPERUSER` permission and instead only the minimum permissions required to operate CAST Imaging are granted:

```sql
psql
*create role my_role LOGIN PASSWORD 'my_password' NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
*grant create, connect, temporary on database postgres to my_role;
```

## Starting and stopping your instance

### CAST Storage Service

A Microsoft Windows service is automatically created as part of the installation process and you can use this to start/stop/restart your instance. The service will be set to start automatically when the host is rebooted.

### PostgreSQL on Linux

If you have used the official PostgreSQL repository for your distribution's package manager system to [install](../postgresql/) the instance, a **SystemD** service entry will be created to ensure that the service starts automatically when the host is rebooted.

You can also use this to start/stop/restart etc. your instance:

```bash
systemctl stop postgresql-<version>.service --> TO MANUALLY SHUTDOWN
systemctl start postgresql-<version>.service --> TO MANUALLY START
systemctl restart postgresql-<version>.service --> TO MANUALLY RESTART
systemctl status postgresql-<version>.service --> TO CHECK STATUS
systemctl reload postgresql-<version>.service --> TO RELOAD CONFIGURATION
```

## Define your database instance in CAST Imaging

### During a from scratch installation

When [installing CAST Imaging from scratch](../../global/):

- on [Microsoft Windows](../../global/windows/) you will need to define your database instance as part of the installation process.
- on [Linux via Docker](../../global/docker/), a database instance is provided automatically as an image.
  
### As an additional database

If you need to declare the database instance as an additional resource to help load balance your analyses, use the [CSS and Measurement Settings](../../../administer/settings/global-configuration/css-measure) in the CAST Imaging admin UI.

![](adddb.jpg)
