---
title: "CAST Imaging components: an overview"
linkTitle: "CAST Imaging components"
type: "docs"
weight: 20
no_list: true
---

***

## Overview

CAST Imaging is a grouping of various components that work together to produce the results that allow you to investigate your application. This page provides an overview of the different components and how end users access them.

## End-user entry point

There is one single point of entry for all users, whether Administrators, Application Owners or simply those that need to view analysis results: this is known as **CAST Imaging** and is accessed in a web browser. All actions are completed using this component:

- application onboarding and scan 
- analysis configuration
- extension management
- deep analysis
- result generation
- result consumption

![](end-user.jpg)

## Technical components

These technical components are "invisible" to end-users but together they form the end-user entry point **CAST Imaging**:

### Imaging Services

**Imaging Services** is the component providing the end-user access for application onboarding, analysis configuration/operation and result consumption. One single instance of this component is required and provided by default in both [Microsoft Windows](../global/windows/) and [Linux via Docker](../global/docker/) installers.

![](imaging_services.jpg)

{{% alert color="info" %}}For users of the previous version of the product ("V2"), this component wraps together the following previous components:<br><br><ul><li>CAST Console</li><li>CAST Dashboards</li></ul>{{% /alert %}}

### Imaging Viewer

**Imaging Viewer** provides the main result consumption capabilities to the **Imaging Services** component. One single instance of this component is required and provided by default in both [Microsoft Windows](../global/windows/) and [Linux via Docker](../global/docker/) installers.

![](viewer.jpg)

### Analysis Node

This component provides the analysis capabilities to the **Imaging Services** component and consists of two sub-components:

- **CAST Imaging Core** - the technical analysis "engine"
- **Node Service** - a technical component that interfaces with the **Imaging Services** component

There can be one or multiple instances of this component in a deployment. Multiple instances can be deployed in order to load-balance your analysis requirements. A node is usually a dedicated machine (physical or virtual) on which the two sub-components are installed.

{{% alert color="info" %}}CAST provides the **CAST Imaging Core** component automatically when installing CAST Imaging on [Linux via Docker](../global/docker/). When installing CAST Imaging on [Microsoft Windows](../global/windows/), this component must be obtained and installed separately.{{% /alert %}}

### Database

CAST Imaging requires an RDBMS component to store both the data generated during analyses and persistence data (settings/properties etc.). CAST supports only **PostgreSQL** as follows:

- **CAST Storage Service** - a PostgreSQL instance packaged and provided by CAST for installation on Microsoft Windows with its own custom installer and pre-configured settings.
- **PostgreSQL** - a standard installation direct on Linux, or through Docker, of an official PostgreSQL release.

There can be one or multiple instances of this component in a deployment. Multiple instances can be installed in order to load-balance your analysis and data storage requirements.

{{% alert color="info" %}}CAST provides one single instance of the database component automatically as a container when installing CAST Imaging on [Linux via Docker](../global/docker/). When installing CAST Imaging on [Microsoft Windows](../global/windows/), this component must be obtained and installed separately. Additional remote instances can also be deployed in both cases.{{% /alert %}}
