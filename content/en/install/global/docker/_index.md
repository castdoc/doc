---
title: "Installation on Linux via Docker"
linkTitle: "Linux via Docker"
type: "docs"
weight: 21
---

***

## Overview

This install option is specifically for:
- `Docker` running on a Linux operating system
- those wanting to perform a clean install (see [In-place component update](../../../administer/update/component/) if you have already performed an installation and now want to update to a newer release)

The installation media is provided in [com.castsoftware.imaging.all.docker](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.all.docker&version=latest) and comprises an installation script and various configuration files. All Docker images are pulled direct from https://hub.docker.com during the installation.

The following components are provided as Docker images:

- **CAST Imaging Services** including:
    - **CAST Dashboards**
    - **Database instance** additional remote database instances can also be declared post install
- **CAST Imaging Viewer**
- **CAST Imaging Node Service** including:
    - **CAST Imaging Core ≥ 8.4**

The installation script is completely flexible and allows you to install in whichever way suits your environment:

- All components on one machine in a single Docker instance ("standalone" mode - ideal for POCs or testing purposes, or occasionally very small production deployments)
- Components distributed across multiple machines in separate Docker instances (i.e. load balancing mode - this is the recommended installation method)

This flexibility is achieved by running the installation script with specific install commands:

| Install type | Command | Components installed | Containers created |
|---|---|---|---|
| Single machine | `all` | All components - one machine installation scenario. If you choose this option, you must ensure that your machine has sufficient resources to run all components: see [Requirements](../../../requirements/). | <ul><li>admin-center</li><li>analysis-node</li><li>auth-service</li><li>console</li><li>dashboards</li><li>etl</li><li>gateway</li><li>neo4j</li><li>open_ai_manager</li><li>postgres</li><li>server</li><li>sso-service</li></ul> |
| Distributed | `imaging-services` | <ul><li>CAST Imaging Services</li><li>CAST Dashboards</li><li>Database instance</li></ul> | <ul><li>admin-center</li><li>auth-service</li><li>console</li><li>dashboards</li><li>gateway</li><li>postgres</li><li>sso-service</li></ul> |
| Distributed | `imaging-viewer` | <ul><li>CAST Imaging Viewer</li></ul> | <ul><li>etl</li><li>neo4j</li><li>open_ai_manager</li><li>server</li></ul> |
| Distributed | `analysis-node` | <ul><li>CAST Imaging Node Service (includes CAST Imaging Core ≥ 8.4)</li></ul> | <ul><li>analysis-node</li></ul> |

{{% alert title="Note" color="info" %}}<ul><li>The CAST Imaging Core ≥ 8.4 component is provided via the <code>all</code> and <code>analysis-node</code> commands as a Docker image and therefore does not need downloading or installing manually. Release Notes for this component are provided in <a href=../../../release-notes/com.castsoftware.imaging.core>com.castsoftware.imaging.core</a>.</li><li>CAST's on premises <a href="../../extend-local/">Extend Local Server</a> (used where it is not possible to access CAST Extend over the internet) is not included and must be installed separately where required.</li></ul>{{% /alert %}}

{{% alert title="Warning" color="warning" %}}CAST does not support cross-platform installations, i.e. some CAST Imaging components on Linux/Docker and other components on Microsoft Windows, except where a PostgreSQL instance installed on Linux/Docker is being used with CAST Imaging installed on Microsoft Windows.{{% /alert %}}

## Requirements

- [Hardware requirements](../../../requirements/hardware/)
- [Software requirements](../../../requirements/software/)
- [Database requirements](../../../requirements/db/)
- [Disk space requirements](../../../requirements/disk/)

## Step 1 - Determine your installation method

- If you are installing all components on **one machine**, connect to the machine and proceed to [Step 2](#step-2---download-the-installation-media). Ensure that your machine has sufficient resources to run all components: see [Requirements](../../../requirements/).

- Alternatively, if you want to install the components on **multiple machines**, you need to determine which machine will run the **CAST Imaging Services** (`imaging-services`) components because these components must be installed first before any others. Connect to this machine and then proceed to [Step 2](#step-2---download-the-installation-media).

## Step 2 - Download the installation media

Download the latest release of the installer using `curl`:

```bash
curl -# -O -J "https://extend.castsoftware.com/api/package/download/com.castsoftware.imaging.all.docker/<version>?platform=linux_x64" -H "x-nuget-apikey: <api-key>" -H "accept: application/octet-stream"
```

Where:

- `-#`: enables a download progress bar.
- `-O`: (`--remote-name`) ensures the file is downloaded and saved to the current folder using the same name as the remote file.
- `-J`: (`--remote-header-name`) ensures the `-O` option uses the server-specified Content-Disposition filename instead of extracting a filename from the URL. If the server-provided filename contains a path, that is stripped off before the filename is used.
- `<version>`: defines the specific release you want to download: use `latest` to download the most recent release, or specify the specific release, e.g.: `3.0.0-beta13`.
- `-H`: (`--header`) defines the additional header to include in information sent.
- `<api-key>`: defines your CAST Extend API key. You can obtain this from https://extend.castsoftware.com/#/profile/settings.

For example to download the latest release:

```bash
curl -# -O -J "https://extend.castsoftware.com/api/package/download/com.castsoftware.imaging.all.docker/latest?platform=linux_x64" -H "x-nuget-apikey: a9999a9a-c999-999d-999b" -H "accept: application/octet-stream"
```

Unzip the resulting file anywhere on your local disk. The following files/folders will be visible:

- cast_imaging_node (folder)
- cast_imaging_services (folder)
- cast_imaging_viewer (folder)
- cast-imaging-install.sh (file)
- configuration.conf (file)

## Step 3 - Configure your installation

Locate the `configuration.conf` file at the root of the unzipped files and open it in a text editor such as `nano` or `vi`. Change only the following:

- `IMAGING_SERVICES_HOSTNAME`, `IMAGING_VIEWER_HOSTNAME`, `IMAGING_NODE_HOSTNAME`: for each entry, configure the current machine's FQDN (fully qualified domain name) or its static IP address - run the command `hostname -f` (FQDN) or `hostname -I` (IP address) to determine this:
  - for a single machine installation scenario, all three entries should be the same.
  - for a distributed installation scenario, you should enter the FQDN/IP address of the machine that will host that particular service. Ensure that machines can communicate with each other over the network, whichever option you use (FQDN/IP address).
- `IMAGING_NODE_VOLUME_DELIVERY_FOLDER`, `IMAGING_NODE_VOLUME_DEPLOY_FOLDER`, `IMAGING_NODE_VOLUME_SHARED_FOLDER`: set the path of the required data folders (as described in [file storage requirements](../../../requirements/disk/)):
    - for a single machine installation scenario, use local folders
    - for a distributed installation scenario with multiple node machines, use locally mounted network share drives so that all nodes can access the same resources, for example: `/mnt/shared/delivery`. Note that the paths must be identical on each machine and that UNC paths are not supported.
- `IMAGING_VIEWER_VOLUME_SHARED_FOLDER`: set the path of the folder which will be used by the `imaging-viewer` component for its configuration files. This should be a folder on the **local machine**, but should NOT exist already.

For example for a distributed installation scenario:

```
IMAGING_SERVICES_HOSTNAME=imaging-services.corp.domain.com
IMAGING_VIEWER_HOSTNAME=imaging-viewer.corp.domain.com
IMAGING_NODE_HOSTNAME=imaging-node.corp.domain.com

IMAGING_NODE_VOLUME_DELIVERY_FOLDER=/mnt/shared/delivery
IMAGING_NODE_VOLUME_DEPLOY_FOLDER=/mnt/shared/deploy
IMAGING_NODE_VOLUME_SHARED_FOLDER=/mnt/shared/common-data

IMAGING_VIEWER_VOLUME_SHARED_FOLDER=/home/CAST/imaging-viewer
```

{{% alert color="info" %}}<ul><li>All paths should use the Linux syntax forward slashes</li><li>The use of <code>localhost</code>, <code>127.0.0.1</code> or a simple hostname (i.e not a FQDN) for the various <code>_HOSTNAME</code> variables is not supported.</li><li>Other variables (for example port numbers) can be changed, however, CAST recommends you leave them at their default where possible.</li></ul>{{% /alert %}}

## Step 4 - Make files executable

Run the following commands to ensure that the installation scripts are executable:

```text
chmod +x cast-imaging-install.sh
```

If you are installing the **CAST Imaging Viewer** component on this machine (via the `all` or `imaging-viewer` commands), then you must also run the following commands, where:

- `<path>`: points to the grandparent folder of the folder defined for the variable `IMAGING_VIEWER_VOLUME_SHARED_FOLDER`. For example, if you have set `IMAGING_VIEWER_VOLUME_SHARED_FOLDER=/home/CAST/imaging-viewer` then you should run `chmod -R u+rwx /home`
- `<user>`: set to the username of the user that will run the installation script

```text
chmod +x cast_imaging_viewer/imagingsetup
chmod -R u+rwx <path>
chown <user>:<user> -R <path>
```

## Step 5 - Run the installation

### Scenario 1 - Install all components on one machine

![](one_machine.png)

Run the following command:

```text
./cast-imaging-install.sh all
```

On completion, check the status of the various Docker containers - there should be *12* in total:

```text
docker ps
```

### Scenario 2 - Install components on multiple machines

![](multiple_machines.png)

On each machine on which you want to install a component, ensure that you follow [Step 2](#step-2---download-the-installation-media), [Step 3](#step-3---configure-your-installation) and [Step 4](#step-4---make-files-executable), i.e.:

- download and unzip the installation media on each machine on which you want to install a component
- ensure the `configuration.conf` file on each machine is **identical**
- ensure the relevant files are executable

Then run the appropriate installation command on each machine for the component you would like to install. Ensure that the `imaging-services` component is always installed **first** and is completed before any other components are installed:

```text
./cast-imaging-install.sh imaging-services
./cast-imaging-install.sh imaging-viewer
./cast-imaging-install.sh analysis-node
```

{{% alert title="Note" color="info" %}}<ul><li>You can install any number of CAST Imaging Node Services (<code>analysis-node</code>) on dedicated machines to load balance your analysis requirements</li><li>You should only install CAST Imaging Services (<code>imaging-services</code>) and CAST Imaging Viewer (<code>imaging-viewer</code>) once in your network</li></ul>{{% /alert %}}

On completion, check the status of the various Docker containers:

```text
docker ps
```

| Command | No. of containers |
|---|:-:|
| `imaging-services` | 7 |
| `imaging-viewer` | 4 |
| `analysis-node` | 1 |

## Step 6 - Post install tasks

### Set permissions on persistent volumes

For each CAST Imaging Node Service (`analysis-node`) you have installed (including where you have used the `all` command) you must execute the following command on each machine to ensure that the `root` user has access to the persistent volumes shared with the CAST Imaging Node container, where:

- `<root_data_folder>`: points to the root of the data folder. For example, if you have set `IMAGING_NODE_VOLUME_DELIVERY_FOLDER=//machine/shared/delivery` then you should run `chmod -R u+rwx //machine/shared`


```text
chown -R 0:0 <root_data_folder>
```

### Open firewall ports

To ensure that:

- your users can access all CAST Imaging resources in their browser
- CAST Imaging components can communicate correctly in a multi-machine installation scenario

...you should ensure that port `8090` (TCP) is opened **inbound** on the machine hosting the `imaging-services` component (this is also valid for one machine installations using the `all` option).

## Step 7 - Initial start up configuration

When the install is complete, browse to the URL below and login using the default local `admin/admin` credentials: 

```
http://IMAGING_SERVICES_HOSTNAME:8090
```

You will be prompted to configure:

- your licensing strategy. Choose either a `Named Application` strategy (where each application you onboard requires a dedicated license key entered when you perform the onboarding), or a `Contributing Developers` strategy (a global license key based on the number of users):

![License key](../windows/license_key.jpg)

- [CAST Extend settings](../../../administer/settings/system-settings/extend) / [Proxy settings](../../../administer/settings/system-settings/proxy):

![CAST Extend settings](../windows/extend.jpg)

As a final check, browse to the URL below and ensure that you have at least one CAST Imaging Node Service, the CAST Dashboards and the CAST Imaging Viewer components listed:
```
http://IMAGING_SERVICES_HOSTNAME:8090/console/#/aic/manage/nodes
```
![Services](../windows/services.jpg)

## Step 8 - Configure authentication

Out-of-the-box, CAST Imaging is configured to use [Local Authentication](../../authentication/local/) via a simple username/password system. Default login credentials are provided (`admin/admin`) with the global `ADMIN` [profile](../../../administer/settings/user-permissions/) so that installation can be set up initially.

CAST recommends configuring CAST Imaging to use your on-premises enterprise authentication system such as LDAP or SAML Single Sign-on instead before you start to onboard applications. See [Authentication](../../authentication/) for more information.

## What is installed?

The following Docker containers will be created and are set to start automatically:

| Command | Container (Port) |
|---|---|
| `imaging-services` | <ul><li>postgres (2285)</li><li>gateway (8090)</li><li>console (8091)</li><li>auth-service (8092)</li><li>sso-service (8096)</li><li>dashboards (8097)</li><li>admin-center (2381, 8098)</li></ul> |
| `imaging-viewer` | <ul><li>neo4j (7473, 7474, 7687)</li><li>etl (9001)</li><li>open_ai_manager (8082)</li><li>server (9000, 8084, 8083)</li></ul>  | 
| `analysis-node` | <ul><li>analysis-node (8089)</li></ul> |

## What about analysis data storage?

All analysis data will be stored in the folders you defined in the `configuration.conf` file.

## What happens if a container does not start?

If a container fails to start, run the following command to display the container logs - this will give an indication of what may have gone wrong:
```text
docker logs <container_name>
```

## Uninstall process

### Remove everything from your Docker instance

{{% alert title="Warning" color="warning" %}}
Use these commands with care if you have items unrelated to CAST running in your Docker instance: they will remove everything from your Docker instance.
{{% /alert %}}

Run the following command to stop all running containers on your Docker instance:

```text
docker stop $(docker ps -a -q)
```

Now run the following commands to completely remove all containers:

```text
docker rm $(docker ps -a -q)
```

Now run the following command to completely remove all images:

```text
docker rmi $(docker images -a -q)
```

Now run the following command to completely remove all build cache:

```text
docker builder prune -a
```

Now run the following command to show any data left behind. This should only list volumes:

```text
docker system df -v
```

The volumes contain generated data for analyses and the database instance. If you want to ALSO remove the data volumes, run the following command, but note that this process cannot be reversed:

```text
docker volume prune -a
```

Finally, delete the installer ZIP file, the unzipped folder and any installation folders.

### Remove only CAST Imaging related items from your Docker instance

Run the following commands to stop the `imaging-services` containers:

```text
cd /<unzipped-folder>/cast-imaging-service
docker compose down
```

Run the following commands to stop the `node` containers:

```text
cd ../cast-imaging-node
docker compose down
```

Run the following commands to stop the `viewer` containers:

```text
// go to the "viewer" install location, by default this is set to `/home/CAST/imaging-Viewer` (defined as imaging_path below)
cd $imaging_path
docker compose down
```

Finally run the following commands to remove the images:

```text
docker images | grep $servicename | awk '{print $3}' | xargs -I {} docker rmi -f {}
...
docker image prune
```

### What is not removed?
- Analysis data files in the folders defined by the variables `IMAGING_NODE_VOLUME_xxx`
- Viewer data files in the folder defined by the variable `IMAGING_VIEWER_VOLUME_SHARED_FOLDER`
- Any application schemas on additional remote database instances you have been using.

If you intend to perform a clean install on the same machine(s) you should ensure that these items are removed first.