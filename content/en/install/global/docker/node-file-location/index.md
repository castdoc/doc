---
title: "Docker/Linux analysis-node file storage locations"
linkTitle: "analysis-node file storage locations"
type: "docs"
weight: 10
no_list: true
---

***

## Overview

When analyzing your applications, each CAST Imaging `analysis-node` will need to store various data, for example:

- logs
- extensions
- source code
- temporary files

The table below explains where these folders will be located by default.

## Locations

| Folder | Default path | Remarks |
|---|---|---|
| `delivery` | On machine(s) where `analysis-node` is installed: `/opt/cast/shared/delivery` | Contains reference information about applications and the various source code deliveries that have occurred for a given application.<br><br>For a **single machine** install or where only one single `analysis-node` is installed, this can be left as is.<br><br>For a **distributed/multi-node** deployment, a mounted network folder that all nodes can access is required, e.g. the parent `/opt/cast/shared` folder can be mounted to a network folder on a NAS or SAN. |
| `deploy` | On machine(s) where `analysis-node` is installed: `/opt/cast/shared/deploy` | Used to store certain types of source code which require further extraction and processing before analysis, such as `.castextraction` files or `.PDS` dump files.<br><br>For a **single machine** install or where only one single `analysis-node` is installed, this can be left as is.<br><br>For a **distributed/multi-node** deployment, a mounted network folder that all nodes can access is required, e.g. the parent `/opt/cast/shared` folder can be mounted to a network folder on a NAS or SAN. |
| `common-data` | On machine(s) where `analysis-node` is installed: `/opt/cast/shared/common-data` | A common location used for node related data (logs, backup, source code upload and other folders used by the node).<br><br>For a **single machine** install or where only one single `analysis-node` is installed, this can be left as is.<br><br>For a **distributed/multi-node** deployment, a mounted network folder that all nodes can access is required, e.g. the parent `/opt/cast/shared` folder can be mounted to a network folder on a NAS or SAN. |
| `Logs` | On machine(s) where `analysis-node` is installed: `/opt/cast/core/CAST/Logs` | Used for storing all technical logs produced by the node with regard to code delivery/analysis activities. One sub-folder folder will be created per application onboarded in CAST Imaging. The log files will contain the path and the name of the source file. For some warning messages (e.g.: syntax error), the path of the source code involved in the warning is shown in the log.<br><br>Must always be located on the node machine and is required on each node. |
| `LISA` / `LTSA` | On machine(s) where `analysis-node` is installed: `/opt/cast/core/CASTMS/LISA` and `opt/cast/core/CASTMS/LTSA` | Location to store temporary files generated during the analysis process on each node:<ul><li>Large Intermediate Storage Area (LISA) - cleaned on completion of each analysis.</li><li>Large Temporary Storage Area (LTSA) - is permanent and contains preprocessed source code files, archive files for analysis etc.</li></ul>Some technology extensions need to preprocess the source code before performing the analysis. In this case, the source code that will be analyzed is stored in the LISA folder.<br><br>Must always be located on the node machine and is required on each node. |
| `extensions` | On machine(s) where `analysis-node` is installed: `/opt/cast/core/Extensions` | Used for storing CAST technology extensions obtained by CAST Imaging from https://extend.castsoftware.com.<br><br>Must always be located on the node machine and is required on each node. |