---
title: "Global installation guide for CAST Imaging 3.x"
linkTitle: "Global install"
type: "docs"
weight: 30
no_list: true
---

***

{{< cardpane >}}
{{% card header="**Microsoft Windows**" footer="See [Installation on Microsoft Windows](windows/)." %}}

<i class="fa-brands fa-windows"></i> This install option is specifically for a direct installation on your Microsoft Windows machine. The installation script is completely flexible and allows you to install in whichever way suits your environment:

- All components on one machine
- Each component distributed across multiple dedicated machines (load balancing mode, supporting multiple analysis nodes)

 The CAST Imaging Core and database instance components are not included but are required to complete the deployment. Additional remote database instances can also be declared post install.
{{% /card %}}

{{% card header="**Linux via Docker**" footer="See [Installation on Linux via Docker](docker/)." %}}

<i class="fa-brands fa-docker"></i>/<i class="fa-brands fa-linux"></i> This install option is specifically for a direct install on `Docker` running on a Linux operating system. The installation script is completely flexible and allows you to install in whichever way suits your environment:

- All components on one machine in a single Docker instance
- Components distributed across multiple machines in separate Docker instances (load balancing mode, supporting multiple analysis nodes)

All components are provided as Docker images including the CAST Imaging Core component and a database instance. Additional remote database instances can also be declared post install.
{{% /card %}}
{{< /cardpane >}}
