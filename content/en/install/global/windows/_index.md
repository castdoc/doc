---
title: "Installation on Microsoft Windows"
linkTitle: Microsoft Windows
type: "docs"
weight: 20
no_list: true
---

***

## Overview

This install option is specifically for:
- Microsoft Windows
- those wanting to perform a clean install (see [In-place component update](../../../administer/update/component) if you have already performed an installation and now want to update to a newer release)

The installation media is provided in [com.castsoftware.imaging.console](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.console&version=latest) and comprises an installation script (.bat file), various configuration files and the binaries themselves.

The following components are provided:

- **CAST Imaging Services** including:
    - **CAST Dashboards**
- **CAST Imaging Viewer**
- **CAST Imaging Node Service** (does not include **CAST Imaging Core ≥ 8.4**)

The installation script is completely flexible and allows you to install in whichever way suits your environment:

- All components on one machine ("standalone" mode - ideal for POCs or testing purposes, or occasionally very small production deployments)
- Each component distributed across multiple dedicated machines, optionally with multiple CAST Imaging Node Services
 (i.e. load balancing mode - this is the recommended installation method)

This flexibility is achieved by running the installation script with specific install commands on specific machines:

| Install type | Command | Configuration file | Components installed |
|---|---|---|---|
| Single machine | `all` | `config-all.conf` | All components - single machine installation scenario. If you choose this option, you must ensure that your machine has sufficient resources to run all components: see [Requirements](../../../requirements/).<br><br>Requires:<ul><li>the [CAST Imaging Core ≥ 8.4](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.core&version=latest) component, which is not included in the installer and must be downloaded and  [installed](imaging-core/) separately on the same machine.</li><li>access to the [database](../../../requirements/db/) component, which is not included in the installer and must be downloaded and installed separately either on the same or another dedicated machine.</li></ul> |
| Distributed | `imaging-services` | `config-imaging-services.conf` | <ul><li>CAST Imaging Services</li><li>CAST Dashboards</li></ul>Should only be installed once (on a single machine) per installation of CAST Imaging.<br><br>Requires:<ul><li>access to the [database](../../../requirements/db/) component, which is not included in the installer and must be downloaded and installed separately either on the same or another dedicated machine.</li></ul> |
| Distributed | `imaging-viewer` | `config-imaging-viewer.conf` | <ul><li>CAST Imaging Viewer</li></ul>Should only be installed once (on a single machine) per installation of CAST Imaging.<br><br>Requires:<ul><li>access to the <code>imaging-services</code> component.</li><li>access to the [database](../../../requirements/db/) component, which is not included in the installer and must be downloaded and installed separately either on the same or another dedicated machine.</li></ul> |
| Distributed | `analysis-node` | `config-analysis-node.conf` | <ul><li>CAST Imaging Node Service</li></ul>Can be installed multiple times, once per separate dedicated machine to load balance.<br><br>Requires:<ul><li>access to the <code>imaging-services</code> component.</li><li>the [CAST Imaging Core ≥ 8.4](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.core&version=latest) component, which is not included in the installer and must be downloaded and [installed](imaging-core/) separately on the same machine.</li><li>access to the [database](../../../requirements/db/) component, which is not included in the installer and must be downloaded and installed separately either on the same or another dedicated machine.</li></ul> |

{{% alert title="Note" color="info" %}}<ul><li>The <a href="../../../requirements/db/">database</a> component is not included but at least one instance is required on your network to complete the deployment and must be installed separately, preferably on a dedicated machine. The instance must be accessible to all components over the internal network. Additional database instances can also be declared post install to load balance.</li><li>CAST's on premises <a href="../../extend-local/">Extend Local Server</a> (used where it is not possible to access CAST Extend over the internet) is not included and must be installed separately where required.</li></ul>{{% /alert %}}

{{% alert title="Warning" color="warning" %}}CAST does not support cross-platform installations, i.e. some CAST Imaging components on Linux/Docker and other components on Microsoft Windows, except where a PostgreSQL instance installed on Linux/Docker is being used with CAST Imaging installed on Microsoft Windows.{{% /alert %}}

## Requirements

- [Hardware requirements](../../../requirements/hardware/)
- [Software requirements](../../../requirements/software/)
- [Database requirements](../../../requirements/db/)
- [Disk space requirements](../../../requirements/disk/)

{{% alert color="info" %}}CAST recommends disabling **Quick Edit** mode in your command (CMD) window before running the install. This mode (if enabled) will pause the batch script if there is any interaction with the command window (e.g. a mouse click) during the process and will not re-start the script until a key is pressed:<br><br>![](quickedit.jpg){{% /alert %}}

## Step 1 - Determine your installation method

If you are installing all components on **one machine**, connect to the machine and proceed to [Step 2](#step-2---download-the-installation-media). Ensure that your machine has:
- sufficient resources to run all components: see [Requirements](../../../requirements/).
- access to the [database](../../../requirements/db/) component
- an installation of [CAST Imaging Core ≥ 8.4](imaging-core/)

Alternatively, if you want to install the components on **multiple machines**, you first need to determine which machine will run the **CAST Imaging Services** (`imaging-services`) component because this component must be installed first before any others. Connect to this machine and then proceed to [Step 2](#step-2---download-the-installation-media). Ensure the following:
- all machines must have access to the [database](../../../requirements/db/) component. This is not included in the installer and must be downloaded and installed separately either on the same or another dedicated machine BEFORE you begin the installation.
- machines on which you are installing the `analysis-node` component require the [CAST Imaging Core ≥ 8.4](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.core&version=latest) component, which is not included in the installer and must be downloaded and [installed](imaging-core/) separately on the same machine BEFORE you begin the installation of the `analysis-node` component.

## Step 2 - Download the installation media

Download the latest release of the installer and unzip it anywhere on your local disk. The following files and folders will be visible:

![](windows_unzip.jpg)

{{% alert color="info" %}}The resulting folder for the unzipped files must not contain one of the following characters: `( or ! or )`. The installer will fail to run if this is the case.{{% /alert %}}

## Step 3 - Configure your installation

### *.conf files

Locate the `*.conf` files at the root of the unzipped files. Open the files with a text editor and update the [installation variables](variables/):

- If you want to install all components on a single machine, update only the `config-all.conf` file.
- If you want to install components on separate dedicated machines, update the following files, depending on your installation scenario:
    - `config-imaging-services.conf`
    - `config-imaging-viewer.conf`
    - `config-analysis-node.conf`

{{% alert title="Note" color="info" %}}All variables must be identical across all `*.conf` files.{{% /alert %}}

### Open firewall ports

In a **multi-machine/distributed installation** scenario, to ensure that:

- your users will be able to access all CAST Imaging resources in their browser
- CAST Imaging components can communicate correctly with each other correctly

...you should ensure that all ports listed in [Hardware requirements](../../../requirements/hardware) are opened **inbound** on the relevant machine.

In a **single machine installation** scenario, only port `8090` (TCP) is should be opened **inbound** if you need to access CAST Imaging from another machine on the network.

### LICENSE_KEY and EXTEND_API_KEY variables

If you want to avoid having to manually input the **global license key** and the **CAST Extend API key** on first login to CAST Imaging (see [below](#step-5---initial-start-up-configuration)) use the `LICENSE_KEY` and `EXTEND_API_KEY` as explained in [Microsoft Windows installation variables](variables/).

### CAST_INSTALLATION_CONF Microsoft Windows system environment variable

By default, the CAST Imaging installer will store information/properties files related to the installation in the following folder on disk:

```text
%PROGRAMDATA%\CAST\CAST\.install
```

This information is re-used during [in-place updates](../../../administer/update/component/) to a new release and during product uninstall. If you would prefer to store this folder in another location, you can set a Microsoft Windows system environment variable called `CAST_INSTALLATION_CONF` on all machines where you will install CAST Imaging components and point it to a local folder of your choice:

![](env_variable.jpg)

## Step 4 - Run the installation

### Scenario 1 - Install all components on one machine

![](../docker/one_machine.png)

Open a CMD window with elevated permissions (right click, `Run as administrator`) and execute the following command from the root of the unzipped files:

```text
cast-imaging-install.bat package=all
```

On completion, check the status of the various Microsoft Windows Services, there should be *11*:

![](windows_services.jpg)

### Scenario 2 - Install components on multiple machines

![](../docker/multiple_machines.png)

On each machine on which you want to install a component, ensure that you follow [Step 2](#step-2---download-the-installation-media) and [Step 3](#step-3---configure-your-installation), i.e.:

- download and unzip the installation media on each machine on which you want to install a component
- ensure the relevant `*.conf` file on each machine contains the correct [installation variables](variables/)

Then open a CMD window with elevated permissions (right click, `Run as administrator`) and execute the appropriate installation command on each machine for the component you would like to install. Ensure that the `imaging-services` component is always installed **first** and is completed before any other components on other machines are installed:

```text
cast-imaging-install.bat package=imaging-services
cast-imaging-install.bat package=imaging-viewer
cast-imaging-install.bat package=analysis-node
```

{{% alert title="Note" color="info" %}}<ul><li>You can install any number of CAST Imaging Node Services (<code>analysis-node</code>) on dedicated machines to load balance your analysis requirements</li><li>You should only install CAST Imaging Services (<code>imaging-services</code>) and CAST Imaging Viewer (<code>imaging-viewer</code>) once in your network</li></ul>{{% /alert %}}

On completion, check the status of the various Microsoft Windows Services:

| .conf file | No. of services |
|---|:-:|
| `package=imaging-services` | 6 |
| `package=imaging-viewer` | 5 |
| `package=analysis-node` | 1 |

## Step 5 - Initial start up configuration

When the install is complete, connect to CAST Imaging using your browser via the following URL:
- `http://localhost:8090` if you are using the machine on which the `imaging-services` component has been installed
- `http://PUBLIC_URL:8090` from a remote machine on the network

Login using the default `admin/admin` credentials. You will be prompted to configure:

- your licensing strategy. Choose either a `Named Application` strategy (where each application you onboard requires a dedicated license key entered when you perform the onboarding), or a `Contributing Developers` strategy (a global license key based on the number of users):

![License key](license_key.jpg)

- [CAST Extend settings](../../../administer/settings/system-settings/extend) / [Proxy settings](../../../administer/settings/system-settings/proxy):

![CAST Extend settings](extend.jpg)

{{%alert color="info" %}}If you have configured the `LICENSE_KEY` and `EXTEND_API_KEY` variables as explained in [Microsoft Windows installation variables](variables/), then this initial configuration will not be displayed.{{% /alert %}}

As a final check, browse to the URL below and ensure that you have at least one node, the CAST Imaging Viewer and the CAST Dashboard components listed:

```
http://PUBLIC_URL:8090/admin/services
```
![Services](services.jpg)

## Step 6 - Configure authentication

Out-of-the-box, CAST Imaging is configured to use [Local Authentication](../../authentication/local/) via a simple username/password system. Default login credentials are provided (`admin/admin`) with the global `ADMIN` [profile](../../../administer/settings/user-permissions/) so that installation can be set up initially.

CAST recommends configuring CAST Imaging to use your on-premises enterprise authentication system such as LDAP or SAML Single Sign-on instead before you start to onboard applications. See [Authentication](../../authentication/) for more information.

## What is installed?

The following Microsoft Windows services, set to start automatically and running with `LocalSystem`:

| Component | Service name | Port |
|---|---|---|
| `imaging-services` | CAST Imaging Authentication | 8092 |
| `imaging-services` | CAST Imaging Console Service | 8091 |
| `imaging-services` | CAST Imaging Control Panel | 2381, 8098 |
| `imaging-services` | CAST Imaging Dashboards | 8097 |
| `imaging-services` | CAST Imaging Gateway Service | 8090 |
| `imaging-services` | CAST Imaging SSO Service | 8096, 9002 |
| `imaging-viewer` | CAST Imaging Viewer AI Manager | 8094 |
| `imaging-viewer` | CAST Imaging Viewer Frontend | 8093 |
| `imaging-viewer` | CAST Imaging Viewer ETL | 9011 |
| `imaging-viewer` | CAST Imaging Viewer Backend | 9010 |
| `imaging-viewer` | CAST Imaging Neo4j Graph Database | 6372, 7483, 7484, 7697 |
| `analysis-node` | CAST Imaging Analysis Node instance | 8099 |

{{% alert title="Note" color="info" %}}<ul><li>All Microsoft Windows services will be set to <strong>"Automatic"</strong> start. This means that they will automatically start up whenever the host machine is started or rebooted. However, since some Microsoft Windows services are dependent on each other, you may find that when you reboot the host machine, some services will have failed to start because a dependent service has not started quickly enough. See <a href="service-startup-checker">Services startup checker</a> for information about a tool that will ensure your services are all running.</li><li>All Microsoft Windows services will be set to use the <code>Local System account</code> unless you have specifically defined a dedicated user via the <code>.conf</code> file using the <code>START_AS_USER</code>, <code>...START_AS_USER</code>, <code>START_AS_PASSWORD</code>, <code>...START_AS_PASSWORD</code>, see <a href="variables/">installation variables</a> and <a href="../../../requirements/software/#microsoft-windows-services">Software requirements</a>.</li></ul>{{% /alert %}}

## Uninstall process

The installation media contains a dedicated uninstaller script called `cast-imaging-remove.bat`. To run the uninstaller, open a CMD window with elevated permissions (right click, `Run as administrator`) and execute the relevant command from the root of the unzipped files to remove the required component depending on what is installed on the current machine:

```text
cast-imaging-remove.bat package=all
cast-imaging-remove.bat package=imaging-services
cast-imaging-remove.bat package=imaging-viewer
cast-imaging-remove.bat package=analysis-node
```

The relevant `*.conf` files should contain the [Installation variables](variables/) that match the components you have installed.

{{% alert color="info" %}}To avoid a prompt during the uninstall process, ensure that the `DELETE_ADDITIONAL_DATA_FOLDERS` is explicitly set to either `true` to force the removal of existing analysis data storage folders defined by the `SHARED_FOLDER`, `DELIVERY_FOLDER` and `DEPLOY_FOLDER` variables, or `false` to retain these folders. If you intend to perform a clean install you should set this option to `true`.{{% /alert %}}

{{% alert color="warning" %}}Do not use the Microsoft Windows **Add/Remove Program** feature to uninstall CAST Imaging components, always use the `cast-imaging-remove.bat` file.{{% /alert %}}

The installer will remove all components and related Microsoft Windows Services, including the following database items (the uninstaller will use the `*.conf` file to obtain details of the target database instance):
- `keycloak_v3` database
- `admin_center` schema
- `analysis_node` schema

### What is not removed?

- Any schemas associated with applications stored on your database instance(s), e.g. `<app_name>_local`, `<app_name>_central`, `<app_name>_mngt`.
- `general_measure` schema (or equivalent name).
- CAST Imaging Core, wherever this component has been installed. Use the `Add or remove programs` feature to uninstall.
- CAST Storage Service, wherever this component has been installed. Use the `Add or remove programs` feature to uninstall.