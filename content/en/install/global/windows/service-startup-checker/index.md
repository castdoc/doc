---
title: "Microsoft Windows Services startup checker"
linkTitle: "Services startup checker"
type: "docs"
weight: 40
no_list: true
---

***

## Overview

The Microsoft Windows Services for the `CAST Imaging Services` and `CAST Imaging Node` packages are dependent on each other, therefore you may find that when you reboot the host machine, some Microsoft Windows Services will have failed to start because a dependent service has not started quickly enough.

The Microsoft Windows Services in question are as follows:

CAST Imaging Services

- CAST Imaging Authentication
- CAST Imaging Console Service
- CAST Imaging Control Panel
- CAST Imaging Gateway Service
- CAST Imaging SSO Service

CAST Imaging Analysis Node

- CAST Imaging Analysis Node

To resolve this issue, CAST provides a batch script (which can be run either manually or automatically via a `Scheduled Task`) that will check whether the services are up and running, and if not, start them for you.

{{% alert title="Note" color="info" %}}This batch script is designed to be run on machines where you have [installed](..) CAST Imaging using the `all` and `imaging-services` components.{{% /alert %}}

## Where is the batch script located?

You can find the batch script in the following location, post installation:

```
%PROGRAMFILES%\CAST\Imaging\CAST-Imaging-sync-all-services-on-startup.bat
```

## How do I run the batch script manually?

The script needs to be run with elevated local permissions, therefore, right click the `.bat` file and select `Run as administrator`.

The script will launch a command window which will self close when the check/startup is complete. Check that your services are now **Running** in the Microsoft Windows Services control panel.

## How do I run the batch script automatically?

The script can be set to run automatically on machine reboot via a Microsoft Windows `Scheduled Task` using the `Create Basic Task` option:

![](basic_task.jpg)

Tips:

- Set the `Triggers` to `At system startup`
- Enable the `Run whether user is logged on or not` and `Run with highest privileges` options:

![](general.jpg)

- You may need to set a delay and set the task to run every minute (for example) for a certain period of time after reboot to ensure that any CAST Imaging Microsoft Windows Services that have failed to start are started eventually:

![](trigger.jpg)
