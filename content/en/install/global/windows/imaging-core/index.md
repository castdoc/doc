---
title: "Install CAST Imaging Core for Microsoft Windows"
linkTitle: "Install CAST Imaging Core"
type: "docs"
weight: 10
no_list: true
---

***

## Overview

CAST Imaging Core is the "analysis engine" providing essential services to CAST analyzers. It must be installed manually on every single `analysis-node` in your CAST Imaging installation for Microsoft Windows, including single machine installations.

{{% alert color="info"  title="Note" %}}<ul><li>For <a href="../../docker/">Docker/Linux</a> installations, CAST Imaging Core 8.4.x is provided as a Docker image which also includes the `analysis-node` service, eliminating the need for manual installation.</li><li>CAST Imaging Core 8.4.x can co-exist on the same machine with CAST AIP Core 8.3.x (com.castsoftware.aip).</li></ul>{{% /alert %}}

## Installation methods

### Interactive installation procedure

1. Download and unzip the latest release of [com.castsoftware.imaging.core](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.core&version=latest) on your machine.
1. Right-click the `setup.bat` file at the root of the unzipped files and select `Run as administrator`.
1. Follow the interactive installer prompts, leaving all options at their default where possible.

### Silent installation (for multiple analysis-nodes)

#### Step 1 - Create an Answer File

1. On your first `analysis-node`, download and unzip the installation media.
1. Open the Command Prompt (CMD) with administrator privileges (right click, `Run as administrator`).
1. Navigate to the folder containing `setup.exe` (located in the `data` folder).
1. Run the following command (replace with your desired path for storing the answer file - note that this folder must already exist before running the command):

```text
setup.exe /r /f1"n:\temp\setup.iss"
```

5. Complete the installation normally. **Important:** Do NOT use the "back" button during setup as this will invalidate the answer file.
1. When the installation completes, the answer file (`setup.iss`) will be created at your specified location.

#### Step 2 - Deploy to additional analysis-nodes

1. On each subsequent `analysis-node`, download and unzip the installation media.
1. Open the Command Prompt (CMD) with administrator privileges (right click, `Run as administrator`).
1. Navigate to the required location and run:

```text
setup.exe /s /f1"n:\temp\setup.iss" /f2"c:\path\to\log.txt"
```

4. The installation will run unattended using the parameters in your answer file.
1. A progress indicator will appear in the task bar, and the setup will close automatically when complete.

#### Silent install log message codes

| Code | Meaning |
|---|---|
| 0 | Success |
| 1 | General error |
| 2 | Invalid mode |
| 3 | Required data not found in the Setup.iss file |
| 4 | Not enough memory available |
| 5 | File does not exist |
| 6 | Cannot write to the response file |
| 7 | Unable to write to the log file |
| 8 | Invalid path to the InstallShield Silent response file |
| 9 | Not a valid list type (string or number) |
| 10 | Data type is invalid |
| 11 | Unknown error during setup |
| 12 | Dialogs are out of order |
| 51 | Cannot create the specified folder |
| 52 | Cannot access the specified file or folder |
| 53 | Invalid option selected |

## What is installed?

### Files

After installation, CAST Imaging Core files are stored in:

```text
%PROGRAMFILES%\CAST\<version>
%PROGRAMDATA%\CAST\CAST\<version>
%APPDATA%\CAST\CAST\<version>
```

### Third-party software

- Visual C++ 64-bit Redistributable Packages for Visual Studio 2015-2022
- Visual C++ 32-bit Redistributable Packages for Visual Studio 2015-2022
- Microsoft Build Tools 2015 (BuildTools_Full.exe)
- NET Framework 4.7.2

{{% alert color="info" %}}The installer will check for and skip installation of any components already present.{{% /alert %}}

## Uninstall

Use the Microsoft Windows "Add or remove programs" option to uninstall CAST Imaging Core. Third-party software is not removed.