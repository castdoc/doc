---
title: "Microsoft Windows installation variables"
linkTitle: Installation variables
type: "docs"
weight: 20
no_list: true
---

## Overview

A list and explanation of the variables available for an [Installation on Microsoft Windows](..).

## config-all.conf (single machine)

| Variable | Description |
|---|---|
| `PUBLIC_URL` | Defines the "public" hostname (FQDN - fully qualified domain name) for the current machine to allow users to access CAST Imaging over the internal network. For example, `imaging.corp.domain.com`. If you do not intend to access the installed UI from remote devices in your network (i.e. you will be accessing it from the same machine it is installed on), then you can leave this variable set to `localhost`. |
| `HOSTNAME_xxx` | Defines the hostname for communication between specific CAST Imaging services. These variables can be updated to the current machine's "public" hostname (FQDN - fully qualified domain name), or you can leave these variables at the default `localhost` since all communication is within the same machine. Whichever you choose, all variables must use the same type, i.e. all must use `localhost` or all must use the FQDN. |
| `ANALYSIS_NODE_HOSTNAME` | Defines the hostname for your `analysis-node` service. This variable can be updated to the current machine's "public" hostname (FQDN - fully qualified domain name), or you can leave this variable at the default `localhost` since all communication is within the same machine. |
| `PORT_xxx` or `xxx_PORT` | Defines the TCP port number for each CAST Imaging service. These variables can be customized, or you can leave these variables at the default. |
| `CSS_INFOS` | Details of your local or remote CAST Storage Service/PostgreSQL instance in the format `HOSTNAME:PORT`, e.g. the FQDN or hostname such as `css.corp.domain.com:2284`. Only use `localhost:PORT` if your CAST Storage Service/PostgreSQL instance is installed on the SAME machine as ALL other components. See also [Database requirements](../../../../requirements/db/). |
| `CSS_USER` and `CSS_PASSWORD` | Credentials (username/password) for your CAST Storage Service/PostgreSQL instance defined via `CSS_INFOS`. Note that if you do not want to enter the password into the configuration file for security reasons, leave the `CSS_PASSWORD` entry blank and you will be prompted for the password during the installation process. |
| `CSS_ENCRYPTED_PASSWORD` | This variable is specifically and ONLY for use when your target CAST Storage Service/PostgreSQL instance is configured with credentials (either username and/or password) other than the default `operator/CastAIP`. If this is the case, you should enter the custom username under `CSS_USER`, the unencrypted custom password under `CSS_PASSWORD` (you can leave the variable blank and fill in during the installation when prompted if necessary) and you should finally enter an encrypted version of the same password as used for `CSS_PASSWORD` in `CSS_ENCRYPTED_PASSWORD`. To encrypt the password, you can use the a tool available here: `%PROGRAMFILES%\CAST\Imaging\CAST-Imaging-Analysis-Node\admin\aip-encryption-tool.bat`. |
| `V3_DB_NAME` | The database you would like to use to store the `admin-center` and `analysis-node` persistence schemas (`postgres` or other custom database). See also [Database requirements](../../../../requirements/db/). |
| `KEYCLOAK_DB` | The name of the database you would like to use to store the CAST Imaging authentication system (Keycloak) persistence database. See also [Database requirements](../../../../requirements/db/). |
| `INSTALL_DIR` | Root installation location for this installer, e.g. `C:\Program Files\CAST\Imaging`. If the chosen path is anything other than `C:\Program Files`, it must not contain any white space. |
| `DATA_DIR` | Sets the installation data folder (avoid using a network drive (even mapped)). A default data folder will be assigned if you do not provide a value here, as follows:<ul><li>With <code>INSTALL_DIR</code> set to the default (or any folder in <code>%PROGRAMFILES%</code>), <code>DATA_DIR</code> will be automatically set to <code>%PROGRAMDATA%\CAST\Imaging</code>.</li><li>With <code>INSTALL_DIR</code> set to a custom location, then a folder called <code>Data</code> will be created in the root of <code>INSTALL_DIR</code>.</li></ul> |
| `AIP_INSTALLDIR` | CAST Imaging Core component installation location (i.e. where CAST Imaging Core is installed on the local machine) e.g: `C:\Program Files\CAST\8.4`. |
| `DELETE_ADDITIONAL_DATA_FOLDERS` | Set to `true` if you would like the installer to delete the contents of the folders defined by the options `SHARED_FOLDER`, `DELIVERY_FOLDER` and `DEPLOY_FOLDER` (this is recommended when you want to perform a clean install and you want to re-use the same folders). If the option is set to `false` (default) then these folders will not be deleted and will be re-used during the installation. |
| `SHARED_FOLDER`, `DELIVERY_FOLDER`, `DEPLOY_FOLDER` | Details of the folder paths for analysis data storage for each node (each path must be distinct). In a single machine installation scenario (all components on one single machine) and you do not intend to add additional nodes in the future, these paths can point to local paths on the machine itself. If no value is assigned, the default paths will be used: `C:\cast-node\common-data`, `C:\cast-node\delivery` and `C:\cast-node\deploy`. See also [analysis-node file storage locations](../node-file-location/) and [Software requirements](../../../../requirements/software/#microsoft-windows-services) for more information about the user account required to run the Microsoft Windows Services when using share network folders. |
| `SOURCES_FOLDER_LOCATION` | Optional (only available for single machine/standalone installations): auto configures a path to your application source code for delivery purposes. See [Configuring source code delivery from a folder](../../../../administer/settings/global-configuration/source-folder-location/). This path will be checked for validity during the installation - if it is not accessible it is logged in the installation log and will not be configured. |
| `INSTANCE_ID` | This variable determines how the name of the Node Service is displayed in CAST Imaging in [Services](../../../../administer/settings/services). If you make no change, the default is used: `MACHINE_HOSTNAME:8099`. |
| `USE_IPADDRESS` | This variable is set to `false` by default and should be left in this position for standalone installations (all on one machine) . Set to `true` if you are installing CAST Imaging on multiple machines (distributed/enterprise installation) or you are adding additional nodes to an initial installation all on one machine. |
| `XXX_START_AS_USER` and `XXX_START_AS_PASSWORD` | These variables determine the user account used to run the Microsoft Windows services. See also [Software requirements](../../../../requirements/software/#microsoft-windows-services). |
| `LICENSE_KEY` | Optional: The global license key (**Contributing Developers**) in plain text. This will avoid the need to provide this key during the initial start-up of the `imaging-services` component. Note that if you intend to use **Named Application** license keys, **do not use this variable** (you will need to choose the **Named Application** option in the initial configuration wizard). |
| `EXTEND_API_KEY` | Optional: The Extend API key in encrypted format. To encrypt the key use the executable JAR (requires an installation of Java) located in `\tools\bin\imaging-encryption-tool.jar` in the unzipped global installer folder. This will avoid the need to provide this key during the initial start-up of the `imaging-services` component. |

## config-imaging-services.conf (distributed installation)

| Variable | Description |
|---|---
| `PUBLIC_URL` | Defines the "public" hostname (FQDN - fully qualified domain name) for the current machine to allow users to access CAST Imaging over the internal network. For example, `imaging.corp.domain.com`. This variable must always be updated to the current machine's "public" hostname (FQDN - fully qualified domain name). |
| `HOSTNAME_xxx` | Defines the hostnames for the `imaging-services` service. All these variables must be identical within the file, must always be updated to the current machine's "public" hostname (FQDN - fully qualified domain name) and must be identical to `PUBLIC_URL`. |
| `CSS_INFOS` | Details of your local or remote CAST Storage Service/PostgreSQL instance in the format `HOSTNAME:PORT`, e.g. the FQDN or hostname such as `css.corp.domain.com:2284`. Do not use `localhost:PORT` even if your CAST Storage Service/PostgreSQL instance is installed on the SAME machine as the `imaging-services` component. See also [Database requirements](../../../../requirements/db/). |
| `CSS_USER` and `CSS_PASSWORD` | See description [above](#config-allconf-single-machine) |
| `CSS_ENCRYPTED_PASSWORD` | See description [above](#config-allconf-single-machine) |
| `V3_DB_NAME` | See description [above](#config-allconf-single-machine) |
| `KEYCLOAK_DB` | See description [above](#config-allconf-single-machine) |
| `SECURED_CONNECTION` | Used for the health check actioned at the end of the component installation process to check that the component is "up" and running. For an initial installation of this component where [HTTPS](../../../https/) has **not yet** been enabled, this option must be set to `false`. If you subsequently enable [HTTPS](../../../https/) and are performing a component **update** to a new release, this variable must be changed to `true`. |
| `INSTALL_DIR` | See description [above](#config-allconf-single-machine) |
| `DATA_DIR` | See description [above](#config-allconf-single-machine) |
| `SHARED_FOLDER`, `DELIVERY_FOLDER`, `DEPLOY_FOLDER` | Details of the folder paths for analysis data storage for each node (each path must be distinct). In an enterprise/distributed deployment, a mapped network share drive or a UNC path of the network share must be used, for example: `\\server\imaging\shared` etc., so that all nodes can access common data. If no value is assigned, the default paths will be used: `C:\cast-node\common-data`, `C:\cast-node\delivery` and `C:\cast-node\deploy` and then these must be shared across the network. See also [analysis-node file storage locations](../node-file-location/) and [Software requirements](../../../../requirements/software/#microsoft-windows-services) for more information about the user account required to run the Microsoft Windows Services when using shared network folders. |
| `XXX_START_AS_USER` and `XXX_START_AS_PASSWORD` | See description [above](#config-allconf-single-machine) |
| `LICENSE_KEY` | See description [above](#config-allconf-single-machine) |
| `EXTEND_API_KEY` | See description [above](#config-allconf-single-machine) |

## config-analysis-node.conf (distributed installation)

| Variable | Description |
|---|---|
| `PUBLIC_URL` | Defines the "public" hostname (FQDN - fully qualified domain name) for the current machine. For example, `imaging.corp.domain.com`. This variable must always be updated to the current machine's "public" hostname (FQDN - fully qualified domain name) and must be identical to `HOST_HOSTNAME`. |
| `HOST_HOSTNAME` | Defines the hostname for the `analysis-node` service. This variable must always be updated to the current machine's "public" hostname (FQDN - fully qualified domain name) and must be identical to `PUBLIC_URL`. |
| `CSS_INFOS` | Details of your local or remote CAST Storage Service/PostgreSQL instance in the format `HOSTNAME:PORT`, e.g. the FQDN or hostname such as `css.corp.domain.com:2284`. Do not use `localhost:PORT` even if your CAST Storage Service/PostgreSQL instance is installed on the SAME machine as the `analysis-node` component. See also [Database requirements](../../../../requirements/db/). |
| `CSS_USER` and `CSS_PASSWORD` | See description [above](#config-allconf-single-machine) |
| `V3_DB_NAME` | See description [above](#config-allconf-single-machine) |
| `KEYCLOAK_DB` | See description [above](#config-allconf-single-machine) |
| `HOSTNAME_xxx` | Defines the hostname for communication from the node service to the specific CAST Imaging services. These variables must always be updated to the "public" hostname (FQDN - fully qualified domain name) of the  machine hosting the `imaging-services` component (to match the `PUBLIC_URL` defined in the `config-imaging-services.conf` or `config-all.conf` files) so that the Node can access the required services over the internal network. |
| `SECURED_CONNECTION` | Used for the health check actioned at the end of the component installation process to check that the component is "up" and running. For an initial installation of this component where [HTTPS](../../../https/) has **not yet** been enabled, this option must be set to `false`. If you subsequently enable [HTTPS](../../../https/) and are installing an **additional** node or you are performing a component **update** to a new release, this variable must be changed to `true`. |
| `INSTALL_DIR` | See description [above](#config-allconf-single-machine) |
| `DATA_DIR` | See description [above](#config-allconf-single-machine) |
| `AIP_INSTALLDIR` | See description [above](#config-allconf-single-machine) |
| `DELETE_ADDITIONAL_DATA_FOLDERS` | Set to `true` if you would like the installer to delete the content of the folders defined by the options `SHARED_FOLDER`, `DELIVERY_FOLDER` and `DEPLOY_FOLDER` in the `config-imaging-services.conf` or `config-all.conf` files (this is recommended when you want to perform a clean install). If the option is set to `false`(default) then these folders will not be deleted and will be re-used during the installation. |
| `INSTANCE_ID` |  See description [above](#config-allconf-single-machine) |
| `USE_IPADDRESS` | This variable is set to `false` by default and should be set to `true` if you are installing CAST Imaging on multiple machines (distributed/enterprise installation) or you are adding additional nodes to an initial installation all on one machine. |
| `XXX_START_AS_USER` and `XXX_START_AS_PASSWORD` | These variables determine the user account used to run the Microsoft Windows services. See also [Software requirements](../../../../requirements/software/#microsoft-windows-services). |

## config-imaging-viewer.conf (distributed installation)

| Variable | Description |
|---|---|
| `HOSTNAME_VIEWER` | Defines the hostname for `imaging-viewer` service. This variable must always be updated to the current machine's "public" hostname (FQDN - fully qualified domain name), i.e. the same as `PUBLIC_URL` defined in the `config-imaging-services.conf` file. |
| `HOSTNAME_CONTROLPANEL` | Defines the hostname for communication from the `imaging-viewer` service to the specific `imaging-services` services, therefore this variable must always be updated to the "public" hostname (FQDN - fully qualified domain name) of the machine hosting the `imaging-services` component (to match the `PUBLIC_URL` defined in the `config-imaging-services.conf` file) so that the `imaging-viewer` service can access the required services over the internal network. |
| `INSTALL_DIR` | See description [above](#config-allconf-single-machine) |
| `DATA_DIR` | See description [above](#config-allconf-single-machine) |
