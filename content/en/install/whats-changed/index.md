---
title: "What's changed in CAST Imaging v3?"
linkTitle: "What's changed in CAST Imaging v3?"
type: "docs"
weight: 10
no_list: true
---

***

## Overview

CAST Imaging v3 is a unification of two distinct existing systems, CAST Console v2 and CAST Imaging Viewer v2. As part of this "unification process" some features and behaviour that exist in the "v2" products has changed. This page provides a non-exhaustive list of these changes and some answers to frequently asked questions.

{{% alert color="info" %}}You can also view the [release notes](../../release-notes/) for the various components.{{% /alert %}}

## FAQ

- Q: What's the migration path from CAST Imaging v2 (deployed on Microsoft Windows) to CAST Imaging v3 (deployed on Microsoft Windows)?
  - A: The migration is supported in release 3.2 together with Imaging Core 8.4.1, released in January 2025. The process is documented in [Migration from v2](../migrate/). CAST technical support will provide assistance during this migration.
- Q: What's the migration path from CAST Imaging v2 (deployed on Microsoft Windows) to CAST Imaging v3 (deployed on a Linux container)?
  - A: There is no migration available from Microsoft Windows to Linux. Data is encoded and stored differently on the Linux version, which prevents a migration. Customers should consider deploying v3 on a Linux container as a new CAST Imaging installation, and then re-onboard all their applications, re-create all personal content previously created (saved views, tags, annotations, groupings, etc.) and do without historical dashboards data.
- Q: What's the migration path from CAST Imaging v2 (deployed on a Linux container) with Node/AIP Core on Microsoft Windows?
  - A: There is no migration available in this specific scenario. Customers should consider deploying v3 on a Linux container as a new CAST Imaging installation, and then re-onboard all their applications, re-create all personal content previously created (saved views, tags, annotations, groupings, etc.) and do without historical dashboards data.
- Q: Are dashboards available in CAST Imaging v3?
  - A: All three dashboards will be available in CAST Imaging in a future release (currently planned for Q1 2025).

## Changes

### UI

There is now one single "unified" UI interface for end-users and administrators. This interface allows users to:

- onboard applications
- configure analyses
- access results in "Viewer" and "Engineering Dashboard"
- administer and configure global settings

### CAST Imaging Core (ex. AIP Core)

CAST Imaging v3 requires CAST Imaging Core 8.4: [com.castsoftware.imaging.core](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.core&version=latest). This component is provided as follows:

- For **Microsoft Windows** deployments: as a "standalone" installation - see [Install CAST Imaging Core](../global/windows/imaging-core).
- For **Docker/Linux** deployments: in an image that includes the Node service (this image is automatically installed as part of the [global install](../global/docker)).

### Technology coverage with CAST Imaging Core 8.4

The following technologies are not supported by CAST Imaging Core 8.4 although they are supported by CAST AIP Core (com.castsoftware.aip) 8.3: 

- C/C++ (available in a future release, currently planned for the end of Q1 2025)
- SAP ABAP (available in a future release, currently planned for Q2 2025)
- Oracle Forms/Reports (available in a future release, currently planned for H2 2025)
- EGL (not planned as of today, the plans might be revised in Q2 2025)
- FLEX (not planned as of today, the plans might be revised in Q2 2025)
- Fortran (not planned as of today, the plans might be revised in Q2 2025)
- PeopleSoft (not planned as of today, the plans might be revised in Q2 2025)
- SAP BusinessObjects (not planned as of today, the plans might be revised in Q2 2025)
- SAP PowerBuilder (not planned as of today, the plans might be revised in Q2 2025)
- Siebel (not planned as of today, the plans might be revised in Q2 2025)
- Swift Objective-C (not planned as of today, the plans might be revised in Q2 2025)
- TIBCO (not planned as of today, the plans might be revised in Q2 2025)
- VisualBasic (not planned as of today, the plans might be revised in Q2 2025)

### Hardware requirements - RAM

- For standalone mode deployments (all components on one machine), 32GB RAM is the absolute minimum requirement.
- For enterprise/distributed mode deployments, 16GB RAM absolute minimum, 32GB RAM highly recommended. On a machine configured as a node where the [com.castsoftware.securityforjava](../../technologies/jee/extensions/com.castsoftware.securityforjava/) extension is used for JEE [Security Dataflow](../../interface/analysis-config/security-dataflow/) analyses, 32GB RAM is required.

See [What hardware do I need?](../../requirements/hardware/).

### Database requirements

CAST Imaging v3 requires an RDBMS to store both the data generated during analyses and persistence data (settings/properties etc.) and at the current time CAST supports only `PostgreSQL` as follows:

- CAST Storage Service ≥ 4.13.x
- PostgreSQL 13.x, 14.x, 15.x

{{% alert color="info" %}}<ul><li>See <a href="../../requirements/db/">What are the database requirements?</a></li><li>When installing CAST Imaging on <a href="../global/docker/">Linux via Docker</a>, CAST provides a database instance (<strong>PostgreSQL 13.3</strong>) as a Docker image. By default, this instance will be used by CAST Imaging for both analysis data and persistence data storage needs. However, you are free to install additional database instances, for example to separate the storage of persistence data and analysis data on two separate instances.</li></ul>{{% /alert %}}

### Features

#### Legacy vs Fast Scan onboarding workflow

CAST Imaging v3 does not support the "legacy" onboarding workflow that is available in CAST Console v2. In other words, any applications that are migrated to CAST Imaging v3 and were previously using the "legacy" onboarding workflow will be automatically converted to use the Fast Scan onboarding workflow as part of the [Migration from v2](../migrate) and any new applications created in CAST Imaging v3 will always use this Fast Scan workflow.

A reminder that the Fast Scan onboarding workflow functions as follows:

- Source code is uploaded via a ZIP file or directly from a source code folder.
- An initial fast scan is then run and results of this are displayed.
- The delivered source code can then be inspected (size, structure etc.), source code filters (exclusions) can be defined and an architecture preview diagram can be consulted. You will be alerted you if the architecture preview produced by the fast scan identifies missing source code - you can provide this missing code before you start the analysis process.
- Following that, an analysis and publishing of the results are actioned.

{{% alert color="info" %}}When using the [com.castsoftware.aip.console.tools](https://extend.castsoftware.com/#/extension?id=com.castsoftware.aip.console.tools&version=latest) automation extension, the two step workflow used in the Fast Scan onboarding workflow (fast scan then analysis) is avoided.{{% /alert %}}

#### Security Dataflow

The [Security Dataflow](../../interface/analysis-config/security-dataflow/) feature is now **enabled by default** for supported technologies (JEE and .NET) and will be triggered automatically during the initial analysis for **new applications** (in CAST Console v2 this feature is always disabled and must be manually enabled). This change may impact analysis performance for large applications and will likely increase the number of violations identified during the analysis for all applications. You can manually disable the Security Dataflow feature after the intial analysis has completed, however, this will likely change the number of identified violations for your application. Finally you should also take note of the RAM requirement (see above) for node machines when the application contains JEE source code which will trigger the installation of the [com.castsoftware.securityforjava](../../technologies/jee/extensions/com.castsoftware.securityforjava/) extension.

Note that when [migrating from v2 to v3](../migrate/), Security Dataflow settings are preserved for migrated applications.