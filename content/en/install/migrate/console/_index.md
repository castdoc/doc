---
title: "Step 2 - Migration from CAST Console v2"
linkTitle: "Step 2 - CAST Console v2"
type: "docs"
weight: 30
no_list: true
draft: true
---

***

{{% alert color="info" %}}This documentation is currently only valid for migrations from v2 installed on **Microsoft Windows** to CAST Imaging v3 installed on **Microsoft Windows**.{{% /alert %}}  

These instructions are specifically for those that are currently using **CAST Console v2** (**standalone** or **enterprise** deployments) with analyzed applications who want to migrate to CAST Imaging v3. This process will manage the migration of the following:

- persistence data schemas:
    - aip_node (enterprise deployment)
    - aip_config (enterprise deployment)
    - node_standalone (standalone deployment)

- analysis data file storage:
    - deploy
    - delivery
    - common-data (enterprise deployment) / shared (standalone deployment)
  
Choose your CAST Console v2 deployment scenario:

{{< cardpanehome >}}
{{% cardhomereference %}}
<img class="no-border" src="../com.castsoftware.aip.console.standalone.png">&nbsp;[Standalone edition](standalone/)
{{% /cardhomereference %}}
{{% cardhomereference %}}
<img class="no-border" src="../com.castsoftware.aip.console.enterprise.png">&nbsp;[Enterprise edition](enterprise/)
{{% /cardhomereference %}}
{{< /cardpanehome >}}