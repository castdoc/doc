---
title: "Step 2 - Migration from CAST Console v2 - Enterprise"
linkTitle: "Enterprise"
type: "docs"
weight: 20
draft: true
---

***

{{% alert color="info" %}}This documentation is currently only valid for migrations from v2 installed on **Microsoft Windows** to CAST Imaging v3 installed on **Microsoft Windows**.{{% /alert %}}  

## Overview

These instructions are specifically for those that are currently using **CAST Console v2** in **"enterprise"** mode (i.e. via this [extension](https://extend.castsoftware.com/#/extension?id=com.castsoftware.aip.console&version=latest)) with analyzed applications who want to migrate to CAST Imaging v3.

The migration is a two step process with two dedicated CLI tools:

- `Console-V2-Node-Backup-Enterprise.exe`
- `Console-V3-Migration-Enterprise.exe`

The basic process required by these tools is as follows:

- Install and initialize CAST Imaging v3 (it is not necessary to install components on the same machines as your CAST Console v2 components)
- Run the `Console-V2-Node-Backup-Enterprise.exe` tool from any of your v2 node machines via the command prompt with the appropriate parameters and options. The tool will generate an output ZIP file containing:
    - all data from the v2 `delivery`/`deploy`/`common-data` folders
    - the v2 `aip_node` and `aip_config` schemas
- Transfer the output ZIP to any machine hosting the v3 `analysis-node` component (i.e. one of your v3 node machines)
- Run the `Console-V3-Migration-Enterprise.exe` tool on the machine hosting the v3 `analysis-node` component via the command prompt with the appropriate parameters and options. The tool will:
    - use the data in the output ZIP and copy all data into the v3 `delivery`/`delivery`/`common-data` folders
    - migrate the contents of the v2 `aip_node` and `aip_config` schemas in the output ZIP to the equivalent v3 `admin_center` and `analysis_node` schemas
- Check that your applications are present in CAST Imaging v3
- Stop and then disable the CAST Console v2 Microsoft Windows Services (optional)    

## Requirements

See [Requirements](../../requirements). 

## Process

### Step 1 - Prepare the Console-V2-Node-Backup-Enterprise.exe tool

- Connect to a v2 node machine (if you have multiple nodes this step only needs to be run once, no matter which node you choose).
- Obtain the `Console-V2-Node-Backup-Enterprise.exe` tool.
- Create a `.bat` file and use it to execute the migration tool (you can run the tool direct from the CMD/PowerShell window if you prefer). When using a batch file, CAST recommends setting the password for the database user defined by the option `--user`, otherwise you will be prompted to enter this during the migration. To set the password, use the following syntax on the first line of your batch file:

```
SET CSSPASSWORD=CastAIP
```

- Define the required command line using the available parameters and options as shown below:

| Command | Required? | Description |
|---|:-:|---|
| `-h`, `--help` | N/A | Show this help message and exit. |
| `--process <all\|schemaonly\|sharedfolderonly>` | :x: | Determines the type of migration: use `all` unless directed to use a different option by CAST Support (default: `all`) |
| `--casthomedir "<path>"` | :white_check_mark: | CAST Imaging Core 8.3 installation folder, e.g.: `%PROGRAMFILES%\CAST\8.3` (default: None) |
| `--user <user>` | :x: | Username used to connect to database instance defined by `--host` (default: `operator`) |
| `--host "<hostname>"` | :x: | Hostname/IP address of the database instance where the v2 `aip_node` and `aip_config` schemas are stored (default: `localhost`) |
| `--port <port>` | :x: | Port for database instance defined by `--host` (default: `2284`) |
| `--database <database>` | :x: | Name of the database in which the v2 `aip_node` and `aip_config` schemas are stored on the database instance (default: `postgres`) |
| `--log "<path>"` | :white_check_mark: | Path to a `.castlog` log file (default: None) |
| `--workingdir "<path>"` | :white_check_mark: | Temporary processing location on disk (default: None) |
| `--outputdir "<path>"` | :white_check_mark: | Location on disk where the output ZIP file will be stored (default: None) |
| `--zipfilename "<name>"` | :white_check_mark: | Name of the output ZIP file (without the `.zip` extension) (default: None) |
| `--psqldir "<path>"` | :x: | `psql.exe` file location. By default `psql.exe` provided in CAST Imaging Core 8.3 is used. (default: None) |


Example with minimum command line options, which assumes that the database is running remotely and with default port/login credentials:

```
SET CSSPASSWORD=CastAIP
Console-V2-Node-Backup-Enterprise.exe  --casthomedir "%PROGRAMFILES%\CAST\8.3" --host "my_database_host" --log "C:\temp\v3_migration\log\migration_backup.castlog" --workingdir "C:\temp\v3_migration\working_backup" --outputdir "C:\temp\v3_migration\output" --zipfilename "migration_output"
```

### Step 2 - Run the Console-V2-Node-Backup-Enterprise.exe tool

Execute the batch file with elevated permissions (right click, `Run as administrator`). On completion, check the log file. If the process is successful, the last line of the log will show "Done".

### Step 3 - Obtain and transfer the output ZIP file

Locate the output ZIP generated by the the `Console-V2-Node-Backup-Enterprise.exe` tool and transfer it to a machine hosting the v3 `analysis-node` component (i.e. a v3 node machine). If you have multiple v3 nodes this step only needs to be run once, no matter which node you choose.

### Step 4 - Prepare the Console-V3-Migration-Enterprise.exe tool

- Connect to the v3 node machine to which you have transferred the output ZIP file in Step 3
- Obtain the `Console-V3-Migration-Enterprise.exe` tool
- Create a `.bat` file and use it to execute the migration tool (you can run the tool direct from the CMD/PowerShell window if you prefer). When using a batch file, CAST highly recommends setting the password for the database user defined by the option `--user`, otherwise you will be prompted to enter this during the migration. To set the password, use the following syntax on the first line of your batch file:

```
SET CSSPASSWORD=CastAIP
```

- Define the required command line using the available parameters and options as shown below:

| Command | Required? | Description |
|---|:-:|---|
| `-h`, `--help` | N/A | Show this help message and exit. |
| `--process <all\|migration\|customscript\|move\|mngtscript\|fastscan\|admincenter>` | :x: | Determines the type of migration: use `all` unless directed to use a different option by CAST Support (default: `all`) |
| `--casthomedir "<path>"` | :white_check_mark: | CAST Imaging Core 8.4 installation folder, e.g.: `%PROGRAMFILES%\CAST\8.4` (default: None) |
| `--zipfile "<path>"` | :white_check_mark: | Full path to the file transferred in Step 3 and used as input here (default: None) |
| `--user <user>` | :x: | Username used to connect to database instance defined by `--host` (default: `operator`) |
| `--host "<hostname>"` | :x: | Hostname/IP address of the database instance where the v3 `aip_node` and `aip_config` schemas are stored (default: `localhost`) |
| `--port <port>` | :x: | Port for database instance defined by `--host` (default: `2284`) |
| `--database <database>` | :x: | Name of the database in which the v3 `admin_center` and `analysis_node` schemas are stored on the database instance (default: `postgres`) |
| `--apikey "<apikey>"` | :white_check_mark: | CAST Imaging v3 API key. You can obtain this by clicking the user icon in the top right corner of the screen and selecting "Profile". (default: None) |
| `--log "<path>"` | :white_check_mark: | Path to a `.castlog` log file (default: None) |
| `--workingdir "<path>"` | :white_check_mark: | Temporary processing location on disk (default: None) |
| `--psqldir "<path>"` | :x: | `psql.exe` file location. By default `psql.exe` provided in CAST Imaging Core is used. (default: None) |
| `--backup true\|false` | :x: | Choose whether to backup the v3 `admin_center` and `analysis_node` schemas before their content is replaced (default: false) |
| `--backupdir "<path>"` | :x: | Choose a location in which to store the v3 `admin_center` and `analysis_node` schema backups if `--backup true` (default: None) |

Example with minimum command line options, which assumes that the database is running remotely and with default port/login credentials:

```
SET CSSPASSWORD=CastAIP
Console-V3-Migration-Enterprise.exe  --casthomedir "%PROGRAMFILES%\CAST\8.4" --zipfile "C:\temp\v3_migration\migration_output.zip" --host "my_database_host" --apikey "wSJHDUxMN4KVTINSDWWo" --log "C:\temp\v3_migration\log\migration_restore.castlog" --workingdir "C:\temp\v3_migration\working_restore"
```

### Step 5 - Run the Console-V3-Migration-Enterprise.exe tool

Execute the batch file with elevated permissions (right click, `Run as administrator`). On completion, check the log file. If the migration process is successful, the last line of the log will show "Done".

### Step 6 - Check CAST Imaging v3

Check that all your applications are visible in CAST Imaging v3.

{{% alert color="info" %}}If the migration log shows errors, you should investigate and contact [CAST Support](https://help.castsoftware.com/hc/en-us/requests/new) if you are unable to proceed.{{% /alert %}}

### Step 6 - Stop and then disable the CAST Console v2 Microsoft Windows Services

CAST recommends stopping and disabling the existing CAST Console v2 Microsoft Windows Services to:
- improve performance on the machine
- avoid analyses being run from v2

## What's next?

You must now perform the application update and re-analysis as described in [Step 3 - Perform the application schema update and re-analysis in CAST Imaging v3](../../applications/).