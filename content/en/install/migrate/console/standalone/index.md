---
title: "Step 2 - Migration from CAST Console v2 - Standalone"
linkTitle: "Standalone"
type: "docs"
weight: 10
draft: true
---

***

{{% alert color="info" %}}This documentation is currently only valid for migrations from v2 installed on **Microsoft Windows** to CAST Imaging v3 installed on **Microsoft Windows**.{{% /alert %}}  

## Overview

These instructions are specifically for those that are currently using **CAST Console v2** in **"standalone"** mode (i.e. via this [extension](https://extend.castsoftware.com/#/extension?id=com.castsoftware.aip.console.standalone&version=latest)) with analyzed applications who want to migrate to CAST Imaging v3.

The migration process uses a dedicated CLI tool called `Console-V3-Migration-Standalone.exe`. The basic process required by this tools is as follows:

- Install and initialize CAST Imaging v3 (you must [install](../../global/windows/) **all** CAST Imaging v3 components (via the `all` option) on the same machine as v2.)
- Run the migration tool from the command prompt with the appropriate parameters and options
- The tool will:
     - copy all data from the v2 `delivery`/`delivery`/`shared` folders to the equivalent folders you have defined for v3
     - migrate the contents of the v2 `node_standalone` schema to the equivalent v3 `admin_center` and `analysis_node` schemas
- Check that your applications are present in v3
- Stop and then disable the CAST Console v2 Microsoft Windows Services (ioptional)    

## Requirements

See [Requirements](../../requirements).

## Migration tool command line parameters

| Command | Required? | Description |
|---|:-:|---|
| `-h`, `--help` | N/A | Show this help message and exit. |
| `--process <all\|migration\|customscript\|move\|mngtscript\|fastscan\|admincenter>` | :x: | Determines the type of migration: use `all` unless directed to use a different option by CAST Support (default: `all`) |
| `--casthomedir "<path>"` | :white_check_mark: | CAST Imaging Core v3 installation folder, e.g.: `%PROGRAMFILES%\CAST\8.3` (default: None) |
| `--v2aipnodedir "<path>"` | :white_check_mark: | CAST Console v2 data folder containing the `application-standalone.yml` file, e.g.: `%PROGRAMDATA%\CAST\AIP-Console-Standalone` (default: None) |
| `--user <user>` | :x: | Username used to connect to database instance defined by `--host` (default: `operator`) |
| `--host "<hostname>"` | :x: | Hostname/IP address of the database instance where the v2 `node_standalone` schema and the v3 `admin_center` and `analysis_node` schemas are stored (default: `localhost`) |
| `--port <port>` | :x: | Port for database instance defined by `--host` (default: `2284`) |
| `--database <database>` | :x: | Name of the database in which the v2 `node_standalone` schema and the v3 `admin_center` and `analysis_node` schemas are stored on the database instance (default: `postgres`) |
| `--apikey "<apikey>"` | :white_check_mark: | CAST Imaging v3 API key. You can obtain this by clicking the user icon in the top right corner of the screen and selecting "Profile". (default: None) |
| `--log "<path>"` | :white_check_mark: | Path to a `.castlog` log file (default: None) |
| `--workingdir "<path>"` | :white_check_mark: | Temporary processing location on disk (default: None) | 
| `--psqldir "<path>"` | :x: | `psql.exe` file location. By default `psql.exe` provided in CAST Imaging Core is used. (default: None) |
| `--backup true\|false` | :x: | Choose whether to backup the v3 `admin_center` and `analysis_node` schemas before their content is replaced (default: false) |
| `--backupdir "<path>"` | :x: | Choose a location in which to store the v3 `admin_center` and `analysis_node` schema backups if `--backup true` (default: None) |

## Process

### Step 1 - Prepare the Console-V3-Migration-Standalone.exe tool

- Connect to your machine
- Obtain the `Console-V3-Migration-Standalone.exe` tool
- Create a `.bat` file and use it to execute the migration tool (you can run the tool direct from the CMD/PowerShell window if you prefer). When using a batch file, CAST recommends setting the password for the database user defined by the option `--user`, otherwise you will be prompted to enter this during the migration. To set the password, use the following syntax on the first line of your batch file:

```
SET CSSPASSWORD=CastAIP
```

### Step 2 - Define the command line

Example with minimum command line options, which assumes that the database is also running on the same machine as the tool:

```
SET CSSPASSWORD=CastAIP
Console-V3-Migration-Standalone.exe  --casthomedir "%PROGRAMFILES%\CAST\8.3" --v2aipnodedir "%PROGRAMDATA%\CAST\AIP-Console-Standalone" --apikey "wSJHDUxMN4KVTINSDWWo" --log "C:\temp\v3_migration\log\migration.castlog" --workingdir "C:\temp\v3_migration\working"
```

### Step 3 - Run the batch or command line

Run the migration tool on the machine: execute the batch file with elevated permissions (right click, `Run as administrator`). On completion, check the log file. If the migration process is successful, the last line of the log will show "Done".

### Step 4 - Check CAST Imaging v3

Check that all your applications are visible in CAST Imaging v3.

{{% alert color="info" %}}If the migration log shows errors, you should investigate and contact [CAST Support](https://help.castsoftware.com/hc/en-us/requests/new) if you are unable to proceed.{{% /alert %}}

### Step 5 - Stop and then disable the CAST Console v2 Microsoft Windows Services

CAST recommends stopping and disabling the existing CAST Console v2 Microsoft Windows Services to:
- improve performance on the machine
- avoid analyses being run from v2

There are two services:

- CAST AIP Console Standalone Service
- CAST AIP Node Log Service

![](disable_services.jpg)

## What's next?

You must now perform the application update and re-analysis as described in [Step 3 - Perform the application schema update and re-analysis in CAST Imaging v3](../../applications/).