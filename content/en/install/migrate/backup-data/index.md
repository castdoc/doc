---
title: "Step 1 - Backup your data"
linkTitle: "Step 1 - Backup your data"
type: "docs"
weight: 20
no_list: true
---

***

{{% alert color="info" %}}This documentation is only valid for installations of CAST Imaging on **Microsoft Windows**.{{% /alert %}}

## Overview

This initial step involves taking a backup copy of your v2 data **on your v2 machines** and transferring it to the relevant machine where your CAST Imaging v3 components are installed. These backups will be restored as part of the migration process. All backup actions are performed on machines where **v2** components are installed.

![](backup.jpg)

{{% alert color="info" %}}Note that when you are migrating from a **CAST Console v2 standalone** installation it is not necessary to backup any **Console related data** since in this scenario, CAST Imaging v3 needs to be installed on the same machine and therefore the transfer of data between CAST Console v2 and CAST Imaging v3 is automated as part of [Step 2 - Migrate your data](../migrate-data/). The backup of **CAST Imaging Viewer v2** data is still required and must be actioned (see [Step 1 - Backup your CAST Imaging Viewer v2 results](#step-1---backup-your-cast-imaging-viewer-v2-results)).{{% /alert %}}

## Requirements

See [Requirements](../requirements/).

You should also ensure:

- that you have a copy of the [com.castsoftware.imaging.migrationtool](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.migrationtool&version=latest) extension on the machines where the CAST Console v2 backup will be run:
  - for an **enterprise/distributed migration**, any machine acting as a CAST Console v2 node.
  - for a **standalone migration**, the machine where all CAST Imaging v2 components are installed.
- that you have access to the **Neo4j backup tool** provided with your v2 installation in `%PROGRAMFILES%\CAST\ImagingSystem\neo4j\bin\neo4j-admin`.

## Process

### Step 1 - Backup your CAST Imaging Viewer v2 results

{{% alert color="info" %}}These instructions are specifically for those that are using **CAST Imaging Viewer v2** (i.e. via this [extension](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging&version=latest)) with analyzed applications who want to migrate to CAST Imaging v3.{{% /alert %}}

On the machine hosting your CAST Imaging Viewer v2 installation, run a backup of your existing Neo4j databases using the **Neo4j backup tool** provided with your v2 installation. This backup will be restored to the CAST Imaging v3 installation (described in a later step). For example:

```batch
set HEAP_SIZE=4G
"%PROGRAMFILES%\CAST\ImagingSystem\neo4j\bin\neo4j-admin" backup --from=localhost:6362 --backup-dir=C:\\temp\\backup --database=* --include-metadata=all --pagecache=4G
```

Where:
- `--from` defines the machine on which CAST Imaging is running - set this to `localhost:6362`. "6362" is the default listening port for backup operations.
- `--backup-dir` specifies an absolute path to the target folder for the resulting backup. This folder must already exist. You must use escaped backslashes in the path.
- `--database=*` the `*` option will backup all databases (one "tenant" in CAST Imaging v2 is equal to one database)
- `--include-metadata` set this to `all` to ensure that any roles you have defined are also backed up
- `--pagecache` defaults to 8MB, so use this option to ensure you specify a large amount of RAM (e.g. 4GB) to improve performance.

A consistency check will be actioned by default - this is recommended, but can be disabled using `--check-consistency=false`. The resulting backup of each CAST Imaging v2 database (i.e. tenant) will be stored in a dedicated sub folder of the folder defined by the `--backup-dir` command.

{{% alert color="info" %}}See also https://neo4j.com/docs/operations-manual/4.2/backup-restore/online-backup/ for more information.{{% /alert %}}

When the backup is complete:

- If the CAST Imaging v3 `imaging-viewer` component is installed on the same machine, there is nothing further to do - the target folder needs to be referenced during the migration, see [Step 2 - Migrate your data](../migrate-data/).
- If the CAST Imaging v3 `imaging-viewer` component is installed on a different machine, transfer the folder and its entire contents to this other machine.

### Step 2 - Backup your CAST Console v2 enterprise installation

{{% alert color="info" %}}These instructions are specifically for those that are currently using **CAST Console v2** in **"enterprise"** mode (i.e. via this [extension](https://extend.castsoftware.com/#/extension?id=com.castsoftware.aip.console&version=latest)) with analyzed applications who want to migrate to CAST Imaging v3.{{% /alert %}}

Connect to a v2 node machine (if you have multiple nodes this step **only needs to be run once on any node machine**). Obtain the `Console-V2-Node-Backup-Enterprise.exe` tool (provided in the [com.castsoftware.imaging.migrationtool](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.migrationtool&version=latest) extension). The tool will generate an output ZIP file containing:
- all data from the v2 `delivery`/`deploy`/`common-data` folders
- the v2 `aip_node` and `aip_config` schemas

Create a `.bat` file and use it to execute the backup tool (you can run the tool direct from the CMD/PowerShell window if you prefer). When using a batch file, CAST recommends setting the password for the database user defined by the option `--user`, otherwise you will be prompted to enter this during the migration. To set the password, use the following syntax on the first line of your batch file:

```bash
SET CSSPASSWORD=CastAIP
```

Define the required command line using the available parameters and options as shown below:

| Command | Required? | Description |
|---|:-:|---|
| `-h`, `--help` | N/A | Show this help message and exit. |
| `--process` | :x: | Determines the type of migration: use `all` unless directed to use a different option (`schemaonly`, `sharedfolderonly`) by CAST Support (default: `all`) |
| `--casthomedir "<path>"` | :white_check_mark: | CAST AIP Core 8.3 installation folder, e.g.: `%PROGRAMFILES%\CAST\8.3` (default: None) |
| `--user <user>` | :x: | Username used to connect to database instance defined by `--host` (default: `operator`) |
| `--host "<hostname>"` | :x: | Hostname/IP address of the database instance where the v2 `aip_node` and `aip_config` schemas are stored (default: `localhost`) |
| `--port <port>` | :x: | Port for database instance defined by `--host` (default: `2284`) |
| `--database <database>` | :x: | Name of the database in which the v2 `aip_node` and `aip_config` schemas are stored on the database instance (default: `postgres`) |
| `--log "<path>"` | :white_check_mark: | Path to a `.castlog` log file (default: None). This folder MUST exist on disk before running the action. |
| `--workingdir "<path>"` | :white_check_mark: | Temporary processing location on disk (default: None). This folder MUST exist on disk before running the action. |
| `--outputdir "<path>"` | :white_check_mark: | Location on disk where the output ZIP file will be stored (default: None).This folder MUST exist on disk before running the action. |
| `--zipfilename "<name>"` | :white_check_mark: | Name of the output ZIP file (without the `.zip` extension) (default: None) |
| `--psqldir "<path>"` | :x: | `psql.exe` file location. By default `psql.exe` provided in CAST AIP Core 8.3 (defined by the `--casthomedir` option) is used. (default: None) |


Example with minimum command line options, which assumes that the database is running remotely and with default port/login credentials:

```
SET CSSPASSWORD=CastAIP
Console-V2-Node-Backup-Enterprise.exe  --casthomedir "%PROGRAMFILES%\CAST\8.3" --host "my_database_host" --log "C:\temp\v3_migration\log\migration_backup.castlog" --workingdir "C:\temp\v3_migration\working_backup" --outputdir "C:\temp\v3_migration\output" --zipfilename "migration_output"
```

Execute the batch file with elevated permissions (right click, `Run as administrator`). On completion, check the log file. If the process is successful, the last line of the log will show **"Done"**.

Finally, locate the output ZIP generated by the tool and transfer it to a machine hosting the v3 `analysis-node` component (i.e. a v3 node machine). If you have multiple v3 nodes you can copy the output ZIP to any of them.

## What's next?

You must now perform the migrate action as described in [Step 2 - Migrate your data](../migrate-data/). This will migrate the v2 data to CAST Imaging v3.
