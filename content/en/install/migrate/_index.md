---
title: "Migration from v2"
linkTitle: "Migration from v2"
type: "docs"
weight: 110
no_list: true
---

***

CAST Imaging v3 is a unification of two distinct systems, **CAST Console v2** and **CAST Imaging Viewer v2**. To aid your transition to this new "unified" product, CAST provides various command line tools for each legacy v2 system that will migrate you to CAST Imaging v3. In addition to this, since CAST Imaging v3 supports only CAST Imaging Core 8.4.x, an application update is also necessary, performed in the CAST Imaging v3 UI.

Information, requirements and instructions for each step are provided below:

{{< cardpanehome >}}
{{% cardhomereference header="**Information**" %}}
<i class="fa-solid fa-list-check"></i>&nbsp;[Requirements](requirements/)
{{% /cardhomereference %}}
{{% cardhomereference header="**Step 1**" %}}
<i class="fa-solid fa-download"></i>&nbsp;[Backup your data](backup-data/)
{{% /cardhomereference %}}
{{% cardhomereference header="**Step 2**" %}}
<i class="fa-regular fa-window-restore"></i>&nbsp;[Migrate your data](migrate-data/)
{{% /cardhomereference %}}
{{% cardhomereference header="**Step 3**" %}}
<img class="no-border" alt="Application update/rescan" src="com.castsoftware.aip.png">&nbsp;[Update/rescan your apps](applications/)
{{% /cardhomereference %}}
{{< /cardpanehome >}}
