---
title: "Step 1 - Migration from CAST Imaging Viewer v2"
linkTitle: "Step 1 - CAST Imaging Viewer v2"
type: "docs"
weight: 20
no_list: true
draft: true
---

***

{{% alert color="info" %}}This documentation is currently only valid for installations of CAST Imaging Viewer v3 on **Microsoft Windows**.{{% /alert %}}

## Overview

These instructions are specifically for those that are currently using **CAST Imaging Viewer v2** (i.e. via this [extension](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging&version=latest)) with analyzed applications who want to migrate to CAST Imaging v3. This process will manage the migration of the following:

- all application results (including those imported via a .CSV import action) stored in CAST Imaging Viewer v2
- roles defined and configured in CAST Imaging Viewer v2

The migration process uses two tools:
- the **native Neo4j backup tool** provided with your CAST Imaging Viewer v2 installation.
- a dedicated CLI tool called `Neo4jVersionUpgrade.exe` which is provided with your CAST Imaging Viewer v3 installation.

The basic process required by this tools is as follows:

- Install and initialize CAST Imaging v3 (it is not necessary to install components on the same machine as CAST Imaging Viewer v2).
- Backup your existing CAST Imaging v2 Neo4j databases (there may be more than one) using the native Neo4j backup tool.
- Transfer the results of the Neo4j backup to the machine where the CAST Imaging v3 `imaging-viewer` component is installed.
- Run the migration tool from the command prompt with the appropriate parameters and options on the machine where the CAST Imaging v3 `imaging-viewer` component is installed.
- The tool will restore the backup of the v2 Neo4j database into the v3 Neo4j database.
- Check that your applications are present in v3
- Stop and then disable the CAST Imaging Viewer v2 Microsoft Windows Services (optional).

## Requirements

See [Requirements](../requirements/).

## Process

### Step 1 - Run a backup

On the machine hosting your CAST Imaging Viewer v2 installation, run a backup of your existing Neo4j databases using the Neo4j backup tool provided with your v2 installation. This backup will be restored to the CAST Imaging v3 installation. For example:

```batch
set HEAP_SIZE=4G
"%PROGRAMFILES%\CAST\ImagingSystem\neo4j\bin\neo4j-admin" backup --from=localhost:6362 --backup-dir=C:\\temp\\backup --database=* --include-metadata=all --pagecache=4G
```

Where:
- `--from` defines the machine on which CAST Imaging is running. "6362" is the default listening port for backup operations.
- `--backup-dir` specifies an absolute path to the target folder for the resulting backup. This folder must already exist. You must use escaped backslashes in the path.
- `--database=imaging` the `*` option will backup all databases (one "tenant" in CAST Imaging v2 is equal to one database)
- `--include-metadata` set this to `all` to ensure that any roles you have defined are also backed up
- `--pagecache` defaults to 8MB, so use this option to ensure you specify a large amount of RAM (e.g. 4GB) to improve performance.

A consistency check will be actioned by default - this is recommended, but can be disabled using `--check-consistency=false`. The resulting backup of each CAST Imaging v2 database will be stored in a dedicated sub folder of the folder defined by the `--backup-dir` command.

{{% alert color="info" %}}See also https://neo4j.com/docs/operations-manual/4.2/backup-restore/online-backup/ for more information.{{% /alert %}}

### Step 2 - Copy backup to CAST Imaging v3 host machine

Copy the entire contents of the folder (defined by the `--backup-dir` command) generated in the previous step over to the machine where the CAST Imaging v3 `imaging-viewer` component is installed.

### Step 3 - Stop all CAST Imaging v3 Microsoft Windows services

Before performing the migration itself, ensure that you stop all CAST Imaging v3 Microsoft Windows services as highlighted in the following image:

![](services.jpg)

### Step 4 - Perform the initial restore action

#### Prepare a .json file

On the machine where the CAST Imaging v3 `imaging-viewer` component is installed, create a text file called `restore.json`. This file contains the instructions that will be used by the migration tool. For example:

```json
{
  "database_names": [
    "test",
    "neo4j",
    "imaging"
  ],
  "backup_dir": "C:\\temp\\backup",
  "admin_path": "C:\\Program Files\\CAST\\Imaging\\Cast-Imaging-Viewer\\neo4j\\bin",
  "operation": "restore",
  "log": "C:\\temp\\log\\restore.log"
}
```

Where:

- `database_names` is an array containing the list of all databases to be restored. You should include all databases that exist in the backup output, except for the `system` database as this is not required.
- `backup_dir` is the location on the local disk of your folder containing the backup you generated previously. You must use escaped backslashes in the path.
- `admin_path` is the location of the Neo4j `bin` folder, which is part of your CAST Imaging v3 `imaging-viewer` installation. By default this will be in `%PROGRAMFILES\CAST\Imaging\Cast-Imaging-Viewer\neo4j\bin`. You must use escaped backslashes in the path.
- `operation` is the action to perform. Use `restore`.
- `log` is the path on the local disk to a `.log` file to record the process. You must use escaped backslashes in the path.

#### Run the restore action

On the machine where the CAST Imaging v3 `imaging-viewer` component is installed, run the migration tool located in `%PROGRAMFILES%\CAST\Imaging\CAST-Imaging-Viewer\tools\Neo4jVersionUpgrade.exe` referencing the `restore.json` file you created previously, for example:

```batch
"%PROGRAMFILES%\CAST\Imaging\CAST-Imaging-Viewer\tools\Neo4jVersionUpgrade.exe" --config C:\temp\restore.json
```

This will perform the initial restore action.

### Step 5 - Start all CAST Imaging v3 Microsoft Windows services

Restart all CAST Imaging v3 Microsoft Windows services as highlighted in the following image:

![](services.jpg)

### Step 6 - Perform the create action

#### Prepare a .json file

On the machine where the CAST Imaging v3 `imaging-viewer` component is installed, create a text file called `create.json`. This file contains the instructions that will be used by the migration tool. For example:

```json
{
  "database_names": [
    "test",
    "neo4j",
    "imaging"
  ],
  "operation": "create",
  "log": "C:\\temp\\log\\create.log"
}
```

Where:

- `database_names` is an array containing the list of all databases to be created. The list of databases should be identical to the list used in the `restore.json` file in Step 4.
- `operation` is the action to perform. Use `create`.
- `log` is the path on the local disk to a `.log` file to record the process. You must use escaped backslashes in the path.

#### Run the create action

On the machine where the CAST Imaging v3 `imaging-viewer` component is installed, run the migration tool located in `%PROGRAMFILES%\CAST\Imaging\CAST-Imaging-Viewer\tools\Neo4jVersionUpgrade.exe` referencing the `create.json` file you created previously, for example:

```batch
"%PROGRAMFILES%\CAST\Imaging\CAST-Imaging-Viewer\tools\Neo4jVersionUpgrade.exe" --config C:\temp\create.json
``` 

This will perform the database creation action.

{{% alert color="info" %}}At this time, it is not possible to view the results via the CAST Imaging v3 UI.{{% /alert %}}

### Step 7 - Stop and then disable the CAST Imaging v2 Microsoft Windows Services

CAST recommends stopping and disabling the existing CAST Imaging v2 Microsoft Windows Services to:
- improve performance on the machine
- avoid results being consulted from v2

## What's next?

You must now perform the CAST Console v2 to CAST Imaging v3 migration as described in [Step 2 - Migration from CAST Console v2](../console/). This migration will ensure that the "analyzed" applications you have migrated as part of the CAST Imaging Viewer migration process are correctly matched to their counterparts in CAST Imaging v3.
