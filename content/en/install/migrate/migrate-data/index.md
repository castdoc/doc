---
title: "Step 2 - Migrate your data"
linkTitle: "Step 2 - Migrate your data"
type: "docs"
weight: 20
no_list: true
---

***

{{% alert color="info" %}}This documentation is only valid for installations of CAST Imaging on **Microsoft Windows**.{{% /alert %}}

## Overview

This step involves migrating the following data to your CAST Imaging v3 installation, i.e.:

- data backed up in [Step 1 - Backup your data](../backup-data/) from **CAST Imaging Viewer v2** and **CAST Console v2 enterprise**
- data in-situ from **CAST Console v2 standalone**

All migration actions are performed on machines where CAST Imaging v3 components are installed.
  
## Requirements

See [Requirements](../requirements/).

You should also ensure:

- that you have completed [Step 1 - Backup your data](../backup-data/).
- that you have a copy of the [com.castsoftware.imaging.migrationtool](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.migrationtool&version=latest) extension on all machines where the migration will be run:
  - the machine where the CAST Imaging v3 `imaging-viewer` component is installed
  - for a **v2 CAST Console enterprise/distributed** migration, a machine where the CAST Imaging v3 `analysis-node` component is installed (any node can be used)
  - for a **v2 CAST Console standalone** migration, the machine where all CAST Imaging v3 components are installed

{{% alert color="warning" title="Order of migration actions" %}}When you are migrating to a v3 distributed/enterprise deployment where the CAST Imaging v3 components are on dedicated / separate machines, it is mandatory to run the migration actions in a **specific order**, i.e.: the migration of CAST Imaging Viewer v2 data **must ALWAYS be actioned BEFORE** the migration of CAST Console v2 data. In other words, the first action should be performed on the machine where the CAST Imaging v3 `imaging-viewer` component is installed using the `option=Neo4j` in the `migration.conf` file, and the next action should be performed on a machine where the CAST Imaging v3 `analysis-node` component is installed (any node can be used) using the `option=Console` in the `migration.conf` file.{{% /alert %}}

## Process

### Step 1 - Set-up the migration configuration file

The migration process is run via a batch file which refers to a configuration file containing various options that determine how the migration is actioned. Therefore, the first step is to set-up this configuration file: you can find it at the root of the unzipped migration tool extension, called `migration.conf`.

The configuration file contains three distinct sections, described below:

1. [General options](#general-options)
2. [Neo4j options](#neo4j-options)
3. [Console options](#console-options)

#### General options

| Option | Description |
|---|---|
| `option=` | Choose one of the following - all options should ONLY be run once:<br><br>- `All`: migrate both CAST Imaging Viewer v2 and Console v2, i.e. for a standalone/single machine deployment of CAST Imaging v3 where `imaging-viewer`, `imaging-services` and `analysis-node` components are all installed on the SAME machine.<!--, or for an enterprise/distributed deployment of CAST Imaging v3 where at least `imaging-viewer` and `analysis-node` are on the same machine--><br><br>- `Neo4j`: migrate only CAST Imaging Viewer v2, must be run on the machine where the CAST Imaging v3 `imaging-viewer` component is installed. This option must be run BEFORE the `Console` option.<br><br>- `Console`: migrate only CAST Console v2, must be run on a machine where a CAST Imaging v3 `analysis-node` component is installed. This option must be run AFTER the `Neo4j` option. |
| `log=` | A local folder for storing the migration log files. Use a standard Microsoft Windows path with back slashes, e.g.: `C:\temp\log`. The installer will attempt to create this folder if it cannot be found. |
| `apikey=` | A **CAST Imaging v3 API key** - you can obtain this by clicking the user icon in the top right corner of the screen and selecting **Profile**:<br><br>![](profile.jpg)<br><br>![](profile2.jpg) |
| `GATEWAY_HOMEPAGE_URL=` | The "public access URL" of your CAST Imaging v3 installation (defined during the [installation](../../global/windows/)), relative to the machine on which you are running the migration action. For example if all your v2 and v3 components are installed on one machine, you can leave this set to the default `http://localhost:8090`. Otherwise, set this URL to match the location of the `imaging-services` component in your network. |

#### Neo4j options

These options define the **CAST Imaging Viewer v2** migration:

| Option | Description |
|---|---|
| `neo4j_adminpath=` | Path to the Neo4j administration tool `neo4j-admin` in your CAST Imaging v3 installation. Unless you have customized this location, use the default path (using standard Microsoft Windows back slashes): `C:\Program Files\Cast\Imaging\CAST-Imaging-Viewer\neo4j\bin`. |
| `neo4j_backupdir=` | Path (using standard Microsoft Windows back slashes) to the local folder containing the backup generated from CAST Imaging Viewer v2, as described in [Step 1 - Backup your data](../backup-data/). |
| `neo4j_host=`, `neo4j_port=`, `neo4j_database=`, `neo4j_user=`, `neo4j_password=`, `neo4j_scheme=` | The details of you Neo4j instance, i.e. as installed with CAST Imaging v3. Unless you have customized this, use the default options provided in the configuration file. |

#### Console options

These options define the **CAST Console v2** migration:

| Option | Description |
|---|---|
| `console=` | Choose one of the following depending on your migration strategy:<br><br>- `Standalone`<br>- `Enterprise` |
| `process=` | Determines the type of migration: use `ALL` unless directed to use a different option by CAST Support. |
| `casthomedir=` | Path (using standard Microsoft Windows back slashes) to the CAST Imaging Core 8.4 installation folder, e.g.: `C:\Program Files\CAST\8.4`. |
| `v2aipnodedir=` | Only valid for a **standalone** migration: CAST Console v2 data folder containing the `application-standalone.yml` file, e.g.: `C:\ProgramData\CAST\AIP-Console-Standalone`. Will be ignored when `console=Enterprise`. |
| `zipfile=` |  Only valid for an **enterprise** migration: path (using standard Microsoft Windows back slashes) to the ZIP file containing the backup generated from CAST Console v2 enterprise (via the  `Console-V2-Node-Backup-Enterprise.exe` tool), as described in [Step 1 - Backup your data](../backup-data/). Will be ignored when `console=Standalone`. |
| `css_user=` | Username used to connect to database instance defined by `css_host=` (default: `operator`). |
| `css_host=` | Hostname/IP address of the database instance where the CAST Imaging v3 `admin_center` and `analysis_node` schemas are stored. (default: `localhost`). For a **standalone** migration, the v2 `node_standalone` schema must also exist on this instance. |
| `css_port`= | Port for database instance defined by `css_host` (default: `2284`). |
| `css_database=` | Name of the database in which the CAST Imaging v3 `admin_center` and `analysis_node` schemas are stored on the database instance (default: `postgres`). For a **standalone** migration, the v2 `node_standalone` schema must also exist in this database. |
| `workingdir=` | Path (using standard Microsoft Windows back slashes) to a temporary processing location on disk., e.g.: `C:\Temp\WorkDir`. The installer will attempt to create this folder if it cannot be found. | 
| `psqldir=` | Optional: path (using standard Microsoft Windows back slashes) to `psql.exe` file. By default `psql.exe` provided in CAST Imaging Core is used when no value is provided for this option. |

### Step 2 - Run the migration

To run the migration:

- Connect to your chosen **CAST Imaging v3 machine**, depending on the migration options you have configured in the `migration.conf` file.
- Open a brand new CMD window (**do NOT re-use any existing CMD windows that you may have used to perform other actions**) with elevated permissions (right click, `Run as administrator`) and navigate to the root of the unzipped migration tool extension.
- Execute the following command to set the password for the database user defined by the option `css_user=` in the `migration.conf` file (otherwise you will be prompted to enter this during the migration):

```
SET CSSPASSWORD=CastAIP
```

- Now execute the `Cast_Imaging_V3_Migration.bat` file provided at the root of the unzipped migration tool extension.
- On completion, check the log file (defined by the option `log=` in the `migration.conf` file). If the migration log shows errors, you should investigate and contact [CAST Support](https://help.castsoftware.com/hc/en-us/requests/new) if you are unable to proceed.

### Step 3 - Check CAST Imaging v3

Connect to CAST Imaging v3 and login with a user that has the default `Admin` profile or a custom profile with the `Administrator` role - see [User Permissions](../../../administer/settings/user-permissions/). Check that all your applications are visible in CAST Imaging v3.

Successfully migrated applications will show the followng:

The status **Migration in process** in the [Application landing page](../../../interface/landing-page/) - you can still view their results if required by clicking the status, but you will be informed that to finalize the migration, you will need to complete additional steps - see [What's next?](#whats-next):

![](landing.jpg)

- For an application that has "imaging-viewer" results (i.e. data in the Neo4j database):

![](landing2.jpg)

- For an application that has no "imaging-viewer" results:

![](landing3.jpg)

{{% alert color="info" %}}<ul><li>The status <strong>Migration in process</strong> will be displayed until all additional steps are completed.</li><li>Applications whose results were originally "imported" into v2 from `.csv` will show as <strong>Ready to view</strong> and can be consulted immediately (click the status).</li></ul>{{% /alert %}}

In addition, a banner in the [Application analysis configuration - Overview](../../../interface/analysis-config/overview/) panel informing you about the need to perform [Step 3 - Perform the application schema update and re-analysis in CAST Imaging v3](../../applications/) - note that some features in this panel will be disabled until this step is fully completed.

![](rescan.jpg)

### Step 4 - Check the migration report

As part of the migration process, a status report will be generated and stored on disk in text format: this report will contain key information regarding the migration status, details about each migrated application, the technologies they contain, the extensions that require an update, and advice about what to do next. This report must be consulted BEFORE you proceed with [Step 3 - Application update and re-analysis](../applications/) as it will tell you whether you should complete this step or not.

The migration report is located at the root of the unzipped migration tool extension (along side the `Cast_Imaging_V3_Migration.bat` file), called `MigrationReport.csv`. The report contains the following information:

| Column name | Description |
|---|---|
| **MIGRATED APPLICATION** | This column lists the name of each application that has been migrated during the process. It provides a clear overview of which applications were processed and migrated successfully. |
| **STATUS** | This column shows the status of the migration for each application. The possible values for the STATUS column are:<ul><li><strong>ALL_GOOD</strong>: Application migrated to CAST Imaging V3 - all technologies of your application are supported. Next Action: Everything is fine. Upgrade your application to Imaging Core 8.4. No need to rescan.</li><li><strong>IMPOSSIBLE_RESCAN</strong>: Application migrated to CAST Imaging V3 - none of its technologies are supported yet. Next Action: None of the technologies in your source code are supported yet and therefore it is not possible to upgrade and rescan your application. Once a technology is supported, you will be able to upgrade your application, and rescan your source code.</li><li><strong>NEED_RESCAN</strong>: Application migrated to CAST Imaging V3 - all technologies of your application are supported. Next Action: Upgrade your application to Imaging Core 8.4 then rescan and analyze your source code to complete the migration.</li><li><strong>POSSIBLE_RESCAN_WITH_UNSUPPORTED_TECHNOLOGIES</strong>: Application migrated to CAST Imaging V3 - some technologies are not supported yet. Next Action: You can upgrade the application to Imaging Core 8.4, then rescan and analyze the source code to complete the migration. Note that for technologies that are not supported yet, the corresponding results will be removed.</li></ul>
| **YOUR NEXT ACTION** | This column provides instructions or recommendations (explained above) for the next steps you should take for each migrated application. It aligns with the STATUS column and highlights any necessary actions, such as upgrading the application, rescanning, or addressing unsupported technologies.
| **NON SUPPORTED TECHNOLOGIES** | This column lists any technologies or features used by the application that are not supported in CAST Imaging v3. It will help you identify any technologies that might need special attention or alternative solutions after migration.|
| **EXTENSIONS THAT WILL BE UPDATED AFTER RESCAN** | This column shows any extensions that will be updated to the latest compatible release after a subsequent rescan of the application. |

Example report for hypothetical applications that fall into each STATUS category described previously:

| MIGRATED APPLICATION | STATUS | YOUR NEXT ACTION | NON SUPPORTED TECHNOLOGIES | EXTENSIONS THAT WILL BE UPDATED AFTER RESCAN |
|---|---|---|---|---|
| App1 | Application migrated to CAST Imaging V3 - all technologies of your application are supported. | Everything is fine. Upgrade your application to Imaging Core 8.4. No need to rescan.  |   |   |
| App2 | Application migrated to CAST Imaging V3 - none of its technologies are supported yet. | None of the technologies in your source code are supported yet and therefore it is not possible to upgrade and rescan your application. Once a technology is supported, you will be able to upgrade your application, and rescan your source code. | SAP |   |
| App3 | Application migrated to CAST Imaging V3 - all technologies of your application are supported. | Upgrade your application to Imaging Core 8.4 then rescan and analyze your source code to complete the migration. |   | com.castsoftware.jee<br>com.castfotware.dotnet |
| App4 | Application migrated to CAST Imaging V3 - some technologies are not supported yet. | You can upgrade the application to Imaging Core 8.4, then rescan and analyze the source code to complete the migration. Note that for technologies that are not supported yet, the corresponding results will be removed. | Power Builder<br>C/CPP<br>SAP<br>Visual Basic |   |

## What's next?

You must now perform the application update and re-scan as described in [Step 3 - Perform the application update and re-scan in CAST Imaging v3](../applications/).