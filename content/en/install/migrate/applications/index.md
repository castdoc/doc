---
title: "Step 3 - Application update and re-scan"
linkTitle: "Step 3 - Application update and re-scan"
type: "docs"
weight: 40
no_list: true
---

***

{{% alert color="info" %}}This documentation is only valid for installations of CAST Imaging on **Microsoft Windows**.{{% /alert %}}

## Overview

The final step in the v2 to v3 migration process is an **application/schema update** to the new release (8.4.x) of CAST Imaging Core, i.e. an update process from CAST Imaging Core 8.3.x to 8.4.x for all applications that have been migrated from v2. Then depending on the information provided in the [migration report](../migrate-data/#step-4---check-the-migration-report) a **re-scan must be run** on the updated applications to ensure results can be generated going forward. This is an obligatory step in the process because v3 requires the use of [CAST Imaging Core 8.4.x](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.core&version=latest) and certain specific releases of extensions.

This update process is performed in the CAST Imaging UI, using the [Applications](../../../administer/settings/applications/) panel - see also [Update](../../../administer/settings/applications/update/).

{{% alert color="info" %}}This step is not applicable to applications whose results were originally imported into CAST Imaging Viewer v2 (from CSV).{{% /alert %}}

## What actions does CAST Imaging perform during an application update?

- An automatic backup of the Application's Management, Analysis and Dashboard database schemas and its **Delivery** folder is performed automatically - see [Backups](../../../administer/settings/applications/details/backups/) for details.
- An update of the **application schemas** and the **Delivery** folder to the new CAST Imaging Core release.

## What happens if the update fails?

If the update fails for any reason, an automatic roll back of the application update (schemas and delivery folder) will be performed using the backup taken automatically at the start of the update process.

## What about extensions?

Extensions installed during the most recent analysis with v2 will NOT be automatically updated during the **application update** step, even if a more recent version of an extension is available on [CAST Extend](https://extend.castsoftware.com) or in [Extend Local Server](../../extend-local/) (online mode). However, exceptionally, all extensions that require updating for compatibility reasons, will be updated during the **application re-scan** action: this will only happen during the first re-scan after an application update.

## Impacts of updating on existing result data

Each CAST Imaging Core release and each new extension release provides new features and updates which improve the value of the platform and justify an update. However, there are a number of changes or improvements which can impact your existing measurement results/grades:

- New or improved rules to perform deeper analysis
- Updates to the Assessment Model, e.g. changes to rule weights, severity or thresholds. This can be mitigated by using the "Preserve assessment model" option during the update
- Improvements to the language analysis system, e.g. more fine-grained detection of objects or links
- Extended automatic discovery of files included in the analysis
- Bug fixes to improve the precision of results
- And, unfortunately, a new release may also introduce new bugs which may impact the results until they are discovered and removed

CAST highly recommends consulting the following information to determine any changes that have been introduced:

- the release notes for [CAST Imaging Core 8.4.x](../../../release-notes/com.castsoftware.imaging.core/) and [CAST AIP Core 8.3.x](https://doc.castsoftware.com/export/AIPCORE/8.3).
- [Change Forecast](https://extend.castsoftware.com/change-forecast/) as well as individual release notes for your [target extensions](../../../technologies/).

## Process

### Step 1 - Update process

All actions listed below are performed in your CAST Imaging v3 instance:

- Login to CAST Imaging v3 with a user that has the default `Admin` profile or a custom profile with the `Administrator` role - see [User Permissions](../../../administer/settings/user-permissions/).
- Determine the [Assessment Model Strategy](../../../administer/settings/global-configuration/assessment-model) - in the majority of scenarios leave the setting at the default **Preserve Assessment Model but enable new rules**.
- Browse to the [Applications](../../../administer/settings/applications/) panel in your CAST Imaging v3 installation - all applications will be eligible for an update except those whose results were originally imported into CAST Imaging Viewer v2 (from CSV). Perform the application update by selecting an application then clicking the icon highlighted below:

![](upgrade_action.jpg)

- When the update is complete, check the **Core version** column to ensure that it is showing 8.4.x:

![](upgrade_check.jpg)

### Step 2 - Re-scan process

Finally, depending on the information provided to you in the migration report (see [Step 4 - Check the migration report](../migrate-data/#step-4---check-the-migration-report)), you may need to run a **re-scan** on the freshly updated application. This is NOT a re-analysis, instead a re-scan requires that you deliver the application source code again (this will also generate and make available new results (structural flaws and views)):

![](analysis1.jpg)

![](analysis2.jpg)

![](analysis3.jpg)

