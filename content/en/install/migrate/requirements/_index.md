---
title: "Requirements for migration from CAST Imaging Viewer v2"
linkTitle: "Requirements"
type: "docs"
weight: 10
no_list: true
---

***

## Overview

Information and requirements that must be understood and in place before starting a migration from v2 to v3 are listed below.

## Requirements

| Check | Description |
|:-:|---|
| :white_check_mark: | Perform a manual backup (using the [CSSAdmin tools](../../../export/STORAGE/Maintenance+activities+for+CAST+Storage+Service+and+PostgreSQL)) of any relevant v2 schemas on your CAST Storage Service/PostgreSQL instance(s) to ensure that you can action a rollback should the migration fail for whatever reason (this task is in addition to [Step 1 - Backup your data](../backup-data)). This includes:<ul><li>application result schemas (<code>_MNGT</code>, <code>_CENTRAL</code>, <code>_LOCAL</code>).</li><li>measure schemas (<code>general_measure</code>)<li>persistence schemas (<code>aip_node</code> / <code>aip_config</code> / <code>node_standalone</code>). |
| :white_check_mark: | Ensure all CAST Console v2 persistence schemas use their default names, i.e.: <ul><li>v2 enterprise install: <code>aip_node</code> / <code>aip_config</code></li><li>v2 standalone install: <code>node_standalone</code></li></ul> If they do not, please [contact CAST Support](https://help.castsoftware.com/hc/en-us/requests/new). |
| :white_check_mark: | Some technologies that are fully supported in v2 are not (yet) supported in CAST Imaging v3 - you can see a list [here](../../whats-changed/#technology-coverage-with-cast-imaging-core-84). To help you decide whether you can migrate your applications, CAST provides a dedicated command line tool (called **Compatibility Inspector**) which produces key information about your applications and their eligibility for migration (i.e. whether they contain unsupported technologies and whether they can be migrated or not) - to find out more about this tool, see [Compatibility Inspector](compat-tool/). CAST highly recommends that you run it BEFORE you embark on a migration. |
| :white_check_mark: | CAST Imaging v3 must be [installed and initialized](../../global/windows/) (i.e. initial startup wizard requesting the CAST license key and the CAST Extend API key must be completed). Ensure the following:<ul><li>you have defined different locations for the <code>delivery</code>/<code>deploy</code>/<code>common-data</code> folders in v3 than the equivalent folders used in CAST Console v2</li><li>with regard to the <strong>nodes</strong>, a minimum of one node is required.<li>with regard to the <strong>database instance</strong>:<ul><li>if you are migrating from CAST Console v2 installed in <strong>enterprise mode</strong>, you can define any database instance during the installation of CAST Imaging v3 for your persistence schemas (i.e. you can re-use the same database instance used in v2 for the <code>aip_node</code> and <code>aip_config</code> schemas, or you can use a fresh database instance)</li><li>if you are migrating from CAST Console v2 installed in <strong>standalone mode</strong>, you must define (during the v3 installation) the </strong>same database instance</strong> used for the v2 persistence schema (i.e. the CAST Console v2 <code>node_standalone</code> schema and the CAST Imaging v3 <code>admin_center</code> and <code>analysis_node</code> schemas must be stored on the same database instance).</li></ul><li>ensure you have installed <a href="https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.core&version=latest">CAST Imaging Core 8.4.1</a> as part of the v3 installation process - see <a href="../../global/windows/imaging-core/">Install CAST Imaging Core for Microsoft Windows</a>. |
| :white_check_mark: | <span style="color:red"><strong>IMPORTANT!</strong><br><br>Do not onboard any <strong>new applications</strong>, nor <strong>import any "viewer" results</strong> in CAST Imaging v3. In other words, ensure that CAST Imaging v3 is entirely "empty" before starting the migration process. Any applications that exist in CAST Imaging v3 will be lost during the migration and may even cause the migration to fail. The migration tool will ensure that the target instances are empty before migration: if they are not empty, the tool will stop the migration.</span> |
| :white_check_mark: | Ensure that you login to CAST Imaging v3 with a user that has the default <code>Admin</code> profile or a custom profile with the <code>Administrator</code> role - see <a href="../../../administer/settings/user-permissions/">User Permissions</a>. |
| :white_check_mark: | Generate an <strong>API key</strong> in CAST Imaging v3 - you can obtain this by clicking the user icon in the top right corner of the screen and selecting "Profile". |
| :white_check_mark: | If you are using <a href="../../extend-local/">Extend Local Server</a> in <strong>offline mode</strong> and the required extensions do not exist, the migration will be blocked - see <a href="#dealing-with-extend-local-server">Dealing with Extend Local Server</a> for information about how to deal with this. |
| :white_check_mark: | If you are migrating a CAST Console v2 <strong>enterprise</strong> installation, you will need to manually move all downloaded extensions stored on your node machines to your equivalent CAST Imaging v3 node machines - see <a href="#dealing-with-extensions-downloaded-to-node-machines">Dealing with extensions downloaded to Node machines</a> for information about how to do this. |
| :white_check_mark: | Download the [com.castsoftware.imaging.migrationtool](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.migrationtool&version=latest) extension. You will need it on each machine where you will perform a migration action. |

## Information and FAQ

### What has changed in v3?

See [What's changed in CAST Imaging v3?](../../whats-changed/).

### Which migration paths are supported?

| v2 deployment scenario | v3 on Microsoft Windows | v3 on Linux via Docker |
|---|:-:|:-:|
| CAST Imaging/Console v2​ (Microsoft Windows) | :white_check_mark: | :x: |
| CAST Imaging/Console v2​ (Linux via Docker) with Node on Microsoft Windows | :x: | :x: |

In addition:

- migration from CAST Imaging v2/CAST Console v2 installed on **Microsoft Windows** to CAST Imaging v3 installed on **Linux via Docker** is **not supported**
- migration from a CAST Console v2 **standalone mode** installation to a CAST Imaging v3 **distributed mode** installation (components on dedicated machines) is **not supported**.

### Which releases of CAST products are supported?

#### v2

| Software | Release | Notes |
|---|:-:|---|
| CAST Console v2 | ≥ 2.11.0 | If you are using an older release, you must first update to a compatible release - see [AIP Console upgrade](../../../export/DASHBOARDS/AIP+Console+upgrade). |
| CAST Imaging v2 | ≥ 2.20.1 | If you are using an older release, you must first update to a compatible release - see [CAST Imaging upgrade](../../../export/IMAGING/CAST+Imaging+upgrade). |
| CAST AIP Core | ≥ 8.3.54 | If you are using an older release for your v2 Nodes, you must first update to a compatible release - see [AIP Core upgrade](../../../export/AIPCORE/Upgrade+AIP+Core). |

#### v3

| Software | Release | Notes |
|---|:-:|---|
| [CAST Imaging v3](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.console&version=latest) | ≥ 3.2.0 | - |
| [CAST Imaging Core 8.4](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.core&version=latest) | ≥ 8.4.1 | This change means that an obligatory step in the migration process is to update your applications from 8.3.x to 8.4.x. |

{{% alert color="warning" %}}CAST Imaging Core 8.4.x does not support all the technologies supported in 8.3.x - you can see a list [here](../../whats-changed/#technology-coverage-with-cast-imaging-core-84). You should be aware that applications containing unsupported technologies migrated to v3 can be re-scanned (in v3), but any objects related to unsupported technologies in the application will be removed during the initial post-update re-scan.{{% /alert %}}

### Are there any v3 infrastructure deployment constraints?

- When migrating from CAST Imaging Viewer v2, you can [install](../../global/windows/) CAST Imaging v3 components on the same, or on different machines from your v2 deployment.
- When you are migrating from CAST Console v2 installed in **enterprise mode** (i.e. distributed across different machines), you can [install](../../global/windows/) CAST Imaging v3 components either on the same machine(s) as the v2 components, or on different machines.
- When you are migrating from CAST Console v2 installed in **standalone mode**, you must [install](../../global/windows/) **all** CAST Imaging v3 components (via the `all` option) on the same machine as v2.

In summary:

<table><thead>
  <tr>
    <th colspan="2">From</th>
    <th>To</th>
    <th>Machine where CAST Imaging v3 is installed</th>
  </tr></thead>
<tbody>
  <tr>
    <td rowspan="2">CAST Console ≥ 2.11.0 with AIP Core ≥ 8.3.54</td>
    <td>Enterprise edition</td>
    <td rowspan="3">CAST Imaging ≥ 3.2.0<br><br>CAST Imaging Core ≥ 8.4.1</td>
    <td>CAST Imaging v3 can be installed on the same or a different machine than CAST Console v2 Enterprise.</td>
  </tr>
  <tr>
    <td>Standalone edition</td>
    <td>CAST Imaging v3 must be installed on the same machine as CAST Console v2 Standalone.</td>
  </tr>
  <tr>
    <td colspan="2">CAST Imaging ≥ 2.20.1</td>
    <td>CAST Imaging v3 can be installed on the same or a different machine than CAST Imaging v2.</td>
  </tr>
</tbody>
</table>

### How is the migration performed and in what order?

The migration process uses:

- several command line (CLI) tools provided in an extension called [com.castsoftware.imaging.migrationtool](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.migrationtool&version=latest)
- the **CAST Imaging v3 UI**

**All applications** managed in CAST Console v2 will be migrated to CAST Imaging v3 (the current release of [com.castsoftware.imaging.migrationtool](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.migrationtool&version=latest) does not allow you to choose the applications to migrate).

Essentially the process is as follows:

- [Step 1 - Backup your data](../backup-data/) - On your v2 machines, backup existing v2 data for **CAST Console enterprise** and **CAST Imaging Viewer** and transfer the output to relevant v3 machines. This backup step is NOT required when you are migrating from a **CAST Console v2 standalone** installation since in this scenario, CAST Imaging v3 needs to be installed on the same machine and therefore the transfer of data between CAST Console v2 and v3 is automated as part of [Step 2 - Migrate your data](../migrate-data/) - you still need to backup your CAST Imaging Viewer v2 data however. Backups are run with different tools:
  - CAST Imaging Viewer v2 - **Neo4j backup tool** provided with your v2 installation
  - CAST Console v2 enterprise - `Console-V2-Node-Backup-Enterprise.exe` tool provided in the [com.castsoftware.imaging.migrationtool](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.migrationtool&version=latest) extension.
- [Step 2 - Migrate your data](../migrate-data/) - Migrate the v2 data to CAST Imaging v3 using a batch script and configuration file provided in the [com.castsoftware.imaging.migrationtool](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.migrationtool&version=latest) extension. This step is actioned on the CAST Imaging v3 machines.
- [Step 3 - Application update and re-scan](../applications/) - Perform the application schema update and re-scan in the CAST Imaging v3 UI.

### What about technologies in my applications that are unsupported in CAST Imaging v3?

Some technologies that are fully supported in v2 are not (yet) supported in CAST Imaging v3 - you can see a list [here](../../whats-changed/#technology-coverage-with-cast-imaging-core-84). To help you decide whether you can migrate your applications, CAST provides a dedicated command line tool (called **Compatibility Inspector**) which produces key information about your applications and their eligibility for migration (i.e. whether they contain unsupported technologies and whether they can be migrated or not) - to find out more about this tool, see [Compatibility Inspector](compat-tool/). CAST highly recommends that you run it BEFORE you embark on a migration.

In addition, as part of the migration process, a status report will be generated and stored on disk in text format: this report will contain key information regarding the migration status, details about each migrated application, the technologies they contain, the extensions that require an update, and advice about what to do next. This report must be consulted BEFORE you proceed with [Step 3 - Application update and re-scan](../applications/) as it will tell you whether you should complete this step or not.

### What about extensions?

Some extensions have specific releases for use with **CAST Imaging Core 8.4.x** and these extensions will be automatically updated to the most recent release during the application re-scan to ensure that analyses will function correctly. The migration status report (mentioned above) will contain a list (per application) of the extensions that will be updated. In particular, the following will always be updated:

- [com.castsoftware.dotnet](https://extend.castsoftware.com/#/extension?id=com.castsoftware.dotnet&version=latest) ≥ 2.0.x
- [com.castsoftware.jee](https://extend.castsoftware.com/#/extension?id=com.castsoftware.jee&version=latest) ≥ 2.0.x
- [com.castsoftware.mainframe](https://extend.castsoftware.com/#/extension?id=com.castsoftware.mainframe&version=latest) ≥ 2.0.x
- [com.castsoftware.securityforjava](https://extend.castsoftware.com/#/extension?id=com.castsoftware.securityforjava&version=latest) ≥ 2.0.x

#### What about Extension Strategy settings?

The migration tool does NOT transfer any custom extension strategy settings that may have been made in CAST Console v2 (in the [v2 Extension Strategy](../../../export/AIPCONSOLE/Administration+Center+-+Extensions+Strategy) admin panel). CAST Imaging v3 will contain only the default settings, therefore you will need to recreate the same settings in the [v3 Extensions Strategy](../../../administer/settings/extensions-strategy/) admin panel (assuming that you require the same). You can do this at any time, but before you onboard any NEW applications.

#### Dealing with extensions downloaded to Node machines

If you are migrating a CAST Console v2 **enterprise** installation, you will need to manually move all downloaded extensions stored on your node machines to your equivalent CAST Imaging v3 node machines. Downloaded extensions are stored in the following folder:

```text
%PROGRAMDATA%\CAST\CAST\Extensions
```

- If you have **one single node** in both v2 and v3, copy the entire contents of this folder to the same location on the v3 node machine (i.e. where the `analysis-node` component is installed)
- If you have **multiple nodes** in v2 and/or v3, you will need to copy the entire contents of this folder from all your v2 nodes to the same location on all your v3 nodes. In other words, each v3 node machine will contain the same extensions, which is a merge of the extensions that are present on all the v2 node machines.

{{% alert color="info" %}}This step is not required for CAST Console v2 standalone installations.{{% /alert %}}

#### Dealing with Extend Local Server

If you are using [Extend Local Server](../../extend-local/) in **offline mode** and the required extensions do not exist in your server, the migration will be blocked. Therefore you must ensure that you do the following:

- remove all existing extensions stored on the server: to do so, delete the contents of the folder `%PROGRAMDATA%\CAST\Extend\packages`
- restart the Extend Local Server via the Microsoft Windows service
- use [ExtendCli](../../../administer/extend-local/manual-content-update/extend-cli/) to create a new bundle specifically containing the required extensions, i.e. by running the following commands (where `<api-key>` = your Extend API key and where `<imaging-core-release>` = the required release of CAST Imaging Core, i.e. 8.4.1):

```bash
ExtendCli.exe config set api-key <api-key>
ExtendCli.exe bundle clear
ExtendCli.exe bundle pack -o C:\CAST\temp -a <imaging-core-release>
```
- then [Update Extend Local Server with the new bundle](../../../administer/extend-local/manual-content-update/).

### What about required free disk space?

As part of the migration process, the contents of the v2 `delivery`, `deploy` and `common` data folders will be copied to equivalent v3 folders. As such, you will need free disk space as follows:
  - Minimum 10GB for the installation of CAST Imaging v3 (if all components are on the same machine).
  - The same size as the v2 `delivery`, `deploy` and `common` data folders combined since they will be duplicated in your CAST Imaging v3 installation.
  - Minimum 5GB for the migration process itself.

### What about application schemas? 

The migration tools do not modify or move the application result schemas (`_MNGT`, `_CENTRAL`, `_LOCAL`) and as part of the migration process, details of all database instances used for result storage in CAST Console v2 will be transferred automatically to CAST Imaging v3 (i.e. they will be visible in the [CSS and Measurement Settings](../../../administer/settings/global-configuration/css-measure/) panel post-migration).

### What about CAST Dashboards?

Technically, the migration tools do not "move" the application schemas (`_MNGT`, `_CENTRAL`, `_LOCAL`), nor the `_MEASURE` schema, they are simply re-used in CAST Imaging v3, therefore:

- If you are using CAST Dashboards (Health/Management and Engineering) **embedded** in CAST Console v2, then there is nothing to do: these Dashboards will be available in CAST Imaging v3 via menu options in the UI.
- If you are using **standalone** CAST Dashboards, there is also nothing to do: your existing standalone dashboards will function exactly as pre-migration.

### What about application results imported into CAST Imaging Viewer v2 (via CSV)?

All application results that have been manually imported into CAST Imaging Viewer v2 (from a `.CSV` export) will be automatically transferred to CAST Imaging v3. The application results can be consulted when [Step 2 - Migrate your data](../migrate-data/) is complete.

### What about users and authentication configuration?

The migration tool does not transfer any of the following that has been configured in v2:

- authentication settings
- users/groups
- permissions

All settings will need to be manually configured in CAST Imaging v3 post migration. See:

- [Authentication](../../authentication/)
- [User Permissions](../../../administer/settings/user-permissions/)

### What about Legacy vs Fast Scan onboarding workflow

CAST Imaging v3 does not support the "legacy" onboarding workflow that is available in CAST Console v2. In other words, any applications that are migrated to CAST Imaging v3 and were previously using the "legacy" onboarding workflow will be automatically converted to use the Fast Scan onboarding workflow as part of the migration process.

A reminder that the Fast Scan onboarding workflow functions as follows:

- Source code is uploaded via a ZIP file or directly from a source code folder.
- An initial fast scan is then run and results of this are displayed.
- The delivered source code can then be inspected (size, structure etc.), source code filters (exclusions) can be defined and an architecture preview diagram can be consulted. You will be alerted you if the architecture preview produced by the fast scan identifies missing source code - you can provide this missing code before you start the analysis process.
- Following that, an analysis and publishing of the results are actioned.

Some limitations apply:

- Fast Scan onboarding mode uses the rapid delivery mode without version history. As a result, following the migration and application update, rescan and analysis:
  - **Version history** will no longer be available for your application. Version history is retained in CAST Dashboards however.
  - **Source code** for your application will no longer be present in the "deploy" folder and is instead accessed directly from either the "upload" folder (for a ZIP or archive file) or directly from the source folder location. This means that CAST Dashboards will report added/deleted objects even though the source code is identical.
- Any **modules** that use the **Module per subfolder** configuration will need to be re-created post migration- see [What about module configuration?](#what-about-module-configuration) below.
- Any **custom Analysis Units** will need to be re-created post migration since these are not retained.
- **Added/deleted objects** will be recorded in CAST Dashboards post migration if the original source code was delivered (either via a ZIP or a source folder location) in **one single root folder** (for example a code download from Github). This is caused by a change in the GUID of objects - even though the objects themselves are unchanged. This may affect long-term trending.

{{% alert color="info" %}}When using the [com.castsoftware.aip.console.tools](https://extend.castsoftware.com/#/extension?id=com.castsoftware.aip.console.tools&version=latest) automation extension, the two step workflow used in the Fast Scan onboarding workflow (fast scan then analysis) is avoided.{{% /alert %}}

### What about multi-node deployments for distributed/enterprise deployments?

If you have multiple analysis nodes in v2, it is not necessary to deploy an equal number of analysis nodes in v3 in order to complete the migration: one single analysis node in v3 is all that is required. You can install additional analysis nodes post migration.

Technically, data that you backup in [Step 1 - Backup your data](../backup-data/) will be collected for ALL your analysis nodes and the migration process described in [Step 2 - Migrate your data](../migrate-data/) will migrate all that data.

{{% alert color="info" %}}The migration tool does NOT handle situations where specific applications are assigned to a specific node in v2. During the migration, all applications will be set to "load balancing" mode, where any analysis node in V3 will be eligible to run the analysis. If you need to assign a specific application to a specific node in v3, this should be done post-migration, see [Managing a node](../../../administer/settings/applications/details/node/).{{% /alert %}}

### What about CAST Imaging Viewer v2 datasources for source code display?

The migration tool does not handle datasources that have been manually configured in CAST Imaging Viewer v2 for source code display - these are defined using the following icon in v2 (and explained [here](../../../export/IMAGING/Admin+Center+-+Configuring+data+source)):

![](datasource.jpg)

In addition, the concept of datasources does not exist in CAST Imaging v3: instead, as long as the appropriate CAST Storage Service/PostgreSQL instance is configured as explained in [CSS and Measurement Settings](../../../administer/settings/global-configuration/css-measure/) then source code will be automatically displayed. Therefore you should ensure that all relevant CAST Storage Service/PostgreSQL instances are configured in CAST Imaging v3 when the migration is complete.

### What about module configuration?

For the most part, module configuration is correctly migrated to CAST Imaging v3, however, for **manual** module configurations that use the **Module per subfolder** option, this is not the case and the customization will need to be manually re-created in CAST Imaging v3, AFTER the initial rescan/analysis that finalizes the migration, in some situations, i.e.:

- for applications using the **fast scan onboarding workflow** in v2, if the source code was uploaded via a **ZIP file**, then the **Module per subfolder** configuration (specificially the path) will need to be re-created because the folder on disk where the zip is stored has changed in CAST Imaging v3. You will need to update the path to the required folder in the following shared path: `common-data\upload\<app>\main_sources\<folder>`. For applications created from a **source code location** there is nothing to do.
- for applications using the **legacy onboarding workflow** in v2 and migrated to the **fast scan onboarding workflow**, the **Module per subfolder** configuration will need to be re-created because the deploy folder has changed in CAST Imaging v3. You will need to update the path to the required folder in the following shared path: `deploy\<app>\main_sources\<folder>`. This applies to applications created both from a ZIP file or a from a source code location.

The module configuration can be found in the [Config](../../../interface/analysis-config/config/) > [Modules](../../../interface/analysis-config/config/modules/) option - use the edit button (4) to change the path:

![](modules.jpg)

![](modules1.jpg)

## What's next?

Instructions for each step are provided here:

- [Step 1 - Backup your data](../backup-data/)
- [Step 2 - Migrate your data](../migrate-data/)
- [Step 3 - Application update and re-scan](../applications/)
