---
title: "Using the v3 Compatibility Inspector tool"
linkTitle: "Compatibility Inspector"
type: "docs"
weight: 10
no_list: true
---

***

## Overview

Some technologies that are fully supported in v2 are not (yet) supported in CAST Imaging v3 - you can see a list [here](../../../whats-changed/#technology-coverage-with-cast-imaging-core-84). To help you decide whether you can migrate your applications, CAST provides a dedicated command line tool (called **Compatibility Inspector**) which produces key information about your applications and their eligibility for migration (i.e. whether they contain unsupported technologies and whether they can be migrated or not) - this tool is explained below. CAST highly recommends that you run it BEFORE you embark on a migration.

## Where can I obtain the tool?

The Compatibility Inspector is provided as part of the [com.castsoftware.imaging.migrationtool](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.migrationtool&version=latest) extension. You can find it at the root of the unzipped extension in a folder called **CompatibilityAssessment**.

The tool comprises:

- A `Cast_Imaging_V2_Compatibility_Inspector.bat` file to run the tool
- An `assessment.conf` configuration file to configure the tool

## Where should I run the tool?

The Compatibility Inspector should be run on a **CAST Console v2 node** machine:

- for a CAST Console v2 standalone deployment, on the host machine
- for a CAST Console v2 enterprise deployment, on ANY node machine

## Process

### Step 1 - Set-up the configuration file

The Compatibility Inspector is run via a batch file which refers to a configuration file containing various options that determine how the tool is run. Therefore, the first step is to set-up this configuration file: you can find the `assessment.conf` file at the root of the unzipped migration tool extension in a folder called **CompatibilityAssessment**. Change the options to match your environment, as explained below:

| Option | Description |
|---|---|
| `console=` | Choose one of the following depending on your migration strategy:<br><br>- `Standalone`<br>- `Enterprise` |
| `v2_css_user=` | Username used to connect to the database instance defined by `v2_css_host=` (default: `operator`). |
| `v2_css_host=` | Hostname/IP address of the database instance where the v2 `aip_node` / `aip_config` / `node_standalone` schemas are stored (default: `localhost`). |
| `v2_css_port`= | Port for database instance defined by `css_host` (default: `2284`). |
| `v2_css_database=` | Name of the database in which the the v2 `aip_node` / `aip_config` / `node_standalone` schemas are stored on the database instance (default: `postgres`). |
| `log=` | A local folder for storing the log files. Use a standard Microsoft Windows path with back slashes, e.g.: `C:\temp\log`. This folder MUST exist on disk before running the action - the installer will attempt to create it if it cannot be found. |
| `casthomedir=` | Path (using standard Microsoft Windows back slashes) to the CAST AIP Core 8.3 installation folder on the host machine, e.g.: `C:\Program Files\CAST\8.3`. |

### Step 2 - Run the Compatibility Inspector

To run the tool:

- Connect to your chosen CAST Console v2 node machine.
- Open a CMD window with elevated permissions (right click, `Run as administrator`) and navigate to the root of the unzipped tool extension.
- Execute the following command to set the password for the database user defined by the option `v2_css_user=` in the `assessment.conf` file (otherwise you will be prompted to enter this during the migration):

```
SET CSSPASSWORD=CastAIP
```

- Now execute the `Cast_Imaging_V2_Compatibility_Inspector.bat` file provided alongside the `assessment.conf` file.
- On completion, check the log file (defined by the option `log=` in the `assessment.conf` file).

### Step 3 - Check the results

The tool will generate a `.CSV` report in the current folder, called `MigrationCompatibilityAssessmentReport.csv`.

This report provides, for all applications, a STATUS as follows:

- **Fully supported with CAST Imaging v3** - all technologies are supported in CAST Imaging v3 and you can re-scan your application.
- **Partially supported with CAST Imaging v3** - some technologies in the application are not supported in CAST Imaging v3, followed by a list of the unsupported technologies. You can re-analyze your application but any objects related to unsupported technologies will be removed during the initial post-update re-scan.
- **Partially supported with CAST Imaging v3** - none of the technologies in the application are supported in CAST Imaging v3 and it will not be possible to re-scan your application.

For example:

![](report.jpg)
