---
title: "Quickstart"
linkTitle: "Quickstart"
description: "Simple instructions to get you up and running with CAST Imaging v3 on a single Microsoft Windows machine"
type: "docs"
no_list: true
---

## Overview
Instructions for installing CAST Imaging v3 on one single Microsoft Windows machine from scratch: this quickstart guide covers the installation and initial configuration of the required components, basic analysis and information about accessing and consuming your results and is aimed at those wanting up and running quickly and use the installation from the same Microsoft Windows machine (e.g. for POCs/demos etc.). The following steps are explained:

- [Overview](#overview)
- [Requirements](#requirements)
- [Install CAST Storage Service](#install-cast-storage-service)
- [Install CAST Imaging Core](#install-cast-imaging-core)
- [Install CAST Imaging](#install-cast-imaging)
- [Complete initial configuration](#complete-initial-configuration)
- [Onboard a simple application](#onboard-a-simple-application)
  - [Prepare your source code](#prepare-your-source-code)
  - [Onboard your new application](#onboard-your-new-application)
  - [Start your analysis](#start-your-analysis)
  - [Consult your results](#consult-your-results)

{{% alert color="info" title="Note" %}}Installation instructions for **Linux via Docker** can be found [here](../install/global/docker/).{{% /alert %}}

## Requirements

 See [Requirements](../requirements).

## Install CAST Storage Service
CAST Storage Service is a PostgreSQL database used by CAST Imaging to store configuration and analysis results, see [CAST Storage Service for Microsoft Windows](../install/db/css/).

1. Download and unzip the latest release of [com.castsoftware.css](https://extend.castsoftware.com/#/extension?id=com.castsoftware.css&version=latest) on your local disk.
1. Execute the `setup.bat` file with elevated permissions (right click, `Run as administrator`) at the root of the unzipped files. 

{{% alert color="info"  title="Note" %}}Leave all settings at their default position.{{% /alert %}}

After successful installation you will find a functioning CAST Storage Service (CSS) on `localhost:2284` with `operator/CastAIP` default credentials.

## Install CAST Imaging Core

CAST Imaging Core 8.4 is the analysis engine providing core services to the CAST analyzers.

1. Download and unzip the latest release of [com.castsoftware.imaging.core](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.core&version=latest) on your local disk. 
1. Execute the `setup.bat` file with elevated permissions (right click, `Run as administrator`) at the root of the unzipped files.

{{% alert color="info"  title="Note" %}}Leave all settings at their default position.{{% /alert %}}

## Install CAST Imaging
CAST Imaging is the web application used to launch analyses of your application and view results. CAST Imaging Node Service provides communication between CAST Imaging and the CAST Imaging Core.

1. Download and unzip the latest release of [com.castsoftware.imaging.console](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.console&version=latest) on your local disk.
1. Locate the `config-all.conf` file at the root of the unzipped files and open it with a text editor. 
Check that all parameters match your installation requirements. A full explanation of each parameter is provided in [Installation variables](../install/global/windows/variables/), but leaving the settings at their default will provide a working installation.
1. Open a CMD window with elevated permissions (right click, `Run as administrator`) and execute the following command from the root of the unzipped files:<br>
    ```text
    cast-imaging-install.bat package=all
    ```

{{% alert color="info"  title="Note" %}}<ul><li>A database called `keycloak_v3` will be created on the CAST Storage Service for storing authentication data. If this database already exists it will be re-used and this may cause unexpected behaviour.</li><li>Persistence schemas called `admin_center` and `analysis_node` will be created in the database of your choice on the target CAST Storage Service/PostgreSQL instance. If these schemas already exist they will be re-used and this may cause unexpected behaviour.</li></ul>{{% /alert %}}

## Complete initial configuration

CAST Imaging component will be opened in your default browser (alternatively browse to `http://localhost:8090`).

1. Login using the default `admin/admin` credentials. You will be prompted to configure:<br>
    - your licensing strategy. Choose either a `Named Application` strategy (where each application you onboard requires a dedicated license key entered when you perform the onboarding), or a `Contributing Developers` strategy (a global license key based on the number of users):<br><br>
    ![License key](images/license_key.jpg)<br><br>
    - [CAST Extend settings](../../../administer/settings/system-settings/extend/) / [Proxy settings](../../../administer/settings/system-settings/proxy/):<br><br>
    ![CAST Extend settings](images/extend.jpg)<br><br>
1. Browse to the URL below and ensure that you have one node and the CAST Imaging Viewer components listed:<br>

    ```
    http://localhost:8090/admin/services
    ```
    ![Services](images/services.jpg)

## Onboard a simple application

### Prepare your source code

Prepare a ZIP file containing the source code to onboard and analyze:

1. Create a temporary folder
1. Add your client code in one sub-folder
1. Add your database code in another sub-folder
1. Zip the two sub-folders into one single ZIP file

For example:
```
D:\temp
    |---JEE
    |---SQL
```

### Onboard your new application

1. Browse to `http://localhost:8090/home/applications`
1. Click `Onboard application` 
1. Enter a name for your application (1)
1. Click the upload button (2) to attach the ZIP file you created previously:<br><br>
    ![Onboard app](images/onboard_app.jpg)<br><br>
1. Click `Run Scan` (1) to begin the initial "fast scan" of your source code: this allows CAST to discover what is in the ZIP file and how to handle it during the analysis process:<br><br>
    ![Run scan](images/onboard_app2.jpg)

### Start your analysis

When the ZIP file is scanned, results are automatically displayed.

1. Scroll to the bottom of the page
1. Check the panel at the bottom: if you have a green tick (1), you are good to go and you can start the analysis process (2):<br><br>
    ![Green tick](images/green_tick.jpg)

{{% alert color="info"  title="Note" %}}If you do not have a green tick, you  need to re-organize the source code in your ZIP file.{{% /alert %}}

### Consult your results

When the analysis is complete, browse to `http://localhost:8090/home/applications`, and click `Ready to view` for your application:

![Ready to view](images/ready_to_view.jpg)

The results of your application analysis will be displayed in CAST Imaging Viewer. Use the left hand panel, or double click items in the "view" to navigate your application and the objects within it:

![Results](images/results.jpg)
