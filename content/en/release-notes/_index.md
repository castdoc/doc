---
title: "Release Notes"
linkTitle: "Release Notes"
type: "docs"
no_list: false
---

{{% alert color="info" %}}This page contains links to release notes for **CAST Imaging components** except for technology extensions which are located alongside the [extension documentation](../technologies/).{{% /alert %}}