---
title: "Right panel - Tags"
linkTitle: "Tags"
type: "docs"
no_list: true
weight: 30
---

***

![](tags.jpg)

Displays a list of tags associated with any of the objects or nodes in the view. Tags fall into two categories:

- tags that have been **manually** added to an object or node. These are listed under the **Custom** heading - in the image above, there is one tag called "Test tag"
- tags that have been **automatically added by CAST**. These are listed under the **Property** heading, for example for:
    - **Status**: is the object or node (i.e. the objects in the node) considered Added/Deleted/Unchanged in these results
    - **External Libraries**: iis the object or node (i.e. the objects in the node) provided by a third-party framework or library
    - **Module**: does the object belong to a specific module
    - **Service**: does the object belong to a specific service type

Tags are interactive, i.e. clicking them will highlight the object(s) or node(s) in the view which have the tag assigned to them:

![](tag_highlight.jpg)


{{% alert color="info" %}}Tags are added via a dedicated action icon in the view, see [here](../../view/actions/#tags-tool).{{% /alert %}}
