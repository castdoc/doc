---
title: "Right panel - Compare"
linkTitle: "Compare"
type: "docs"
no_list: true
weight: 90
---

***

{{% alert color="beta" %}}This feature is currently in beta and we would welcome feedback.{{% /alert %}}

![](compare.jpg)

Displays historical information about objects and links directly in the current view:

- available at **Object level** only
- requires that the application has been **imported twice or more** into CAST Imaging whether via result import or via an analysis so that a comparison can be generated
- functions hand-in-hand with the [History](../history/) feature.

See [Working with the compare feature](../../../../explore-results/history-compare/compare/) for more details.
