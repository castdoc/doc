---
title: "Right panel - History"
linkTitle: "History"
type: "docs"
no_list: true
weight: 80
---

***

{{% alert color="beta" %}}This feature is currently in beta and we would welcome feedback.{{% /alert %}}

![](history.jpg)

Displays historical information about objects and links:

- available at **Object level** only
- requires that the application has been **imported twice or more** into CAST Imaging whether via result import or via an analysis so that a comparison can be generated
- functions hand-in-hand with the [Compare](../compare/) feature.

See [Working with object and link history](../../../../explore-results/history-compare/history/) for more details.
