---
title: "Right panel - Objects and links in the view"
linkTitle: "Objects and links in the view"
type: "docs"
no_list: true
weight: 10
---

***

![](objects-links.jpg)

Displays information about the items in the view. Use the `Hide All` option and `toggle switches` to either hide or display the corresponding item in the view.

| Item | Description |
|---|---|
| Object Types | The object or node type. |
| External Object Types | Any "third-party" object or node type for external frameworks and libraries present in the view. |
| Links | The type of links between items present in the view. Sub-links |
| Link Origin |  |
| Link Categories |  |
| Object Identifiers | An item in the view may be specifically categorised, for example "start" and "end point" objects in transactions. This section lists all items that belong to a specific category using a "badge" system. The badge is interactive and will highlight all corresponding items in the view when clicked.<br><br>![](object_identifier.jpg) |
