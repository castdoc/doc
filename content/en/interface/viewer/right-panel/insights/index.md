---
title: "Right panel - Insights"
linkTitle: "Insights"
type: "docs"
no_list: true
weight: 20
---

***

## Overview

![](insights.jpg)

Provides information about the following "insights" that are present in items in the current view:

- Structural Flaws
- CloudMaturity Blockers/Boosters
- Containerization Blockers
- Open Source Safety
- Green Deficiencies

Use the **toggle switches** to highlight any items in the view that correspond to a category:

![](highlight_insights.jpg)

## What insights are available?

### Structural Flaws

These insights are flaws found during an analysis of your application (i.e. critical violations of rules). These flaws indicate that the object's code violates a specific programming practice and will need refactoring or updating to ensure the flaw is no longer present.

The interface provides:

- the number of objects where the same flaw exists
- a link to the full rule documentation to be opened a new tab - allowing you to understand why your code violated the rule and how you can potentially refactor it to remove the flaw:

![](flaws.jpg)

Full documentation:

![](flaw2.jpg)

{{% alert color="info" %}}To view Structural Flaws requires:<br><br><ul><li>the application to be analyzed with the extension [com.castsoftware.qualitystandards](../../../../technologies//multi/com.castsoftware.qualitystandards/) (this extension is Force Installed)</li><li>the application to be analyzed with the extension [com.castsoftware.consoleinformation](../../../../technologies//multi/com.castsoftware.consoleinformation/) ≥ 1.0.2-funcrel (this extension is Force Installed, but ensure that you are using the correct release)</li><li>a license which includes use of the CAST Dashboards</li></ul>{{% /alert %}}

### CloudMaturity Blockers/Boosters and Containerization Blockers

These insights provide information about how well prepared your application is for cloud migration, i.e. moving from an on premises deployment to a cloud scenario. More precisely, this is a list of items or behaviour found in the application that could either hinder a move to the cloud and may therefore need attention (a blocker) or that already conform to cloud best-practices (a booster).

{{% alert color="info" %}}To view Blockers/Boosters requires:<br><br><ul><li>the application to be analyzed with the extension [com.castsoftware.highlight2mri](../../../../technologies//multi/com.castsoftware.highlight2mri/) (this information is provided by [CAST Highlight](https://www.castsoftware.com/highlight))</li></ul>{{% /alert %}}

### Open Source Safety

Identifying the appropriate version to update a vulnerable third-party component can be time-consuming, especially when multiple components have accumulated vulnerabilities due to delayed updates. As a technical lead or software engineer, these insights provide information about potentially vulnerable third-party components used in your application and allow you to examine their compatibility with other components. Additionally, you can plan updates by understanding the release frequency of these components, identifying gaps, and receiving recommendations for the closest or safest version to utilize.

See [Dealing with outdated third-party components](../../../../explore-results/learn/third-party/) for more detail about this feature.

{{% alert color="info" %}}To view Open Source Safety insights requires:<br><br><ul><li>the application to be analyzed with the extension [com.castsoftware.highlight2mri](../../../../technologies//multi/com.castsoftware.highlight2mri/) (this information is provided by [CAST Highlight](https://www.castsoftware.com/highlight))</li><li>the relevant fields in [System Settings - CAST Highlight Settings](../../../../administer/settings/system-settings/highlight) to be populated.</li></ul>{{% /alert %}}

### Green Deficiencies

These insights provide information about how environmentally friendly your application is: i.e. a list of checks focused on programming practices and engineering principles that could be detrimental to the environment are run against your application and any code that violates these checks is flagged and displayed. For example, checks such as "Avoid queries without WHERE condition" - i.e. flagging SQL queries that return many results resulting in increased processing power and time to complete.

See [How green is your application?](../../../../explore-results/learn/green) for more detail about this feature. You can also find a list of checks here: https://doc.casthighlight.com/greenpatterns/.

{{% alert color="info" %}}To view Green Deficiencies insights requires:<br><br><ul><li>the application to be analyzed with the extension [com.castsoftware.highlight2mri](../../../../technologies//multi/com.castsoftware.highlight2mri/) (this information is provided by [CAST Highlight](https://www.castsoftware.com/highlight))</li></ul>{{% /alert %}}
