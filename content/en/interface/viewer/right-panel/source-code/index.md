---
title: "Right panel - Source code"
linkTitle: "Source code"
type: "docs"
no_list: true
weight: 70
---

***

![](source_code.jpg)

Displays an object's source code (only available at Object level). The cursor will automatically scroll to the first line in the parent file in which the object is defined. A small snippet of the entire parent file source code is shown by default.

Use the following options to aid your investigation:

| Option | Description |
|:-:|---|
| ![](show_full.jpg) | Refreshes the window and displays the entire parent file source code. |
| ![](open_new.jpg) | Opens the source code in a floating window which can be resized as required. |
| ![](night.jpg)| Toggles between light and dark theme. |

{{% alert color="info" %}}<ul><li>Source code is fetched automatically direct from the CAST Storage Service/PostgreSQL instance which is used to store the application analysis results.</li><li>For applications where only the results have been imported, a data source will need to be <a href="../../../../administer/settings/applications/data-source/">configured</a> before source code can be displayed.</li><li>See also <a href="../../../../administer/performance/source-code/">Source code does not display</a> to troubleshoot issues with source code display.</li></ul>{{% /alert %}}
