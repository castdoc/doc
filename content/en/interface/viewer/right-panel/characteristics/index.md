---
title: "Right panel - Characteristics"
linkTitle: "Characteristics"
type: "docs"
no_list: true
weight: 50
---

***

Displays detailed information about an item (node/object or link) that has been selected in the view:

{{< tabpane text=true >}}
{{% tab "Node" %}}
Details about nodes (i.e. multiple grouped objects) are limited:<br><br>
![](charac1.jpg)
{{% /tab %}}
{{% tab "Object" %}}
A high-level of detail is provided for specific objects:<br><br>
![](charac2.jpg)<br><br>**Associated to**<br><br>Additional associated information is available for objects, showing items the object is assoicated with, where applicable:<ul><li>Associated Transaction(s)</li><li>Data Call Graph(s)</li><li>Module(s)</li><li>Service(s)</li><li>Subset(s)</li><li>Project(s)</li><li>Saved views</li></ul>Selecting the "associated to" item (e.g. a Data Call Graph the object is part of) will navigate directly to that item:<br><br>![](associated_to.jpg)
{{% /tab %}}
{{% tab "Reference" %}}
Basic details are provided for references, (i.e. where there are multiple links between the two items because the items are nodes):<br><br>
![](charac3.jpg)
{{% /tab %}}
{{% tab "Link" %}}
Details for links between objects:<br><br>
![](charac4.jpg)
{{% /tab %}}
{{< /tabpane >}}

{{% alert color="info" %}}Characteristics can also be accessed by right clicking the item in the view and selecting **Show details**:<br><br>![](right_click.jpg){{% /alert %}}