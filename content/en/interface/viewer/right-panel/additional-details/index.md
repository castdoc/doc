---
title: "Right panel - Additional details"
linkTitle: "Additional details"
type: "docs"
no_list: true
weight: 60
---

***

![](additional_details.jpg)

Provides complementary (to the information provided in [Characteristics](../characteristics)) details about the item selected in the view. Typically this will include:

- **Fullname**: the selected object's "fullname" as determined during the analysis process
- **Filename**: the parent file name in which the object is defined
- **Number of code lines**: the number of lines of code which define the selected object 