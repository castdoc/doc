---
title: "Right Panel - Viewer"
linkTitle: "Right panel"
type: "docs"
no_list: false
weight: 30
---

***

The right panel will displays additional contextual information about items in the view. Click the item in the image you would like to know more about:

<img src="right_panel.jpg" usemap="#image-map">

<map name="image-map">
    <area target="" alt="Objects and links in the view" title="Objects and links in the view" href="object-link/" coords="0,2,258,36" shape="rect">
    <area target="" alt="Insights" title="Insights" href="insights/" coords="1,38,258,66" shape="rect">
    <area target="" alt="Tags" title="Tags" href="tags/" coords="0,68,258,99,102,83" shape="rect">
    <area target="" alt="Post-It" title="Post-It" href="post-its/" coords="0,102,258,131" shape="rect">
    <area target="" alt="Characteristics" title="Characteristics" href="characteristics/" coords="2,133,257,162" shape="rect">
    <area target="" alt="Additional details" title="Additional details" href="additional-details/" coords="3,163,258,192" shape="rect">
    <area target="" alt="Source code" title="Source code" href="source-code/" coords="0,195,258,221" shape="rect">
    <area target="" alt="History" title="History" href="history/" coords="1,223,258,254" shape="rect">
    <area target="" alt="Compare" title="Compare" href="compare/" coords="0,256,257,301" shape="rect">
</map>