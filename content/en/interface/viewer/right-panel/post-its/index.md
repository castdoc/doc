---
title: "Right panel - Post-Its"
linkTitle: "Post-Its"
type: "docs"
no_list: true
weight: 40
---

***

![](sticky_note.jpg)

Displays a list of Post-Its attached to the view itself, or to one or multiple objects/nodes visible in the current view. Clicking them will display the corresponding Post-It:

![](sticky_note_click.jpg)

{{% alert color="info" %}}Post-Its:<br><br><ul><li>are added either<ul><li>via a dedicated action icon in the view, see [here](../../view/actions/#post-its-tool).</li><li>or are automatically added via the [AI services features](../../../../explore-results/ai)</li></ul></li><li>can also be accessed by right clicking the item in the view and selecting **Show details**:<br><br>![](right_click.jpg)</li></ul>{{% /alert %}}