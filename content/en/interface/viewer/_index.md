---
title: "CAST Imaging Viewer"
linkTitle: "Viewer"
type: "docs"
no_list: false
weight: 30
---

***

Click the item in the image you would like to know more about:

<img src="all.jpg" usemap="#image-map">

<map name="image-map">
    <area target="" alt="Left panel" title="Left Panel" href="left-panel/" coords="1,3,347,294" shape="rect">
    <area target="" alt="View" title="View" href="view/" coords="353,0,1022,756" shape="rect">
    <area target="" alt="Right panel" title="Right panel" href="right-panel/" coords="1036,0,1425,756" shape="rect">
</map>