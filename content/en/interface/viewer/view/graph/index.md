---
title: "View - Graph options"
linkTitle: "Graph options"
type: "docs"
no_list: true
weight: 40
---

![](graph_options.jpg)

## Expand / Collapse All

{{% alert color="info" %}}Only available in the **Application** scope at **Object** level when drill down mode is set to **Display children with caller/callees** in the [left panel](../../left-panel).{{% /alert %}}

![](expandall_icon.jpg)![](collapseall_icon.jpg)

- **Expand all** (ungroup) all nodes in one go
- **Collapse all** (regroup) all nodes in one go (only displayed once the **Expand all** option has been selected).

## Transaction details / Update start and end points

{{% alert color="info" %}}Only available as follows:<br><br><ul><li>When using the <strong>Transaction</strong> scope and the <strong>Simplified Call Graph</strong> is active.</li><li>When using the <strong>Get start and end points</strong> option.</li></ul>{{% /alert %}}

![](details_icon.jpg)

Allows you to set the required depth level between the source object and the end points via the **Update Hops** option visible in the [left panel](../../left-panel).

## View zoom options

![](graph_zoom.jpg)

From left to right:

- Zoom in
- Focus center (zoom reset) - all nodes visible
- Zoom out

## Undo/redo

![](undoredo.jpg)

Undo and redo options for actions such as:

- adding callers/callees
- repositioning objects
- adding custom objects/links
- ungrouping manual and auto-grouped objects
- hide/unhide objects

{{% alert color="info" %}}The standard keyboard commands `CTRL+X` and `CTRL+Y` are also supported.{{% /alert %}}

## Graph info

![](graphlegend_icon.jpg)

Provides basic information about the items in the view:

![](graphlegend.jpg)

## Loading icon

![](loading_icon.jpg)

In the majority of scenarios, this button will be invisible. However, when accessing a node (double click) which contains more than 1000 child nodes, or more than 2000 links, a progressive rendering system is invoked in order to avoid situations where the rendering process locks the browser. Portions of the full graph are displayed progressively in steps and when this occurs, this icon will be displayed. You can click this button to stop the progressive loading if it is continuing for too long.

