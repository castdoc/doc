---
title: "View - Action options"
linkTitle: "Action options"
type: "docs"
no_list: true
weight: 10
---

## Overview

The Action toolbar provides options for changing the layout of the items in the view. Each is explained below.

![](toolbar.jpg)

{{% alert color="info" %}}<ul><li>Not all options in the toolbar are available at all levels.</li><li>The toolbar can be moved anywhere in the view by dragging and dropping it.</li><li>The toolbar can be collapsed to improve view visibility using the icon at the bottom of the toolbar:<br><br><img src="collapse.jpg"></li></ul>{{% /alert %}}

## Add objects

![](add_objects_search_icon.jpg)

Only visible in a tab which has been added following the use of the [global search](../../../../explore-results/search/global/) feature to display results. Using this action icon will open the search popup and any new objects you select for display as a result of the search will be added to the existing tab. See also [Tips](../../../../explore-results/search/global/#add-new-search-results-to-an-existing-tab).

## Select

![](select_icon.jpg)

Option selected by default, allows you to interact with the items in the view, for example, to drill-down or select.

## Selection tool

![](selection_icon.jpg)

Provides various methods for selecting multiple objects and nodes:

![](selection_icon_sub.jpg)

## Insert tool

![](insert_icon.jpg)

Provides methods for inserting both text and shapes (rectangles or circles) into the view, for example to highlight specific items:

![](insert_icon_sub.jpg)

For example, a rectangle with a red border has been added, alongside some text and an arrow:

![](insert_example.jpg)

{{% alert color="info" %}}Shapes, text and arrows are not persistent. In other words, if you close the current view or refresh your browser, the inserts will be lost. To ensure the items persist, use the **Save a view** option:<br><br>![](save_view.jpg){{% /alert %}}

## Drawing tool

![](drawing_icon.jpg)

Provides a method to draw a freehand shape in the view in the colour of your choice, for example to highlight specific items or add your own notes:

![](drawing_icon_sub.jpg)

For example:

![](drawing_icon_example.jpg)

{{% alert color="info" %}}Freehand shapes are not persistent. In other words, if you close the current view or refresh your browser, the shapes will be lost. To ensure the items persist, use the **Save a view** option:<br><br>![](save_view.jpg){{% /alert %}}

## Graph layout tool

![](graphlayout_icon.jpg)

Provides layout options for the items in the view (`Sequential` is set as the default):

![](graphlayout_icon_sub.jpg)

{{% alert color="info" %}}You can set a preferred layout for all applications using the [Preferences](../../../../administer/viewer-prefs/) > **Preferred Graph Layout** option.{{% /alert %}}

## Add concept node tool

{{% alert color="info" %}}Only available in the **Application** scope at **Level 5**.{{% /alert %}}

![](addconcept_icon.jpg)

Provides a means to add an object type of your choosing to the view. All possible object types can be added, except those that already exist on the view whether as other concept nodes, or object types resulting from analyses:

![](addconcept_icon_sub.jpg)

Concept object type nodes are displayed as squares to differentiate from object types nodes resulting from an analysis (displayed as circles):

![](addconcept_icon_square.jpg)

##  Visual Grouping/Group By tool

{{% alert color="info" %}}Available at **Levels 2 - 5** for all Aggregated by views. Not available for **App-to-App Dependencies** view.{{% /alert %}}

![](visualgrouping_icon.jpg)

Allows you to apply a different level specific grouping to the level you are currently working in:

![](visualgrouping_icon_sub.jpg)

For example, if you are working in level 5 you can choose to view the nodes in level 5 using the abstract grouping from any of the other Levels 1 - 4, listed as:

- Architecture (level 1 grouping)
- Services (level 2 grouping)
- Business (level 3 grouping)
- Technology (level 4 grouping)

Choosing Architecture (level 1) when in level 5 (for example) will show the following:

![](visualgrouping_example.jpg)

The circles are added to the view to highlight the nodes that are grouped in their chosen level grouping - e.g. in this example, Level 1 groupings:

- User Interaction
- System Interaction
- Services
- Data Services

## Spotlight tool

{{% alert color="info" %}}See also [Spotlight search](../../../../explore-results/search/spotlight/).{{% /alert %}}

![](spotlight_icon.jpg)

Provides a mechanism to search for nodes or objects that are present in the current view:

![](spotlight_icon_example.jpg)

Searches are performed on the node or object name and matching items are then highlighted directly in the view:

![](spotlight_icon_example2.jpg)

## Group selected nodes tool

{{% alert color="info" %}}Only available at **Level 5 and Objects level**.{{% /alert %}}

![](groupnodes_icon.jpg)

Allows you to group specific nodes or objects into single nodes. For example, you may want to group together SQL object types, or JEE object types so that the view is easier to understand.

Clicking the icon will group all nodes or objects that are selected in the view and display them as follows:

![](groupnodes_icon_example.jpg)

You will also be prompted to give a name to this group:

![](group_by_name.jpg)

The grouped items will move around the graph as one item. To remove or rename the grouping, use the right-click contextual menu on the grouped item:

![](groupnodes_icon_menu.jpg)

**Tip**: Double click the grouped item to change the position of individual items within it.

## Nodes Display tool

{{% alert color="info" %}}Only available at **Objects level**.{{% /alert %}}

![](hidenodes_icon.jpg)

Hides (or unhides) any objects that are selected in the view - for example those that may not be of interest.

![](hidenodes_icon_sub.jpg)

## (Un)Group links tool

{{% alert color="info" %}}Only available at **Objects level**.{{% /alert %}}

Toggles between grouped links (default position) and ungrouped. Explanation: some links at object level will be "highlighted". This indicates that there is more than one link between the objects and therefore these links are grouped:

![](edgegrouping1.jpg)

Using the Ungroup links option, all links between the two objects will be displayed:

![](edgegrouping2.jpg)

Use the option again to toggle between the two states (grouped/ungrouped).

## Graph display options tool

![](graphoptions_icon.jpg)

Provides various view display options:

![](graphoptions_icon_sub.jpg)

| Option | Description |
|---|---|
| Object label | Toggle the display of the object/node name. Enabled by default. |
| Relationship label | Toggle the display of the name of the link between two objects/nodes. Enabled by default. |
| Badge count | Toggles the object/node badge count - i.e. the small circle with a number in it depicting the number of "child" items a particular grouped object has. Enabled by default. |
| External libraries / Isolated nodes | Filter on the type of objects/nodes that are displayed in the view. Toggling the option will remove and re-display corresponding objects from the view.<br><br><ul><li>**External objects** are those originating in third-party libraries or system assemblies. Enabled by default. </li><li>**Isolated nodes** are those that are not linked to any other node. Enabled by default.</li></ul> |
| BelongsTo links | Toggle the display of `BelongsTo` links (interactions between parent and child objects, for example, C++ Classes and Methods). Disabled by default. When enabled, `BelongsTo` links are displayed in pink making them distinct from other link types. |
| Shortest path | {{% alert color="info" %}}Only available for **Transaction** and **Data Call Graph** scopes at **Objects level**.{{% /alert %}} Displays the critical (i.e. shortest) path between two objects (source and target).<br><br>The view will show the critical (i.e. shortest) path between the input node (i.e. the selected transaction/node) and any "output nodes" (any nodes/objects not involved will be temporarily hidden). Disabled by default.<br><br>For example:<br><br>![](shortest_path1.jpg)<br><br>![](shortest_path2.jpg) |
| Main objects only | {{% alert color="info" %}}Only available for **Transaction** and **Data Call Graph** scopes at **Objects level**.{{% /alert %}}Toggles the view to show a simplified version of the Transaction or Data Call Graph including only the main objects and excluding any sub-objects. Disabled by default.<br><br>For example, option disabled:<br><br>![](mainobjects1.jpg)<br><br>Option enabled:<br><br>![](mainobjects2.jpg) |
| Tooltips | Displays a short tooltip explanation about the item when the item is selected in the view. Enabled by default.<br><br>![](tooltip.jpg) |

## Post-Its tool

![](sticky_icon.jpg)

Use this feature to add detailed documentation or notes to a view and/or  to specific objects/nodes or groups of objects/nodes:

![](sticky_icon_sub.jpg)

When a Post-It has been assigned to an object or node, the item's badge will update to indicate that a Post-It exists:

![](sticky_note_badge.jpg)

{{% alert color="info" %}}Post-Its:<br><br><ul><li>are viewed via the right panel, see [here](../../right-panel/post-its).</li><li>can also be accessed by right clicking the item in the view and selecting **Show details**:<br><br>![](right_click_sticky.jpg)</li></ul>{{% /alert %}}

## Tags tool

![](tags_icon.jpg)

Use this feature to add custom tags to specific objects/nodes or groups of objects/nodes. Tags can then be used as a search term to find items of interest more quickly:

![](tags_example.jpg)

{{% alert color="info" %}}Custom tags are viewed via the right panel, see [here](../../right-panel/tags).{{% /alert %}}
