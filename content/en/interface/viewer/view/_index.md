---
title: "View - Viewer"
linkTitle: "View"
type: "docs"
no_list: false
weight: 20
---

***

The "view" is where your application's nodes and objects are displayed for you to investigate. Various options and icons are available to you to help you navigate your application: click the item in the image you would like to know more about:

<img src="view.jpg" usemap="#image-map">

<map name="image-map">
    <area target="" alt="Action options" title="Action options" href="actions/" coords="1,27,67,475" shape="rect">
    <area target="" alt="Switcher" title="Switcher" href="switcher/" coords="236,4,406,48" shape="rect">
    <area target="" alt="Zoom controls" title="Zoom" href="zoom/" coords="645,589,597,442" shape="rect">
    <area target="" alt="Graph options" title="Graph options" href="graph/" coords="400,538,593,588" shape="rect">
</map>