---
title: "View - Zoom controls"
linkTitle: "Zoom controls"
type: "docs"
no_list: true
weight: 30
---

![](zoom_control.jpg)

Interactive slider to control the which level is currently displayed in the view.

{{% alert title="Tips" color="info" %}}<ul><li>Only available in the <strong>Application</strong> scope and <strong>CAST Taxonomy</strong> aggregation mode.</li><li>Level control is also available in the <a href="../../left-panel/">left panel</a> via the <strong>Drill down level</strong> option.</li></ul>{{% /alert %}}