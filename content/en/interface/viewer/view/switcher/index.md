---
title: "View - Switcher"
linkTitle: "Switcher"
type: "docs"
no_list: true
weight: 20
---

![](switcher.jpg)

Allows you to switch between a graphical representation (default position) of the items in your application, or a simple list containing "at a glance information" about all the items in the current view.

Displaying the items in list form allows you to see more easily the items in your application, any children they may have and whether they are considered **external** (i.e. originating in third-party components):

![](list.jpg)

A filter is available at object level to help you find the objects you need. The filter is based on the properties attached to the objects visible in the view:

![](filter.jpg)

{{% alert title="Tips" color="info" %}}<ul><li>Selecting an item or items in the <strong>List</strong> and then switching back to the "graph" will retain the selection.</li><li>All columns are sortable when using the <strong>List</strong> view.</li></ul>{{% /alert %}}