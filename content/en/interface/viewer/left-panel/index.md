---
title: "Left Panel - Viewer"
linkTitle: "Left panel"
type: "docs"
no_list: true
weight: 10
---

***

## Overview

CAST Imaging manipulates a very large number of objects, and therefore for clarity and ease of understanding, different methods are provided for you to display objects - these are known as:

- **Perimeters** (i.e the scope of objects you want to explore)
- **Perspectives** (i.e. a way to group objects at application level)
- **Levels** (i.e. a level of abstraction)

The left panel allows you to manage these display methods, and to save and export the contents of the [view](../view):

![](left-panel.jpg)

## Perimeter / Scope

The perimeter defines the "scope" of objects displayed within the view. CAST provides various default scopes that will display the items in your application in specific ways, for example:

![](scope-dropdown.jpg)

Selecting a specific scope limits the investigation of objects identified within it, for example:

![](scope-description.jpg)

Some scopes provide additional "perimeters", for example specific transactions or call graphs:

![](scope-select.jpg)

### Available perimeters / scopes

| Name | Description |
|---|---|
| Application | Corresponds to the entire application. |
| Module | Content is predefined during the application analysis process - either manually or automatically depending on platform strategies. Object grouping within the module can correspond to a functional part of the application, a technology, or can also be entirely custom. Each module can be selected individually to focus on a specific perimeter.<br><br>- If only one module is defined during the analysis process corresponding to the scope of the application, then the option will be disabled.<br>- The contents of this scope cannot be modified in CAST Imaging Viewer. |
| Transaction / Data Call Graph | Transaction and Data Call Graph are based on vertical call graphs starting from an Input (Interface, batch, Web UI element etc.) down to an output (table, file, API call etc.) for Transaction and vice versa for Data Call Graph (from an output to an input).<br><br>- Transaction or Data Call Graph are automatically calculated during the application analysis process.<br>- The contents cannot be modified in CAST Imaging Viewer.<br>- Each Transaction or Data Call Graph can be selected individually. |
| Architecture Layer | An Architecture Layer is a subset of an Architecture Model built in the "Architecture Studio". The content of a Layer is defined manually - rules are used to group objects during the application analysis process.<br><br>- The contents of this scope cannot be modified in CAST Imaging.<br>- An Architecture Layer is an optional feature and will therefore be disabled in CAST Imaging Viewer if no Architecture Model is associated to the application.<br>- Each Layer can be selected individually for specific investigation. |
| Service | This perspective is built around business capabilities which are independently deployable by fully automated deployment machinery. Services are automatically detected during the analysis process, provided that a specific extension [com.castsoftware.architecture](https://extend.castsoftware.com/#/extension?id=com.castsoftware.architecture&version=latest) is used.<br><br>- The contents of this scope cannot be modified in CAST Imaging Viewer.<br>- Each Service can be selected individually. |
| Project Structure | Displays the projects within an application: each project file identified in the application during the analysis by the [com.castsoftware.architecture](https://extend.castsoftware.com/#/extension?id=com.castsoftware.architecture&version=latest) (for example a Visual Studio project file such as a `.csproj` file, or a Maven `pom.xml` project file) is represented by a node in this perimeter. You can find more technical details about the project files that are supported in the extension [documentation](../../../technologies/multi/com.castsoftware.architecture/).
| Custom |  |


## Perspective / Aggregated By

A Perspective provides a way to group objects and to get a specific abstracted view of the complete application:

![](perspective.jpg)

Perspectives are only available for the Application scope:

![](perspective-dropdown.jpg)

### Available perspectives

| Name | Description |
|---|---|
| CAST Taxonomy | Default grouping option determined by CAST, available across all Scopes. |
| Module | Content is grouped per "Module" defined manually or automatically during the application analysis process. |
| Service | Content is grouped per "Microservice", determined automatically during the application analysis process, provided that a specific extension [com.castsoftware.architecture](https://extend.castsoftware.com/#/extension?id=com.castsoftware.architecture&version=latest) is used. |
| RDBMS table inventory | A dedicated grouping showing the interaction between SQL table objects and provides access to their constituent column names. Grouping exists only where at least one SQL table object types exists in the application. |
| Classes inheritance | A dedicated grouping showing the inheritance that exists between different classes in the application. Grouping exists for all technologies where a "class" object type exists, for example JEE (Java class) and .NET (VB.NET or C# class). |
| Project structure | Content is grouped per "Project" (for example a Visual Studio project file such as a `.csproj` file, or a Maven `pom.xml` project file) determined automatically during the analysis process, provided that a specific extension [com.castsoftware.architecture](https://extend.castsoftware.com/#/extension?id=com.castsoftware.architecture&version=latest) is used. |
| ABAP Packages | A dedicated grouping showing the interaction between ABAP Package objects. Grouping exists only for SAP ABAP technologies. |

## Levels

Items in the view are further aggregated together via specific levels determined by CAST. Each level groups items by their main "type" and progressively more detail is provided until "Level 5" where a further drill down will reach the "Object" level (the highest level of detail available):

![](levels-dropdown.jpg)

When looking at your application in a level, the nodes you see are "virtual", in other words they do not represent physical objects in your application. To view physical objects, drill down to "object" level.

The levels available for selection depend on the Perspective you have chosen:

- for "CAST Taxonomy", **levels 1-5** are available for direct selection in the dropdown list, with **Objects** level available via drill down in the view itself.
- for all other Perspectives, **levels 4-5** and **Objects** are available in the dropdown list

### Available levels

| Level | Name | Example contents (non-exhaustive) |
|---|---|---|
| Level 1 | Abstracted Service Level | Highest level, objects grouped into nodes such as:<br><br>- Data Services<br>- Services<br>- System Interaction<br>- User Interaction |
| Level 2 | Service Level | - Communication Services<br>- Database Services<br>- Logic Services<br>- Monitoring Services<br>- Output Services<br>- Screen Interaction<br>- Web Interaction |
| Level 3 | Concept Level | - Business Logic<br>- Business Logic Coordination<br>- Data Access Services<br>- Exposed API<br>- Logger<br>- RDBMS Services<br>- Reporting Services<br>- Rich Client Presentation<br>- Web Coordination<br>- Web Presentation |
| Level 4 | Technology/Framework Level | Specific object groupings such as:<br><br>- Hibernate Data Access<br>- HTML Presentation<br>- Jackson<br>- Jasper Reports<br>- Java Business Logic<br>- Java Core |
| Level 5 | Object Type Level | More detailed object groupings such as:<br><br>- HTML Pages<br>- J2EE Scoped Bean<br>- Java Class<br>- Java Class DAO |

### Level example

At Level 1, the lowest level of detail is provided:

![](level-example1.jpg)

At Level 5, much more detail is provided: 

![](level-example2.jpg)