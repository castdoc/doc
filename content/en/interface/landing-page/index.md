---
title: "Application landing page"
linkTitle: "Application landing page"
type: "docs"
no_list: false
weight: 10
---

***

## Overview

The **Application landing** page is the main page displayed immediately after logging in to CAST Imaging. It lists all applications that have already been onboarded (or are in the process of being onboarded) and which the current logged in user has permission to interact with (see [User Permissions](../../administer/settings/user-permissions)). It also provides an option to onboard new applications:

![](landing_page.jpg)

{{% alert title="Note" color="info" %}}You can return to the application landing page using the hamburger menu or by clicking "CAST Imaging", both located in the top left corner of your screen.{{% /alert %}}

## What does the Add an application button do?

The large blue button in the top right corner (if you do not see this button, your user login does not have sufficient permission) allows you to create a brand new application in CAST Imaging and kick-off the application onboarding process. Clicking the button will give you the option to either:

- **Onboard a new application** for deep code analysis and result generation
- **Import an application view** i.e. import only analysis results in ZIP file format - results generated in previous releases of CAST Imaging Viewer (for example using the `etl-automation` tool) can be uploaded. See also [What does the icon in the Name column indicate?](#what-does-the-icon-in-the-name-column-indicate) below.

### Rules around application naming

- The following characters are not authorized in application names: `\ / : * ? " < > | &` (note that when a blank space is used it will be automatically converted to an underscore for internal use - the display name will use the blank space)
- Identical application names are not authorized.
- Application names are case insensitive, therefore attempting to create an application called `TEST` when `test` already exists will result in an error.

## What do the application statuses mean?

The **Status** column indicates the current "state" of a given application in CAST Imaging. For example:

- **Ready to view** indicates that the deep analysis and result generation process / application import process has completed successfully and results can be accessed.
- **Actions pending** indicates that the initial fast scan has completed, but the deep analysis and result generation process has not yet been actioned.
- **In progress** indicates that a job is currently in progress:
    - an initial fast scan
    - a deep analysis
    - an application import
- **Requires attention** indicates that there may be an issue with the current application (for example the analysis failed) that requires manual intervention.
- **Execution queued** indicates that other jobs are ongoing and there is not enough node resources to start the current job. The job will be started automatically as soon as resources are available.
- **Deleting** indicates that the current application is in the process of being removed (only users with the `Administrator` [role](../../administer/settings/user-permissions) can perform this action).

## How do I access results or application configuration?

Each application listed in the landing page is clickable:

![](clickable_row.jpg)

Depending on the status of the application, the action of clicking the row will give different results:

- Status = **Ready to view**: access the most recent analysis results
- Status = **Actions pending**, **Requires attention**, **In progress**: a dialog is displayed explaining the current status and how to proceed. If there are existing results, you will be optionally directed to them.

## What do the application size indicators mean?

Applications are categorized by size (Small, Medium, Large, XLarge, XXLarge, XXXLarge) according to the **number of lines of code** calculated by CAST Imaging during the initial fast scan process. Files that are not considered source code (i.e. image files for example) are not included in this value.

## What do the icons in the Name column indicate?

Two icons may be visible:

### Results only

When an icon is visible in the **Name** column as shown in the image below, this indicates that only the application's analysis results are available for viewing. In other words the results have been uploaded via a results ZIP file using the **Add an application** > **Import an application view** option and there is no possibility to configure any analysis options:

![](name_column.jpg)

{{% alert title="Note" color="info" %}}Application analysis results ZIP files can be generated using the `etl-automation` [tool](https://doc.castsoftware.com/export/IMAGING/Automation+tool+export+and+import+process) available in previous releases of CAST Imaging Viewer.{{% /alert %}}

### Job in progress

![](in_progress.jpg)

When any job related to the application is in progress (new analysis, new fast scan etc.) then this "in progress" icon will be displayed

## How do I use the Actions menu?

The **Actions** menu provides additional options for each application:

![](actions.jpg)

Options will either be available or not depending on:

- whether a job is currently in progress
- the current user's [permissions](../../administer/settings/user-permissions/)

### Run a new scan

This option is to be used when you have updated application source code and you want to run a **new analysis using this updated source code**. The option will prompt you to upload a new ZIP file or choose from a source code folder (you will only be offered the option that was used for the initial application onboarding), and then you can proceed as with a first time analysis: fast scan, start analysis process and check results.

### Configure the analysis

This option (see [Application analysis configuration](../analysis-config/)) directs you to the **Overview** panel where you can check the results of the initial Fast Scan process. Other panels are available allowing you to:

- view logs
- manage extensions that will be or have been used in the analysis process
- make advanced changes to the analysis configuration
- check transaction and function point information (available with appropriate CAST licenses only)
- configure security dataflow analyses (available with appropriate CAST licenses only)
- check snapshot configuration

![](analysis_config.jpg)

### Customize the results

This option (see [Customize the results](../../explore-results/customize/)) focuses on managing your application analysis results (the option is not available if an analysis has not successfully completed) and allows you to:

- hide specific "level 5" nodes from the results (this is useful if these nodes are technically irrelevant and are "polluting" the results).
- add annotations and/or tags in bulk
- generate automatic functional modules

![](customize.jpg)

### View Engineering Dashboard

Launches the [Engineering Dashboard](../../export/DASHBOARDS/Engineering+Dashboard+User+Guide) (available with appropriate CAST licenses only) allowing you to investigate the application from a quality perspective (based on the results of rules triggered during the analysis):

- application risk
- transaction risk
- structural flaws

![](ed.jpg)

This option is available only based on the current [user permissions](../../administer/settings/user-permissions/):

- if the user has the `Administrator` permission, the option will be available for all applications
- if the user has the `Application Owner` permission on the application, the option will be available
- if the application is assigned to a profile which includes any dashboard related permissions, the option will be available
- if the application is assigned to two or more profiles and only one profile includes dashboard related permissions, the option will be available

In addition, the option will NOT be available:

- when the application exists only via imported results (the option is not visible at all)
- where the application has not yet been analyzed (the option is visible but disabled)

### View Management Dashboard

Launches the [Management Dashboard](../../export/DASHBOARDS/Health+Dashboard+User+Guide) (available with appropriate CAST licenses only) designed to provide an "at-a-glance" summary of your software health in terms of robustness, security, efficiency, changeability, transferability and quality, including information about

- Accurate sizing metrics
- Heatmap to quickly find the highest risk
- Trending analysis to benchmark performance over time
- Fast facts on the evolution of your software

![](hd.jpg)

The option is available in exactly the same situations as described above for the [View Engineering Dashboard](#view-engineering-dashboard) option.

### Rename / delete

These two options provide quick access to options that are otherwise provided in the [admin settings](../../administer/settings). They both require a user login with the following [permissions](../../administer/settings/user-permissions), otherwise they are disabled:

- the `Admin` predefined profile 
- or the `Application Creator` predefined profile
- or a custom profile containing the `Application Owner` role and permission over the given application/domain (or all applications/domains)