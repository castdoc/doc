---
title: "Application analysis configuration - Extensions"
linkTitle: "Extensions page"
type: "docs"
no_list: true
weight: 30
---

***

## Overview

The **Extensions** page provides configuration options for the CAST Extensions used during the code analyses actioned by CAST Imaging:

![](extensions.jpg)

There are three tabs, each is explained below.

## Included

![](included.jpg)

Included extensions are the extensions that will be used when you run the next analysis for your application. These extensions are automatically determined by CAST Imaging as part of the fast scan process and will be installed for every single source code delivery. You can also:

- manually include additional extensions that have not been included automatically, if necessary using the [Available](#available) tab
- manually change the release of a specific extension

Immediately following a fast scan, this tab will be populated with the extensions CAST Imaging has identified as required to analyze the source code in your application. After an analysis has been run, this tab allows you to configure the extensions for the next analysis.

{{% alert color="info" %}}Extensions listed in this tab will respect the [Extensions Strategy](../../../administer/settings/extensions-strategy/) that is currently in force.{{% /alert %}}

Each "included" extension is represented by a rectangular item in the list, for example for the HTML5/JavaScript Analyzer:

![](extension_info.jpg)

| Item | Description |
|---|---|
| 1 | Name of the extension. This is a clickable link that will take you direct the extension in the CAST Extend public website. Note that when using [Extend local server](https://doc.castsoftware.com/export/EXTEND/CAST+Extend+local+server) (in offline or connected  mode), the clickable link will not be available. In addition, when the extension does not exist in the CAST Extend public website (for example  internal or custom extensions that have not been published to Extend) the link will produce a 404 page not found. |
| 2 | Indicates the extension category (**Product** / **User Community** / **Labs**) and the origin (**Discovered**, **Force Install**, **Shipped**) - see below. |
| 3 | Allows you to remove **Discovered** and **Force Installed** extensions, for example to maintain identical results between successive versions. Note that CAST Imaging will not "include" an extension that has been deleted. If you require the extension again, it must be manually included using the [Available](#available) tab. |
| 4 | Allows you to change the specific release of an extension - see [Change extension](#changing-extension-version) version. |
| 5 | Indicates that a newer release of the extension is available in CAST Extend (local or public). |

### Extension origin priority

CAST Imagng obtains details about which extensions to use and where to get them from, from several different sources - these are known as the extension "origin":

- extensions that are already installed
- extensions that have been discovered
- settings defined in [Extensions Strategy](../../../administer/settings/extensions-strategy/)

When an extension has multiple "origins", Console uses a priority system to determine which "origin" should be used to include the extension in a given Application version. The priority used is as follows:

- CUSTOM are overwritten by SHIPPED which are overwritten by DISCOVERED which are overwritten by FORCE_INSTALL

Where: 

- **CUSTOM** = Extensions that are manually "included" using the [Available](#available) tab interface.
- **SHIPPED** = Extensions that are shipped with CAST Imaging Core on the relevant node.
- **DISCOVERED** = Extensions that CAST Imaging automatically determines (during the fast scan process) must be installed.
- **FORCE_INSTALL** = Extensions selected by a user with the [ADMIN role](../../../administer/settings/user-permissions) for use during an Application analysis (see [Extensions Strategy](../../../administer/settings/extensions-strategy/force-install/)).

### Extension categories

#### Discovered

![](discovered.jpg)

Any extension that is flagged as Discovered has been automatically detected as "required" during the fast scan. I.e your source code has been examined and CAST Imaging has identified that a specific extension is required to correctly analyzed some of your code.

Note:

- If this "discovered" extension is not present on the target node used by the application (in %PROGRAMDATA%\CAST\CAST\Extensions), then depending on the [Extensions Strategy](../../../administer/settings/extensions-strategy/force-install/) in force (Version Range - Alpha/Beta/Funcrel/LTS - and Version number lock) CAST Imaging will download the most recent release of the extension from CAST Extend automatically during the Application analysis.

- If this "discovered" extension is already present on the target node (in %PROGRAMDATA%\CAST\CAST\Extensions) AND if it is also a "shipped" extension, then CAST Imaging will first check whether a newer release of the extension exists on CAST Extend, and depending on the [Extensions Strategy](../../../administer/settings/extensions-strategy/force-install/) in place (Version Range - Alpha/Beta/Funcrel/LTS - and Version number lock) it will download the most recent recent of the extension from CAST Extend automatically during the Application analysis.

- CAST Imaging will not, in general, auto install **User Community** or **Labs** extensions (any extension with an ID like `com.castsoftware.uc.nameoftheextension` or `com.castsoftware.labs.nameoftheextension`). There are some exceptions to this, for example, the following User Community extensions that provide "Universal Analyzer" technology to perform analyses may be installed if the relevant source code is discovered. This includes extensions such as:

    - [Perl](https://extend.castsoftware.com/#/extension?id=com.castsoftware.uc.Perl&version=latest)
    - [ColdFusion](https://extend.castsoftware.com/#/extension?id=com.castsoftware.uc.ColdFusion&version=latest)
    - [Natural-Adabas](https://extend.castsoftware.com/#/extension?id=com.castsoftware.uc.natural.adabas&version=latest)
    - [DataStage](https://extend.castsoftware.com/#/extension?id=com.castsoftware.uc.datastage&version=latest)
    - [XPDL](https://extend.castsoftware.com/#/extension?id=com.castsoftware.uc.xpdl&version=latest)
    - [INGRES](https://extend.castsoftware.com/#/extension?id=com.castsoftware.uc.ingres&version=latest)
    - [CA Ideal](https://extend.castsoftware.com/#/extension?id=com.castsoftware.uc.caideal&version=latest)
    - [RDL](https://extend.castsoftware.com/#/extension?id=com.castsoftware.uc.rdl&version=latest)
    - [Pacbase](https://extend.castsoftware.com/#/extension?id=com.castsoftware.uc.pacbase&version=latest)
    - [SQR](https://extend.castsoftware.com/#/extension?id=com.castsoftware.uc.sqr&version=latest)

#### Force Installed

![](force_installed.jpg)

Any extension that is flagged as Force Installed has been specifically selected by an Admin user for use during an Application analysis (see [Extensions Strategy](../../../administer/settings/extensions-strategy/force-install/)).

#### Manually included

These are extensions that you have decided to add yourself manually from the [Available](#available) tab. These extensions will be downloaded and stored on the Node used by the Application. These extensions are not "flagged" in any specific way, however, they could **additionally** be flagged as **Discovered** or **Force Installed**.

#### Product/User Community/Labs

An extension always falls into one of three categories, explained in [Contributor types](https://doc.castsoftware.com/export/EXTEND/Contributor+types):

Product:

![](product.jpg)

User Community:

![](uc.jpg)

Labs:

![](labs.jpg)

### Working with included extensions

Any changes you make, such as adding new extensions via the [Available](#available) tab, or changing the version number of an existing included extension, require action before the changes will be taken into account - a yellow banner will appear at the top of the screen with an option to run the extension installation action on its own without having to run an analysis, or to run an analysis (which includes the install extensions step):

![](banner.jpg)

#### Changing Extension release

You can change the extension version that will be used in the next Application analysis by clicking the extension version drop down and selecting the version you require:

![](change.jpg)

{{% alert color="info" %}}An Admin user may have implemented an [extensions Strategy](../../../administer/settings/extensions-strategy/force-install/) and therefore you may not be able to change the version of the extension, or the extension version you are looking for may not be available.{{% /alert %}}

## Available

![](available.jpg)

Available extensions are extensions that are not active for the current analysis, but can be included manually if you wish to.

The content of this panel come directly from CAST Extend, (i.e. the public website https://extend.castsoftware.com or your CAST Extend local server) and reflects the [extensions Strategy](../../../administer/settings/extensions-strategy/force-install/) in force.

You can add an unused extension or change the release of an extension that has already been used:

![](change.jpg)

![](change2.jpg)

## Installed

![](installed.jpg)

Installed extensions are the list of extensions that were installed and used for the most recent analysis. The screen is read only, and is a simple record for your own use.

## Filtering extensions

You can filter extensions so that it is easier to see exactly what is being used:

![](filters.jpg)

| Filter | Description |
|---|---|
| Discovered | Show only Discovered extensions |
| Force Installed | Show only extensions that are Force Installed. |
| Updates | Show only extensions where a newer version is available. |

## Using the Change Forecast tool

The Change Forecast tool is provided at https://extend.castsoftware.com/change-forecast/ to help you prepare for an update of a given extension. The tool provides the ability to choose two extension releases (i.e. the current extension you are using and the target extension you want to update to) and view the changes that have occurred between the two releases. Knowing what has changed enables you to understand the impacts of updating your extension release on your existing analysis results. Changes include updates such as:

- resolved issues
- new features
- other modifications

CAST Imaging has a direct link to the **Change Forecast** tool: click the "i" icon highlighted below:

![](change_forecast.jpg)

This icon will load the **Change Forecast** tool in your browser (external access to https://extend.castsoftware.com is required), with a list of the extensions that have updates available. This allows you to view the changes that have been added in the updated releases of the extensions:

![](change_forecast1.jpg)

## Troubleshooting

If you are facing errors during the "install extensions" step, it is possible that the folder used to store the extensions on the Node may be corrupt, for example:

```text
INF: 2022-09-17 20:28:14: Downloading extension 'com.castsoftware.typescript.1.10.1-funcrel'
ExtensionDownloder 2.0.0 c CAST 2019

Root element is missing.
ERR: 2022-09-17 20:28:21: Unable to download extension 'com.castsoftware.typescript.1.10.1-funcrel'. Return code from Extension Downloader: 1
ERR: 2022-09-17 20:28:21: Unable to download extensions
com.castsoftware.aip.node.jobs.steps.analysis.ExtensionDownloadException: Failed to download the extension 'com.castsoftware.typescript.1.10.1-funcrel'
```

The storage folder is located on the node in the following location by default (but may be located in a different place in your own environment):

```text
%PROGRAMDATA%\CAST\CAST\Extensions
```

To try to resolve the error, you can rename the "Extensions" folder (for example to "Extensions_old") and then create a brand new empty "Extensions" folder. Doing this will force CAST Imaging to download all the required extensions again - but this may impact overall performance of the analysis.



