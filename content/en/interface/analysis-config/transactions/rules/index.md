---
title: "Application analysis configuration - Transactions - Rules"
linkTitle: "Transactions - Rules page"
type: "docs"
no_list: false
weight: 30
---

***

## Overview

The **Rules page** shows detailed information in multiple sections about the **rules** (default and custom) used to identify Transaction entry points, Data entities, Transaction end points and excluded items:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128063.jpg)


## Legend
Rules are grouped into an expandable section according to the type of item they identify:

### Transaction entry points / end points

Lists the rules that define what is considered either a **transaction entry point** or an **end point**.

### Data entities

Lists the rules that define what is considered a **data entity**, for example SQL objects such as tables.

### Data entities - built in types

This section is divided into three sections:

#### Built-in types

Contains a **predefined list of object types** that are **ALWAYS** considered as Data entities (e.g.: SQL Tables etc.):

- these object types have predefined Function Point values that cannot be modified.
- it is not possible to create new, edit, or delete any entries in this list.
- object types that appear in this list are available as selection criteria items for custom Transaction Entry Point, Data Entity, End Point, and Excluded item rules - this allows you to define specific configuration rules that include the object types already listed as Built-in types for Data Entities.

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128023.jpg)

Note that where two metamodel type or category descriptions are the same, the name of the metamodel type or category is appended in brackets. For example:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128021.jpg)

#### Excluded table names regular expressions (case insensitive)

See [Working with Data entities - built-in types](#working-with-data-entities---built-in-types) below.

#### Table prefixes excluded from PK/FK analysis

See [Working with Data entities - built-in types](#working-with-data-entities---built-in-types) below.

#### Excluded items

Lists the rules that define the objects that will be **excluded** from the Transaction configuration.

#### Generic sets

Lists all the Generic Sets that have been created in [Free Definition mode](#working-in-free-definition-mode). Generic Sets define **a set of objects** and can be used in other rules.

## Upload/Download rule options

The **Upload** and **Download** options enable you to export/import Transaction Configuration rules as custom configurations:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128014.jpg)

### Upload

When the **Upload** button is clicked, you can choose an existing `.tccsetup` file (created with the legacy CAST Transaction Configuration Center tool, or downloaded from a previous release of CAST Console) to upload. The rules in the file will all be considered to be "**custom**" (and part of the "custom" package).

If you attempt to upload a `.tccsetup` file that contains **identical rules** to those that already exist in other packages, an error will occur preventing you from making the upload:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128012.jpg)

You can manually edit the `.tccsetup` file to change or remove conflicting rules if necessary.

### Download

When the **Download** option is clicked, you will be prompted to choose the base package you would like to export. The export will result in a `.tccsetup` file (an `XML` based file):

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128013.jpg)

#### Technical information

The download will contain the following items:

- all Entry Points, Data Entities, Excluded Items, and End Points. Their export will contain the following properties:
    - description
    - DET value
    - RET value
    - contribute value
    - the current Activation status is not exported
- all Generic Sets
- all table name prefixes that have to be excluded from PK/FK analysis
- all descriptions and regular expressions defining the table names to be excluded from Data Entities
- all downloaded configuration rules are considered to be "custom" (and part of the "custom" package), even if the package that has been downloaded is pre-defined (i.e. shipped out of the box)
- calibration rules for Entry Points, Data Entities, Excluded Items, and End Points, are exported regardless of their Activation status (enabled or disabled)
- if a set part of the definition of an Entry Point, Data Entity, Excluded Item, End Point, or Generic Set references another set through a member-of, an excluded-from, a caller-of, or a callee-of dependency, then the definition of the referenced set will be part of the exported items only if its package is the same one as the package being exported; if not, still the dependency will be exported, but not the definition of its referenced set.

## Table key

Each section in each table has similar column headings, as follows:

### Selection box

The check box enables you to select multiple rules and apply changes "en masse" - for example, you can use the **Activation slider** on multiple rules, rather than having to individually apply the change to each rule.

### Name

Name of the Rule.

### Package

The name of the Package the rule belongs to. Packages with names are predefined standard configuration files (`.tccsetup`) containing common rule (i.e. sets) configurations for specific technologies - these are provided "out-of-the box" via CAST Imaging Core or with the extension you have installed.

Packages marked as "**Custom**" are those containing **user-defined rules**.

### Objects

The total number of objects that the rule has identified during the analysis/snapshot.

### DET (Data Element Types)

Data Entities and Transaction End Points only:

CAST will set a default value for the **DET (Data Element Types)** of the objects detected by the rule. This is so that the real **Function Point value** for the objects can be determined by CAST using its internal algorithms.

- DET is set at **3** as default for Data Entities
- DET is set at **1** as default for Transaction End Points
- A value of **0** or **empty** is not authorized. Only positive integer values are allowed. The RET value cannot be greater than the DET value. 

### RET (Record Element Types)

Data Entities only:

CAST will set a default value for the **RET (Record Element Types)** of the objects detected by the rule. This is so that the real **Function Point value** for the objects can be determined by CAST using its internal algorithms.

- DET is set at **1** as default for Data Entities
- A value of **0** or **empty** is not authorized. Only positive integer values are allowed. The RET value cannot be greater than the DET value.

### Contribute

Transaction End Points only:

CAST will use either YES or NO for the Contribute value of the objects detected by the rule. This is so that the real **Function Point value** for the objects can be determined by CAST using its internal algorithms.

- If the **Contribute value** is set to **YES**, the objects identified by the rule will be then contributing to the computed Function Point value of the transaction, and each of the end points will contribute its DET value to the DET value of the transaction and 1 FTR (**File Type References**) to the FTR of the transaction.
- If the **Contribute value** is set to **NO**, then nothing is counted for these end points. These will lead to end the transaction with out any contribution in terms of Function Points.

### Updated

Rules flagged as updated, are those that have been changed (i.e. as part of an update or the installation of a new version of an extension). User-defined rules (those that are part of the "custom" package) will never be flagged as updated.

Note that all predefined rules are flagged as Updated when using Console for the first time.

### Technical

Only visible in the **Excluded Items** section:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128020.jpg)

This option enables you to designate any objects in the rule as **"technical objects"** in the next snapshot. To designate the objects captured by the rule as "technical" from the next snapshot, enable the slider (by default it is set to disabled). It is only possible to set custom rules to ENABLED - all predefined rules are always set to disabled:

- **DISABLED**: When disabled, objects which are Entry Points, Data Entities or End points are excluded i.e. the complete Data Function or Transactional Function that has these objects as entry points, data entities or end points will not considered at all for Function Point calculations. But if these objects are not entry points, data entities or end points but are part of Transactional Function, then they will have an impact on the status of the Transactional Function when the Automated Enhancement Point (AEP) measure is being used (the default).
- **ENABLED**: When enabled, objects which may be (but not necessarily) entry points, data entities and end points are considered as "technical objects" and they do not impact the status of Transactional Functions when the Automated Enhancement Point (AEP) measure is being used (the default). If the objects are entry points, data entities or end points the complete Data Function or Transactional Function which have these objects as entry points, data entities or end points will not be considered at all for Function Point calculations and the objects will be considered as "technical objects".

You can also enable the Technical option when you create a new custom rule:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128018.jpg)

### Activation

This slider allows you to activate or deactivate a specific rule. Deactivating a rule will cause any objects identified by the rule to be ignored in a subsequent snapshot, or when the [computing Transactions](Computing+Function+Points+and+Transactions.html). Note that all predefined rules and newly created custom rules are Active by default.

### View or Edit

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128074.jpg) (view) or ![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128050.jpg) (edit)

*   For a **predefined rule**, the "**view**" icon will be displayed. This allows you to view a read-only visualization of the selected rule. A new page will open showing the rule graphically. This can help understand how the rule functions.
*   For a **custom rule**, the "[Edit](#editing-custom-rules)" icon will be displayed. This allows you to edit the selected rule. A new page will open showing the rule graphically.

See [Free Definition](#working-in-free-definition-mode) below for more information.

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128059.jpg)

### Action menu

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128076.jpg)

This icon provides further options:

- [Check content](#checking-rule-content) > For all rules
- [Delete](#deleting-custom-rules) > For custom rules only

### Check all content

See [Checking rule content](#checking-rule-content) below.

###  Add

See [Creating new custom rules](#creating-new-custom-rules) below.

## Creating new custom rules

You can create new rules for any type by using the **Add** icon:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128062.jpg)

A new rule dialog will then be displayed. Required fields will depend on the type of rule you are creating:

Transaction entry point:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128068.jpg)

Data Entity:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128070.jpg)

Transaction end point:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128069.jpg)

Excluded item:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128019.jpg)

Generic set:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128064.jpg)

Click **Save** to complete the creation process. You will now need to further refine the new custom rule using the [Free Definition editor](#working-in-free-definition-mode) - this will enable you to configure **selection criteria** (object type, matching type etc.) so that the objects you require are picked up by the rule.

- New custom rules will be assigned to the "Custom" package:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128061.jpg)

- New custom rules will not be taken into account until a new snapshot is generated, or the [compute Function Points](Computing+Function+Points+and+Transactions.html) action is used.
- You can check the content of a new rule using the [Check Content option](#checking-rule-content).

## Editing custom rules

You can edit the basic criteria (i.e. name/attributes) used for a custom rule as follows - click the ![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128050.jpg) icon next to the rule:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128049.jpg)

A floating dialog box will be displayed enabling you to modify the basic criteria for the rule:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128067.jpg)

To edit more complex **selection criteria** use the [Free Definition editor](#working-in-free-definition-mode).

## Checking rule content

You can check the content (i.e. the objects captured) of a custom or predefined rule in various ways:

- To check the content for an **individual** custom or predefined rule:
    - click the ![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128076.jpg) icon next to the rule and select **Check Content**
    - click the tick icon on the rule itself when [Working in Free Definition mode](#working-in-free-definition-mode):

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128058.jpg)

*   To check the content of **all** custom or predefined rules in a specific section, click the ![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128060.jpg) button at the top of a section.

A floating dialog box will be displayed showing the objects that have been captured by the rule or rules:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128077.jpg)

### Name

Name of the object.

### Type

The type of object.

### Excluded

Indicates whether the object has been "captured" by an exclusion rule.

### Fullname

Fullname of the object.

## Deleting custom rules

Custom rules that you have created can be deleted by clicking the ![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128076.jpg) icon next to the rule and selecting **Delete**:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128075.jpg)

The custom rule will be removed and any objects identified only by this rule will no longer be included in Transaction / Function Point data when a new snapshot is generated or the [compute Function Points](Computing+Function+Points+and+Transactions.html) is run.

{{% alert color="info" %}} Note that it is not possible to delete predefined rules.{{% /alert %}}

## Working with Data entities - built-in types

The **Data entities - built-in types** section contains two customizable sub-sections to manage **excluded tables** and **table prefixes excluded from PK/FK analysis**. These sections are explained in more detail below.

### Excluded table names regular expressions (case insensitive)

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128024.jpg)

This section allows you to define criteria based on Regular Expressions that are used to match specific table names. Some predefined entries will also be listed in the table. All matching tables will NOT be considered Data Entities at all. The section will be pre-populated with rules pre-defined by CAST. These pre-defined rules target table names that should never be taken into account during the computation of the set of objects that will be identified as being **Data Entities**, for example tables containing the words "temp", "session", "error" etc. This is in line with the **OMG CISQ AFP Specifications**.

To add a new Regular Expression to define a criteria click **ADD,** enter the Description (to help you remember what the regular expression targets) and the Regular Expression in the dialog and click **UPDATE**:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128027.jpg)

- ONLY server side table names will be matched against the Regular Expression. All other object types are not taken into account.

To edit or delete an existing Regular Expression, use the relevant option in the table:

- **1 = Edit** existing prefix
- **2 = Delete** existing prefix

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128026.jpg)

Pre-defined CAST regular expressions that have been deleted can be restored by pressing the **RESET** button.

#### Other options:

##### Reset

Use this button to reset the list to the rules pre-defined by CAST. Any custom rules will be removed.

##### Check all content

This option will produce a list of the tables that are being excluded and not considered as Data Entities. The **Filtered By** column shows whether the table is being excluded due to a default rule ("**LOOKUP TABLE ALGORITHM**", see [OMG CISQ AFP Specifications for Data Entities](#omg-cisq-afp-specifications-for-data-entities) below) or by a default or a custom Regular Expression ("**REGULAR EXPRESSION**") as shown in the image below:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128025.jpg)

#### OMG CISQ AFP Specifications for Data Entities

By default, CAST will pre-populate the **Excluded table names** (case insensitive regular expressions) with a set of pre-defined rules to EXCLUDE certain objects from being considered as Data Entities. These pre-defined rules are in line with the **OMG CISQ AFP Specifications** for Data Entities.  
In addition, the **OMG CISQ AFP Specifications** dictate that other database tables (over and above the pre-defined rules discussed above) should NOT be considered as Data Entities. These objects are those with a lookup structure that match the following criteria:

- Have one primary key
- Optionally, have (only) one integer attribute to support order (a single integer attribute is allowed to support indexing and sorting the lookup data)
- Have no other database table with a cascade delete relation to it
- Have less than three text attributes or have a set of text attributes whose names match "name", "message", "type", "code", "description", "desc", or "label".

Objects that match all of the above criteria will NOT be considered as Data Entities, and are listed as having the "**LOOKUP TABLE ALGORITHM**" property shown in the **Filtered by** column of the objects list shown by **CHECK ALL CONTENT**. This behavior cannot be altered or overridden. This explains why some database tables are never displayed as Data Entities. This behavior can be altered using the **Activate look-up table filtering** option in [Application - Function Points - Settings](../../function-points/).

### Table prefixes excluded from PK/FK analysis

This section allows you to define a list of prefixes that are used to name your SQL tables. Any prefix on the list will be excluded when the Function Points computation algorithm attempts to detect Data Functions from SQL tables using Foreign key (FK) and Primary Key (PK) relationships. So if all or a majority of your tables use a naming convention that includes prefixes such as `T\_` and `DT\_` (for example), you need to add these prefixes to the list so that they are excluded and unrelated tables are not grouped together.

To add a new Table Prefix, click **ADD,** enter the prefix in the dialog and click **UPDATE**:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128030.jpg)

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128029.jpg)

To edit or delete an existing prefix, use the relevant option in the table:

- **1 = Edit** existing prefix
- **2 = Delete** existing prefix

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128028.jpg)

## Working in Free Definition mode

Free Definition mode is designed with two main purposes in mind:

- Provides a graphical visualization of how the rule functions - [read only mode](#free-definition---read-only-mode).
- Provides a method to edit custom rules graphically and configure selection criteria to capture specific objects - [edit mode](#free-definition---edit-mode)

### Accessing Free Definition mode

To access **Free Definition mode**, click the ![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128074.jpg) button in the rules panel next to a rule you want to view or edit.

### Free Definition - read-only mode

Read only mode (the default mode) is used for **predefined rules.** It provides a simple visualization of how the rule functions. Take for example this standard predefined rule which defines Transaction Entry Points for Java (`javax.swing`). Read-only mode is shown in the header bar since changes cannot be saved:


![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128057.jpg)

This shows that objects that will be considered as Transaction Entry Points will be:

- All **Java Class** objects calling any object in a **set of of objects** via an **Inherit** link down to a depth of **20**.
OR
- All **Java Interface** objects calling any object in a **set of objects** via a **RelyOn** link down to a depth of **1**.
OR
- All **Java Method** objects calling any object in a **set of objects** via an **Access** link down to a depth of **1**.

The called set of objects must be captured by the predefined rule "**Standard Entry Point - Java - javax.swing (GS)**", which matches any Java Class, Java Interface or Java Method that has a fullname like:

- `javax.swing.JPanel`
- `javax.swing.JFrame`
- `javax.swing.JWindow`
- `javax.swing.RootPane` 
- `javax.swing.JDesktopPane`
- `javax.swing.Box`

#### Zoom option

The Free Definition mode (whether in read-only or edit mode) offers a **zoom option** to help view the details of complex rules. Click the plus or minus buttons to access it:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128056.jpg)

#### Member-of and Excluded-from links

When a rule is more complex and stipulates a group of objects, either to be included (member-of) or excluded (excluded-from), then the Free Definition mode can show this using **arrows**:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128055.jpg)

### Free Definition - edit mode

Edit is only available for **custom rules**.

#### Adding and editing selection criteria

When a custom rule is created (see [Creating new custom rules](#creating-new-custom-rules)) it is only possible to configure the **basic rule criteria** such as the name and any attributes (such as DET/RET for a Data Entity). If you want to configure **more complex selection criteria** such as the **object type and match type**, you can use the **Free Definition edit mode**.

You can configure the following allowing a complex set of criteria to be used:

- One or multiple "**selection blocks**" linked by an **OR** operator
    - Include **sub-objects** of the target objects (or not)
    - Include **external** objects
- Within each "**selection block**" you can configure one or multiple "blocks" linked by AND / OR operators to capture objects based on the following match criteria:  
    - Match (using multiple operators) by object **Name**
    - Match (using multiple operators) by object **Full Name**
    - Match (using multiple operators) by object **Path**
    - Match (using multiple operators) by object **Type**
    - Match objects based on a "**member of**" grouping of **existing predefined/custom rules** - i.e. capturing an object using an existing predefined or custom rule
    - Match objects based on a "**excluded from**" grouping of **existing predefined/custom rules** - i.e. excluding an object using an existing predefined or custom rule

##### Adding a simple selection block

In the right hand panel, click the **New Selection Criteria** button to add a new selection block:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128048.jpg)

Now edit the block to choose if you would like sub-objects of any matched objects included:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128047.jpg)

External objects (e.g. third-party libraries etc.) are **included by default**.

A new selection block will then be added - you can add as many of these selection blocks as you require: they will be linked with an OR operator:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128046.jpg)

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128045.jpg)

Use the option button to configure the selection block. You can choose from the following matching criteria:

*   Match (using multiple operators) by object **Name**
*   Match (using multiple operators) by object **Full Name**
*   Match (using multiple operators) by object **Path**
*   Match (using multiple operators) by object **Type**
*   Match objects based on a "**member of**" grouping of **existing predefined/custom rules** - i.e. capturing an object using an existing predefined or custom rule
*   Match objects based on a "**excluded from**" grouping of **existing predefined/custom rules** - i.e. excluding an object using an existing predefined or custom rule

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128044.jpg)

In this example we have chosen to match objects based on **Property - Identification**, **type** with the = match operator:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128043.jpg)

You can select other match operators - depending on the matching criteria you selected, there may be more operators available:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128042.jpg)

You can also add additional matches - or remove them using the close icon:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128041.jpg)

Save your choices to update the selection criteria and view the updated selection block with your choices:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128043.jpg)

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128040.jpg)

##### Adding a Member of or Excluded from selection block

As previously, add your selection block and then choose the match criteria, this time selecting either **Member of** or **Excluded from** - i.e. capturing an object using an **existing predefined/custom rule**:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128039.jpg)

Choose the rule you want to use, and **Save** the configuration to update the selection block:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128038.jpg)

A **link** is used to depict the **Member of** or **Exclude from** matching criteria:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128037.jpg)

##### Removing criteria

You can remove criteria from a selection block, or you can remove the entire selection block from the rule:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128036.jpg)

##### Saving/discarding changes

When you have finished your modifications, remember to save the changes;

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128035.jpg)

Or discard them if they are not required:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128034.jpg)

Quitting edit mode without first saving any changes you have made will result in the following dialog:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128065.jpg)

#### Show only Application types

An analysis needs to have already been run for this option to function.

The **Show only Application types** option is available in **Edit mode** only. This option is particularly useful as it narrows down the choice of items when you are creating a rule. The total list of items that can be present in the Analysis schema is very long, so restricting the list to only those items relevant to the current Application can save time:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128051.jpg)

When selected (by default this option is not active), this option will **hide all items that are not used by the current Application.** For example, when editing a rule and adding in a **property**:

##### Option active

Only those types present in the application are available for selection in the drop down:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128033.jpg)

##### Option inactive (default position)

All possible types present in the application are available for selection in the drop down:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128032.jpg)

## Working with the legacy CAST Transaction Configuration Center (TCC)

A two way synchronization system is in place for the legacy **CAST Transaction Configuration Center (TCC)**:

### Rules created in CAST Transaction Configuration Center (TCC)

Any rules created under **Free Definition**, **By naming**, **By inheritance** and **By type** (in any category such as Entry Points, Data Entities etc.) will be automatically displayed in CAST Imaging when the **Save** button is clicked in **CAST Transaction Configuration Center (TCC)**. These rules will ALL be classed as "**custom**" rules in CAST Imaging and can be edited/viewed in **Free Definition mode**:

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128016.jpg)

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128017.jpg)

Refresh the Rules page in CAST Imaging to ensure that any rules that have recently been created in CAST Transaction Configuration Center are updated.

### Rules created in CAST Imaging

Any **custom rules created in CAST Imaging** will be visible in CAST Transaction Configuration Center as **Free Definition rules** in the relevant category (such as Entry Points, Data Entities etc.):

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128015.jpg)

![](https://doc-data.castsoftware.com/AIPCONSOLE/attachments/576128011/576128016.jpg)

Newly created rules in CAST Imaging will only be visible in CAST Transaction Configuration Center when the **live connection is refreshed**, for example, reconnecting, running compute etc.