---
title: "Application analysis configuration - Transactions - Overview"
linkTitle: "Transactions - Overview page"
type: "docs"
no_list: false
weight: 10
---

***

The **Overview page** shows general information:

![](overview.jpg)

- the **total number** of transactions identified in the application during the analysis
- the number of **complete/incomplete** transactions - the number of complete and incomplete transactions will equal the total number of transactions. A transaction is deemed to be **incomplete** when it does not reach an end-point or a data entity. These incomplete transactions are "empty Transactional Functions" with a Function Point value of 0.



