---
title: "Application analysis configuration - Transactions"
linkTitle: "Transactions page"
type: "docs"
no_list: false
weight: 40
---

***

The **Transaction page** contains information and configuration options with regard to transactions that CAST Imaging has identified during the analysis process:

![](transactions.jpg)

In programming, a **transaction** is generally known as a group of related actions that need to be performed as one. A simple example of a transaction is the requirement to update a database and at the same time produce an event for consumption by other services. CAST automatically identifies these transactions as part of the analysis process and results are display here in the **Transactions page**, and also visually:

Select the page you'd like to know more about from those listed below: