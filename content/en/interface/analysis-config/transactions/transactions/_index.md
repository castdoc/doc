---
title: "Application analysis configuration - Transactions - Transactions"
linkTitle: "Transactions - Transactions page"
type: "docs"
no_list: false
weight: 20
---

***

## Overview

The Transactions panel shows detailed information in two sections about the number of transactions and data entities identified in the application during the analysis:

![](transactions1.jpg)

## Transactions panel

This expandable section shows detailed information about each transaction identified in the application during the analysis:

![](trans1.jpg)

### Table description

#### Download

![](download.jpg) Export the contents of the table to a comma-separated values (CSV) file, which can be viewed using Microsoft Excel or similar.

#### Filter

![](filter.jpg) Filter the contents of the table, which can be particularly useful if you have a large number of items.

#### Hide incomplete transactions

Transactions marked in pink are deemed to be incomplete, i.e., they do not reach an end-point or a data entity. These incomplete transactions are "empty Transactional Functions" with a Function Point value of 0. You can see the total number of incomplete transactions, at a glance, in the top left corner:

![](incomplete1.jpg)

You can hide these incomplete transactions using the toggle available above the table. When toggled, only complete transactions are shown:

![](incomplete2.jpg)

#### Full Name	

Shows the full name of the object/file. Rolling the mouse pointer over the item will show the full name if it is too long to be displayed in the column.

#### Type

The Type of Transaction

#### Data Entities

The total number of data entities that the transaction reaches - these data entities will be displayed in detail in the Data Entities section.

#### End Points

The total number of End Points that the Transaction reaches.

#### Excluded

Indicates whether the transaction has been "captured" (i.e. identified) by an exclusion rule.

#### Action menu

- [Show rules](#show-rules)
- [Show details](#show-details)
- [View call graph](#view-call-graph)
- [View objects](#view-objects)
- [Check architecture](#check-architecture)

### Action menu options

#### Show rules

For each transaction listed in this section, you can view the rules that identified the item as a transaction:

![](rules1.jpg)

This will then show the rule or rules - you can view more details about the rule in the [Rules ](../rules/) section:

![](rules2.jpg)

#### Show details

For each transaction listed in this section, you can view the details of the transaction (data entities and end-points):

![](details1.jpg)
![](details2.jpg)

#### View call graph

For each transaction listed in this section, you can view the full call graph. See [Call Graph](call-graph/) for more information:

![](call-graph.jpg)

#### View objects

For each Transaction listed in this section, you can view all objects that are called by the selected transaction, together with their source code:

![](view-objects1.jpg)

Objects are displayed in a popup and source code (where available) can be viewed:

![](view-objects2.jpg)

| Item | Description |
|---|---|
| Download | This option will export the contents of the list to a comma-separated values (CSV) file, which can be viewed using Microsoft Excel or similar. |
| Name/Fullname | Name/Fullname of the object calling the selected Transaction. |
| Type | Type of object calling the selected Transaction. |
| Role | Role of the object in the selected Transaction:<br><br><ul><li>Entry Point (in case of merged Transaction, this role is displayed only for the Entry Point object of the merged root)</li><li>Data entities (those issued from user-defined rules will appear before those issued from built-in rules):</li><ul><li>Data Entity (setup)</li><li>Data Entity (built-in)/</li></ul><li>End Point</li><li>Objects from the reduced call graph</li><li>Objects from the full call graph</li></ul><br>Objects are listed in the order shown above. |
| Code | When the object is "generated" and is therefore outside the application boundary:<br><br>![](code1.jpg)<br><br>When the object is "external" and is therefore outside the application boundary:<br><br>![](code2.jpg) |

#### Check architecture

For each transaction listed in this section, you can check the objects contained within it against an [Architecture Model](../../config/architecture):

![](check1.jpg)

A new screen will open allowing you to select the existing Architecture Model you want to run the check with:

![](check2.jpg)

Any objects that form part of the transaction AND that violate the selected Architecture Model will be flagged in the results dialog box:

![](check3.jpg)

## Data Entities panel
This section shows detailed information about each Data Entity identified in the application during the analysis:

![](data_entities.jpg)

### Table description

#### Name/Fullname

Name/Fullname of the Data Entity (i.e. the file or object). 

#### Type

The Data Entity type.

#### Excluded

Indicates whether the transaction has been "captured" (i.e. identified) by an exclusion rule.

#### Download

![](download.jpg) This option will export the contents of the table to a comma-separated values (CSV) file, which can be viewed using Microsoft Excel or similar.

#### Filter

![](filter.jpg) This option allows you to filter the contents of the table, which can be particularly useful if you have a large number of items.

#### Action menu

- [Show rules](#show-rules-1)
- [View call graph](#view-call-graph-1)

### Action menu options

#### Show rules

For each Data Entity listed in this section, you can view the rules that identified the item as a Data Entity. See [above](#show-rules): this describes the identical feature for transactions.

#### View call graph

For each Data Entity listed in this section, you can view the full call graph. See [Call Graph](call-graph/) for more information.
