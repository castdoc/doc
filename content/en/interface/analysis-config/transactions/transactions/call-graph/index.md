---
title: "Application analysis configuration - Transactions - Call Graph"
linkTitle: "Transactions - Call Graph page"
type: "docs"
no_list: false
---

***

## Overview

CAST Imaging provides a graphical representation of an object's call links to enable you to quickly view how **Data Entities** and **Transactions** interact with each other. You can click one object and see a graphical representation of all of the other objects that it calls and it is called by:

![](call_graph.jpg)

## Accessing the Call Graph

The Call Graph can be accessed for a Transaction or Data Entity object via the [Transactions page](..) as follows:

![](access.jpg)

## Default vs. Tree view

When the Call Graph is opened, by default the view will display objects in a **"concentric"** style. Use the Tree View button to switch to a view that displays objects in a **hierarchical** style:

![](tree_view.jpg)

## Other features

### Tool tips on nodes (name, full name, type)

Role the mouse over the node to display the properties:

![](tips1.jpg)

You can also copy the properties to the clipboard using the **Copy** button:

![](tips2.jpg)

## Legend

A Legend is available (along the bottom of the Call Graph) to explain the different types of nodes that can exist:

![](legend1.jpg)

- Selecting a type on the legend will highlight all corresponding nodes in the graph by enlarging them.
- If a specific type of object is not present in the Call Graph, the Legend will disable the corresponding item in the Legend.
- Display of the legend can be toggled on and off using the switch:

![](legend2.jpg)

## Path highlighting

Selecting a node in the graph will automatically highlight the transaction from the Transaction start-point to end-point:

![](path.jpg)

## Toggle External objects	

By default, External Objects (objects that are external to the application boundary, for example predefined packages that are used in the application source code - displayed as gray dots in the call graph) are always displayed in the call graph. If you do not want to see them, they can be toggled on and off using the switch:

![](external.jpg)

{{% alert color="info"%}}Note that the **External Object** toggle switch is only available in **Tree View** mode.{{% /alert %}}

## SCC (Strongly Connected Components) objects

SCC objects are shown in the Call Graph as a single node:

- A legend item called **Strongly Connected Component** is used to highlight them.
- In the tooltip for SCC nodes, additional text denoting the number of objects in that component is displayed.
- On copying properties of a SCC node, properties of all the objects in the SCC are also copied.

![](scc.jpg)
