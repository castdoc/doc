---
title: "Overview - Identified Frameworks section"
linkTitle: "Identified Frameworks section"
type: "docs"
no_list: true
weight: 70
---

***

![](frameworks.jpg)

The Identified Frameworks section lists all the frameworks that have been detected by Console during the fast scan phase. The icon depicts how the identified framework will be analyzed, using the same legend as in the [Software Composition](../software-composition/) section:

![](frameworks2.jpg)

- **Product Extension** > an extension provided and supported by CAST
- **Community Extension** > an extension built by the CAST wider community (not supported by CAST)
- **No Known Extension** > this framework will not be taken into account since there is no extension available to support it