---
title: "Overview - Header section"
linkTitle: "Header section"
type: "docs"
no_list: true
weight: 10
---

***

![](header.jpg)

## Last action date & time

Date and time the any action was performed: scan, analysis etc.

## Status

Indicates the current state of the application:

- **Fast scan done** > The fast scan process is complete - i.e. the uploaded source code has been scanned to determine the contents: technologies/languages/frameworks. 
- **Deep analysis done** > The source code has been analyzed and results are ready. Note that if any missing dependencies are detected in your source code during the analysis (i.e. code that is calling another piece of code that cannot be found) a warning icon will be displayed. This should be fully investigated and corrected because it means that results may not be coherent. Clicking the warning triangle will direct you straight to the log file to see the missing dependencies alerts:

![](warning.jpg)

## Total files

Two values are displayed:

- Total number of files found during the fast scan process, including files that are not considered source code (i.e. image files)
- Total number of files that have been designated as source code (i.e. programming language types) during the fast scan process or during a **Refresh/Upload New** (see below). 

## Application size

Two values are displayed:

- Total number of files in all files found during the fast scan process, including files that are not considered source code
- Total number of Lines of Code (LoC) in the designated source code, found during the fast scan process or during a **Refresh/Upload New** (see below)

In addition, an indicator shows the "size" of the application as determined by CAST:

![](sizing.jpg)

## Alert

![](alert.jpg)

The alert icon indicates that the most recent job actioned on the current application contains one or more alerts (the number of alerts is indicated) that need to be looked at. Clicking the icon will display the alerts as a slide in panel on the right:

![](alert_slide.jpg)

## New scan	

Enables you to upload a new source code ZIP file or deliver new source code from a [folder](../../../../administer/settings/global-configuration/source-folder-location/). You can do this even if you have not yet run an analysis, i.e. when the previous fast scan has highlighted some deficiencies in the delivered code that you want to correct. 

- If you have already uploaded a source code **ZIP**, the button will display a dialog box enabling you to choose a new ZIP file. A new fast scan will then be automatically actioned on the new source code.

- If you have already delivered source code from a **source folder location**, the button will display a dialog box showing the location of the previous source folder. Ensure this existing source folder contains the new updated source code before proceeding.

### Technical information

Technically, the following things occur when a New Scan is triggered:

- any excluded files are discarded
- the current version is deleted and a new one is created with the same name, same title, same release date
- the new source code is scanned and data in the Overview page is refreshed using the scan results
- no associated snapshots are deleted