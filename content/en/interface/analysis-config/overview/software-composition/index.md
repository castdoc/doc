---
title: "Overview - Software Composition section"
linkTitle: "Software Composition section"
type: "docs"
no_list: true
weight: 30
---

***

![](soft-comp.jpg)

{{% alert color="info" %}}This section will be collapsed when a deep analysis has been actioned.{{% /alert %}}

## Overview

The Software Composition section provides details of the uploaded source code - note though that like the [Header](../header/) section, this only shows details of source code that has been designated as source code (i.e. programming language types) during the fast scan process or during a **Refresh/Upload new**. In other words, files that are not considered source code (i.e. image files for example) are not included in this data.

## Left panel

On the left an interactive chart depicts the content of the uploaded source code that has been designated as source code (i.e. programming language types), using three different measures:

- **Lines of code**: total lines of code per technology
- **File Count**: total number of files per technology
- **File Size**: total file size per technology, in bytes

Rolling the mouse pointer over the items will display more information:

![](soft-comp1.jpg)

## Right panel

On the right the same information is displayed in table format. In addition, a column shows how the identified technology will be analyzed, using:

- **Product Extension** > an extension provided and supported by CAST
- **Community Extension** > an extension built by the CAST wider community (not supported by CAST)
- **No Known Extension** > this technology will not be analyzed since there is no extension available to support it

![](soft-comp2.jpg)