---
title: "Overview - Architecture Preview section"
linkTitle: "Architecture Preview section"
type: "docs"
no_list: true
weight: 60
---

***

![](arch-preview.jpg)

A graphical representation of the delivered source code before an analysis is run - this is determined during the fast scan process. The section's primary aim is to help check the completeness of the source code that has been delivered.

Zoom controls are available in the upper right corner.