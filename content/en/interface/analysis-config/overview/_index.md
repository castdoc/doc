---
title: "Application analysis configuration - Overview"
linkTitle: "Overview page"
type: "docs"
no_list: false
weight: 10
---

***

The **Overview** page contains the results of the initial fast scan (a process to determine the application's technologies/languages/frameworks etc.) for newly onboarded applications:

![](overview.jpg)

When the fast scan phase is complete, users are directed automatically to this Overview page where the delivered source code can be inspected (size, structure etc.) for completeness, source code filters (exclusions) can be defined and any "additional options" such as automatic extension installation, activation of Security Dataflow analysis etc. can be activated. Following that, an analysis can be launched.

When an analysis has been completed and results are available in the [Viewer](../../viewer/), this Overview page remains available and additional information will be presented about the deep analysis results.

The main goal of this page is to allow source code to be inspected before it is sent for analysis to ensure that the correct items have been delivered and any unwanted code can be excluded.

Choose the section would like to know more about from those listed below.
