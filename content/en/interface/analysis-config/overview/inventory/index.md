---
title: "Overview - Complete File Inventory section"
linkTitle: "Complete File Inventory section"
type: "docs"
no_list: true
weight: 40
---

***

![](file-inventory.jpg)

{{% alert color="info" %}}This section will be collapsed when a deep analysis has been actioned.{{% /alert %}}

Provides a complete inventory of all the files delivered for analysis - note that this list may reflect files that were not sent for analysis (i.e. because they are unsupported, such as image files).
