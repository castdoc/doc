---
title: "Overview - Zip/ Folder Content section"
linkTitle: "Zip/ Folder Content section"
type: "docs"
no_list: true
weight: 20
---

***

![](content.jpg)

{{% alert color="info" %}}This section will be collapsed when a deep analysis has been actioned.{{% /alert %}}

## Overview

The Zip/Folder Content section provides details of the source code that has been uploaded (either via a ZIP file or via the source folder location) with the means to filter (i.e. exclude) certain files and folders.

## File Filter option

![](file_filter.jpg)

Allows you to:

- exclude specific files and/or folders using regular expression based exclusions
- enable/or disable various rules to exclude or include specific projects or components

### Expressions

A set of exclusion expressions will be predefined via the "default"  [Exclusion Template](../../../../administer/settings/global-configuration/exclusion-templates) which contains the most common items that should be excluded. Items excluded in this way are not sent for analysis. If you make any changes, use the **Update** button to apply them:

![](file_filter2.jpg)

You can add new custom filters as required: the pattern matching system uses glob patterns (see https://docs.oracle.com/javase/tutorial/essential/io/fileOps.html#glob for examples of how this system works). Enter an expression to match the folders/files you want to exclude and then click **Add** to add the expression to the list of excluded items:

![](file_filter3.jpg)

For example:

- `*.txt` will exclude all files with the extension .txt
- `tests/` will exclude any folders named tests and everything inside them - e.g. `root_folder/tests`, `root_folder/another_folder/tests`
- `*.Tests/` will exclude any folders whose name includes `.Test`s (for example `C:\Support\Delivery\Sample.Tests\sample\`)
- patterns starting with `/` will exclude starting only from the root folder. In other words, `/tests/` will exclude everything in the specific folder `root_folder/tests` but not `root_folder/another_folder/tests`

Take the following hypothetical example where an application has been delivered that contains the same `.SQL` file in four locations - this is not correct and three of them need excluding:

![](example1.jpg)

To exclude the `*.SQL` files located under the parent folder `JSP`, you could manually exclude them by unticking them in the UI, however (if you have multiple files to exclude) you can also define an expression to automatically do this for you e.g.:

```text
[J]*/**/*.sql
```

Where:

- `[J]*/` matches any folder in the root path starting with an uppercase `J` (in our case `JSP`) - if you have multiple folders in the root path, you can use the pipe character, for example `[J|e]*/` will match all folders starting with an uppercase `J` and all folders starting with a lower case `e`.
- `**/` traverses all nested sub folders of the matched folder beginning with `J`
- `/*.sql` matches any files named `<something>.sql`

Applying this expression automatically excludes the `.SQL` files located under the folder `JSP`:

![](example2.jpg)

- CAST highly recommends using the **Update Data** option if you configure exclusions - see below.
- Excluding a folder or files via the UI will automatically add the specific items to the **File Filter** list. For example, the folder `pageart` has been manually excluded using the UI and this is reflected in the **File Filter** dialog:

![](example3.jpg)

- If you have already actioned an analysis and you subsequently exclude files/folders, then a banner will be displayed in the **[Config](../../config)** screen to prompt you that the the configuration has changed and that you should run a new analysis in order to see the changes:

![](example4.jpg)


### Rules

This section enables you to configure the "exclusion" rules for specific projects identified during the source code delivery. When an exclusion rule is matched, then the project in question will be ignored. The aim of these rules is to avoid a situation where multiple projects (and therefore Analysis Units) are generated for a given piece of source code when more than one is not needed. If you are unsure, you should leave the default settings as they are and review them as a post analysis action item:

![](rules_dialog.jpg)

- for an Application's first version, all options are selected except **Exclude Maven Java projects when an Eclipse project also exists** and **Exclude Java Files project located inside other Java Files Project**.
- for an Application's subsequent analyses, exclusion rules are pre-selected according to the options chosen in the previous analysis.
- the option **Exclude all empty projects** refers to projects that do not have associated source code.
- the option **Exclude Test code** will exclude all folders named "test" that are discovered during the source code delivery

Items excluded by a rule will NOT be reflected in the **Zip Content/Folder Content** section but will still be sent for analysis and are then excluded during the analysis process. For example, the **Exclude Test Code** rule (when ticked) will exclude all folders (and their contents) called `test`, however, all folders called `test` will still be sent for analysis and will not be marked as excluded:

Folders called `test` are sent for analysis:

![](rules_dialog2.jpg)

Folders called `test` are only excluded during the analysis and appear as **"not analyzed"** in the **Analysis Report** section:

![](rules_dialog3.jpg)

## Update data	

The option should be run if you have added source code exclusions. It will run a scan on the existing uploaded source code to update the data in the following sections in the **Overview** page:

- Header
- Software Composition
- Architecture Preview
- Identified Frameworks

It is not mandatory to run the option, however, doing so can help you understand the impact of the source code exclusions you have added.

## Left panel

The delivered source code is depicted in tree format. This is interactive and selecting an item in the tree will update the middle and right hand panels. In addition, a filter can be set to exclude an item from the subsequent analysis process by clicking the icon shown in the image below. When the icon is shown in red, the entire selected folder and all files, sub folders and files will be excluded from the analysis (i.e. these items will NOT be sent for analysis):

![](left_panel.jpg)

- CAST highly recommends using the **Update Data** option if you configure exclusions - see above.
- Excluding a folder or files via the UI will automatically add the specific items to the File Filter list. For example, the folder `pageart` has been manually excluded using the UI and this is reflected in the **File Filter** dialog:

![](left_panel2.jpg)

- If you have already actioned an analysis and you subsequently exclude files/folders, then a banner will be displayed in the [Config](../../config) page to prompt you that the the configuration has changed and that you should run a new analysis in order to see the changes:

![](left_panel3.jpg)

## Middle panel

This panel depicts the content of an item selected in the left panel and divides them into categories as follows showing the total number of files:

- Files selected for deep analysis
- Files not selected for deep analysis

And then:

- Programming
- Documentation
- Data
- Prose
- Markup

Each item depicts, per technology type, the total number of files that will be sent for analysis and those that will not be sent for analysis - i.e. have been excluded through one of the exclusion methods:

![](middle_panel.jpg)

{{% alert color="info" %}}Items in the categories themselves are interactive and when clicked, will update the content in the right hand panel.{{% /alert %}}

## Right panel

The right panel displays the content of selections made in the left and middle panels and provides:

- a search mechanism
- a way to exclude individual files
- a way to view source code

All files that will be sent for analysis will be **ticked**, files that are NOT sent for analysis will be displayed with a **strikethrough**:

![](right_panel.jpg)

### Search mechanism

The search mechanism is a simple filter on the file name itself. For example, entering `auth` shows the following files:

![](right_panel2.jpg)

### Exclusion mechanism

Files that have already been excluded via a specific filter, or because the parent folder has been excluded using the icons in the left panel, will be displayed with a **strikethrough** and a **disabled unticked check box** as shown below (these items are NOT sent for analysis):

![](right_panel3.jpg)

To exclude individual files, untick the files - the file text will use **strikethrough**:

![](right_panel4.jpg)

Files that are excluded will contain roll over tooltip information to explain which pattern has excluded the file. For example, the following file was excluded in the right hand panel using the tick box and the pattern listed in the roll over tooltip will be added as a filter:

![](right_panel5.jpg)

This file was excluded by a filter called `bookdemo/`:

![](right_panel6.jpg)

- CAST highly recommends using the **Update Data** option if you configure exclusions - see above.
- Excluding a folder or files via the UI will automatically add the specific items to the **File Filter** list. For example, the folder `pageart` has been manually excluded using the UI and this is reflected in the **File Filter** dialog:

![](left_panel2.jpg)

- If you have already actioned an analysis and you subsequently exclude files/folders, then a banner will be displayed in the [Config](../../config) page to prompt you that the the configuration has changed and that you should run a new analysis in order to see the changes:

![](left_panel3.jpg)

### Code viewer

Selecting a file in the list will display its source code:

![](right_panel8.jpg)

