---
title: "Overview - Advanced Platform Configuration section"
linkTitle: "Advanced Platform Configuration section"
type: "docs"
no_list: true
weight: 100
---

***

![](advanced.jpg)

## Overview

This section is only displayed if you have configured **more than one** of either of the following:

- Multiple CAST Storage Service/PostgreSQL instances for analysis or measurement requirements - see [CSS and Measurement settings](../../../../administer/settings/global-configuration/css-measure/).
- Multiple nodes, i.e. you are running CAST Imaging in distributed/enterprise mode which allows for multiple Nodes to be configured

It allows you to select the specific **target CAST Storage Service/PostgreSQL instance** (for the database schemas required for the new application) OR the **target Node** (for deep analysis requirements). If you do not make a selection - i.e. you leave the options set to **"ANY"**, CAST Imaging will function in "load balancing" mode and will choose the CAST Storage Service/PostgreSQL or Node automatically.

## Technical notes

- If you have ALREADY run a deep analysis, the UI will prevent you from choosing a different CAST Storage Service instance or Node for any subsequent analysis related actions to ensure stability.
- **Load Balancing behaviour**, when ANY is selected:
    - **CAST Storage Service/PostgreSQL** > For the deep analysis step (result storage), the CAST Storage Service/PostgreSQL instance with the lowest number of CAST related schemas already stored on it will be used.
    - **Nodes** > For the deep analysis step, the least busy node running the same release of AIP Core as used for the initial fast scan will be selected.
- **Node manual selection** > only nodes running the same release of CAST Imaging Core as used for the initial fast scan of the onboarding process will be made available for selection - this is to prevent analysis errors. This may mean that it is not possible to choose a specific node.
- See also [Managing a node](../../../../administer/settings/applications/details/node/).