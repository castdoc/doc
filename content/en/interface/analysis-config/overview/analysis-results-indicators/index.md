---
title: "Overview - Analysis Results Indicators section"
linkTitle: "Analysis Results Indicators section"
type: "docs"
no_list: true
weight: 90
---

***

![](analysis-indicators.jpg)

{{% alert color="info" %}}This section is only displayed when an analysis has been completed.{{% /alert %}}

## Overview

This section displays a set of indicators for a given analysis/snapshot. These indicators are designed to provide basic information quickly so that the analysis/snapshot can be validated. The indicators are generated during the analysis/snapshot in a dedicated step and can be generated on-demand manually (see below):

![](indicators.jpg)

- Some indicators require just an analysis to be run and some require a snapshot as well. This is noted in the **Available Indicators** table below.
- The feature can be disabled if required - see **Configuring Indicators**.
- You can enable and disable individual indicators - see [Analysis Results Indicators](../../../../administer/settings/global-configuration/analysis-results-indicators/) in the Admin center.

## Content

### Categories filter

This drop down filters the indicators in the list by category. By default, all categories are displayed in the list:

![](categories.jpg)

### Update banner

![](banner.jpg)

This banner is displayed when CAST Imaging detects that a configuration change has been made and that your data should be updated. If the **Update** button is clicked, then a job will run to ensure that all Analysis Results Indicator data is correct. The triggers for this banner are identical to the triggers described in the section **Update Banner** in the [Config](../../config) section.

### Show all indicators

![](showall.jpg)

By default this toggle switch is disabled, which means only Indicators that have a positive value (displayed in the **Value** column) will be displayed. All indicators which have **N/A** in the **Value** column will be hidden. Enable the option to show all Indicators regardless of their value.

### Snapshot selector

![](snapshot.jpg)

Choose the snapshot you would like to view indicators for. By default the most recent snapshot for the Application will be displayed.

### Search

![](search.jpg)

Use this to search on the Indicator name.

### Download report

![](download-report.jpg)

This option enables you to download Microsoft Excel reports containing detailed information about the indicators in each category:

- If **All Categories** is selected (see option above) then a `ZIP` file will be downloaded containing the relevant `XLSX` files - one per category
- If a specific category is selected, then a `XLSX` file will be downloaded.

The file name used for the `ZIP` file and the `XLSX` files will contain a time stamp using the following format: `<year><month><day>_<hour><minute><second>`. The time stamp is generated when the **Download** button is clicked.

#### Excel file report contents

- The first sheet has information related to application name, snapshot name, snapshot date and category for which this report is generated. It also contains a summary for all the indicators belonging to that category i.e. name, description, thresholds, value, status, details, remedy action and justification.
- The indicator value is computed along with status which is a star rating based on the thresholds of that indicator.
- The details column contains the hyperlink to the details sheet of that indicator.

### Recompute indicators

This option enables you to recompute the indicators without needing to run a new analysis:

- The option is only available when the most recent analysis/snapshot is selected in the selector
- Justification entered for the indicators is retained after recomputation
- Any actions that modify analysis data will be reflected in the analysis/snapshot indicator results after recomputation.

{{% alert color="info" %}}This button is hidden if the Update Banner (described above) is displayed.{{% /alert %}}

### Indicator

See list of available indicators [below](#available-indicators) for more information.

### Value

The value generated for the current analysis. Can be a ratio or a percentage. For some indicators when only one analysis exists, the value may be N/A, for example:

- for indicators measuring the variation between two analyses/snapshots, the value will be `N/A` on the first analysis/snapshot
- for the **Large SCC count** indicator the value can be `N/A` when no option is set in the Analysis schema to count the large SCC, so the indicator can not be calculated. 

### Status

Status of the indicator - the more stars the better the results. Rolling the mouse over the stars will show the thresholds required to improve:

![](status.jpg)

### Justification

A free text field enabling you to enter a justification for the result. Free text is saved and is retained for the next analysis/snapshot that is generated. For example:

- Do you agree?
- Is the coverage as expected?

Use the icon to add a new justification explanation:

![](justification1.jpg)

And enter the justification in the pop-up:

![](justification2.jpg)

The icon changes to indicate a justification has been added:

![](justification3.jpg)

{{% alert color="info" %}}Justifications can only be edited for the most recent analysis/snapshot. If the selector is changed to a historic analysis/snapshot, the field becomes read-only.{{% /alert %}}

### Remedy action

The Remedy Action option provides a suggestion for how to improve the result in the next analysis/snapshot. Any links are clickable:

Remedies are displayed inline:

![](remedy.jpg)

If they are larger than the available space, rolling the mouse over them will display the full text in a popup:

![](remedy2.jpg)

Some indicators store results in a `.CSV` file - click this option to download the `.CSV` file. This can help you work out why a poor result has been produced, for example. `.CSV` files are generated and stored in the following location:

```text
\\share\common-data\snapshot-indicator\{appGuid}\{snapshotGuid}
```

## Clickable indicators

Some Indicators are clickable: clicking the link will take you to the relevant configuration page within the UI:

![](clickable.jpg)

## Available indicators

| Requires snapshot or analysis? | Indicator Name | Description | Stars/Threshold | Details in CSV file | Remedy Action |
|---|---|---|---|---|---|
| Snapshot/Analysis | Transactions with high number of objects | Lists transactions that are highly complex with a large number of objects. | &lt;4000 = 3 stars<br>&gt;=4000 and &lt; 4500 = 2 stars<br>&gt;=4500 and &lt; 5000 = 1 star |  | When possible reduce the complexity of this transaction by removing unnecessary elements. |
| Snapshot | Artifacts in transactions variation | Variation in percentage of artifacts involved in transactions compared to the previous snapshot. | &gt;=4500 and &lt; 5000 | 1 stars | Review  the source code delivery with regards to the transaction configuration.  If the value is not within the required range, then you should review  the transactions configuration, identify entry and end points, identify  missing links or provide justification on objects in technical side if  they are actually valid. If they are invalid, then take necessary  actions to include them as valid. |
| Snapshot | Objects not in Dashboard Service | Percentage of objects created in the Analysis Service that have not been transferred to the Dashboard Service.  | &gt;=4500 and &lt; 5000 | 1 stars | Check  the definition of user-defined modules to ensure that objects have not  been missed. If they are required in Dashboard Service, then add them to  a module. |
| Snapshot | Entry Points moved to technical part | Percentage of transactions that were considered as functional and that are now considered as technical. <br>Associated details contain the transactions that led to TF and that does not now.  | 3: =0%<br>2: &gt;0% and &lt;10%<br>1:&gt;=10% and &lt;=20% | List of transactions that were in functional part and that are now in technical part. | Check if links are broken during rescan due to change in source code or due to deleted artifacts. |
| Analysis only | Unexpected objects count | Percentage of objects related to third-party libraries or that are generated by analysis tools captured by modules.<br> <br>These objects are identified by finding any of the following tags in their comments:<br>generated by, generated on, generated code<br>copyright, (c)<br>MIT License License: MIT<br>Apache license, licensed to Apache software foundation<br>General Public License, GPL, GNU<br>creative commons | 3: =0%<br>2: &gt;0% and &lt;=5%<br>1: &gt;5% and &lt;=10%<br>- : &gt;10% | Associated details contain third-party objects and generated objects that have been captured by modules. | Review suspicious objects and decide if they must be excluded from analysis scope. |
| Snapshot | Unreferenced objects variation | Variation in number of not-referenced objects. <br>such as followings: <br>Classes<br>Code ( ASP) <br>Forms  <br>Functions <br>Includes<br>Interfaces <br>JSP pages <br>Modules<br>Files | 3:  &lt;3%<br>2: &gt;=3% and &lt;= 4%<br>1:&lt;5% | - | Justify the variation between 2 snapshots that could be due to added or deleted code which is unreferenced.<br>Another reason could be missing links or new links between objects when compared to previous version. |
| Snapshot | Incomplete Transaction Variation | Variation in percentage of transactions that are incomplete, compared to the previous snapshot. | 3:  &lt;=1% and &gt;=-1%<br>2: ( &gt;=-2% and &lt; -1%) Or (&gt;1% and &lt;=2%)<br>1: (&gt;-3% and &lt; -2%) Or ( &gt;2% and &lt;3%)  | - | Check  if entry points and/or end points have been added or removed in new  version of source code due to added/removed files or have been  added/removed manually.<br>Check if entry points and/or end points  have been added or removed in new version of source code due to  added/removed files or have been added/removed manually. You can also  check if transaction call graphs have been impacted by added or missing  links.  |
| Analysis only | Dynamic Links reviewed | Percentage of Dynamic Links that have been reviewed.  | 3:    &gt;=95%<br>2: &gt;=70  and  &lt;95%<br>1: &gt;=50% and &lt;70  | - | Check if Automatic Link Validator Extension is installed.<br>Review DL manually or define filtering rules to be applied at analysis time.   <br>To review manually see: Application - Config - Summary of Dynamic Links<br>To define rules: Application - Config - Dynamic Links Rules |
| Analysis only | Technologies not interacting with others | Technologies not interacting with others | 3:  =0<br>2: N/A<br>1: N/A | List of technologies not interacting with others | Review source code delivery, analysis configuration, and the cause for potential missing links. |
| Snapshot | Technology wise LOC - No Change | Percentage  of technologies involved in the application for which there is a lack  of variation in LoC compared to the previous version.<br>Associated details contains the list of technologies with no changes in LOC. | 3: =0%<br>2: &gt;0% and &lt; 20%<br>1: &gt;=20% and &lt;30% | The list of technologies with no changes in LOC. | Check  if it is normal to not have any variation in LoC for the technologies  involved in the application. This can denote an issue during the version  delivery. |
| Snapshot | Databases variation | Indicates whether the list of the databases that are accessed by the application has changed since the previous snapshot | 3 : databases are the same in the 2 snapshots<br>2 : N/A<br>1 : N/A<br>- : one/some databases have been added, removed, or renamed | Databases  that have been added and/or those that have been removed since the  previous snapshot; databases that have been renamed or whose host  instance has changed will be listed as added/deleted | Justify the root cause of databases' list change. |
| Snapshot | LoC variation (technology level) | Variation in the number of LoC by technology. | Below, the % retained for rating is the biggest one (in absolute value) among all technologies whose number of LoC has changed:<br>3 : &gt;=-20% and &lt;=20%<br>2 : (&gt;=-30% and &lt;-20%) or (&gt;20% and &lt;=30%)<br>1 : (&gt;=-40% and &lt;-30%) or (&gt;30% and &lt;=40%)<br>- : &lt;-40% or &gt;40%, or a technology that was used in the previous snapshot isn't used any more | Per  technology: whether it was added or removed, or is unchanged, along  with the number of LoC in the previous and current snapshots, and the  LoC variation value and percentage | Find the root cause of variation and justify.<br>1. Check if new files are added/deleted in new version of source code for a particular technology.<br>2.  Check if new version of Technology Extension is applied recently which  contains some enhancements and capturing more lines of code or vice  versa. |
| Snapshot | LoC variation (file level) | Variation in number of LoC for source files.<br>Associated details contain the source files that have been added or removed since previous version. | 3: &gt;=-5% and &lt;=5%<br>2: =5% or =-5%<br>1: (&gt;5% and &lt;10% )  or (&lt;-5% and &gt;-10% )  | Per  file: whether it was added or removed, or is unchanged, along with the  number of LoC in the previous and current snapshots, and the LoC  variation value and percentage | Review the Release Notes to track changes in analyzers.<br>Find the root cause of variation and justify.<br>Check if new files are added/deleted in new version of source code |
| Snapshot | Technologies variation | Variation in the number of technologies identified in the source code | 3 : technologies are the same in the 2 snapshots<br>2 : N/A<br>1 : N/A<br>- : some technologies have appeared and/or some other have disappeared | Technologies that have appeared or disappeared since the previous snapshot | Justify the change.<br>1. Check with Application team if they have added/removed a new technology.<br>2. Add appropriate extensions to analyze that newly added technology. |
| Analysis only | Large SCC count | Count of Strongly Connected Components with more than a certain number of objects | 3:  The 'GRAPH_SAVE_LARGEST_SCC_GROUP' option is set and no large SCC was  found during computation of the Transactions' call graphs<br>2: N/A<br>1: The 'GRAPH_SAVE_LARGEST_SCC_GROUP' option is not set or its value is 0 (it is not known whether large SCCs exist or not)<br>- : The 'GRAPH_SAVE_LARGEST_SCC_GROUP' option is set and there exists at least one large SCC<br>or<br>The 'GRAPH_SAVE_LARGEST_SCC_GROUP' option is set but its value is not an integer &gt;= 0 | The maximum number of objects allowed in an SCC before it is marked as large (the default value is 1000)<br>The maximum number of large SCCs whose objects can be saved for further investigation<br>How many SCCs had their objects saved during the last Function Point computation and what their sizes were | Review links and try to remove extra-links. For more details refers to Troubleshooting guides:<br>Impact of SCC<br>How to check<br>Reduce SCC |
| Snapshot | AFP variation | Variation in functional weight. <br>Associated details contain the TF and DF that appeared, disappeared, or that have been modified since the previous snapshot.  | 3: &gt;-5% and &lt;5%<br>2: =5% or =-5%<br>1: (&gt;5% and &lt;15% )  or (&lt;-5% and &gt;-15% )  | New TF, deleted TF, modified TF<br>New DF, deleted DF, modified DF | Check the transaction configuration rules. <br>Check the source code delivery for new source files and new technologies. <br>Check the source code delivery for missing source files and missing technologies.  |
| Snapshot | LoC per FP variation | Variation in FP density with regards to source code size | 3: &gt;-5% and &lt;5%<br>2: =5% or =-5%<br>1: (&gt;5% and &lt;15% )  or (&lt;-5% and &gt;-15% )  | - | Review source code delivery and transaction configuration. <br>Check if new components arrived in technical part of the application. |
| Snapshot | Recreated functions |  Percentage of functions that disappeared and that reappeared. <br>Associated details contain TF and DF that are recreated. | 3: &gt;=0% and &lt;=2%<br>2:&gt;2% and &lt; 4%<br>1:&gt;=4% and &lt;=5% | TF and DF that have been deleted and recreated.<br>Note:  this is different from Added/Deleted situation in the same snapshot  which is when the object ID of the TF/DF has changed because of change  in the path of the object for example. | Check the transaction configuration rules. <br>Check if objects have been deleted and then recreated. <br>Check if server part has been correctly delivered and analyzed.  |
| Snapshot | New technical code | Percentage of new objects that belong to the technical side of the application. <br>Associated details contain new objects that are assigned to the technical part of the application. | 3: &lt;5%* 3: &lt;5%<br>2: &gt;=5% and &lt; 10%<br>1: &gt;=10% and &lt;=15% | New objects that are in technical part. | Validate with application team if new code is functional or technical. <br>Review the transaction configuration accordingly. |
| Snapshot | Quality rules variation | Variation in number of quality rules. <br>Associated details contain the quality rules that have been added or removed since the previous snapshot. | 3:  &lt;=0<br>2: &gt;0 and &lt;5<br>1: &gt;=5 and &lt;10 | QR that have been added or removed since last snapshot | Validate  new rules that have been added to the Assessment Model. This could be  the consequence of an update (Imaging Core and/or extensions). <br>Review the Release Notes to track changes.<br>Check if new files are added/deleted in new version of source code due to which added/deleted metrics are appearing. |
| Snapshot | Added/Deleted excluded objects count | Count of objects that are excluded from quality rule violations and that present the "added/deleted syndrome". <br>Associated details contain objects excluded from violations and that are also "added/deleted".  | 3:  &lt;=0<br>2: &gt;0 and &lt;2<br>1: &gt;=2 and &lt;5 | objects that are excluded from rule violations and seen as deleted and re-added | Review the list of deleted/re-added objects and investigate the root cause.  |
| Snapshot | Violation variation per rule | Variation in number of violations per rule, when more than 5 rules with at least 10 added or removed violations.<br>Associated details contain the list of rules with too much variation in number of violations. | 3: &gt;-5% and &lt;5%<br>2: (&gt;=5% and &lt;10% ) or (&gt;=-10% and &lt;-5%)<br>1: (&gt;=10% and &lt;15%) Or ( &gt;-15% and &lt;=-10%) |  List of rules with too much variation in number of violations | Check if new files are added/deleted in new version of source code due to which violations are added/deleted.<br>Check  if new version of extension is applied during rescan and whether the  new version includes some fixes related to quality for which the  variation will be recorded. |
| Snapshot | AEFP/AEP variation | Variation in ratio between AEFP and AEP.  | 3:  &gt;-5% and &lt;5%<br>2: =5% or =-5%<br>1: (&gt;5% and &lt;15% )  or (&lt;-5% and &gt;-15% ) | - | Validate with application team the goal of the version.<br>Check if new components or technologies have been added or if existing components or technologies have been removed |
| Snapshot | Added/Deleted objects variation | Variation in number of objects that present the "added/deleted syndrome". Associated details contain "added/deleted" objects. | 3:  &lt;=15%<br>2: &gt;15% and &lt;20%<br>1: &gt;=20% and &lt;=25% | Objects that have been added and deleted since the previous snapshot. | Check if new files are added/deleted in new version of source code<br>Check if same objects are coming added/deleted that may be due to the change in directory path between 2 versions. |
| Snapshot | Changeability variation | Variation in grade for the<br>Changeability Business Criterion.  | 3: &gt;-5% and &lt;5%<br>2: =5% or =-5%<br>1: (&gt;=5% and &lt;15% ) or (&lt;-5% and &gt;-15% ) | N/A | Check if new source files have been delivered.<br>Check if many changes have been done in the version. <br>Justify  the variation that could be due to an increase or decrease in  violations for specific rules which could be because of code change or  because of missing links or because of module definition. |
| Snapshot | Robustness variation | Variation in grade for the Robustness Business Criterion.  | 3:  &gt;-5% and &lt;5%<br>2: =5% or =-5%<br>1: (&gt;=5% and &lt;15% )  or (&lt;-5% and &gt;-15% ) | N/A | Check if new source files have been delivered.<br>Check if many changes have been done in the version. <br>Justify  the variation that could be due to an increase or decrease in  violations for specific rules which could be because of code change or  because of missing links or because of module definition. |
| Snapshot | Transferability variation | Variation in grade for the Transferability Business Criterion.  | 3:  &gt;-5% and &lt;5%<br>2: =5% or =-5%<br>1: (&gt;=5% and &lt;15% )  or (&lt;-5% and &gt;-15% ) | N/A | Check if new source files have been delivered.<br>Check if many changes have been done in the version. <br>Justify  the variation that could be due to an increase or decrease in  violations for specific rules which could be because of code change or  because of missing links or because of module definition. |
| Snapshot | Efficiency variation | Variation in grade for the Performance Business Criterion.  | 3:  &gt;-5% and &lt;5%<br>2: =5% or =-5%<br>1: (&gt;=5% and &lt;10% )  or (&lt;-5% and &gt;-10% ) | N/A | Check if new source files have been delivered.<br>Check if many changes have been done in the version. <br>Justify  the variation that could be due to an increase or decrease in  violations for specific rules which could be because of code change or  because of missing links or because of module definition. |
| Snapshot | Security variation | Variation in grade for the Security Business Criterion.  | 3:  &gt;-5% and &lt;5%<br>2: =5% or =-5%<br>1: (&gt;=5% and &lt;10% )  or (&lt;-5% and &gt;-10% ) | N/A | Check if new source files have been delivered.<br>Check if many changes have been done in the version. <br>Justify  the variation that could be due to an increase or decrease in  violations for specific rules which could be because of code change or  because of missing links or because of module definition. |
| Snapshot | TQI variation | Variation in grade for the TQI index.  | 3:  &gt;-5% and &lt;5%<br>2: =5% or =-5%<br>1: (&gt;=5% and &lt;10% )  or (&lt;-5% and &gt;-10% ) | N/A | Check number of new technologies and new objects. <br>Check new rules. <br>Justify  the variation that could be due to an increase or decrease in  violations for specific rules which could be because of code change or  because of missing links or because of module definition. |
| Snapshot | Critical Violations variation | Variation in number of critical violations. <br>Associated details contain the critical violations that have been added or removed since the previous snapshot.  | 3:  &gt;-5% and &lt;5%<br>2: =5% or =-5%<br>1: (&gt;=5% and &lt;10% )  or (&lt;-5% and &gt;-10% )  | Critical violations added/removed | Check if new rules have been added to the Assessment Model. <br>Check if new source files have been delivered.<br>Check if new objects have been created.<br>Justify  the variation that could be due to an increase or decrease in  violations for specific rules which could be because of code change or  because of missing links or because of module definition. |
| Snapshot | Excluded objects variation | Variation in number of objects that are excluded from quality rule violations. <br>Associated details contain the objects that have been excluded from violations.  | 3:  &gt;-5% and &lt;5%<br>2: =5% or =-5%<br>1: (&gt;=5% and &lt;15% )  or (&lt;-5% and &gt;-15% )  | List of excluded objects | If the variance is +/-5%, then check the excluded objects list and verify if these are real candidates for exclusion.  |
| Analysis only | Artifacts in transactions | Percentage  of artifacts involved in transactions.  Associated details contain  artifacts that are not involved in any transactions. | 3: &gt;=50%<br>2: &lt;50% and &gt;=40%<br>1: &lt;40% and &gt;=30% | List of artifacts not involved in transactions | Review  the source code delivery with regards to the transaction  configuration. If percentage not within required range, then AIA needs  to review the transactions, identify entry/end points, identify missing  links or provide justification on objects in technical side if they are  actually valid. If invalid, then take necessary actions to include them  as valid. |
| Snapshot | Complete Transactions with only end point | Percentage  of complete transactions with no data entity (only end  points). Associated details contain the complete transactions that do  not access any data entity. | 3: &lt;=30%<br>2: &gt;30% and &lt;=50%<br>1: &gt;50% and &lt;=70% | List of complete transactions that do not access any data entity but only end points | Check  if data storage part has been delivered. If yes, then check if it is  expected to have transactions with no access to these data entities. |
| Analysis only | Data entities used by transactions | Percentage  of data entities accessed by transactions. Associated details contain  list of data entities not access by any transaction. | 3: &gt;=90%<br>2: &lt;90% and &gt;=70%<br>1: &lt;70% and &gt;=50% | List of data entities not accessed by any transaction | Review  the source code delivery with regards to the transaction  configuration. Check missing links and validate with application team if  it is normal these data entities are not accessed by any transactions. |
| Analysis only | Incomplete transactions | Percentage  of incomplete transactions (that do not access any data entity and end  point). Associated details contain transactions that does not access any  data entity or end point. | 3: &lt;= 10%<br>2: &lt;= 30% and &gt; 10%<br>1: &lt;= 40% and &gt; 30 % | List of incomplete transactions | Review  data entities and associated transaction configuration  rules. Investigate the incomplete transactions and justify if they are  valid incomplete or not. If possible, add custom end points to make the  incomplete transactions valid.  |
| Snapshot | AEFP/AEP (known as "Part of functional enhancement" in v. ≤ 1.18) | Percentage of AEP that are related to the functional part of the application.  | 3: &gt;= 70%<br>2: &gt;= 50% and &lt; 70%<br>1: &gt;= 30% and &lt; 50% | N/A | Validate with application team the goal of the version.<br>Check if new components or technologies have been added or if existing components or technologies have been removed. <br>Add new entry/end points to cover isolated objects in transaction.<br>Check the list of Technical Points and justify if those are actually technical objects.  |
| Analysis only | Programs/Classes in transactions | Percentage  of programs and classes involved in transactions. Associated details  contain programs and classes that are not involved in any transactions. | 3: &gt;=  50%<br>2: &gt;= 30% and &lt; 50%<br>1: &gt;=10% and &lt; 30% | List of containers not involved in transactions | Review the source code delivery with regards to the transaction configuration. <br>If the value is not within the required range, then you should review the transactions configuration,<br>Identify  entry and end points, identify missing links or provide justification  on objects in technical side if they are actually valid.<br>If they are invalid, then take necessary actions to include them as valid. |
| Snapshot | TF / DF weight Ratio | Ratio  between Transactional Functions weight and Data Functions  weight. Associated details contain DF that are not involved in any TF.  | 3: &gt;=2 and &lt;4<br>2: ( &gt;=1 and &lt;2 )  or (&gt;=4 and &lt;5)<br>1: (&gt;0 and &lt;1) or (&gt;=5 and &lt;6) | List of DF that are not used by any TF. | Check transaction configuration rules. Review incomplete transactions if any. Review DF that are not used by any TF.  |

## Technical information about Indicators

- When a snapshot is deleted all the indicators for the snapshot along with the generated `.CSV` files are deleted. In addition, the consolidation action launched when a snapshot is deleted will only deal with Dashboard schema indicators for the next two snapshots if they exist. During consolidation, any justification text is retained for each indicator.
- Indicators are also deleted when a version and an application are deleted.

## Logging information about Indicators

 If any indicators are skipped during the analysis, the log will include information about the reason the indicator has been skipped:

- `<enabled>false</enabled>`: Indicator xx skipped because it is disabled. 
- `<needFplicense>true</needFplicense>`: Indicator xxx Skipped because it needs the FP license.
- `<enhancementMeasure>EFP</enhancementMeasure>`: Indicator xx skipped because it needs enhancement measure EFP 
- `<enhancementMeasure>AEP</enhancementMeasure>`: Indicator xx skipped because it needs enhancement measure AEP