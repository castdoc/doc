---
title: "Overview - Analysis Reports section"
linkTitle: "Analysis Reports section"
type: "docs"
no_list: true
weight: 80
---

***

![](analysis-reports.jpg)

## Overview

This section is only displayed when an analysis has been actioned. It displays a report on analyzed/not analyzed files using data from the most recent analysis, therefore if you have changed the application source code since the most recent analysis (using the **New Scan** button - see [Header](../header/)) you will need to run a new analysis in order for the data in this section to be accurate.

## Column headings

###  File Extensions	

A list of file extensions found in the delivered source code. Extensions are grouped by technology/language - and the extension that is displayed by default (the primary extension) is the extension with the largest number of files in the delivered source code. Other related file extensions that are found will also be displayed alongside:

![](file_extensions.jpg)

{{% alert color="info" %}}For the **Mainframe - JCL** technology, the extension `.prc` is not considered part of JCL language (in the vast majority of cases, these files do not contain any JCL related code), so files with this extension will be ignored in the analysis report.{{% /alert %}}

### Technology/Language

Technology or language of the file as detected by CAST Imaging.

### Fully Analyzed

The total number of files of this type that were analyzed during the most recent analysis process. This number is taken directly from the storage schema in which the analysis results are stored, in other words this number reflects the number of files that were saved as part of the analysis process.

{{% alert color="info" %}}`.uax`/`.uaxdirectory` files resulting from a [CAST Database extraction](https://doc.castsoftware.com/export/DOCCOM/CAST+Database+Extractor) will be included in the **Fully Analyzed** count.{{% /alert %}}

### Not Analyzed

The total number of files of this type that were sent for analysis by CAST Console but were not analyzed during the most recent analysis process (e.g. they were not saved in the results):

![](not_analyzed.jpg)

Clicking the number will display the following dialog box, providing a list of all the unanalyzed files:

![](not_analyzed2.jpg)

A reason (where possible) will be given in the **Reason** column:

![](not_analyzed3.jpg)

These reasons correspond to **Project Exclusion Rules** set via the **File Filter** button (see [Content](../content/)):

![](not_analyzed4.jpg)

{{% alert color="info" %}}<ul><li>Files that require preprocessing such as <code>.castextraction</code> files resulting from a <a href="https://doc.castsoftware.com/export/DOCCOM/CAST+Database+Extractor">CAST Database extraction</a> or <strong>PDS Dump</strong> files will be included in the <strong>Not Analyzed</strong> count.</li><li>Alerts will be generated about unanalyzed files which can be customized where required.</li></ul>{{% /alert %}}

### View Logs

Clicking this icon will direct you straight to the **Run analysis** log files.

### Search icon

The search option allows you to filter for specific text. The search functions on the columns **File Extensions**, **Technology/Language** and **CAST Extensions**:

![](search_icon.jpg)

### Download icon

Click to download the report as a `.CSV` file. When opened in Microsoft Excel (or equivalent), two tabs are available:

- **Source Files Analysis Summary**: displays an overview, i.e. the same data available in the UI.
- **Details**: displays a list of files with their primary file extension, their technology, the list of CAST extensions that support the technology, and the status (excluded, analyzed, not analyzed).

## Behaviour

Clicking a number in the list will open a popup with more details about the files:

![](discovered.jpg)

To make results easy to use, some files are ignored and are not listed in this report:

- all files with extensions that are not associated directly to a programming language (all resources or data languages for example, or project files like `xml`, `http` or `json`).
- all files with patterns such as `.git`, `.svn`, `node-modules`, `org-eclipse`, `CCAU\.abap`, `IP\.abap`
- special files like `package-info.java`, `*CT.abap`, `*CP.abap`, `hh`, `h++`, `hpp`, `hcc`, `h`, `hxx`, `ph`
- files resulting from CAST Database extractions such as `.castextraction` or `.uaxdirectory`, because they are not directly associated with a language or extension.