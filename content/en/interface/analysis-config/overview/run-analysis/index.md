---
title: "Overview - Run Analysis section"
linkTitle: "Run Analysis section"
type: "docs"
no_list: true
weight: 110
---

***

![](run_analysis.jpg)

## Overview

This section provides the following:

- Information about the state of the source code
- Allows you to start an analysis
- Provides an analysis estimation time in hours and minutes for the analysis action and the result generation

## Content

### Information about the state of the source code

Information about the readiness of the delivered source code for analysis is provided based on the initial fast scan:

#### All clear

If no "issues" are found then the "all clear" is given:

![](run_analysis.jpg)

#### All clear but cannot access CAST Imaging/CAST Dashboards services

If no "issues" are found, but CAST Imaging/CAST Dashboards services are either not configured or not available, the results upload will not occur:

![](info2.jpg)

#### Issues found

If issues are found, then a warning is given with an explanation. In this situation, a warning does not mean that the analysis cannot proceed, however, coherent results may not be produced. For example:

- A warning that the delivered source code has links from JSP to Java, however, no Java source has been delivered:

![](info3.jpg)

- A warning that the delivered source code contains files that are encoded in a format other than UTF-8. Files that do not use UTF-8 encoding can cause issues for some CAST analyzers and may even cause the analyzer to crash, as such this warning invites you to convert the non UTF-8 files into UTF-8. CAST also provides a breakdown of the technologies which contain non UTF-8 by clicking the link highlighted below (a popup is displayed containing the breakdown):

![](info4.jpg)

![](info5.jpg)

- A warning that no supported technology has been found in the delivered source code. There may be a user community extension available that covers this technology. The analysis is not blocked in this situation:

![](info6-1.jpg)

#### Analysis complete

When an analysis has been run, this panel will show:

- the previous analysis duration time
- whether any missing dependencies were detected in your source code during the analysis (i.e. code that is calling another piece of code that cannot be found): a yellow warning icon will be displayed if this is the case. This should be fully investigated and corrected because it means that results may not be coherent. Clicking the warning triangle will direct you straight to the log file to see the missing dependencies alerts.

![](info6.jpg)
![](info7.jpg)

- a failed analysis, suggesting log files are checked before clicking Resume Analysis:

![](info8.jpg)

### Run Analysis

Click the **Run Analysis** button to start the deep analysis process. A popup will then be displayed:

![](run_anal.jpg)

When an analysis is started, a full backup of the onboarding details (e.g. delivered source code and any exclusions that have been set) and is created (in ZIP format) and is stored in the following location (see below). This is so that any manually or automatically (via a filter) excluded folders/files can be removed before the analysis is started. When the analysis action is complete, any excluded files/folders are put back in the original location (ZIP file unzip location or source code folder location):

```text
\\shared\\common-data\backup\source_folder_backups
```

### Deep analysis estimation time

The deep analysis estimation time is provided in hours and minutes and is based on anonymous statistical data that has been collected by CAST using the [Allow CAST to automatically collect anonymous statistical data](../../../../administer/settings/system-settings/extend/) option.

{{% alert color="info" %}}<ul><li>This estimation is only valid for the analysis action and does not include any other actions that may have been enabled.</li><li>When a previous action has been run, "Last execution time" will be displayed instead of an estimate: this is the duration of the last successful job that includes the analysis action.</li></ul>{{% /alert%}}

### Advanced configuration

{{% alert color="info" %}}Enabled only when an initial deep analysis has been completed.{{% /alert %}}

![](advanced_config.jpg)

This option allows you to control what steps in the analysis process are actioned and should only be used if you know what you want to achieve:

![](advanced_config1.jpg)

- Semantic analysis = deep analysis
- Compute structural flaws = generate a snapshot for Dashboards
- Generate views = make results available

## Resuming interrupted jobs

Should your job be interrupted for whatever reason (network issue, issue on the Node etc.), CAST Imaging is able to resume the job from the same point or a previous point. Take for example a job that has been interrupted in the **Install** step:

![](resume1.jpg)

Returning to the Overview page, a **Resume** button will be displayed in place of **Run Analysis**:

![](resume2.jpg)

### Technical details for resume functionality

For each step listed below, CAST Imaging will attempt to resume either from the same step or a previous step:

- <span style="color:green">resume from the same step it failed or was stopped</span>
- <span style="color:orange">resume from a previous step</span>

E.g.:

- **Fast Scan**
	- <span style="color:green">Unzipping source</span>
    - <span style="color:green">Initialize fast scan </span>
	- <span style="color:green">Content discovery</span>
- **Install**
    - <span style="color:green">Exclude files</span>
    - <span style="color:green">Create application schemas</span> 
    - <span style="color:orange">Set up Management database - resume from 'Create application schemas'</span> 
    - <span style="color:orange">Declare application in Management database - resume from 'Create application schemas'</span> 
    - <span style="color:green">Install extensions</span>    
- **Configure**
    - <span style="color:orange">Creating package from source - refresh onboarding, Prepare Version and resume from 'Content discovery'</span>
    - <span style="color:orange">Attaching package to version - refresh onboarding, Prepare Version and resume from 'Content discovery'</span>
    - <span style="color:orange">Delivering version - refresh onboarding, Prepare Version and resume from 'Content discovery'</span>
    - <span style="color:orange">Accepting Version - refresh onboarding, Prepare Version and resume from 'Content discovery'</span>
    - <span style="color:green">Set as current version</span>  
- **Analysis** 
    - <span style="color:green">Run analysis</span>   
    - <span style="color:green">Prepare analysis data</span>   
- **Upload**  
    - <span style="color:green">Create snapshot</span>    
    - <span style="color:green">Generate snapshot indicators</span>    
    - <span style="color:green">Publish to Health Dashboard</span>  
    - <span style="color:green">Upload to Viewer</span>

### Limitations of resume functionality

The resume functionality is available based on the status of the last executed job, therefore if the analysis has been stopped manually:

- and a **new extension is added (but not installed)**, in order to continue the analysis the Resume button should be used so that the "update extension" step is actioned before the analysis step is resumed
- and a **new extension is installed**, when the install extension action is complete the Resume button will be replaced by Run analysis.