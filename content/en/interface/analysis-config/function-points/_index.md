---
title: "Application analysis configuration - Function Points"
linkTitle: "Functions Points page"
type: "docs"
no_list: false
weight: 50
---

***

The **Function Points page** contains information and configuration options with regard to Function Points that CAST Imaging has identified during the analysis process:

![alt text](fp.jpg)

Every piece of developed or implemented software is designed to accomplish a defined set of business functions. Function point analysis involves using a standard metric for determining the overall complexity and size of each application within a system. A **function point** (FP) is therefore the standard unit of measurement to define what business functionality is being provided to the actual user.

Function point calculations are used to determine the cost (dollars or hours) for a single unit. They can be broken into two main types of functions:

- **Data Functions** – Internal Logic or External Interface File
- **Transactional Functions** – External Input, External Output, or External Inquiry

Individual units are categorized for scoring purposes to provide a real-time value for the system as a whole based on the size and complexity of each application. This creates a standardized approach to measuring team productivity, risk, and system complexity without the need for evaluating each line of written code.

{{% alert color="info"%}}If your [license](../../../administer/settings/license/) does not grant you the right to count Function Points, a banner message will be displayed explaining this situation:<br><br>![](banner.jpg){{% /alert %}}

Select the page you'd like to know more about from those listed below: