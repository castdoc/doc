---
title: "Application analysis configuration - Function Points - Overview"
linkTitle: "Function Points - Overview page"
type: "docs"
no_list: false
weight: 10
---

***

The **Overview page** shows general information about the function points in your application:

![](overview.jpg)

## Automated Function Points (AFP)

The total number of **Automated Function Points (AFP)** identified in the application during the analysis, divided into:

- the number of AFP for Transactional Functions (TF)
- the number of AFP for Data Functions (DF)

The number of AFPs for TFs and DFs will equal the total number of AFPs.

## Automated Enhancement Points (AEP)	

The total number of **Automated Enhancement Points (AEP)** identified in the application during the analysis if the AEP measure is active (see [Enhancement Measure](../../config/enhance-measure)), divided into:

- the number of Automated Enhancement Function Points (AEFP)
- the number of Automated Enhancement Technical Points (AETP)

The number of AEFP + AETP will equal the total number of AEP.

## Transactional Functions (AFP)	

An overview of total number of AFP transactional functions, with:

- Number of computed FP
- And either one of the following depending in the value chosen for the FP value for empty Transactional functions setting/
    - Number of Assessed FP. Assessed ratio is also displayed along with assessed FP i.e number of objects per DET
    - Number of Default FP

![](AFP.jpg)   

This panel is only shown when the FP value for empty Transactional functions setting (see [Settings](../settings)) is set to either:
- **ASSESS** and a new analysis is run
- or a specific **Default FP value** and a new analysis is run

![](AFP2.jpg)