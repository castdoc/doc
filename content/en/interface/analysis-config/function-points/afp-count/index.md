---
title: "Application analysis configuration - Function Points - AFP Count"
linkTitle: "Function Points - AFP Count page"
type: "docs"
no_list: false
weight: 20
---

***

## Overview

The **AFP Count** panel enables you to:

- view the default Function Point data for Transactional Functions and Data Functions as produced in the most recent analysis
- calibrate the default results if required:
    - change the type (ILF, EIF, EI, EO, EQ) and the value of each Data Function or Transactional Function that has been identified via the pre-defined or custom rules
    - delete, merge or unmerge objects
- view the calibrated functional size overall, and separately for Transactional Functions and Data Functions
- view the number of Empty Transactional Functions (highlighted in pink):

![](afp-count.jpg)

## Functional Size information

Functional size is displayed as follows:

- overall (total of Transactional Functions and Data Functions)
- separately for Transactional Functions and Data Functions

This data is updated each time the Compute option is actioned, therefore these values may differ from what is stored in the latest analysis:

![](fs1.jpg)

The info button displays the following message to explain the functional size information:

![](fs2.jpg)

## Transactional Function and Data Functions sections (expanded)

Both the Transactional Functions and Data Functions sections are identical, just showing different information:

![](table.jpg)

### Status
	
Indicates whether an item has been:

- Ignored ![](ignored.jpg)
- Deleted ![](deleted.jpg)

See below for more information about how to do this.

### Group	
Indicates whether an item has been:

- Merged ![](merged.jpg)

Clicking the icon will show information about the objects that have been merged together:

**Transactional Functions**

![](tf.jpg)

**Data Functions**

In addition to the merged elements, information is displayed about the details of the merge - i.e. the Data Entities that form the merge:

![](df.jpg)

The **Status** column shows:

- **Main object** - the main Data Entity in the merge
- **Detail** - a detail object (for e.g. a Data Entity that is part of the Data Function because of a Foreign Key - Primary Key dependency)
- **Merged object** - a merged Data Function, main Data Entity, or detail object

If several data elements have the same name and status, each of these objects will result in a distinct row in the details list; for example, 3 rows are shown for `DBDCOMP1` because three underlying distinct Data Entities exist:

![](group.jpg)

### Name

Name of the item.

### Full Name

Shows the full name of the object/file. Rolling the mouse pointer over the item will show the full name if it is too long to be displayed in the column:

![](full_name.jpg)

### Type

Indicates how the CAST algorithm computed the object during the analysis (determines the computed Function Point value of an object). The following can be displayed:

- **Transactional Functions** > **EI** (External Inputs), **EO** (External Outputs), **EQ** (External Inquiries) - or a mix of the above (e.g.: **EO_EQ**).
- **Data Functions** > **EIF** (External Interface Files), **ILF** (Internal Logical File)

### DET

Number of Data Element Types (a non-repetitive field in an ILF) for the object in question.

### RET	

Number of Record Element Types (a subgroup of data elements inside a logical file) for the object in question. Only displayed in the **Data Functions** section.

### FTR	

Number of File Type References (a file referenced by a transaction) for the object in question. Only displayed in the Transactional Functions section.

### FP	
Displays the Function Point value of the object in question - in other words the value determined by the CAST algorithm during the analysis.

### View	

![](view.jpg) Click to view the details of the transactional function (data function, end-points, scope and contribution):

![](view2.jpg)

### Edit
	
![](edit.jpg) Enables you to edit the selected object to change:

- the Type
- the Function Point value

An edit dialog will be displayed enabling you to edit these items. Click Save to complete the edit process:

![](edit2.jpg)

Next time you use the compute Function Points option or run an analysis, the new value will be used and the Function Point size of the object may change as a result.

### Action menu

![](action.jpg)

#### Ignore

In some circumstances, an object may not be relevant. If this is the case, and you no longer wish this object to be included in future Function Point calculations, then you can use this option to ignore the object.

When an object or objects are marked as ignored, the object/objects will still appear in the CAST dashboards for any future snapshot that is generated but the object/objects FP value will be set to 0 and will therefore no longer contribute to the total Function Point value for a given Application/Module etc.

#### Unignore

If you have ignored an object, you may wish to reverse this decision. This is possible using the this option.

When an object or objects is marked as unignored, the object/objects' Computed FP value will be recalculated and will once again contribute to the total Function Point value for a given Application/Module etc.

#### Delete

In some circumstances, an object may not be relevant. If this is the case, and you no longer wish this object to be included in future Function Point calculations, then you can use this option to delete the object.

When an object or objects is marked as deleted, the object/objects will no longer appear in the CAST dashboards for any future snapshot that is generated and the object/objects FP value will no longer contribute to the total Function Point value for a given Application/Module etc. Note that the object is not completely deleted however and will still be visible in previous snapshots.

#### Undelete

If you have deleted an object, you may wish to reverse this decision. This is possible using this option.

When an object or objects is marked as undeleted, the object/objects will now, once again, appear in the CAST dashboards for any future snapshot that is generated and the object's/objects' FP value will now, once again, contribute to the total Function Point value for a given Application/Module etc.

#### Unmerge

Available for unmerging objects that you may have [merged](#merging-objects).

### Enable selection / Merge

![](options.jpg)

- Enable selection option: allows you to select items in the list, for example for merging.
- Merge: See [Merging objects](#merging-objects) below.

### Export to CSV

![](csv.jpg)

This option will export the contents of the table to a comma-separated values (CSV) file, which can be viewed using Microsoft Excel or similar.

## Merging objects

If you wish to group multiple objects together, you can do so using the merge function. To do so, first enable selection for the objects you require:

![](merge1.jpg)

Tick boxes will then appear. Tick the objects you would like to merge:

![](merge2.jpg)

Click the **Merge** button:

![](merge3.jpg)

You will then be asked to choose the root object for the merge - only the root object will be visible after a merge. Click **OK** to confirm the root object:

![](merge4.jpg)

The merged root object will now be displayed with a merge icon in the **Group** column and any non-root objects will no longer be visible:

![](merge5.jpg)


Clicking the icon will show information about the objects that have been merged together:

![alt text](merge6.jpg)

### How are Function Point, DET, FTR and RET values calculated when multiple transactional functions or data functions are merged?

When you merge multiple transactional functions or data functions, you will be requested to choose a "root" item (as explained above). The values of the root item will be used to represent the block of transactional functions or data functions immediately after you have completed the merge. However, to ensure that the "correct" values (i.e. the "sum value" of all merged items) are taken into account going forward, you will need to use the compute Function Points options (note that the "sum value" is not exactly equal to the sum of all items in all cases since some columns/tables could be identical and will therefore count as 1).
