---
title: "Application analysis configuration - Function Points - Enhancement"
linkTitle: "Function Points - Enhancement page"
type: "docs"
no_list: false
weight: 40
---

***

## Overview

The **Enhancement page** enables you to view **Enhancement Size** data about your Application. This data is derived from the Enhancement measure (from CISQ/OMG) used during the analysis process (see [Enhancement Measure](../../config/enhance-measure)). You can find out more about the two Enhancement measures that are available in [CAST OMG-compliant Automated Function Points](https://doc.castsoftware.com/export/AIPCORE/CAST+OMG-compliant+Automated+Function+Points).

![](enhancement.jpg)

## 1 - Selector 	

This selector indicates which snapshot is currently being displayed. If you have multiple snapshots, you can choose the one you require using the drop down:

![](selector.jpg)

{{% alert color="info"%}}A snapshot is generated automatically as part of an analysis.{{% /alert %}}

## 2 - Total	

Total number of **Automated Enhancement Points (AEP)** or **Enhancement Function Points (EFP)** in the current snapshot. When the **AEP measure** is active, this figure is the combined total of Functional (AEFP) and Technical (AETP) points. See also [Enhancement Measure](../../config/enhance-measure/).

## 3 - Excluded Objects

![](excluded1.jpg)

Displays the total number of objects:

- that will be excluded from the next snapshot
- or that were excluded from the previous snapshot

Clicking the number will display a panel listing the excluded objects. This also distinguishes (in the **Processed in snapshot** column) between the objects that were excluded from the previous snapshot (YES) and those that will be excluded in the next snapshot (NO):

![](excluded2.jpg)

## 4 - Recompute Checksums

![](checksums.jpg)

This option can be used when you have excluded objects and you want the data showing in Console to be up-to-date. This process will recompute all the checksum values for all objects, in order to remove the excluded items from the checksum of the transactions. This will also recompute the metrics for AEP and EFP because the status of the transactions may change following a re-computation of the checksum and can therefore take some time to complete.

Recompute checksums is actioned as part of the analysis process during the "compute function point" step.

## 5 - Shared Counter Exclusion

![](shared1.jpg)

This option allows you to view the list of objects that are "shared" for a given transactional function. When the icon is pressed a new window is displayed with no objects listed:

![](shared2.jpg)

Enter a value in the text field in the top left to change the filter - for example if you enter 3, then all objects that are shared 3 or more times per a given transactional function will be listed:

![](shared3.jpg)

You can then exclude any of these objects from the next snapshot in the same way as explained for **Options/View Objects** below. The **Exclude Type** column indicates whether the objects are excluded from the next snapshot or not:

![](shared4.jpg)

- type is **MANUAL** if the exclusion has been added manually using the interface explained in this panel.
- type is **RULE** if exclusion has been added through an excluded item rule with technical enabled during snapshot - see [Transactions - Rules](../../transactions/rules/).

If you exclude objects (or unexclude them) you can use the **Recompute checksums** action (see above) to ensure that data is up-to-date.

## 6 - Download

Ability to download a detailed PDF report about how the Enhancement data has been calculated. An example report is available here [here](example.pdf) (for the AEP measure).

## 7 - Impact on Functional part

Detailed display of each Functional Enhancement (AEP or EFP) point. Expand the section to view the data. Data is split into Transactional Functions and Data Functions (this screenshot shows data derived from the AEP measure):

![](details.jpg)

### Search

![](search.jpg)

Use this option to search for a specific item in the list. The search is actioned on the **Full Name**.

### Download

![](csv.jpg)

This option will export the contents of the table to a comma-separated values (CSV) file, which can be viewed using Microsoft Excel or similar.

### Filter

![](filter.jpg)

This option allows you to filter the contents of the table, which can be particularly useful if you have a large number of items:

![](filter2.jpg)

### Name

Name of the object (Transactional Function or Data Function).

### Full Name	

Shows the full name of the object/file. Rolling the mouse pointer over the item will show the full name if it is too long to be displayed in the column:

![](full_name.jpg)

### AFP

Automated Function Point value.

### Status	

Status in the current snapshot:

- Added
- Modified
- Deleted

### IF / CF	

**IF (Impact Factor)**: Displayed when the EFP measure is active. The Impact Factor of a Transactional Function / Data Function is based on fixed formulas.

**CF (Complexity Factor)**: Displayed when the AEP measure is active. The Complexity Factor of a Transactional Function / Data Function is an adjustment factor (defined by an OMG-specification) which is calculated based on its status (added / modified / deleted) and the complexity of the objects inside the Transactional Function / Data Function.

### AEP / EFP

AEP value (displayed when the **AEP measure** is active) or EFP value (displayed when the **EFP measure** is active) for each object. See [Enhancement Measure](../../config/enhance-measure/).

### Options/View Objects	

Displays the list of objects involved in the transactional function. A sub window is displayed containing the objects:

![](options.jpg)

#### Role

Role of the object in the transaction.

#### Status in TF

Status of the object in the transactional function (ADDED/DELETED/MODIFIED/UPDATED).

#### Status in snapshot

Status of the object in the snapshot (ADDED/DELETED/MODIFIED/UPDATED).

#### Excluded	

Allows you to choose whether to exclude the object. "Excluded" means that the object is not considered in the enhancement calculation for any transactional functions, even if it is itself added, deleted or modified. As a result, it won't impact its parent transactional function's status.

If you want to **exclude/un-exclude multiple objects**, you can select the objects using the check boxes, then use the **Exclude/Unexclude** icons located in top right corner:

![](exclude.jpg)

The **Excluded** slider will move and the **Status in TF **will update, according to your choice:

![](exclude2.jpg)

If you exclude objects (or unexclude them) you can use the **Recompute checksums action** (see above) to ensure that data is up-to-date.

#### Shared Counter

The number of times the object is shared between transactions in the same snapshot.

#### EC level	
Effort Complexity level - category that assesses the complexity of adding, modifying, or deleting an Artifact based on a composite score of five software metrics that assess the complexity of the software environment in which the Artifact is embedded; that is, its size, comment level, algorithmic complexity, data access complexity, and coupling. Their possible values are: LOW, AVERAGE, HIGH, VERY HIGH. See [CAST Automated Enhancement Points Estimation - AEP](https://doc.castsoftware.com/export/AIPCORE/CAST+Automated+Enhancement+Points+Estimation+-+AEP). Not applicable for snapshots that use the [legacy EFP enhancement measure](../../config/enhance-measure).

#### EC

Effort Complexity - a numerical value assigned to an Artifact based on its Effort Complexity Level and its technology. See [CAST Automated Enhancement Points Estimation - AEP](https://doc.castsoftware.com/export/AIPCORE/CAST+Automated+Enhancement+Points+Estimation+-+AEP). Not applicable for snapshots that use the [legacy EFP enhancement measure](../../config/enhance-measure).

## 8 - Impact on Technical part	

Detailed display of each Technical Enhancement point. Expand the section to view the data. Data is displayed by Artifact. That this section will not contain any data when the [legacy EFP enhancement measure](../../config/enhance-measure).

![](TEP.jpg)

Columns headings are as described above, except for:

### Effort Complexity

The Effort Complexity is the Effort Rate of the Artifact based on its Cost Complexity and its technology. The Artifact EC value estimates the complexity of implementing an Artifact or changes to it, based on a composite score of five software metrics that assess the complexity of the software environment in which the Artifact is embedded.

See [CAST Automated Enhancement Points Estimation - AEP](https://doc.castsoftware.com/export/AIPCORE/CAST+Automated+Enhancement+Points+Estimation+-+AEP).

### Equivalence Ratio	
See [CAST Automated Enhancement Points Estimation - AEP](https://doc.castsoftware.com/export/AIPCORE/CAST+Automated+Enhancement+Points+Estimation+-+AEP).

### AEP / EFP

AEP value for each artifact.
