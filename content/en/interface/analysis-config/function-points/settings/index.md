---
title: "Application analysis configuration - Function Points - Settings"
linkTitle: "Function Points - Settings page"
type: "docs"
no_list: false
weight: 30
---

***

## Overview

The **Settings panel** enables you to configure various options:

![](settings.jpg)

By default for the compute action, various default settings are used:

## Activate automatic configuration refresh

CAST uses an automatic "configuration loading mechanism" based on the standard `.TCCSetup` files present in the CAST Imaging Core installation folder and any installed extension folders. `.TCCSetup` files (standard configuration files) contain configuration parameters to automatically define Entry Points, Data Entities, End points and Excluded Items and are known as Packages.

Before computing Function Points (either manually or during an analysis), the setup of the application(s) will be refreshed with the content of the `.TCCSetup` configuration files which are present in the following locations on disk:

```text
%PROGRAMFILES%\CAST\<release>\configuration\TCC

%PROGRAMDATA%\CAST\CAST\Extensions\<extension folder>\configuration\TCC
```

For the loading process, CAST will consider all files that match the following requirements:

- Must have `*.TCCSetup` extension
- Must be located in `%PROGRAMFILES%\CAST\<release>\configuration\TCC`
- If there are extensions installed, we will also consider the files present under `%PROGRAMDATA%\CAST\CAST\Extensions\<extension folder>\configuration\TCC`

By default, this setting will be enabled, meaning that any new configuration package or any new version of an existing configuration package will be automatically installed into the application configuration.

However, if we decide to "freeze" the application configuration, we can set the option to disabled. If the automatic refresh is disabled, only new packages will be installed, but the existing packages will remain in the current version, i.e. even if a new version is available, the package will not be updated.

## Data functions are initialized as	

Allows you to choose between the **EIF** (External Interface Files - this is the default) and **ILF** (Internal Logical File) computation type:

- If the Data Function is linked by **Update/Delete/Insert/Write** links, its type will be changed to **ILF** automatically
- If the Data Function is only linked by **Select/Read** links (i.e. it has no **Update/Delete/Insert/Write** links nor any **UNTYPED Use** links (when the type of the link has not been completely determined by the analyzer)), its type will be changed to **EIF** automatically.

## Activate look-up table filtering

As defined by **OMG CISQ AFP Specifications**, the elements identified as "lookup tables" will be filtered out and NOT considered as Data Entities. This is the default behavior, but it can be changed if you prefer to deactivate this filter by disabling the option. 

## Activate automatic Data Function merge (FK/PK)

CAST may automatically merge a certain table as "Detail" of another one based on Foreign key (FK) and Primary Key (PK) relationships. This is the default behavior, following the OMG CISQ AFP specifications. However, this feature can be deactivated by disabling the option.

{{% alert color="warning" %}}Warning: If this setting is deactivated and then later reactivated, the automatic merge based on Foreign Key (FK) and Primary Key (PK) analysis will only take place if both elements (main and detail) are NOT calibrated. If any of the involved elements is in a calibrated situation (ignored, deleted, manual merge, etc.) the FK-PK automatic merge will not occur.{{% /alert %}}

## Save objects of empty Transactions

Allows you to change whether objects that are part of empty transactions (those that do not reach any Data Entity or End point) should be saved. The default position is ALWAYS:

![](save.jpg)

- ALWAYS: objects of empty Transactions will always be saved
- NEVER: objects of empty Transactions will never be saved
- ONLY AEP: objects of empty Transactions will be saved if the Enhancement Measure used in the application is AEP, but will not be saved if the Enhancement measure of the application is EFP

{{% alert color="info" %}}If objects of empty Transactions are saved and there are any, their (full) call graph can be viewed in the same way as for those of non-empty Transactional Functions (see [Call Graph](../../transactions/transactions/call-graph/)).{{% /alert %}}

## FP value for empty Transactional Functions

Allows you to define a pre-defined Function Point value to be used for Transactional Functions.

Function Points can be only calculated for transactional functions that reach at least one data entity or one end point. If this is not the case, the transactional function will have a value of `0` for both `DET` and `FTR`, and therefore a value of `0` for Function Points (i.e. it will be an "empty transaction"). In order to avoid this situation, you can configure a pre-defined Function Point value to be used for empty transactions.

The possible values are: `0`, `3`, `4`, `5`, `6`, `7` and `ASSESS`. The selected value will be applied to the transactional functions that have a `0` Function Point value after the computation process.

{{% alert color="info" %}}If 0 is selected, it will actually deactivate this feature, i.e. those Transactional Functions that have a 0 Function Point value after the computation process will remain with a 0 Function Point value.{{% /alert %}}

### ASSESS

With regard to the ASSESS option - this will force CAST to assess Function Points for all Transactional Functions for which the call graph does not reach a data entity nor an end-point based on existing Function Points that have been computed. Console determines the average number of nodes in the call graph of non-empty Transactional Functions corresponding to one `DET`. This ratio allows Console to assign the number of `DET` to empty Transactional Functions, depending on the number of nodes in their associated call graph. The Function Point value will be then assessed based on that number of `DET` and with an `FTR=1`.

### Default FP values in the CAST Dashboards

Since it is possible to set a default Function Point value for Transactional Functions, these are considered as valid and can be displayed in the CAST Dashboards via the **Unadjusted Transactional Functions metric (10204)**. A complementary information on the function point detail contains the string ": Estimated" which indicates that the transaction has a default FP value:

![](default.jpg)

- If a **Default FP Transactional Function** is ignored during calibration, the "ignored status" will prevail and therefore it will still be sent with `Function Point = 0` (and no "Estimated" indication)
- If a **Default FP Transactional Function** contains also a user-defined Function Point, the user-defined value will prevail and therefore it will still be sent with `Function Point = <user-defined-value>` (and no "Estimated" indication)
- With regard to **EFP/AEP**: the **Default FP** flag is now stored per transaction (`EFP_Tran_Info.FP_Source`) so it is possible to distinguish the Default FP values from the rest of Transactions. If the `FP_Source = 1` then the Transaction will have a `Default FP` value.
