---
title: "Security Dataflow - Using FlawExplorer to speed up result check"
linkTitle: "Using FlawExplorer to speed up result check"
type: "docs"
no_list: true
weight: 40
---

***

## Overview

If it is necessary to fine tune elements of the Security Dataflow configuration without running an entire snapshot or if it is necessary to browse the results without using the CAST Engineering Dashboard, it is possible to use an unsupported CAST tool called the **FlawExplorer** to visualize the flaws. It takes in input `.flaw` files, and lets you browse the flaws that have been generate during the snapshot. In addition the FlawExplorer is an "offline" tool.

## Using FlawExplorer

`flawExplorer.exe` is located in the CAST Imaging Core installation folder on any of your nodes - double click the file to run it:

![](flawexplorer.png)

Use the **Load a flaw file** option (located in the top lefthand corner of the GUI) to load up a `.flaw` file. `.flaw` files are stored in the Large Intermediate Storage Area (LISA). The path to the `.flaw` file will also be logged in the `SecurityAnalyzer.log` which logs all Security Dataflow actions.

## Typical usage

- In the **Flaws box**: select a flaw in the list. The **Trace box** will be refreshed and contains the corresponding execution trace.
- In the **Trace box**: select a line in the execution trace. The **Code viewer** will be refreshed and contains the bookmark (highlighted in yellow) of the corresponding executed statement.

Hints:

- In the **Flaws box**: You can use keyboard arrows to switch between flaws
- In the **Trace box**: You can use keyboard arrows to play the execution trace.

Flaw names displayed in the FlawExplorer are not exactly the same as those presented in the CAST Engineering Dashboard. For example, the flaw called **Http Response Splitting** as displayed in the FlawExplorer is reported as **Cross-site scripting** in the CAST Engineering Dashboard.