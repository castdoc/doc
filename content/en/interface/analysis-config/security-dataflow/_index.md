---
title: "Application analysis configuration - Security Dataflow"
linkTitle: "Security Dataflow page"
type: "docs"
no_list: true
weight: 60
---

***

## Overview

This section provides an introduction to Security Dataflow - this screen allows the feature to be enabled and displays the results of an analysis where the Security Dataflow option is active.

### What is Security Dataflow?

In order to detect **User Input Security** flaws, CAST provides an analysis feature that implements a dataflow algorithm to detect variables that flow from the user down to a critical resource without prior sanitization, allowing hackers to send them data that will harm IT applications (security vulnerabilities).

As mentioned by the Common Weakness Enumeration (CWE), SANS Institute and OWASP, **Improper Input Validation** is the top group of web programming errors that can lead to security vulnerabilities.

Improper Input Validation is defined by the CWE follows:

*"When software fails to validate input properly, an attacker is able to craft the input in a form that is not expected by the rest of the application. This will lead to parts of the system receiving unintended input, which may result in altered control flow, arbitrary control of a resource, or arbitrary code execution."*

The Security Dataflow feature enables users to detect improper user input validation in the application's source code, which can lead to the following security vulnerabilities:

- SQL Injection (CWE-89)
- Cross-Site Scripting (CWE-79)
- LDAP Injection (CWE-90)
- OS Command Injection (CWE-78)
- XPath Injection (CWE-91)
- Path Manipulation (CWE-99)
- Avoid Log forging vulnerabilities (CWE-117)
- Avoid uncontrolled format string (CWE-134)
- Trust Boundary Violation (CWE-501)
- Sensitive Cookie in HTTPS Session Without 'Secure' Attribute (CWE-614)
- Use of hard-coded credential (java, C#, VB.Net languages) (CWE-798)

Once the source code analysis is complete, intermediate results can be viewed, full results (i.e. the security vulnerabilities) can be viewed in the CAST Engineering Dashboard as standard rules. As dataflow-based rules, the flow of the user input can be tracked in the CAST Engineering Dashboard with bookmarked source code.

CAST currently supports User Input Security checks for the following technologies:

- JEE
- .NET

### Predefined methods

Some well known Input Methods/Target Methods are already pre-defined and taken into account - see [Predefined methods](predefined-methods/).

## Enabling/disabling Security Dataflow

By default, Security Dataflow is **enabled** for all supported technologies by default for the initial application analysis. It can be disabled per application by clicking the slider for the appropriate technology once the initial analysis has completed:

![](sd.jpg)

Note that:

- disabling or enabling Security Dataflow between successive snapshots may impact results: more or less violations may be returned.
- see [Technical details](techical-details/) for more information about the internal mechanisms used by this feature.

## Performance impact when User Input Security is enabled

Performing User Input Security checks **will impact analysis performance** - i.e. the analysis of a given application will take longer to complete if you enable User Input Security checks. This is due to the additional checks and metrics that need to be processed during this type of analysis. However, the impact on performance will vary according to various different aspects related to the Applications you are analyzing:

- Number of lines of code (LOC)
- Number of objects after analysis
- Number of violations after analysis

In other words, the impact will be less noticeable on small/simple Applications, whereas the impact will be much more significant for larger/complex applications.

{{% alert color="warning" %}}User Input Security analyses require a significant of free RAM memory on the target Node for large application. In particular when enabling Security Dataflow for JEE, the [com.castsoftware.securityforjava](../../../technologies/jee/extensions/com.castsoftware.securityforjava/) extension (that is automatically installed) requires a node configured with a minimum of **32GB RAM**. See also [What hardware do I need?](../../../requirements/hardware/).{{% /alert %}}

## Viewing results

### Intermediate results

When Security Dataflow options are enabled and a snapshot has been generated, intermediate results can be viewed at Application level. This gives a basic overview of results:

![](sd_results.jpg)

#### Analysis Findings

This table displays the number of User Input Targets and User Input Sources identified in the analysis, the number of Detected Flaws and the number of Blackboxes used:

| Item | Description |
|---|---|
| Targets | Number of calls to methods accessing resources considered for the rule |
| Sources | Number of calls to methods where variables are populated with user input |
| Detected Flaws | The number of flows from methods with user input down to methods accessing resources without prior sanitization |
| Black-boxes | The number of black-box files that have been loaded for the rule |

#### Analysis Status 

A brief over overview of the success/failure of the Security Dataflow analysis, together with a comment to help debug in case of a failure.

#### Blackbox

When analyzing an application, this application has a defined analysis scope - i.e. some code is analyzed, some is not and is treated as external code. Any external code is "unknown" and cannot be accessed by the analyzer, and as such cannot be taken into account in the normal manner by the Security Dataflow feature. Therefore, security problems in this external source code would normally not be detected at all by CAST Imaging.

In order to resolve this problem, CAST uses the notion of "blackboxing methods". There are two ways that blackboxing is used:

##### Automatic Blackboxing
	
The Security Dataflow feature will automatically generate blackboxes on the fly (during the analysis) for methods with the following characteristics:

- the source code is not available
- there is no predefined blackbox
- no custom blackbox has been already defined

This includes the majority of assemblies for which no source code can be found (framework assemblies, third-party JARs/assemblies, internal frameworks without source code etc.). Based on the method signature and a statistical model, the on the fly feature will then decide on method behavior:

- **User input method**: network read, console input, etc.
- **Target method**: network write, file open, etc.
- **Pass-through method**: similar to "collection" blackboxing

{{% alert color="info" %}}Only external methods written in English are supported by this automatic process.{{% /alert %}}

##### Manual Blackboxing

If the Automatic Blackboxing feature (described above) does not handle external code in the correct way then the recommended action is to create a blackbox manually to alter the way the automatic process functions. When the blackbox has been created, it can be added in to the analysis configuration in this panel. In the example below, one blackbox file (must have the `.blackbox.xml` file extension) has been added in. Once the blackbox has been created, and a new snapshot should be generated to ensure the manual blackbox is taken into account.

![](bbox1.jpg)

To **add** a new blackbox, click Add and then use the upload button in the bottom right corner and select the file from your local hard drive:

![](bbox_upload.jpg)

Now select the uploaded blackbox file and click **Select**:

![](bbox_upload2.jpg)

To remove a blackbox file that you have added, click the trash icon next to the file:

![](bbox_remove.jpg)

Removing a blackbox file that has already been used in an analysis will impact the analysis results.

### Full results

The full results are displayed as rule violations in the CAST Engineering Dashboard and form part of the **Security (60016)** Health Measure and the Technical Criterion called **Secure Coding - Input Validation (66062)**. In addition, results are also generated in an xml file called `BuildAgent.flaw` which can be loaded into the FlawExplorer - see [Using FlawExplorer to speed up result check](flaw-explorer/) and also [Technical Details](techical-details) for more information.

![](full_results.jpg)

### Analyze Security Dataflow option

When an analysis has been run and Security Dataflow has been enabled for a technology in your Application, a button will be enabled in this panel:

![](sd-1.jpg)

This button enables you to run just the Security Dataflow part of an analysis, instead of having to run the entire analysis. This can save a lot of time if you are only interested in viewing the Security Dataflow results - for example, you may have added a manual blackbox and you would like to check the results before committing to using the blackbox in a full analysis.

## Log message troubleshooting

### Security dataflow fails with "Out of Memory" during Run Extensions at Application level

If your Security Dataflow analysis fails with "Out of Memory" errors, there are only two options available to resolve this:

- Increase the amount of RAM available to the Node and rerun the Security Dataflow analysis, if possible. Security Dataflow is very resource hungry both for JEE and .NET, and when using it with JEE projects (when using the [com.castsoftware.securityforjava](../../../technologies/jee/extensions/com.castsoftware.securityforjava/) extension) CAST recommends a minimum amount of RAM (32GB). 
- If you cannot increase the amount of RAM available to your Node, or if the issue persists even after increasing the available RAM allocation, the only remaining option is to:
    - Split your existing Analysis Units into multiple smaller Analysis Units.
    - Cleanup your existing CASTIL located in the LISA folders on your Node.
    - Rerun the Security Dataflow analysis.

### Reached limit while exploring stores instrs caller tree for field

You may see this message in the analysis log when the User Input Security feature is enabled:

```text
Warning MODULMSG ; Job execution  Reached limit while exploring stores instrs caller tree for field XXX
```

This warning message has no impact on the analysis results: when the source code is explored a file can be be examined a limited number of times. When this warning message appears in the analysis log it shows that this limit has been reached.

## Technical information

See the following pages for more technical information about Security Dataflow:

- [Technical details](technical-details)
- [Predefined methods](predefined-methods)
- [Manually configuring blackbox methods](blackbox-methods)
- [Using FlawExplorer to speed up result check](flaw-explorer)
- [Advanced configuration and usage instructions](https://doc.castsoftware.com/export/AIPCONSOLE/User+Input+Security+-+Advanced+configuration+and+usage+instructions)