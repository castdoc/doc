---
title: "Application analysis configuration - Snapshots"
linkTitle: "Snapshots page"
type: "docs"
no_list: true
weight: 70
---

***

## Overview

The **Snapshots** page provides a mechanism to manage Snapshots for the current Application. Once a snapshot has been generated it will be listed in this section. Management options available include:

- Publish All
- Rename
- Consolidate
- Delete

![](snapshots.jpg)

### What are snapshots?

A Snapshot is a "capture" at a specific moment in time of the status of an Application's source code using a set of measures and rules and are primarily used to generate results for the CAST Engineering Dashboard/Health (Management) Dashboard.

## Content

### Publish All Snapshots

This option will loop through all snapshots listed in the screen and any that do not already exist in the storage schema (called the "Measurement") will be uploaded. This means that they will be available for consultation in the Health Dashboard.

### Global Delete

![](global_delete.jpg)

Use this option to completely remove ALL the selected Snapshots from the storage schemas (Dashboard schema and from the Measurement schema) - if the snapshot has been uploaded there. This will start the deletion process and can take some time if the selected Snapshots contain a large amount of data.

If you remove an older snapshot (i.e. not the most recent) then the next most recent snapshot will be updated:

- delta and AEP/EFP metrics are recalculated, and the post process procedures that update schema tables required for dashboards are relaunched.
- new data is also pushed to the associated Measurement schema

### Rename option

![](rename.jpg)

This option allows you to rename an existing snapshot. The snapshot will also be renamed in the CAST Dashboards.

### Consolidate option

![](consolidate.jpg)

This option will recalculate rule/measure/distribution grades and numbers, and also delta measures for the selected snapshot, and then upload the snapshot to the Measurement schema after having deleted the previous one (if it exists). This can take some time if the Snapshot contains a large amount of data.

This operation can sometimes cause data discrepancies with results computed when the quality model was different. To minimise this, the following items are taken into account when using the Consolidate option:

- changes in aggregation weights
- changes in the tree structure (new contributions between a rule and a Technical Criteria, new aggregations, new Technical Criteria etc.)
- changes in the module weight
- changes in the thresholds used to compute grades based on rule compliance ratios and distributions
- changes in critical contributions

Computations executed on the Analysis schema are not taken into account by the Consolidate option:

- it does not re-analyze the source code
- it does not re-compute metrics
- it does not re-run procedures associated with metrics

If you consolidate an older (i.e. not the most recent) snapshot, then the next most recent snapshot will be updated:

- delta and AEP/EFP metrics are recalculated, and the post process procedures that update schema tables required for dashboards are relaunched.
- new data is also pushed to the associated Measurement schema

### Individual delete option

![](delete_individ.jpg)

Use this option to completely remove the selected Snapshot from the Dashboard schema and from the Measurement schema (if the snapshot has been uploaded there). This will start the deletion process and can take some time if the Snapshot contains a large amount of data.

If you remove an older snapshot (i.e. not the most recent) then the next most recent snapshot will be updated:

    delta and AEP/EFP metrics are recalculated, and the post process procedures that update schema tables required for dashboards are relaunched.
    new data is also pushed to the associated Measurement schema
