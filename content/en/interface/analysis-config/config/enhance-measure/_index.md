---
title: "Application analysis configuration - Config - Enhancement Measure"
linkTitle: "Enhancement Measure page"
type: "docs"
no_list: true
weight: 40
---

***

## Overview

This option allows you to switch between the two different Enhancement Function Point measurement systems for the current Application. You can switch between:

- Automated Enhancement Points (AEP) - default
- Enchancement Function Points (EFP) - legacy

![](enhance-measure.jpg)

See the following for more information:

- [CAST OMG-compliant Automated Function Points](https://doc.castsoftware.com/export/AIPCORE/CAST+OMG-compliant+Automated+Function+Points)
- [CAST Automated Enhancement Points Estimation - AEP](https://doc.castsoftware.com/export/AIPCORE/CAST+Automated+Enhancement+Points+Estimation+-+AEP)
- [CAST Enhancement Function Points Estimation - EFP](https://doc.castsoftware.com/export/AIPCORE/CAST+Enhancement+Function+Points+Estimation+-+EFP)

## Automated Enhancement Points (AEP) - default

CAST integrates the estimation of OMG Automated Enhancement Points (AEP). With the AEP feature, CAST displays detailed information about the changes done on both the functional and technical sides between two versions or two snapshots of an application.

Like EFP (see below), the feature helps to evaluate overtime the size of evolutions implemented by teams producing applications. It computes and displays data functions and transactional functions that have been modified, added, or deleted, and information on the technical components of applications that do not belong to any transaction. The unadjusted function points that are calculated are weighted with Complexity Factors based on the objects complexity.

CAST highly recommends that this measurement mode is selected and as such it will be set as the default option.

## Enhancement Function Points (legacy)	

In addition to AEP (see above), CAST also integrates the estimation of Enhancement Function Points (EFP) also called Project Function Points, so that CAST provides both baselining, e.g sizing an entire application using Automated Function Points and Enhancement Function Points. With the Enhancement Function Points feature, CAST is in a position to display detailed information about the changes between two versions of an application and between two snapshots of the same application.

This feature helps to evaluate overtime the efficiency of teams producing applications. It is particularly useful in outsourcing contexts as many outsourcing contracts are based on FP when it comes to determine the size of the deliverables. This feature computes and displays data functions & transactional functions that have been modified, added or deleted.

This measure is considered "legacy".

## Switching action

When changing the Enhancement measurement mode it is important to take into consideration the impact this will have on the analysis/snapshot results and that it will introduce a disruption into the measurement trending. There are important differences between the two measures (AEP and EFP) that must be understood:

- The EFP mode does not manage the technical part of application whereas AEP does.
- When using EFP mode, Transactional Functions are based on the reduced call graph whereas for AEP, Transactional Functions are based on the full call graph.
- The AEP specification defines Complexity Factors based on artifacts' complexity or changes made to data entities.

When moving from EFP to AEP to get more precise values, results will show objects that do not belong to the reduced transaction call graph as "added" and this will impact the measure, even if these objects have not really been added. Points related to the technical part of the Application will also appear in the results. Moreover, taking in to account object complexity to weight the FP values will impact the results as well.

Moving in the opposite direction from AEP to EFP will result in having objects that do not belong to the full transaction call graph to be considered as "deleted". This will also impact the results, even if those objects have not been really removed. In addition, Complexity Factors will be replaced by Impact Factors that are set to 1 by default and points related to the technical part of the application will disappear.

If you plan to change the measurement mode, it is necessary to recreate a new baseline snapshot. To do that, after you change the measure you must generate a new snapshot for the same Application version but it must not be considered as consistent for the new measure. The next snapshot you generate for the Application will be consistent and can be used as the first snapshot using the new enhancement measure.