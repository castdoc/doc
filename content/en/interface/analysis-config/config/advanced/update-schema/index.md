---
title: "Application analysis configuration - Config - Advanced - Update Application schema"
linkTitle: "Update Application schema"
type: "docs"
no_list: true
weight: 40
---

***

## Overview

This section explains how to manage specific tools that can be run after an analysis to enrich the content of the final analysis results or achieve a specific aim. Currently one type of tool is available, the **SQL update tool** which allows you to alter the results via a special SQL query.

![](updatekb.jpg)

For example, you can:

- Update links that have been omitted by analyzers when dynamic programming is in use
- Update erroneous links identified by grep searches
- Managing specific situations (e.g. Java frameworks)
- Overcoming specific limitations inherent in a CAST analyzer (for example searching using the parent type and not just on the object name ("class.function()" calls in the Universal Analyzer), ability to handle virtual functions and the ability to ignore links)

The following actions are also possible:

- Add a new object
- Add a new parent link (Belongs To) to an object
- Add/modify/delete the property of an existing object
- Delete existing links
- Delete existing objects

{{% alert color="info" %}}Any links created via this feature:<ul><li>are considered as dynamic links and will appear in the <a href="../dynamic-links/">Dynamic Links</a> section for validation.</li><li>will not have bookmarks in the parent file to identify the exact location of the link.</li></ul>{{% /alert %}}

## How does the tool work?

A set of entry tables in the Analysis schema must be injected with the relevant data that will modify the data. When the tool is then run, a specific stored procedure (also located in the Analysis schema) is called which will carry out the update using the data in the entry tables.

## Adding a tool

Click the **Add** button and choose the type of tool you want to create:

- Operation on links
- Operation on objects

![](add.jpg)

The choice of tool simply changes the template SQL query and tailors it to actions to change objects or links in the results:

![](add_job.jpg)

### Name / Description 
Enter a name/description for the tool.
### Will be processed

This toggles the tool job:

- **ENABLED** > The tool will be run after the analysis - by default all newly created Tools will be set to ENABLED
- **DISABLED** > The tool will NOT be run.

When a tool is set to DISABLED, this is usually a temporary action. You should delete the tool if you want to permanently remove it.

### SQL Code editor	

Enter the SQL code to be executed in this section. Only standard SQL is supported.

### Run Tool

This button is disabled until the form has been successfully saved. Once saved, you can use this button to execute the current tool.

### Save and Run	

This button will save the data in the tool and then execute it immediately. Note that the SQL is NOT validated - if you want to enable syntax checking, see [Enabling SQL validation](../../../../../administer/misc/sql-validation/).

### Save	

This button will save the data in the tool. Note that the SQL is NOT validated - if you want to enable syntax checking, see [Enabling SQL validation](../../../../../administer/misc/sql-validation/).

## Changes made

Any changes made in the Update Application schema page (e.g. adding a new tool job) will cause a banner to appear at the top:

![](banner.jpg)

This indicates that the changes you have made will impact your analysis results and therefore that the data that is used for the Architecture Studio, Transaction configuration and other results will not be up-to-date. Clicking the **Update** button in the banner will run a special step called **Prepare analysis data** that should complete relatively quickly (more quickly than a full analysis) and ensures that the data used by the Architecture Studio, Transaction configuration and other features is up-to-date based on the changes you have made. 

This step is always actioned when you either run an analysis. When the **Prepare analysis data** step is complete, the banner will no longer be displayed.

## Examples

See [Update Application schema - KB Update SQL Tool examples](https://doc.castsoftware.com/export/AIPCONSOLE/Application+-+Config+-+Update+Application+schema+-+KB+Update+SQL+Tool).