---
title: "Application analysis configuration - Config - Advanced - Dynamic Links"
linkTitle: "Dynamic Links"
type: "docs"
no_list: true
weight: 10
---

***

## Overview

When an analysis is run the authenticity of some links that have been identified between objects cannot be absolutely determined by CAST - these links are known as "dynamic links". Often these "unverified links" systematically occur or have no meaning for the specific technology you are working with. This screen displays:

- a summary of the dynamic links detected in the most recent analysis
- detailed information about each link
- the ability to manually review (see below) each unverified link to state whether the link is correct or false

Reviewing Dynamic Links forms an important part of the analysis validation process.

## Content

![](dynlink.jpg)

### Summary

The Summary section provides a basic overview of the status of Dynamic Links in the Application as a result of the most recent source code analysis. Clicking a row in the table will take you direct to the Manual Review section (see below) with a filter applied to match the contents of the row.

#### Not Reviewed

Number of links that need to be reviewed manually (see below). This means that Console does not know whether the link is real or not and therefore you should review the code that creates the link to determine its status and then either validate or ignore it.

#### Ignored / Validated	

Number of links that:

- have already been validated as correct (Validated)
- have already been ignored as invalid/false (Ignored)

Validation/ignore can occur through a variety of methods:

- Manual review in CAST Imaging (see below)
- Automatic review through:
    - [Automatic Links Validator](../../../../../technologies/multi/com.castsoftware.automaticlinksvalidator/) extension
    - Dynamic Link Rules Dynamic Links Rules (see below)

#### Download

![](download.jpg)

Use this button to download the list of Dynamic Links in this section in CSV format.

### Details

The **Details** section provides more information about the links listed in the **Summary** section. Clicking a row in the table will take you direct to the Manual Review section (see below) with a filter applied to match the contents of the row.

#### Source Type	

Lists the object type that contains the code for the "source" of the link. Typically these are Methods and Constructors, but can be others.

#### Target Type

Lists the object type that is the target of the source object. Typically these are database object types, but can be others.

#### Status	

Indicates the status of the link as explained in the Summary section above.

- Not Reviewed
- Ignored
- Validated

#### Links

Indicates the total number of links that exist between the source and target object type. The total number of links reported in the Summary link (Not Reviewed + Validated) will equal the total number of links in this column.

## Manual review

This button opens a new window where you can manually review each link. By default the window will show all links that need to be reviewed - i.e. all links that are still considered to be "unverified". The first link in the list will be selected and the source code that creates the link will be displayed automatically, along with the specific object highlighted (this is known as a bookmark) - this will help you decide whether the link is correct and needs to be marked as valid, or whether the link is invalid and needs to be marked as ignored:

![](review.jpg)

Sometimes, there may be more than one bookmark for a given link. If this is the case, you can move through the bookmarks using the navigation buttons as highlighted below:

![](review2.jpg)

Links can, however, be in one of three states, indicated in the Status column:

![](review3.jpg)

- **Not reviewed**: links displayed in this state when you open the Dynamic Link Manager after the completion of an analysis - these links must be reviewed.
- **Validated**: links are in this state when they have already been manually or automatically validated as correct or true. By default Validated links are not displayed in the GUI - you will need to specifically select the Validated check box to display them.
- **Ignored**: links are in this state they have already been manually or automatically reviewed as invalid or false. By default Ignored links are not displayed in the GUI - you will need to specifically select the Validated check box to display them.

By default **Validated** and **Ignored** links are not displayed in the GUI - you will need to specifically select the **Validated / Ignored** check box to display them:

![](review4.jpg)

### Manual review options

#### Apply button

Allows you to apply the review status on your links. Note that this button is not active until a specific choice has been made either in the individual settings or in the global settings:

![alt text](apply.jpg)

#### Filters (top right)

Filters the display of Dynamic Links. By default only **To Review** is selected, displaying only those links that are unverified and need to be manually reviewed. You can select a combination of any of the options to display the type of links you need.

The Reference Finder filter will always display any links that have been created via a Reference Finder rule.

#### Status

Current status:

- **Validated**: links are in this state when they have already been manually or automatically validated as correct or true. By default Validated links are not displayed in the GUI - you will need to specifically select the Validated check box to display them.
- **Ignored**: links are in this state they have already been manually or automatically reviewed as invalid or false. By default Ignored links are not displayed in the GUI - you will need to specifically select the Validated check box to display them.
- **Not reviewed**: links displayed in this state when you open the Dynamic Link Manager after the completion of an analysis - these links must be reviewed.

#### Validate/Ignore/To Do radio buttons

![](validate.jpg)

These radio buttons allow you to choose what to do with the link after reviewing its code. Select the appropriate option for the link and then click **Apply** in the top right corner to confirm the choice. You can use the check boxes to review multiple links:

![](validate2.jpg)

### Process of reviewing Dynamic Links

The process of reviewing Dynamic Links is as follows. Ensure that the filter is set to To Review:

![](process.jpg)

Tick the link you want to review:

![](process2.jpg)

If there are multiple dynamic links that you want to Validate (as correct) or Ignore (as false) in one go, you can select multiple using the mouse or using the SHIFT key+mouse combination:

![](process3.jpg)

If necessary, check the source code to help you decided if the link is correct and needs to be marked as valid, or whether the link is invalid and needs to be marked as ignored:

![](process4.jpg)

Click the appropriate radio button to either Validate (as correct) or Ignore (as false) the link:

![](validate.jpg)

**Ignore** dynamic Links when:

- Source code uses a logger information message
- Basic text displayed in a message box or a frame

**Validate** dynamic Links when:

- code uses a manipulation of SQL queries
- code uses a direct call from a business layer to a database table or function
- code links a client part and a server part of an application

Finally click the **Apply** button to confirm the change in status of the link, then close the window:

![](process5.jpg)

The **Summary/Details** screen will then update to reflect your changes.

## Changes made

Any changes made in the Dynamic Links page (e.g. reviewing Dynamic Links and applying the changes) will cause a banner to appear at the top:

![](banner.jpg)

This indicates that the changes you have made will impact your analysis results and therefore that the data that is used for the Architecture Studio, Transaction configuration and other results will not be up-to-date. Clicking the **Update** button in the banner will run a special step called **Prepare analysis data** that should complete relatively quickly (more quickly than a full analysis or snapshot) and ensures that the data is up-to-date based on the changes you have made. 

This step is always actioned when you either run an analysis or generate a new snapshot. When the **Prepare analysis data** step is complete, the banner will no longer be displayed.

## Rules

![](rules_diag.jpg)

See [Dynamic Links Rules](rules/).