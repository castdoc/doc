---
title: "Application analysis configuration - Config - Advanced - Dynamic Links Rules"
linkTitle: "Dynamic Links Rules"
type: "docs"
no_list: true
---

***

## Overview

When an analysis is run, the authenticity of some links that have been identified between objects cannot be absolutely determined by CAST - these links are known as "dynamic links". Often these "unverified links" systematically occur or have no meaning for the specific technology you are working with. To counter this, filter rules (`DLM.XML` files) can be configured to either validate (i.e. retain) or ignore (delete) these recurring dynamic links automatically when an analysis is run. This panel governs the filter rules applied at application level.

## Adding a DLM.XML rule file at Application level

Before you can add a `DLM.XML` rule file at Application level you must upload the `DLM.XML` file or files to the API Node on which it will be used. CAST highly recommends uploading the DML.XML files to the `API_Upload` folder which is physically located on the relevant Node:

![](dlm1.jpg)

Select the required `DLM.XML` file or files and click **Save**:

![](dlm2.jpg)

The `DLM.XML` files will then be visible in the list. Note that you can use the eye icon to view the content of the `DLM.XML` file and the trash icon to delete it:

![](dlm3.jpg)

{{% alert color="info" %}}CAST Imaging does NOT check the uploaded DLM.XML file for consistency or correctness.{{% /alert %}}

## Global level DLM.XML rule files

Dynamic Links Rules can also be applied at global level (i.e. for all applications) - this may be done by an Administrator to ensure that all applications benefit from the automatic validation of links. If this has occurred, you will see a `DLM.XML` file present in the list by default, before you have uploaded any yourself:

![](rules.jpg)

You can view (use the eye icon) the `DLM.XML` file to check what rules it will implement and delete the file (using the trash icon) if it does not do what you require. See [Default Dynamic Links Rules](../../../../../../administer/settings/global-configuration/dynamic-links-rules/) for more information. 

## DLM.XML rule files and filters

Filter rules use XML formatting and must, therefore be created in a file with an `.DLM.XML` extension (for example `rules.DLM.XML`):

- You can create as many XML "rule" files as you need, or you can place all your filter rules in one XML "rule" file.
- The XML rule file will contain a flat list of filtering rules
- A filtering rule is an ordered list of filters, with an action to perform on the filtered links (validate or ignore)
- A rule must be unique for the Application, i.e. it cannot be present in the same rule file more than once and cannot be present in multiple rule files
- A rule must use a unique, case sensitive name, in order to be identified easily and define an action: validate or ignore
- A rule has an optional scope, which means it is possible to specify on what analysis unit/technology you want to apply the rule. 
- A rule must contain a link definition (the link that will be validated or ignored) based on the Callee or Caller object, a link type, or a line/part of a line of source code.
- The rules are executed in the order that they are defined in the XML file, and inside each rule, the filters are also executed in the order that they are defined. The filters are applied with a logical conjunction (an AND operation, i.e. the link has to be accepted by every filter in the rule)
- CAST recommends that the `DlmRulesModel.xsd` is applied to the XML rule file

### Example rule file and explanation

Take the following example rule file and the filters defined in it:

```xml
<?xml version="1.0" encoding="utf-8"?>
<dynamicLinksRules xmlns="http://www.castsoftware.com/DlmRulesModel.xsd" >
 <rule name="Rule1" action="validate" >
 	<scope>
 		<application name="app1">
 			<technology name="J2EE"/>
 		</application>
 	</scope>
 	<calleeName regexp="a2"/>
 	<calleeType names="A_Metamodel_Type"/>
 	<callerFullName regexp="a4"/>
 </rule>
 <rule name="Rule2" action="ignore">
 	<calleeFullName regexp="a4"/>
 </rule>
</dynamicLinksRules>
```

When this rule file is used:

- `Rule1` will be processed first (this has a validate action)
- The entire set of unreviewed Dynamic Links that are available will be taken into account
- The following actions are then executed in the following order:
    - Selecting as a scope all links that belong to:
        - All J2EE links in the application `app1`
    - Filtering by callee name = `a2`
    - Filtering by callee type = `A_Metamodel_Type`
    - Filtering by caller full name = `a4`
    - Tag the result as `Validate`
- Then `Rule2` will be processed
- The entire set of unreviewed Dynamic Links that are available will be taken into account (there is no scope specified at all), except the links that have been processed by `Rule1`.
- The following actions are then executed in the following order:
    - Filtering by callee full name = `a4`
    - Tag the result as `Ignore`

### Filters in detail

As described previously, a filter is an operator that selects links that match criteria. Filters are piped together in order to compose the body of a rule. The final result will be tagged as validated or ignored.

![](filter.jpg)

#### Scope

Application:

```xml
<scope>
	<application name="app1"/>
</scope>
```

Technology:

```xml
<scope>
	<application name="application">
		<technology name="technology"/>
	</application>
</scope>
```

Analysis Unit:

```xml
<scope>
	<application name="application">
		<analysisUnitname="analysisUnit"/>
	</application>
</scope>
```

#### Filter list

| Filter | Description | More details|
|---|---|---|
| `<callerName regexp="string"/>` | Filters the stream according to the caller name | See Filter rules - Regular Expression based |
| `<callerFullName regexp="string"/>` | Filters the stream according to the caller full name |  |
| `<callerType names="string"/>` | Filters the stream according to the caller type | See Filter rules - Metamodel based |
| `<calleeName regexp="string"/>` | Filters the stream according to the callee name | See Filter rules - Regular Expression based |
| `<calleeFullName regexp="string"/>` | Filters the stream according to the callee full name |  |
| `<calleeType names="string"/>` | Filters the stream according to the callee type | See Filter rules - Metamodel based |
| `<linkType names="string"/>` | Filters the stream according to the link type |  |
| `<rawStringFromCode regexp="string"/>` | Filters the stream according to a regular expression on the whole line of code | See Filter rules - Bookmark based<br> |
| `<leftStringFromCode regexp="string"/>` | Filters the stream according to a regular expression on the left part of the code before the matched callee name |  |
| `<rightStringFromCode regexp="string"/>` | Filters the stream according to a regular expression on the right part of the code after the matched callee name |  |

##### Regular Expression based filters

Filters based on Regular Expressions must be in .NET format as described in https://learn.microsoft.com/en-us/dotnet/standard/base-types/regular-expressions. You can use the following "cheat sheet" to get you started:

| Expression | Description|
|---|---|
| `\b` | Start at a word boundary. |
| `(\w+)` | Match one or more word characters. Together, they form a group that can be referred to as \1. |
| `\s` | Match a white-space character. |
| `\1` | Match the substring that is equal to the group named \1. |
| `\b` | Match a word boundary. |
| `\s*` | Look for zero or more occurrences of a white-space character. |
| `[-+]?` | Look for zero or one occurrence of either a positive sign or a negative sign. |
| `([0-9]{0,3}(,[0-9]{3})*(\.[0-9]+)?)` | The  outer parentheses around this expression define it as a capturing group  or a subexpression. If a match is found, information about this part of  the matching string can be retrieved from the second Group object in  the GroupCollection object returned by the Match.Groups property. (The first element in the collection represents the entire match.) |
| `[0-9]{0,3}` | Look for zero to three occurrences of the decimal digits 0 through 9. |
| `(,[0-9]{3})*` | Look for zero or more occurrences of a group separator followed by three decimal digits. |
| `\.` | Look for a single occurrence of the decimal separator. |
| `[0-9]+` | Look for one or more decimal digits. |
| `(\.[0-9]+)?` | Look for zero or one occurrence of the decimal separator followed by at least one decimal digit. |

*Escaping special characters*

Certain special characters used in Regular Expressions may need escaping with a back slash `\`, for example:

- `(`
- `)`
- `"`

Therefore using the following Regular Expression (see below) in a Dynamic Link Manager filter rule:

```xml
<?xml version="1.0" encoding="utf-8"?><dynamicLinksRules xmlns="http://www.castsoftware.com/DlmRulesModel.xsd" > 
	<rule name="Ruleoutprintln" action="ignore" >
		<leftStringFromCode regexp="\t*out.println\(\&quot;&lt;a href"/> 
	</rule> 
</dynamicLinksRules>
```

will match for example:

```java
\t\tout.println("<a href='showDistinctMonster.jsp?MONSTER_ID="lt_monster.getMonsterId()"'>"lt_monster.getStringValue()"</a> <BR/>");
```

##### Metamodel based filters

Filters can refer to CAST metamodel types:

- `<callerType names="string"/>`
- `<calleeType names="string"/>`
- `<linkType names="string"/>`

You can specify one or multiple names as follows:

- Single type: `<calleeType names="MyType1"/>`
- Multiple type: `<calleeType names="MyType1|MyType2|MyType3"/>`

The type expected here corresponds to the metamodel type name, NOT its description.

##### Bookmark based

Filters can refer to the source code from where the link is going (i.e. the bookmark) using a Regular Expression string:

- `<leftStringFromCode regexp="string"/`> - the text before the link, from the first column of the same line
- `<rightStringFromCode regexp="string"/>` - the text after the link, to the last column of the same line
- `<rawStringFromCode regexp="string"/>` - the whole text, including the left part and the right part

This can be explained using the following image:

![](rule_example.jpg)

*Tab setting*

The default tab setting value is set to `4` and this cannot be modified. For programming languages that work with a different tab setting value, the filters `leftStringFromCode` and `rightStringFromCode` may NOT work correctly. In this case, as a workaround, CAST suggests using only `rawStringFromCode`, which is not tab sensitive.

Take the following code:

```java
if (module == null) {
    throw new ValidationException (
        "There is no CurrencyModule defined for currency "
            + currencycode
            + " in module/impl/application/currencies");
}
```
This would give the following:

- Left string: `+ " in module/impl/application/`
- Right string: `");`
- Raw string: `+ " in module/impl/application/currencies");`
- CalleeName: `currencies`