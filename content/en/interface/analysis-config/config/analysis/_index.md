---
title: "Application analysis configuration - Config - Analysis"
linkTitle: "Analysis page"
type: "docs"
no_list: false
weight: 10
---

***

For information about settings related to specific technologies, please see the following pages:

- [.NET](../../../../technologies/dotnet/analysis-config/)
- [JEE](../../../../technologies/jee/analysis-config/)
- [Mainframe](../../../../technologies/mainframe/analysis-config/)

Also see the child pages listed below.
