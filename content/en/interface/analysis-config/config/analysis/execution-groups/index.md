---
title: "Working with Execution Groups"
linkTitle: "Working with Execution Groups"
type: "docs"
no_list: false
weight: 20
---

***

## Overview

Execution Groups are an advanced level feature that represent how auto-generated Analysis Units (i.e source code and configuration details) are analyzed. Depending on a variety of factors, multiple Analysis Units may be grouped together into one Execution Group, or one Execution Group may be created for each Analysis Unit.

Their objectives are as follows:

- to improve analysis performance - i.e. by grouping multiple Analysis Units together in one "analysis execution," analysis time can be improved.
- to allow a group of Analysis Units in one Execution Group to be analyzed in one go, manually
- to reduce the risk of an analysis crashing - an Execution Group has an upper limit on the amount of source code that can be analyzed in one "analysis execution" (this differs per Technology) and this limit is designed to prevent an analysis crashing due to an out of memory error
- to ensure links between Analysis Units are not missed, particularly links determined by the inference engine

## How do they work?

The contents of an Execution Group (i.e. which Analysis Units are grouped together) is determined automatically using internal algorithms specific to each technology type and information (source code size, dependencies between projects) identified during the source code delivery process. For the most part, you should let CAST Imaging manage Execution Groups - however, it is possible to configure them yourself if you find that the automatic management strategy provided by Console is not providing an optimum result.

## Auto-management of Execution Groups

CAST Imaging will automatically manage and define Execution Groups on an Application basis for each supported Technology as listed in the table below. As soon you create custom Execution Groups, the auto-management strategy will be deactivated.

| Technology | Description |
|---|---|
| Oracle PL/SQL and Microsoft T-SQL (from a database extraction) | A source code package corresponds to one RDBMS extraction (that could  contain multiple schemas and therefore multiple Analysis Units), as such  all Analysis Units belonging to the same source code package are  analyzed together in one Execution Group, with no upper source code  limit. |
| ASP | One Execution Group is created per Analysis Unit. |
| C/C++ | One Execution Group is created per Analysis Unit. |
| Oracle Forms/Reports | One Execution Group is created per Analysis Unit. |
| Mainframe | One Execution Group is created per Analysis Unit. |
| JEE | One Execution Group is created for ALL Analysis Units. |
| .NET | One Execution Group is created for ALL Analysis Units. |
| SAP | One Execution Group is created per Analysis Unit. |
| Universal | One Execution Group is created per Analysis Unit. |

## Manually customizing Execution Groups

Manually customizing Execution Groups entails overriding the default rules that is used to group Analysis Units into Execution Groups. This is a highly technical feature and should only be used when you know exactly what you want to achieve. Currently, customizing Execution Groups is only possible for the following technologies:

- C/C++
- JEE
- .NET

To customize Execution Groups, you need to have at least two Analysis Units present in the version for the given technology. The example below shows one Analysis Unit in a default Execution Group (Default execution group) and two Analysis Units that have been moved into a custom Execution Group (TEST):

![](eg.jpg)

| Option | Description |
|---|---|
| ![](eg1.jpg) | Manually run an analysis for the entire Execution Group - i.e. analyze all Analysis Units in the Execution Group. |
| ![](eg2.jpg) | Allows you to bulk activate or deactivate specific Analysis Units through the use of checkboxes next to each Analysis Unit. |
| ![](eg3.jpg) | Modify the Execution Group for the current Analysis Unit. This option will show the following popup:<br><br>![](egA.jpg)<br><br>Entering a new name for an Execution Group will do the following:<br><br><ul><li>If the name entered into the box does not exist yet, a new Execution Group will be created using the chosen name and the current Analysis Unit will be moved to this new Execution Group.</li><li>If the name entered into the box matches an Execution Group that already exists, the current Analysis Unit will be moved to this existing Execution Group.</li><li>If there is only one Analysis Unit in the Execution Group, the existing Execution Group will be renamed to the name entered into the box.</li></ul> |
| ![](eg4.jpg) | Enable/disable  the analysis of the current Analysis Unit (this is valid when running  an analysis for the Execution Unit or whether running an analysis at  Application level). |
| ![](eg5.jpg) | Only visible on custom created Execution Groups:<br><br>![alt text](egB.jpg)<br><br><ul><li>Rename the current Execution Group.</li><li>Ungroup  the Execution Group - in other words, delete the Execution Group and  move all Analysis Units back into the default Execution Group.</li></ul> |

## Things to note

### Ungroup

Analysis Units that are grouped manually in a given Execution Group should always, from that point onwards, be analyzed together in the same Execution Group to retain the integrity of analysis results. If, however, the total source code size of the Analysis Units in the Execution Group exceeds the Execution Group max size threshold in Version N+1, AND the analyzer starts to crash (because the threshold has been exceeded) then there is no alternative but to "ungroup" an Analysis Unit from the Execution Group so as the Execution Group max size threshold is not exceeded. You should be aware that this could change the results of your analyses and should be avoided if possible.

### Impacts of source code size increase between versions

Take the following example:

- In V1 only one Execution Group (A) was created for all Analysis Units in the Application
- In V2, the source code size has increased and exceeds the Execution Group max size and CAST now decides to create additional Execution Groups  (A and B) to accommodate the new source code size

If an Analysis Unit was part of Execution Group A in V1 and is now part of Execution Group B in V2, then objects in the Analysis Unit will appear as removed and newly added in the CAST Engineering Dashboard.

### Impacts of source code size decrease between versions

Take the following example:

- In V1 two Execution Groups (A and B) were created for the Analysis Units in the Application because the source code size of all Analysis Units exceeded the Execution Group max size
- In V2, the source code size has decreased and is now within the Execution Group max size - CAST now decides to create only one Execution Unit (A)

If an Analysis Unit was part of Execution Group B in V1 and is now part of Execution Group A in V2, then objects in the Analysis Unit will appear as removed and newly added in the CAST Engineering Dashboard.