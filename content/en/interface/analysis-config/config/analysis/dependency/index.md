---
title: "Manual dependency configuration"
linkTitle: "Manual dependency configuration "
type: "docs"
no_list: false
weight: 10
---

***

## Overview

Dependencies are an important component of the CAST analysis process: they determine some client/client and client/server links between objects. For the vast majority of situations, dependencies are automatically configured at Application level for each technology and are configured between individual Analysis Units/groups of Analysis Units.

Sometimes, however, dependencies are not configured automatically, particularly for languages managed via the **Universal Analyzer** (typically HTML5/JavaScript, SQL etc.) and this can sometimes lead to missing links and therefore issues with transactions and other analysis results produced by CAST. In this situation, during the analysis review and configuration dependencies may need to be created manually, however, this can be a time consuming process.

To counter this, it is possible to force the creation of a dependency between specific languages/technologies and which is valid for every single application managed on a specific Node - in other words, once this is configured, users will no longer need to manually create dependencies when CAST Imaging has not automatically created them.

The following can be achieved:

- Creation of a dependency between any Technology and any Language of another Technology (this is mainly designed for Universal Analyzer based analyzers which may have multiple languages, but can also be used for non-Universal Analyzer analyzers).
- Creation of a dependency between any Language of another Technology and any Technology.
- Creation of a dependency between any Language and any Language.

## Creating permanent dependencies

The creation of a permanent dependency is actioned via a file called `dependencies-matrix.xml` located on a node (open using a text editor) - you will need to repeat this process for each Node on which you want the same configuration. The file already contains default rules and information that should not be altered:

```text
%PROGRAMDATA%\CAST\Imaging\CAST-Imaging-Analysis-Node\dependencies-matrix.xml
```

New dependencies can be added anywhere between the existing `<dependencies>` tags. Any changes made to the file require a restart of the corresponding Node.

### Creation of a dependency between any Technology and any Language of another Technology

The following syntax is required:

```text
<technology symbol="source_root_technology">
	<allow symbol="target_language" type="language"/>
	<allow symbol="target_language" type="language"/>
	...
</technology>
```

Where:

- the `source_root_technology` item defines the source technology and should use a defined technology name - i.e. one that is already defined in the `dependencies-matrix.xml` file. It must be a single technology (multiple technologies are not supported).
- the target_language item defines the target language and should contain one language only. You cannot provide a list. If you need to specify more than one target, add an additional `<allow>` tag. Example target languages:
    - PLI
    - PLC
    - SQL
    - RPG400
    - PERL
    - PHP
    - HTML5
    - ...

For example, the following will create a permanent dependency between the technology "Sybase ASE sql server" (already defined in the `dependencies-matrix.xml` file) and the target languages `SHELL`, `DTA400` and `CL400`:

```text
<technology symbol="sqlserveranalysis.SybaseDefaultTechnology">
	<allow symbol="SHELL" type="language"/>
	<allow symbol="DTA400" type="language"/>
	<allow symbol="CL400" type="language"/>
</technology>
```

Save the file and restart the Node service so that the changes you have made are taken into account.

### Creation of a dependency between any Language of another Technology and any Technology

The following syntax is required:

```text
<technology symbol="source_language,source_language" type="language">
	<allow symbol="target_root_technology" type="technology"/>
	<allow symbol="target_root_technology" type="technology"/>
	...
</technology>
```

Where:

- the `source_language` item defines the source language and can contain either a single language name or a comma-separated list of languages. Example source languages:
    - PLI
    - PLC
    - SQL
    - RPG400
    - PERL
    - PHP
    - HTML5
    - ...
- the target_root_technology item defines the target technology and should use a defined technology name - i.e. one that is already defined in the `dependencies-matrix.xml` file. It must be a single technology (multiple technologies are not supported) - you cannot provide a list. If you need to specify more than one target, add an additional `<allow>` tag.

For example, the following will create a permanent dependency between the PL1 languages `PLI` / `PLC` and the target technologies `BusinessObjects` / `Mainframe` (already defined in the `dependencies-matrix.xml` file):

```text
<technology symbol="PLI,PLC" type="language">
	<allow symbol="boanalysis.BODefaultTechnology" type="technology"/>
	<allow symbol="mainframeanalysis.MainframeDefaultTechnology" type="technology"/>
</technology>
```

Save the file and restart the Node service so that the changes you have made are taken into account.

### Creation of a dependency between any Language and any Language

The following syntax is required:

```text
<technology symbol="source_language,source_language" type="language">
	<allow symbol="target_language"/>
	<allow symbol="target_language"/>
	...
</technology>
```

Where:

- the `source_language` item defines the source language and can contain either a single language name or a comma-separated list of languages. Example source languages:
    - PLI
    - PLC
    - SQL
    - RPG400
    - PERL
    - PHP
    - HTML5
    - ...
- the `target_language` item defines the target language. It must be a single language (multiple languages are not supported) - you cannot provide a list. If you need to specify more than one target, add an additional `<allow>` tag. Example source languages:
    - PLI
    - PLC
    - SQL
    - RPG400
    - PERL
    - PHP
    - HTML5
    - ...

For example, the following will create a permanent dependency between the languages `DTA400`, `DB400`, `DDS400`, `RPG400`, `RPG300`, `CL400` and the target language `SQL`:

```text
<technology symbol="DTA400,DB400,DDS400,RPG400,RPG300,CL400" type="language">
	<allow symbol="SQL"/>
</technology>
```

Save the file and restart the Node instance so that the changes you have made are taken into account.