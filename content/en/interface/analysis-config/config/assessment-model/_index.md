---
title: "Application analysis configuration - Config - Assessment Model"
linkTitle: "Assessment Model page"
type: "docs"
no_list: true
weight: 50
---

***

## Overview

These options allow to you make basic configuration changes to the default Assessment Model delivered with CAST Imaging Core and any extensions that have been installed:

![](assessment_model.jpg)

At the core of the measurement mechanisms that are used during an analysis is the Assessment Model: a set of rules, sizing/quality measures and distributions in a hierarchy of parent Technical Criteria and Business Criteria that are used to grade an Application and to provide information about any defects (code violations) in the Application's source code. The set of rules, sizing/quality measures and distributions are predefined and preconfigured according to established best practices, however, it is possible to modify some aspects in order to match your own environment, for example:

- The weight (i.e. "importance") of a rule in its parent Technical Criterion. This value can be changed if some contributions are thought more or less important in a given context.
- The criticality of a rule (i.e. whether the rule is considered "critical" or not in a given context). This allows you to "threshold" the grade of the aggregated rule with the lowest grade of its "critical" contribution.
- Whether a rule is enabled or disabled in the next analysis.

At the current time, the following cannot be modified:
- sizing measures
- background facts
- quality measures
- quality distributions
- quality rule parameters and thresholds
- consolidation settings (full application, average of modules etc.)

{{% alert color="info" %}}<ul><li>Any changes that are made will not be actioned until the next analysis is run.</li><li>When a new extension is installed that contains an Assessment Model fragment and this fragment contains new values for weights, criticality and thresholds, any customization that you have made will be retained.</li></ul>{{% /alert %}}

### Need for homogenity

Modifying the Assessment Model is considered standard practice, however, these updates must be performed with care as the legitimacy of trend and comparison information greatly depends on the methodology you use for the update. If the Assessment Model is not homogeneous over time then context and assessment information cannot be compared. Even for one-shot assessments, users will tend to compare assessment results - outside of the dashboard context - from their previous experiences. Homogeneity is therefore as important in this one-shot perspective as in a multiple assessment perspective and you should proceed with care.

## Content

On accessing the option, a list of **Business Criteria** (also known as Health Factors) are displayed. Clicking a Business Criteria will reveal a list of child contributing **Technical Criteria** (by default 10 rows are listed, further rows are displayed using the pagination options at the bottom of the list):

![](tc.jpg)

Clicking a **Technical Criteria** will reveal a list of child contributing rules that are triggered during an analysis/snapshot:

![](child_crit.jpg)

## Making changes to the Assessment Model

Locate the rule you want to change (either in the list or using the search option) and then make the necessary changes. Any changes you make will not be applied until a new analysis is run. For example:

![](am_change.jpg)

{{% alert color="info" %}}<ul><li>When a rule is configured with a <code>0</code> weight, it is enabled (active) and will be triggered during an analysis, but it has no impact on any parent technical criterion, therefore it can be considered as a way to "preview" a rule without impacting the grade of the parent technical criteria and business criteria. In addition, rules with a <code>0</code> weight are never consolidated into the Measure schema, so will not appear in the Health Dashboard.</li><li>Additional information on grade calculation can be found here: <a href="https://doc.castsoftware.com/export/AIPCORE/Grade+and+compliance+calculation">Grade and compliance score calculation</a>.</li></ul>{{% /alert %}}

## Shared customization option

When the [Share Assessment Model Settings](../../../../administer/settings/global-configuration/assessment-model/share/)option is enabled, two things will occur:

- Users with the Application Owner [role](../../../../administer/settings/user-permissions) will no longer be able to make any changes to the Assessment Model for any application (the settings in the current configuration page will be visible but cannot be modified).
- Any rule that is customized and shared across all Assessment Models will have an icon displayed as shown below:

![](share_am.jpg)

## Update Assessment Model option

This option is only available if your login has been granted the [ADMIN role](../../../../administer/settings/user-permissions):

![](update_am.jpg)

This option is aimed at the following scenarios:

- You want to perform a complete reset of your Assessment Model and apply an uncustomized Assessment Model based on the default Assessment Model delivered with CAST Imaging Core and the currently installed extensions.
- You have installed a new release of an extension and you want to ensure that all changes to weight, criticality, thresholds etc. that are implemented in the new release of the extension are applied in the next snapshot. Ordinarily, when a new release of an extension is installed AND that extension includes an Assessment Model fragment which includes changes to weight, criticality, thresholds etc., CAST Imaging will NOT apply the changes since the policy is to reduce the impact on analysis results. The Update Assessment Model option therefore provides a means to apply the changes provided in the extension release.

This option will perform the following actions:

- create a brand new Assessment Model using a combination of the base Assessment Model delivered with CAST Imaging Core on the Application's Node, and the Assessment Model fragments delivered with the currently installed extensions for your Application.
- apply this new Assessment Model for all future snapshots you action

You will need to generate a new snapshot to ensure that the new Assessment Model is applied and used:

- All customization that you have made to the current Assessment Model (i.e. change of weight, change of criticality, activate/deactivate rules) will be lost when this option is used since a brand new Assessment Model is created. Therefore you must ensure that all changes that you have made are recorded. You will need to re-apply those customizations after the update is complete - if you need to keep some of them.
- Using this option will impact your existing analysis/snapshot results when a new analysis/snapshot is run.
- Every time this option is used and a new snapshot is subsequently generated, a new Assessment Model will be created. You should therefore avoid using this option regularly since accumulating multiple Assessment Models can lead to a reduction in performance.
