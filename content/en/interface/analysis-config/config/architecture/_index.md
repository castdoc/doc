---
title: "Application analysis configuration - Config - Architecture"
linkTitle: "Architecture page"
type: "docs"
no_list: true
weight: 30
---

***

## Overview

Architecture Checker models are used to check the organization, architecture and structure of an Application's source code. Within a model, layers and dependencies between these layers are configured to reflect how an Application SHOULD/SHOULD NOT be structured. Each layer captures objects matching a selection criterion and each dependency specifies which link types are authorized or forbidden between these layers.

When a model is assigned to an Application, the model is checked during the application analysis and in its most simplistic form, a "violation" of this model will be reported when the analysis identifies a link between two objects belonging to different layers that does not respect the dependencies (authorized or forbidden) that have been defined in the model itself. 

One Architecture Model will be materialized as a rule named as follows:

`Architecture Check: <architecture_model_name>`

The rule will contribute to the **Architecture Models Automated Checks Technical Criterion**, which in turn contributes to the **Security Health Factor/Business Criterion**. Violations can be viewed both in the Architecture Studio and in the CAST Engineering Dashboard.

![](ed.jpg)

{{% alert color="info" %}}By default Architecture Models materialized as rules will have the following compliance thresholds (these cannot be changed):<br><br><ul><li>Minimum rule compliance percentage required to get a 4.00 grade = 99.0%</li><li>Minimum rule compliance percentage required to get a 3.00 grade = 90.0%</li><li>Minimum rule compliance percentage required to get a 2.00 grade = 70.0%</li><li>Minimum rule compliance percentage required to get a 1.00 grade = 10.0%</li></ul>{{% /alert %}}

This configuration page therefore enables you to do the following:

- View a list of all Architecture Models that have been assigned to the current application. These models will then be checked during the analysis and results produced. Architecture Models can be created, viewed and managed at a global level in the Architecture Studio.
- Architecture Models are published as specific extensions and this screen allows you to assign any number of existing Architecture Models to the current application using the **Add** button.

![](architecture.jpg)

## Content

### Add button

Use this button to attach a new Architecture Model to the Application. By default CAST Imaging will offer the Architecture Models that have already been created/uploaded and published using the [Architecture Studio](https://doc.castsoftware.com/export/AIPCONSOLE/AIP+Console+-+Architecture+Studio).

The **Add** button is disabled if there are no Architecture Models available (you will need to either create one or import one and then publish it), or there are Architecture Models available but they have not been published.

![](archi1.jpg)

A banner will then inform you that the **Install Models** action must be run in order for the Architecture Model to be taken into account. This action can be done manually or automatically as part of the next analysis (when it is run):

![](archi2.jpg)

The **Status** column indicates that the chosen model has been associated (included) with the current application. When the model has been installed, the Status will change to installed:

![](archi3.jpg)

If you have published a newer release of your Architecture Studio model (i.e you made a change and then republished it which will have incremented the version number), you will be offered the newer release:

![](archi4.jpg)

![](archi5.jpg)

### Rule ID

A unique identifier for your model. This ID is used to identify the Architecture Model throughout the CAST dashboards.

### Name

Name of the Architecture Model

### Weight
The weight of the rule. This value defines how "important" the rule is in the parent Technical Criterion "Architecture Models Automated Checks". This is defined when creating the model.

### Critical

Displays whether the rule defined by the Architecture Model is critical or not. This is defined when creating the model.

### Version

Displays the version number of the extension as defined in the [Architecture Studio](https://doc.castsoftware.com/export/AIPCONSOLE/AIP+Console+-+Architecture+Studio).

### Last Modified	

Displays the date the model was last modified. For imported Architecture Models that have not been subsequently modified in Console, the date the model was imported will be displayed.

### Status

Can display either:

- **included** > the Architecture Model has been associated to the current application but the "Install Models" action has not yet been run

![](status1.jpg)

- **installed** > the Architecture Model has been installed, meaning that it will be taken into account during the next analysis:

![](status2.jpg)
