---
title: "Modules - User Defined Modules"
linkTitle: "User Defined Modules"
type: "docs"
no_list: true
---

***

## User Defined Modules

In addition to automatic strategy modules, CAST Imaging offers you the option to create User Defined Modules - these Modules can be configured to contain the exact content you require. Content is configured using:

- a Filter on your analysis results, either:
    - via the UI creating an **Object Filter** (using the Technology, Analysis Units and Object Names filter criteria combination) in order to define the content you require. Certain rules are applicable for User Defined Modules created using an Object Filter::
        - you can add multiple Object Filters per User Defined Module
        - you can choose only one Technology (the drop down will show the technologies found in your application source code)
        - when you have chosen the Technology, the analysis units list will show only analysis units belonging to the selected technology
        - if you do not choose a Technology, the analysis unit list will show all available analysis units in your application
        - you can select multiple analysis units
        - you can add multiple Object Name patterns: the filter result includes all the results of the all the name patterns you have added
        - the Object Name pattern choices are Name, Full Name or Path, with operators Equals, Like or Match
    - via the GUI creating an **Explicit Filter** using an SQL query
- Module per Technology
- Module per Analysis Unit
- Module per Subfolder

## Add a User Defined Module

To add a User Defined Module, first change the auto strategy to either of the following (when the auto strategy is set to any other option, it is not possible to create user defined modules):

- None (Manual Creation)
- Unassigned (recommended)

![](udm1.jpg)

The **Add** button should now be enabled - click it and choose the option you require:

![](udm2.jpg)

Each option is explained below.

### Module - based on a custom Filter

This option allows you to create a module using a custom Filter. You can create:

- multiple Object Filters via the UI
- a single Explicit Filter using a SQL query
- a combination of either

![](customfilter.jpg)

#### Object Filter

The Object Filter screen will be displayed:

![](customfilter1.jpg)

By default, a new User Defined Module will be provided with an empty filter called **"Filter1"**. This filter contains no configuration at all, however, if left as is, it will return all objects associated with the Application (identical to the **Full Content** module). This is of particular importance if you choose to configure an **Explicit Filter** - if the empty **"Filter1"** is left as is, then it will override any **Explicit Filters** you create and return all objects (note that you can use **Filters** and **Explicit Filters** together - see [Behaviour of User Defined Modules using Object Filters](#behaviour-of-user-defined-modules-using-object-filters)).

##### Module Name

Choose a name for the User Defined Module (the name you give to a User Defined Module will form the name of the Module in the results). The name of the Module should only use the following permitted characters - any unauthorized characters will result in an error message:

- 0123456789
- abcdefghijklmnopqrstuvwxyz
- ABCDEFGHIJKLMNOPQRSTUVWXYZ

##### Description

Free text field allowing you to create a description for this module.

##### Filter Name

You can include as many filters as required - enter a name for each Object Filter, or leave as default.

##### Technology Selection

Click the drop down list box to choose a Technology to filter on - the list of available technologies results from the most recent source code delivery (you do not need to have run an analysis). All subsequent options in the filter will then be restricted to this Technology. It is not obligatory to select a Technology: if you do not restrict the Technology to one specific technology, then all subsequent options will also not be restricted.

##### Analysis Unit Selection	

Click **Add** to choose an Analysis Unit to filter on - the list of Analysis Units results from the most recent source code delivery (you do not need to have run an analysis) will be displayed. All subsequent options in the filter will then be restricted to the chosen Analysis Unit(s). It is not obligatory to select an Analysis Unit: if you do not restrict the Analysis Unit, then all subsequent options will not be restricted.

Use the **"+"** icon to select the Analysis Unit(s) you want to filter on:

![](customfilter2.jpg)

A new dialog box will open allow you to select the Analysis Unit(s) - tick the items you want as shown below. This dialog shows all available Analysis Units in the current delivery - by default all Analysis Units will be displayed, even those that are already used in other Modules (the Modules column lists the existing Modules which the Analysis Unit is used in). You can also enable the **Show only not used option** if you only want to see Analysis Units that are not already used in existing modules:

![](customfilter3.jpg)

Selected Analysis Units will automatically appear in the filter:

![](customfilter4.jpg)

##### Object Name Selection

Click to **Add** to create a new **Object Name Selection** (you can include as many Object Name Selections as required):

![](customfilter5.jpg)

###### Field	
Choose the option you require:

- **Name** - the object's short name, for example for a Java function "createStatement"
- **Full Name** - the object's full name, for example for a Java function `java.sql.Connection.createStatement`
- **Path** - the path to the file storage location of the object.

Note:

- Care should be taken when using the Path criterion for Object Name Selection: in some technologies (such as .NET) objects in one path may hold objects that are present in another path and as a result, you may find that because all children and descendants are included, some objects may be present in multiple paths. As a result an object may be present in multiple modules. As an alternative for the .NET technology, you could:
    - identify namespaces of the items you want and use the Full Name criterion to define the module.
    - use an explicit filter based on a SQL query to define the module.
- CAST highly recommends consulting the CAST System View `CSV_FILE_OBJECTS` located in the CAST Analysis Service schema to find out how object names and paths have been recorded.

###### Operator

Choose the operator you require:

- **EQUALS** - use this option to exactly match the Pattern you enter (equivalent to the "=" in SQL)
- **LIKE** - use this option in conjunction with the `%` operator to match similar objects (equivalent to the `LIKE` in SQL):
    - The pattern `sales%` on Name will return objects such as `sales`, `salesdetails`, `salesaddresses`
    - The pattern `%.%..%uth%` on Full Name will return objects such as `SERVER.CASTPUBS..AuthorIDCodes`, `SERVER.CASTPUBS..on_delete_authors`
    - The pattern `%.frm` on Path will return all objects whose path ends in `.frm`
- **MATCH** (Perl Regular Expression) - Use this option if you require a more complex pattern based on a Regular Expression.

###### Pattern

Enter the string or expression you want to match.

###### Delete

Click the delete button to remove the current Object Filter.

#### Explicit Filter

This option allows you to define a specific list of objects that will be included in a Module. You can use this tab in conjunction with the Object Filters option - see [Combining Object Filters and Explicit Content](#combining-object-filters-and-explicit-content) for more information about how the two filters can function together. The list of objects is determined by the insertion of an SQL query that will be run against the CAST Analysis schema and include any objects that match your query:

![](explicitfilter.jpg)

The SQL query that needs to be inserted into the editor must use a CAST pre-defined format. The basic format is as follows:

```sql
delete from §CI_OBJECTS_SET where set_name = '$(ModuleName)';

insert into §CI_OBJECTS_SET(set_name, object_id, error_id)
select '$(ModuleName)', o.object_id, 0
from §CDT_OBJECTS o
where /*fill where clause here*/
```

This query essentially does the following:

1. Empties the `CI_OBJECTS_SET` table in the CAST Analysis schema of any existing entries pertaining to the current User Defined Module you are working on (this is determined automatically by the `$(ModuleName)` macro). `§` symbol is replaced with application local schema name. 
1. Inserts new data into the `CI_OBJECTS_SET` table that determines which specific objects must be included, based on:
    - the `CDT_OBJECTS` table in the CAST Analysis schema (a table that lists all objects that have been saved into the CAST Analysis schema as a result of analyses that have been successfully completed)
    - a where clause that lets you extract specific items from the `CDT_OBJECTS` table

For example, to specify ALL objects in the CAST Analysis schema that are "SQL Tables", modify the basic format SQL by adding the text in the where clause:

```sql
delete from §CI_OBJECTS_SET where set_name = '$(ModuleName)';
 
insert into §CI_OBJECTS_SET(set_name, object_id, error_id)
select '$(ModuleName)', o.object_id, 0
from §CDT_OBJECTS o
where object_type_str = 'SQL Table'
```

{{% alert color="info" %}}SQL validation is not performed on <strong>Create/Edit</strong> or on <strong>Check Content</strong>. If the editor contains a syntax error:<ul><li>When you click <strong>Create/Edit</strong> and the editor contains a syntax error, then the content is saved regardless, but when you try to run the job an error will occur.</li><li>If you click <strong>Check Content</strong> and the editor contains a syntax error, an error will occur.</li></ul>{{% /alert %}}

#### Creating the filter

When the Object Filter configuration is complete, use the **Create** button in the bottom right corner to create the module:

![](customfilter6.jpg)

Your new User Defined Module will be listed in the list of modules:

![](customfilter7.jpg)

#### Check a User Defined Module - Check Content

If you have already run an analysis, you can use the **Check Content** button to check which objects will be matched by the User Defined module and the filters you have defined:

![](customfilter8.jpg)

Objects matched by the User Defined module and filters will be displayed in two panels to the right:

![](customfilter9.jpg)

### Module per technology

This option will create Modules based on the Technologies that have been "discovered" in your source code. One Module will be created for each Technology that has been delivered in the Version. The modules will be created immediately and then generated at the start of the next analysis/snapshot:

![](techmod.jpg)

### Module per Analysis Unit

This option will create Modules based on the Analysis Units that have been "discovered" in your source code.  One Module will be created for each Analysis Unit that has been discovered in the Version. The modules will be created immediately and then generated at the start of the next analysis/snapshot:

![](aumod.jpg)

### Module per Subfolder

This option allows you to create Modules based on the folders delivered in your source code Version. A configuration screen will be displayed allowing you to select the folder. One module will be created for each Subfolder that is a child of the selected folder. The modules will be created immediately and then generated at the start of the next analysis/snapshot:

![](submod.jpg)

![](submod2.jpg)

## Editing an existing User Defined Module

To edit an existing User Defined Module, find it in the list of modules and click the Edit button:

![](editmod.jpg)

For modules created via an automatic strategy choice (Full Content, Per analysis unit), while you can use the Edit button any changes you make cannot be saved.

## Deleting a User Defined Module

To delete an existing User Defined Module, find it in the list of modules and click the Delete button:

![](deletemod.jpg)

- Deleting a module does not delete the module as stored in the Application schemas, therefore results will not be impacted. Changes are only visible in the resultss when a new analysis or snapshot is generated.
- For modules created via an automatic strategy choice (Full Content, Per analysis unit), you can delete a module, but it will be recreated at the start of the analysis/snapshot generation process.

## Behaviour of User Defined Modules using Object Filters

### Combining Object Filters and Explicit Content

When defining a User Define Module, it is possible to combine various different filter options. A User Defined Module can consist of 0 to N Object Filters (defined in the Filters tab) and 0 to 1 Explicit Filters (defined in the Explicit Filters tab). The result of these options is derived using a UNION between the Object Filter(s) and the Explicit Filter.
By default, a new User Defined Module will be provided with an empty filter called "Filter1". This filter contains no configuration at all, however, if left as is, it will return all objects associated with the Application (identical to the Full Content module). This is of particular importance if you choose to configure an Explicit Filter - if the empty "Filter1" is left as is, then it will override any Explicit Filters you create and return all objects.

### Filters

A Filter will target objects using an `INTERSECTION` of the various filter types. A target object must:

- (if the filter is active) belong to a filtered Technology
- (if the filter is active) `AND` belong to one of the selected Analysis Units that function using a `UNION` between Analysis Units
- (if the filter is active) `AND` match one of the expressions on the Name, Full Name and Path that function using a `UNION` between expressions
- (if the filter is active) `AND` group together all conditions on the same field (`Object Name`, `Object Full Name`, or `Object Path`) using the same operator (`=` or `LIKE`): these grouped expressions are handled as `UNIONS` between their respective criteria (`OR`); then, these three groups are handled as intersections (`AND`). For example given the following:

```sql

Object Name = 'A'
Object Name = 'B'
Object Name LIKE 'C%'
Object Name LIKE 'D%'
Object Full Name = 'X.Y'
Object Full Name = 'X.Z'
Object Full Name LIKE 'Z.T%'
Object Full Name LIKE 'Z.U%'
Object Path = 'a/b'
Object Path = 'c/d'
Object Path LIKE 'x/y%'
Object Path LIKE 'z/t%'
```

Leads to the following logical criterion:

```sql
(Object Name = 'A' OR Object Name = 'B')
AND
(Object Name LIKE 'C%' OR Object Name LIKE 'D%')
AND
(Object Full Name = 'X.Y' OR Object Full Name = 'X.Z')
AND
(Object Full Name LIKE 'Z.T%' OR Object Full Name LIKE 'Z.U%')
AND
(Object Path = 'a/b' OR Object Path = 'c/d')
AND
(Object Path LIKE 'x/y%' OR Object Path LIKE 'z/t%')
```

### Behaviour with regard to child objects

User Defined Modules will always include child objects, i.e:

- children of directly matched objects
- children of children and so on

This can sometimes be confusing when using user defined modules defined on full paths in CAST dashboards: violations will be displayed for objects that may not necessarily match the the full path defined in the filter. This is because some objects may be child objects. CAST recommends using object filters based on Analysis Units rather than on the full path to avoid having violations returned for child objects that are not specifically part of the filter.

Take the following example, if the filter is based on the full path, for instance `SomeCode\NextCode\Comments\src`, it will retrieve the file `comment1.cs` and its children, and children of these children etc. In this situation, we can expect that the namespace `viewComment` and objects of the class `comment1` to be part of the module. However, since we have matched the namespace `viewComment` (a child of the `comment1.cs` file), the class `comment2` will also be part of the module as it is a child of the namespace:

![](example.jpg)