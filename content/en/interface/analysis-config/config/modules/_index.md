---
title: "Application analysis configuration - Config - Modules"
linkTitle: "Modules page"
type: "docs"
no_list: true
weight: 20
---

***

## Overview

Modules are used extensively as a means to configure analysis results into meaningful groups or sets for display purposes:

![](imaging_module.jpg)

![](ed_modules.jpg)

Indeed, objects that are not part of a module:

- cannot be seen in results
- cannot be included in Architecture Models
- cannot be included in Transaction/Function Point configurations

The definition of modules within applications also impacts the accessibility and usability of application assessment results. Specifically, in the CAST quality and quantity model, the module is the smallest assessment entity. The definition of a module can improve the relevance of the analysis results by linking modules to development teams and application functional layers etc. See also [Defining consolidation weighting settings](https://doc.castsoftware.com/export/AIPCORE/Defining+consolidation+weighting+settings) for more information.

The content of a module is based on source code. CAST offers various automatic module strategies for display of source code, and sets the **Full Content** option by default (see [Auto Generate](#auto-generate) for more information about each strategy). These automatic module strategies can be modified only **after** an initial analysis is complete using the configuration options explained below.

## Content

![](modules_screen.jpg)

### Auto Generate drop down

See [Auto Generate](#auto-generate) for more information.

### Ignore Empty Modules

This option is enabled by default. When enabled, any modules (whether auto created or [User Defined](udm/) modules) will be ignored during the analysis if they do not contain any objects. If you disable this option, please ensure that all modules (whether auto created or User Defined modules) contain at **least one object** otherwise the analysis will fail.

### Add	

See [User Defined](udm/) modules below. This button is only active when the Auto Generate option is set to:

- None (Manual Creation)
- Unassigned objects

### Global delete	

Use this option to delete multiple modules, selected using the check boxes:

![](global_delete.jpg)

{{% alert color="info" %}}Note that when the Auto Generate option is set to **None (Manual Creation)** and you delete all existing modules, a warning is displayed to ensure that you have at least one module.{{% /alert %}}

### List of modules

A list of modules created in the previous analysis for your application.

- **Generation type**: type of module. This can be either:
    - User defined
    - Full content
    - Per analysis unit
- **Description**: a description of the module:
    - **Full content module**: corresponds to auto Full Content option
    - **Unassigned module**: corresponds to the "catch all " module created using auto Unassigned objects option
    - **Technology module**: corresponds to manual per Technology option
    - **Folder path module**: corresponds to manual per Subfolder option
    - **Analysis Unit module**: corresponds to manual per Analysis Unit option
    - **Free text/blank**: corresponds to a manual module created using Object Filters     
- **Edit/Delete icons**: edit or delete a selected module. For modules created via an automatic strategy choice (Full Content, Per analysis unit):
    - While you can use the **Edit** button any changes you make cannot be saved.
    - You can delete a module, but it will be recreated at the start of the analysis/snapshot generation process.

## Auto Generate

The **Auto Generate** option enables you to change the automatic module strategy chosen for your Application. These modules will only be created when you run a new analysis, therefore you cannot view them in this list until the analysis has completed.

![](auto_gen.jpg)

### None (Manual Creation)	

If you select this option, no Modules are created at all in the next analysis. This effectively disables the automatic strategy entirely. In this situation, you should always create a [User Defined](udm/) module so that your results can be consulted. Choosing this option enables the **Add** button enabling you to create User Defined modules.

Selecting this option and not creating at least one User Defined module or having at least one Module listed in the list of modules will cause the next analysis to fail with the following error:

```text
Error: Internal error occurred. 
Return value: 1000 
  The application is closing. Call CAST support now providing the log file at \AipNode\data\logs\external_logs\c7ea2ce1-77bb-4492-ae85-7342be6a1f48\analyze\analyze-20200722-152438.txt 
  Source Server / My_APP: Missing Generate one module per : 
  Source Server / my_APP: Missing User Defined Modules
``` 

### Full content	 

This is default "automatic" setting where one Module is created for your entire Application during the next analysis, i.e. it will contain all the source code for your Application. Any modules you have added yourself ([User Defined](udm/) modules) will ALSO be created.

### Per analysis unit	

If you select this "automatic" option, one Module is created per Analysis Unit that is created during the next analysis in your Application. Any modules you have added yourself ([User Defined](udm/) modules) will ALSO be created.

### Unassigned objects	

This option is recommended in situations where you wish to use your own User Defined modules. When selected, all other automatic strategy options will be disabled and an automatic module called **Unassigned** will be created to "catch" any objects that were not assigned to one of your User Defined modules.

Choosing this option enables the **Add** button enabling you to create [User Defined](udm/) modules.

{{% alert color="warning" %}}Note that the **Unassigned** module is only visible in CAST Dashboards. It is NOT visible in Viewer, i.e. it is filtered out during the result generation process, therefore you should avid using this strategy option and instead create a dedicated User Defined module for those objects you want to see in Viewer.{{% /alert %}}

## Making changes to the settings

Any changes made in the Modules configuration page (e.g. changing the module configuration) will cause a banner to appear:

![](banner.jpg)

This indicates that the changes you have made will impact your analysis results and therefore that the data that is used for the Architecture Studio, Transaction configuration and results will not be up-to-date. Clicking the **Update** button in the banner will run a special step called **Prepare analysis data** that should complete relatively quickly (more quickly than a full analysis or snapshot) and ensures that the data used by the Architecture Studio, Transaction configuration and results is up-to-date based on the changes you have made. 

If you make a change to the module strategy itself, you will need to run a new analysis in order for the changes to be taken into account - if you want the modules to be visible in the CAST dashboards, then a snapshot will also need to be generated. Remember that:

-  any existing modules created by another automatic strategy will be retained and the modules from the newly selected automatic strategy will be created at the start of the analysis/snapshot generation process. 
- changing strategy will impact existing results.
- choosing **None (Manual Creation)** for the automatic strategy means that you must create at least one [User Defined](udm/) module or have at least one Module listed in the list of modules, otherwise the analysis/snapshot will fail.
