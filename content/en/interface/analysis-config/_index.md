---
title: "Application analysis configuration"
linkTitle: "Application analysis configuration"
type: "docs"
no_list: false
weight: 20
---

***

The **Application analysis configuration** panel comprises multiple sub pages, all used to configure your application analysis. The **Overview** sub page is displayed automatically immediately following an initial fast scan of your application, other pages are only available once an application analysis has been completed. All pages allow you to manage all aspects of the your application analysis.

![](config.jpg)

To access this panel use the **Analysis configuration** option in the [Application landing page](../landing-page/):

![](access.jpg)

Choose the page would like to know more about from those listed below.