---
title: "Application analysis configuration - Logs"
linkTitle: "Logs page"
type: "docs"
no_list: true
weight: 20
---

***

## Overview

The **Logs** page displays all logs for any job related to the current application:

![](logs.jpg)

For example, you can:

- view logs
- view number of errors/warnings in the log
- view number of alerts

![](logs2.jpg)

There are also **Search** and **Filter** options available:

![alt text](logs5.jpg)

- The **Search** option is currently limited to search the log title. No searches are made in the logs themselves.
- The **Logs** with issues option will, when enabled, show only logs that have warnings or missing files.

## Alerts

The alert icon indicates that the most recent job actioned on the current application contains one or more alerts (the number of alerts is indicated) that need to be looked at. Clicking the icon will display the alerts as a slide in panel on the right (these alerts are also displayed in the Job Progress panel):

![](logs3.jpg)

![](logs4.jpg)

## Digging into specific logs

To view specific logs, click the main item to expand the section and view individual actions - for example to view the analysis logs, click the **Run analysis** item:

![](dig.jpg)

All steps in the analysis process are displayed individually in the left hand panel, together with any error or warnings. The right hand panel shows the log details of the selected step:

![](dig2.jpg)

To view log details about a specific step, click the step in the left hand panel:

![](dig3.jpg)

Some steps will offer an easy to read summary, for example when there are specific warnings or errors flagged in the analysis:

![](dig4.jpg)

In this example, one warning has been flagged. The default display will show a summary displaying only the relevant error or warning, listed by ID.

 Detailed log message ID information is taken directly from CAST Extend. If you do not see it (and only see the detailed log file), then:

- either your connection to CAST Extend is not functioning
- or you are using CAST Extend local server in offline mode
- or you are using an older release of CAST Extend local server which does not support this feature (≥ 1.1.1-funcrel is required).

![](dig5.jpg)

Generic log messages for warnings, errors and syntax errors are displayed as well as specific messages with IDs:

![](dig6.png)

You can also switch to detailed view if you would like to see more information:

![](dig7.jpg)

![](dig8.jpg)