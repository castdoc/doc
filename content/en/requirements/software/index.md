---
title: "What software do I need?"
linkTitle: "Software"
type: "docs"
no_list: true
weight: 20
---

***

{{< tabpane text=true >}}
{{% tab "Microsoft Windows" %}}
**OS**

- **Microsoft Windows** 10/11 64bit
- **Microsoft Windows Server** 2016/2019/2022 64bit (Server Core is not supported)

**Java**

- **Java JDK** (≥ 17 LTS and ≤ 20 64bit), with appropriate `%JAVA_HOME%` system environment variable. Most Java variants certified by the OpenJDK Community Technology Compatibility Kit are compatible (you can verify if a company has access to the TCK on the [OpenJDK website](https://openjdk.org/groups/conformance/JckAccess/jck-access.html)).

**Local user permissions - intallation**

- Local administrator privileges (ability to `Run as administrator` on Microsoft Windows)
- The ability to execute Powershell scripts is required during the installation process on Microsoft Windows machines
- The user account running a CAST Imaging installation requires `Full Control` permissions (either directly or as a member of a group) on the root folder where CAST Imaging components will be installed, i.e. if using the default locations:
  - `%PROGRAMFILES%\CAST\`
  - `%PROGRAMDATA%\CAST\`

![Full Control](full_control.jpg)

**Local user permissions - Microsoft Windows Services**
- The default user assigned to all CAST Imaging Microsoft Windows Services during the installation is `LocalSystem`: CAST highly recommends that this user is not used to run these Windows Services on a permanent basis. This is particularly true if you need to use a proxy service to access specific resources such as CAST Extend and when using shared network services for data storage for `deploy`/`delivery`/`common-data` folders (see [here](../file)). In this situation, the user running the Microsoft Windows Service will be used to access the proxy/shared network services. Instead, CAST recommends using the login credentials that match the user account used to install CAST Imaging - for example, this could be a specific "service account" that is created specifically for installing and running CAST Imaging. This service account would also therefore require access to the shared network resources and would need to be granted permission to use the system proxy settings. See the variables [`START_AS_USER`, `...START_AS_USER`, `START_AS_PASSWORD`, `...START_AS_PASSWORD` ](../../install/global/windows/variables) for more information about changing these during the installation.
- The user account ("service account") assigned to the Microsoft Windows Services requires `Full Control` permissions (either directly or as a member of a group) on the following folder if using the default locations:
  - `%PROGRAMFILES%\CAST\`
  - `%PROGRAMDATA%\CAST\`
  - `%APPDATA%\CAST\`
  - Shared network location for `common-data`/`delivery`/`deploy`.

{{% readfile "shared.md" %}}

{{% /tab %}}
{{% tab "Linux/Docker" %}}
**OS**

- **Linux 64bit** (derivatives of Red Hat / Debian - recent stable releases)

**Docker**

- **Docker Engine ≥ 20.10** (must be installed on the host machine) 
- **Docker Compose**, latest stable release embedded in Docker Engine recommended (or at least ≥ 2.x). Older installations of Docker Compose 1.x that require the `docker-compose` command (with hyphen) are not compatible. CAST installation scripts use the `docker compose` command (without hyphen).
- Access to [https://hub.docker.com/](https://hub.docker.com/) on `port 443` via `TCP`.

Containerization via Docker is only supported when using an installation of Linux installed direct on a physical or virtual machine, i.e. the use of WSL (Windows Subsystem for Linux) 1 or 2 on a Microsoft Windows machine, or using Docker Desktop is not supported.

**Local user permissions**

- User in the `sudoers` list.

{{% readfile "shared.md" %}}

{{% /tab %}}
{{< /tabpane >}}
