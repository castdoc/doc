</ul>

**CAST Extend**

- Access to [https://extend.castsoftware.com/](https://extend.castsoftware.com/) on `port 443` via `TCP` (unless you are using [CAST Extend local server](https://extend.castsoftware.com/#/extension?id=com.castsoftware.aip.extendproxy&version=latest))
- A valid [CAST Extend API key](https://extend.castsoftware.com/#/profile/settings)

**Licensing**

- A valid global CAST Imaging license key (provided by your CAST sales contact), either:
   - a `CD license` (Contibuting Developers)
   - a `Named Application` license

**Browsers for end user access**

| Browser | Minimum supported release |
|---|---|
| Microsoft Edge | 44 and above |
| Mozilla Firefox | 95 and above |
| Google Chrome | most recent only |
| Safari | 12 and above |