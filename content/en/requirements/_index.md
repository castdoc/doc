---
title: "System requirements"
linkTitle: "Requirements"
type: "docs"
weight: 20
no_list: true
---

***

{{< cardpanehome >}}
{{% cardhomereference %}}
<i class="fa-solid fa-computer"></i>&nbsp;[Hardware](hardware/)
{{% /cardhomereference %}}
{{% cardhomereference %}}
<i class="fa-solid fa-gears"></i>&nbsp;[Software](software/)
{{% /cardhomereference %}}
{{% cardhomereference %}}
<i class="fa-solid fa-database"></i>&nbsp;[Database](db/)
{{% /cardhomereference %}}
{{% cardhomereference %}}
<i class="fa-solid fa-hard-drive"></i>&nbsp;[Disk space](disk/)
{{% /cardhomereference %}}
{{< /cardpanehome >}}
