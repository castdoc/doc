---
title: "What hardware do I need?"
linkTitle: "Hardware"
type: "docs"
no_list: true
weight: 10
---

***

{{% alert color="info" %}}Hardware requirements depend on various factors related to the Application(s) you are analyzing and these must be considered:
- Number of lines of code (LOC)
- Number of objects after analysis
- Number of violations after analysis
- Technologies used
- Number of users accessing results

Due to these different factors, it is difficult to make precise hardware recommendations, therefore what we provide in this page is indicative only. We provide these figures as a guide to the absolute minimum required to run CAST Imaging and your own configuration will likely require better hardware. Installations on machines with resources that are lower than the minimum required or that barely meet the minimum requirements will have the following limitations:
- analysis runtime and SQL operations will be sub-optimal
- you cannot run more than one application in parallel
- disk space can be quickly overloaded
- in some cases installers may be blocked from running
{{% /alert %}}

## General hardware requirements

These requirements are valid for both **Microsoft Windows** and **Linux via Docker** deployments:

- Physical or virtual machine(s)
- CPU
   - Minimum 1 CPU / 2 cores, e.g.:
      - Intel Core i5, 2.6 GHz
      - Intel Xeon, 2.2 GHz
   - Recommended 1 CPU / 4 cores, e.g:
      - Intel Core i7, 2.8 GHz
      - Intel Xeon, 2.6 GHz
- RAM
   - Single machine as part of an enterprise/distributed deployment:
      - 16GB RAM absolute minimum, 32GB RAM highly recommended. On a machine configured as a **node** where the [com.castsoftware.securityforjava](../../technologies/jee/extensions/com.castsoftware.securityforjava/) extension is used for [Security Dataflow](../../interface/analysis-config/security-dataflow/) 32GB RAM is required.
   - Standalone mode (all components on one machine):
      - 32GB RAM absolute minimum
- Free disk space: 256GB minimum free disk space (SSD or equivalent - using storage/disk with high IOPS values (i.e. SSD disks or SANs configured with SSD) will achieve better performance) - see also [File storage](../file/) for more detailed information.
- Host machines with fixed IP address / hostname recommended

{{% alert color="info" %}}<ul><li>See <a href="#example-on-premises-hardware-configuration">Example on-premises hardware configuration</a> below.</li><li>Accessing and actively using CAST Imaging from the browser on the host machine (for example in a single machine installation scenario) may require GPU resources to be allocated to the machine to ensure optimum performance.</li></ul>{{% /alert %}}

## TCP ports

The following TCP ports are required:

{{< tabpane text=true >}}
{{% tab "Microsoft Windows" %}}
`imaging-services`

- 2381: CAST Imaging Control Panel
- 8090: CAST Imaging Gateway Service
- 8091: CAST Imaging Console Service
- 8092: CAST Imaging Authentication service
- 8096: CAST Imaging SSO Service
- 8097: CAST Imaging Dashboard Service
- 8098: CAST Imaging Control Panel
- 9002: CAST Imaging SSO Service

`analysis-node(s)`

- 8099: CAST Imaging Analysis Node

`imaging-viewer`

- 6372: Neo4j
- 7483: Neo4j
- 7484: Neo4j
- 7697: Neo4j
- 8093: CAST Imaging Viewer Frontend
- 8094: CAST Imaging Viewer AI Manager
- 8284: CAST Imaging Viewer source code
- 8285: CAST Imaging Viewer login
- 9010: CAST Imaging Viewer Backend
- 9011: CAST Imaging Viewer ETL

CAST Storage Service for Microsoft Windows:

- 2284

{{% /tab %}}
{{% tab "Linux/Docker" %}}
- 2285: IMAGING_SERVICES_CONFIG_DB_PORT
- 7473, 7474, 7687: IMAGING_VIEWER_NEO4J
- 8083: IMAGING_VIEWER_HTTP_PORT
- 8084: IMAGING_VIEWER_LOGIN_PORT
- 8090: IMAGING_SERVICES_PORT_GATEWAY
- 8091: IMAGING_SERVICES_PORT_CONSOLE
- 8092: IMAGING_SERVICES_PORT_AUTHORIZATION
- 8096: IMAGING_SERVICES_PORT_KEYCLOAK
- 8097: IMAGING_SERVICES_PORT_DASHBOARDS
- 9000: IMAGING_VIEWER_SERVER_PORT
- 9001: IMAGING_VIEWER_ETL_PORT
- 9980: IMAGING_VIEWER_SOURCECODE_PORT
- 8098: IMAGING_SERVICES_PORT_CONTROL_PANEL
{{% /tab %}}
{{< /tabpane >}}

{{% alert title="Note" color="info" %}}<br><ul><li>when distributing components across multiple machines ("enterprise" installation mode), not all ports will be required on all machines.</li><li>firewall rules may be needed to grant access to `port 8090` - this is the port used by end-users for all access requirements.</li></ul>{{% /alert %}}

## Example on-premises hardware configuration

### Managing multiple applications

If you are managing a large number of applications, we recommend installing multiple nodes to spread the load. The disk space allocated to a single node obviously depends on the size and the number of applications that will be analyzed by the node. In a situation where all the nodes are running analyses, some nodes may need to run more than one analysis in parallel. To avoid overloading the node where more than one analysis is running at the same time, we strongly recommend deploying machines with sufficient resources.

Therefore, to analyze up to 50 applications and to run up to 5 analyses in parallel on one single node with one associated CAST Storage Service/PostgreSQL instance, CAST recommends increasing RAM and DISK resources as follows:

| Component | RAM      | DISK                  |
|-----------|----------|-----------------------|
| Node      | 32GB min | 2TB (SSD recommended) |
| CAST Storage Service / PostgreSQL | 64GB min | 3TB (SSD recommended) |

### Analyzing complex applications

While 90% of JEE, .NET or Mainframe applications can be analyzed with the minimum requirements, some specific (very large or not well balanced) applications require more memory than the minimum recommendations. The following configurations are examples of sizing required for very large applications. The requirements are not a linear function that are based purely on the number of files or lines of code (LoC), instead it is more complex and there is no specific formula to use.

In general, a lack of memory will cause slowness (machines will resort to the use of virtual memory) in the best case, and a crash in the worst case. The numbers presented in the table below are purely indicative and depict the varying memory requirements:

| Application | Node CPU | Peak RAM | Node - RAM (recommended minimum) | Disk space |
|----------------------------------------------------------------------|------------------------|-----------|-------|----------|
| JEE application with 13,000 java files and 6,300 JSP files           |  2 processors, 4 cores | 22 GB     | 32 GB | 256 GB   |
| JEE application with 21,000 java files, 14 JSP files, 2,800 projects |  As above              | 10 GB     | 16 GB | As above |
| JEE application with 30,000 java files and 1,200 JSP files           |  As above              | 12 GB     | 16 GB | As above |
| .NET application with 18,785 C# files                                |  As above              | 12 GB     | 16 GB | As above |
| .NET application with 23,000 C# files and 4,100 cshtml files         |  As above              | 20 GB     | 32 GB | As above |
| Mainframe application with 10,000 COBOL programs                     |  As above              | 2 GB      | 8 GB  | As above |

{{% alert title="Note" color="info" %}}For JEE, the above does not include the [com.castsoftware.securityforjava](../../technologies/jee/extensions/com.castsoftware.securityforjava/) extension, which requires a minimum of 32GB RAM.{{% /alert %}}
