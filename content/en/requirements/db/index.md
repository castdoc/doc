---
title: "What are the database requirements?"
linkTitle: "Database"
type: "docs"
no_list: true
weight: 30
---

***

## General database requirements

CAST Imaging requires an RDBMS to store both the data generated during analyses and persistence data (settings/properties etc.). CAST supports only [PostgreSQL](https://www.postgresql.org/) as follows:

{{< tabpane text=true >}}
{{% tab "Microsoft Windows" %}}
- **[CAST Storage Service](https://extend.castsoftware.com/#/extension?id=com.castsoftware.css&version=latest)** - a PostgreSQL instance packaged and provided by CAST for installation on Microsoft Windows with its own custom installer and pre-configured settings.

or:

- an **official and supported (by CAST) PostgreSQL release** installed direct on a Linux machine, or through Docker via Linux.

Note that PostgreSQL installed on Linux has consistently proven to provide better performance than CAST Storage Service installed on Microsoft Windows and therefore CAST recommends this configuration where possible.
{{% /tab %}}
{{% tab "Linux/Docker" %}}
An **official and supported (by CAST) PostgreSQL release** installed direct on a Linux machine, or through Docker via Linux.

Note that when installing CAST Imaging on [Linux via Docker](../../install/global/docker/), CAST provides a database instance as a Docker image (see [below](#postgresql) for details). By default, this instance will be used by CAST Imaging for both analysis data and persistence data storage needs. However, you are free to install additional database instances in whatever configuration you require, for example to separate the storage of persistence data and analysis data on two separate instances.
{{% /tab %}}
{{< /tabpane >}}

## Requirements for analysis data

Three schemas will be created automatically in a database of your choice (`postgres` by default) for **each application** you onboard in CAST Imaging:
- `<application_name>_local`
- `<application_name>_central`
- `<application_name>_mngt`

An **additional schema** will be created automatically in a database of your choice (`postgres` by default) if you are using the CAST Health/Management Dashboard, where results for all applications are consolidated together:
- `general_measure` (or other custom name)

## Requirements for persistence data

- A database for authentication persistence data is required (`keycloak_v3` by default but the name can be customized) and will be created automatically during the installation - if this database already exists it will be re-used and this may cause unexpected behaviour
- Two schemas called `admin_center` and `analysis_node` will be created automatically in the database of your choice (`postgres` by default) during the installation - if these schemas already exist they will be re-used and this may cause unexpected behaviour

## Supported RDBMS releases

### CAST Storage Service

You can download this component [here](https://extend.castsoftware.com/#/extension?id=com.castsoftware.css&version=latest) and you can find out more information about how to install and configure it [here](../../install/db/css/). Supported releases are listed below:

| Release | Bundled PostgreSQL release | Architecture | Default port no. |                                         
|---|---|---|---|
| 4.13.x | 13.x | 64bit | 2284 |

### PostgreSQL

CAST does not provide an installer for Linux Operating Systems or Docker, however, you can install an official PostgreSQL release as listed in the table below on a 64bit Linux Operating System (supported by PostgreSQL) and use it as a storage host for CAST Imaging. 

| PostgreSQL release | Architecture | Notes | 
|---|---|---|
| 15.x | 64bit | CAST provides a database instance (v. 15.11) as a Docker image when [installing](../../install/global/docker/) CAST Imaging on Linux via Docker. |
| 14.x | 64bit | |
| 13.x  | 64bit | |

See [here](../../install/db/postgresql/) for more information about how to install and configure an instance direct on Linux, or [here](https://hub.docker.com/_/postgres) for more information about how to install an official PostgreSQL image on Docker.
