---
title: "What disk space requirements do I need?"
linkTitle: "Disk space"
type: "docs"
no_list: true
weight: 40
---

***

## Installation disk space requirements

These requirements are valid for both **Microsoft Windows** and **Linux via Docker** deployments:

### Single machine (standalone mode) deployment scenario

A CAST Imaging installation will consume approx. **20GB** of disk space. This includes the following (but does not include any result storage requirements):
- The CAST Imaging installer ZIP file
- The CAST Imaging installer unzipped on disk
- Installed software (whether Microsoft Windows programs or Docker images/containers), including CAST Imaging Core and the database component (CAST Storage Service for Microsoft Windows / PostgreSQL provided as a Docker image)

### Distributed (enterprise mode) deployment scenario
  
Each CAST Imaging component will consume differing amounts of disk space (not including any result storage requirements), therefore the total disk space for an installation depends on the combination of components you install on each machine. The figures below include:

- The CAST Imaging installer ZIP file
- The CAST Imaging installer unzipped on disk
- Installed software (for Linux/Docker, this includes Docker images, containers and volumes)

| Component | Size (Microsoft Windows) | Size (Linux/Docker) |
|---|---|---|
| `imaging-services` | approx. 3GB | approx. 2.5GB |
| `imaging-viewer` | approx. 4GB | approx. 3GB |
| `analysis-node` + Imaging Core | approx. 5GB | approx. 8.5 GB|
| CAST Storage Service/PostgreSQL | approx. 4GB | N/A (included in `imaging-services`) |

## Analysis and result storage disk space requirements
 
{{% alert color="info" %}}Specific disk space requirements for analysis and result storage depend on various factors related to the application(s) you are analyzing and these must be considered:
- Number of lines of code (LOC)
- Number of objects after analysis
- Number of violations after analysis
- Technologies used

Due to these different factors, it is difficult to make precise disk space recommendations, therefore what we provide in this page is indicative only. We provide these figures as a guide to the absolute minimum required.
{{% /alert %}}

These recommendations are valid for both **Microsoft Windows** and **Linux via Docker** deployments:

### Single machine (standalone mode) deployment scenario

- 256GB minimum
- Multiple complex/large applications: 1TB minimum

{{% alert color="info" %}}See also [Hardware requirements](../hardware/).{{% /alert %}}

### Distributed (enterprise mode) deployment scenario

The following assumes that each component is installed on a dedicated machine where we recommend **256GB** free disk space for the entire machine (see also [Hardware requirements](../hardware/).) If you are analyzing complex/large applications or you are doubling up components on one machine (e.g. `imaging-services` + `imaging-viewer`), you may need find that 256GB is sufficient initially, but CAST recommends increasing the disk space where possible:

| Component | Size  
|---|---|
| `imaging-services` | 256GB (minimum) |
| `imaging-viewer` | 256GB (minimum) |
| `analysis-node` | 256GB (minimum) - see also [Microsoft Windows analysis-node file storage locations](../../install/global/windows/node-file-location/) or [Docker/Linux analysis-node file storage locations](../../install/global/docker/node-file-location/). |
| CAST Storage Service/PostgreSQL | 256GB (minimum) |
| Shared network storage (for multi node scenario) `delivery`/`deploy`/`common-data` | 256GB (minimum) / 1TB (for multiple complex/large applications).<br><br>This requirement is specific to a multi-node deployment (distributed) scenario where all `analysis-node` components require read/write access to the same set of folders. You can read more information about this requirement in  [Microsoft Windows analysis-node file storage locations](../../install/global/windows/node-file-location/) and [Docker/Linux analysis-node file storage locations](../../install/global/docker/node-file-location/). |



