---
title: "2024-06-25: False positive anti-virus flag for CAST Storage Service"
linkTitle: "2024-06-25"
type: "docs"
no_list: true
---

<!-- A redirect exists for this page /notices/css-false-positive/ /notices/2024-06-25/ -->

***

## Notice

<table>
    <tbody>
    <tr>
    <td><strong>Impacted software</strong></td>
    <td>CAST Storage Service</td>
    </tr>
    <tr>
    <td><strong>Release</strong></td>
    <td>4.13.x</td>
    </tr>
    <tr>
    <td><strong>Date</strong></td>
    <td>2024-06-25</td>
    </tr>
    </tbody>
</table>

## Details

Some customers have reported that their anti-virus systems have flagged the following file as malicious:

```text
%PROGRAMFILES%\CAST\CastStorageService4\bin\wxmsw313ud_aui_vc_x64_custom.dll
```

This file is a certified .dll file provided by PostgreSQL (CAST Storage Service is a repackaged PostgreSQL database instance) and is used in the PGAdmin program delivered with CAST Storage Service 4.13.x and as such, this is a false positive report. If you are concerned, you should contact your anti-virus provider for further information.
