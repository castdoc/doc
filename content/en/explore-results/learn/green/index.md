---
title: "How green is your application?"
linkTitle: "How green is your application?"
type: "docs"
weight: 20
no_list: true
---

***

## Overview

Are you interested to find out whether your application uses specific programming practices that could be damaging to the environment, i.e, by requiring more CPU than necessary? CAST can provide this via a list of checks focused on programming practices and engineering principles that could be detrimental to the environment. These checks are run against your application during the analysis and any code that violates these checks is flagged and displayed. For example, checks such as "**Avoid queries without WHERE condition**" - i.e. flagging SQL queries that return many results resulting in increased processing power and time to complete.

This article explains how to quickly identify the use of environmentally deficient components in your application using CAST Imaging, therefore making their upgrade or removal a much easier and safer task.

## How does this feature work?

The information about green deficiencies is provided by [CAST Highlight's](https://www.castsoftware.com/highlight) SCA database: see the list of checks here: https://doc.casthighlight.com/greenpatterns/.

You will therefore need a **CAST Highlight subscription** in order to leverage this feature. In addition, your application must be fully analyzed in **CAST Imaging** ensuring that the extension [com.castsoftware.highlight2mri](https://extend.castsoftware.com/#/extension?id=com.castsoftware.highlight2mri&version=latest) is installed.

## How do I find out where the green deficiencies are located?

Your application needs to have the **Ready to view** status in the [Landing page](../../../interface/landing-page/):

![](../ready_to_view.jpg)

Then click **Ready to view**, expand the **Assess green impact** and select the **"Green Deficiencies"** tile:

![](tile.jpg)

{{% alert color="info" %}}If you do not see this tile, it's likely that your application does not meet the requirements (see above). Contact your CAST representative to discuss how to obtain a CAST Highlight subscription.{{% /alert %}}

Clicking the **Green deficiencies** tile will display the following screen, where all outdated third-party components are listed:

![](panel.jpg)

| Item | Description |
|---|---|
| Name | Lists the name of **Green deficiency** check that has been violated. |
| Category | The **category** to which the Green deficiency check belongs - these are determined by CAST Highlight. |
| Object Count | Displays the **total number of objects** within the application that violate the Green deficiency check. Additionally, you can filter by the object count value using the icon in the header (you can enter the count value and select on the option from the list). |

{{% alert color="info" %}}You can also access Green deficiencies via the [Insights](../../../interface/viewer/right-panel/insights/) menu option in the [right-panel](../../../interface/viewer/right-panel/):<br><br>![](right-panel.jpg){{% /alert %}}

## Where are the green deficient items used in my application?

Once you have a list of the green deficient items, the next step is find out where these components are being used in your application so that you can plan to update them. The right panel shows a high-level view of the items containing the deficient code. Clicking the **Investigate** button opens up the relevant objects and nodes in full screen mode. A hypothetical example is explained below.

### Example

Take the following example where the check **"Avoid calling a function in a condition loop"** has been violated a number of times. Look at the right panel high-level overview to see adherence:

![](example1.jpg)

Next click the **Investigate** button to drill-down to the details - this will help you understand the impact of updating the item, or if you want to remove it entirely. You can also save this information as a view in order to plan the update:

![](../save.jpg)

It is also possible to view the exact piece of source code that defines where your custom object is calling the the class - click the link between the objects to do so:

![](../code.jpg)

Finally, you can add a [Post-It](../../../interface/viewer/right-panel/post-its) to the view to inform your co-workers what needs doing:

![](sticky.jpg)