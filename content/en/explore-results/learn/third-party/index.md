---
title: "Dealing with outdated third-party components"
linkTitle: "Dealing with outdated third-party components"
type: "docs"
weight: 10
no_list: true
---

***

## Overview

Keeping track of external/third-party packages or components used in a software application and obtaining visibility on their versions, vulnerabilities, and adherence with rest of the software is crucial for several reasons:

- Understanding the external packages and their versions helps ensure compatibility and proper functioning of the software. Different versions may introduce new features, bug fixes, or even breaking changes.
- Monitoring for known vulnerabilities in external packages is essential for maintaining the security of the software. Many vulnerabilities are discovered and patched over time, and using outdated or vulnerable versions can expose the application to potential security risks.
- Overall, having visibility helps to maintain software integrity and security. It enables developers to make informed decisions regarding updates and patches thereby reducing potential risks in the software.

This article explains how to quickly identify the use of external/third-party components in your application using CAST Imaging, therefore making their upgrade or removal a much easier and safer task.

## How does this feature work?

The information about outdated third-party components is provided by [CAST Highlight's](https://www.castsoftware.com/highlight) SCA database: containing 118+ million Open-Source components gathered from various forges that we crawl such as Github, GitLab, Maven Central, NPM, NuGet, RubyGem, Packagist, etc. Depending on the component name and version detected in an application scan, CAST Highlight finds possible vulnerabilities (CVEs) by cross-referencing the National Vulnerablity Database (NVD) from NIST, across 150+ thousand known vulnerabilities.

You will therefore need a **CAST Highlight subscription** in order to leverage this feature. In addition, your application must be fully analyzed in **CAST Imaging** ensuring that:

- the relevant fields in [CAST Highlight Settings](../../../administer/settings/system-settings/highlight/) are populated
- the extension [com.castsoftware.highlight2mri](https://extend.castsoftware.com/#/extension?id=com.castsoftware.highlight2mri&version=latest) is installed

## How do I find out which of my application's components are outdated?

Your application needs to have the **Ready to view** status in the [Landing page](../../../interface/landing-page/):

![](../ready_to_view.jpg)

Then click **Ready to view**, expand the **Do you want to learn about your application?** and select the **Third-party components** tile:

![](third_party.jpg)

{{% alert color="info" %}}If you do not see this tile, it's likely that your application does not meet the requirements (see above). Contact your CAST representative to discuss how to obtain a CAST Highlight subscription.{{% /alert %}}

Clicking the **Third-party components** tile will display the following screen, where all outdated third-party components are listed:

![](panel.jpg)

| Item | Description |
|---|---|
| Component Name | Lists the name of the obsolete third-party component found in your application. |
| Version | Displays the version number of the obsolete third party component found in your application. |
| Release Date | Displays the release date of the version of the obsolete third party component found in your application. |
| Gap | Displays the age of the obsolete third party component - i.e. the difference between today's date and the release date of the version found in your application. The larger the gap the higher the prioritization should be for modernization. Ages are listed in three categories:<br><br><ul><li><font color="red"><strong>Red</strong></font> - two years old or more</li><li><font color="orange"><strong>Orange</strong></font> - between one and two years old</li><li><font color="green"><strong>Green</strong></font> - one year old or less</li></ul> |
| CVE | Displays statistics about any CVEs (Common Vulnerabilities and Exposures etc.) that are present in the obsolete third-party component. The total count of CVEs is displayed together with the specific number per criticality:<br><br><ul><li>Critical (<font color="black"><strong>black</strong></font>) > these should be prioritised for modernization</li><li>High (<font color="red"><strong>red</strong></font>)</li><li>Medium (<font color="yellow"><strong>yellow</strong></font>)</li><li>Low (<font color="grey"><strong>grey</strong></font>)</li><li>Advisory CVEs</li></ul> |
| Safer & Closest Version | As its name indicates, this component version has less vulnerabilities and is the version released closest to the current version found in the application. This is typical information you would consider to make the component safer. It also represents a lower risk of functional issues related to the upgrade, as the gap with the current component version in use is usually smaller. |
| Safest Version  | This is the ultimate upgrade goal for a component containing vulnerabilities. This version is the one with the lowest number of vulnerabilities across the component timeline. It might be risky to directly upgrade a component to this version as the gap with your current version can be quite significant, possibly multiple major releases newer. Note that the safest version is not necessarily the latest published version, as it is the closest version to the one detected in the application. |
| Release per year | Displays the average number of releases per year of the third party component. |
| Object Count | Displays the total number of objects present in the third party component – for example, for a Java framework, you may find Java Class objects within the framework. Additionally, you can filter by the object count value using the icon in the header (you can enter the count value and select on the option from the list). |

{{% alert color="info" %}}You can also access outdated third-party components via the [Insights](../../../interface/viewer/right-panel/insights/) menu option in the [right-panel](../../../interface/viewer/right-panel/):<br><br>![](right-panel.jpg){{% /alert %}}

## Where are these outdated components used in my application?

Once you have a list of the outdated third-party components, the next step is find out where these components are being used in your application so that you can make an update plan. The right panel shows a high-level view of the items interacting with the third-party component in your application. Clicking the **Investigate** button opens up the relevant objects and nodes in full screen mode. A hypothetical example is explained below.

### Example

Take the following example where the **Jackson Databind** component (a general data-binding functionality for [Jackson](https://github.com/FasterXML/jackson)) is flagged as five years old, with 65 CVEs, 22 of which are critical. This is a great candidate to upgrade. Look at the right panel high-level overview to see adherence:

![](example1.jpg)

We can see two types of elements, Java classes and MVC classes that have some connections to the third-party component.

Next click the **Investigate** button to drill-down to the details - this will help you understand the impact of updating the component, or if you want to remove it entirely. CAST Imaging will show you the internal elements (classes) and whether they  concentrated or scattered throughout your application. The red square in the image below highlights the external class from the Jackson component and all other items correspond to internal classes that part of the application and which call the Jackson class: 

![](example2.jpg)

You can save this information as a view in order to prepare the upgrade:

![](../save.jpg)

It is also possible to view the exact piece of source code that defines where your custom object is calling the the class - click the link between the objects to do so:

![](../code.jpg)

Finally, you can add a [Post-It](../../../interface/viewer/right-panel/post-its) to the view to inform your co-workers what needs doing:

![](sticky.jpg)