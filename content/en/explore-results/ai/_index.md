---
title: "Leverage AI services"
linkTitle: "Leverage AI services"
type: "docs"
no_list: true
weight: 20
---

***

## Overview

Did you know that CAST Imaging can interface with various third-party Artificial Intelligence services, allowing you to obtain additional AI-driven insight into your application source code? You can obtain information about specific objects, objects within the scope of your application and about specific transactions.

## How does it work?

Set up is simple: obtain an **API key** or other **configuration settings** from a supported AI provider, configure it in CAST Imaging via the **Admin** option and enable the AI-driven CAST Imaging services you require. A user with the Administrator role is required to configure the settings.

You can find out more in [AI Settings](../../administer/settings/ai-settings/):

![](ai.jpg)

## What can I do with AI?

All AI-driven services provided by CAST Imaging provide additional information about the code within your application. For example, you may want to find out exactly what a specific object is designed to do, based on AI's interpretation of its code, or, you may want to get more insight into the full chain of a transaction in your application. Each available service is explained in more detail below.

### Explain code with AI

Enables right click contextual menu options when consulting results at **Object level**:

![](explain1.jpg)

#### Explain the object

This option provides an AI-driven explanation of the code in the object, directly in the source code viewer:

![](explain2.jpg)

AI will also point out issues that it believes may exist in your source code:

![](explain3.jpg)

Finally, two options are made available when the AI explanation has been provided:

- Save the explanation as a [Post-It](../../interface/viewer/right-panel/post-its/) assigned to the object in question
- Re-load the explanation and provided additional details

![](explain4.jpg)

#### Explain object in application context

This option will do two things:

1. Open the object in a **new tab** and display all its **immediate caller and callee** objects
1. Provide an explanation of the object based on its context within the application and store that explanation in a [Post-It](../../interface/viewer/right-panel/post-its/):

![](explain_app.jpg)

### Imaging Assistant

The Imaging Assistant is a simple interface for asking direct questions about the application you are working on. It is available at any level and in all features within CAST Imaging when consulting results:

![](chatbot.jpg)

The responses returned by the chatbot are tailored to your application, for example you could ask about the technology stack that exists in your application, what frameworks are used, any dependencies that may exist etc.:

![](chatbot2.jpg)

{{% alert color="info" %}}CAST utilizes the ChatGPT Model (Built on ChatGPT 4) to power its Imaging Assistant. Behind the scenes, CAST feeds specific data from the Neo4J results storage database into the model. This database holds extracted data from analyses, including relationships and objects. The model processes this data to respond to queries. Consequently, there is no transfer of source code when using the Assistant Chatbot.{{% /alert %}}

#### Ask me anything - quick prompts

Three main "frequently asked questions" are provided as prompts - use these to get started:

![](quick_prompts.jpg)

#### Ask me anything - Predefined view recommendation

The assistant will recommend a predefined view if one is available and relevant to the query, for example after asking about database objects in the application, the assistant will recommend the predefined view **Database Access**:

![](predefined_view.jpg)

#### Summarize with AI

This feature is available for the **Transaction**, **Data Call Graph** and **Module** scopes:

![](summarize.jpg)

It will generate a summary of the purpose of the selected transaction/data call graph/module, display it and then give you the option to save the summary to a [Post-It](../../interface/viewer/right-panel/post-its/):

![](postit1.jpg)

Ensure you **save** the Post-It if you want to access it in a future session from the [Post-It](../../interface/viewer/right-panel/post-its/) section in the [right panel](../../interface/viewer/right-panel/):

![](save.jpg)

### Transaction summary

Creates an AI driven explanation of the objects within a given transaction, displayed in a [Post-It](../../interface/viewer/right-panel/post-its/), associated to the object itself. To access this feature you will need to use the **Customize the Results** option available in the [Landing page](../../interface/landing-page/):

![](customize_results.jpg)

Then choose the **Transaction Summary** tab:

![](transaction_summary_access.jpg)

All transactions in your application will be listed in this panel. To generate the AI-drive explanation:

- select the transactions you are interested in
- choose the method of generating the explanation:
    - **Graph Representation Method**: summarizes transactions by providing the entire transaction graph, offering a comprehensive overview of all connected objects and their relationships for a holistic understanding
    - **Exclusive Object Method**: summarizes transactions by focusing on unique objects within the transaction graph, highlighting key points of divergence or importance
- click the **Generate Transaction Summary** button to begin the process

![](transaction_summary_action.jpg)

A **green tick** will indicate that the process has completed. A **status icon** also exists showing the process of any on-going and queued jobs:

![](transaction_summary_result.jpg)

To view the result:

- consult your application results
- switch to the **Transaction** scope and **select the transaction** you have generated an AI-drive explanation for
- drill down to the **object level**
- the items will display in a new tab
- click the [Post-It](../../interface/viewer/right-panel/post-its/) to view the AI explanation for each object:

![](transaction_summary_postit.jpg)