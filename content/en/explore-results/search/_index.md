---
title: "Search your results"
linkTitle: "Search your results"
type: "docs"
no_list: true
weight: 40
---

***

CAST Imaging provides various methods for searching your results, i.e. to find the specific object or node that you need:

{{< cardpanehome >}}
{{% cardhomereference %}}
<i class="fa-solid fa-globe"></i>&nbsp;[Global search](global/)
{{% /cardhomereference %}}
{{% cardhomereference %}}
<i class="fa-solid fa-magnifying-glass"></i>&nbsp;[Spotlight search](spotlight/)
{{% /cardhomereference %}}
{{% cardhomereference %}}
<i class="fa-solid fa-magnifying-glass-plus"></i></i>&nbsp;[Custom view search](custom-view/)
{{% /cardhomereference %}}
{{< /cardpanehome >}}

***
