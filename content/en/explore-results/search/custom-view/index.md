---
title: "Using the custom view search"
linkTitle: "Custom view search"
type: "docs"
weight: 30
no_list: true
---

***

## Overview

The custom view search is a specific search feature available for the following types of view:

- **Specific scopes/perimeters** provided out of the box (i.e. Transactions, Data Call Graph...)
- **Saved views** views that you have created and saved for future reference

## How does it work?

The search facility allows you to search for specific items for the type of view you are working in. For example:

- **Transaction** and **Data Call Graphs**: search for specific Transactions or Data Call Graphs by filtering on the objects, links, technologies, tags, post-its etc. within them (e.g.: you can search for a transaction containing Java Class objects)
- **Saved views**: search for a saved view based on its name

## How do I access the search option?

### Transaction and Data Call Graphs

Use the icon in the [left-panel](../../../interface/viewer/left-panel/) highlighted in the image below:

![](icon.jpg)

![](transactions.jpg)

{{% alert color="info" %}}The search window functions in the same way as the [Global search](../global/).{{% /alert %}}

### Saved views

Access the list of saved views:

![](saved_views1.jpg)

Use the icon in the [left-panel](../../../interface/viewer/left-panel/) highlighted in the image below:

![](saved_views2.jpg)

{{% alert color="info" %}}The search window functions in the same way as the [Global search](../global/).{{% /alert %}}