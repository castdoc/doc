---
title: "Using the spotlight search"
linkTitle: "Spotlight search"
type: "docs"
weight: 20
no_list: true
---

***

## Overview

Are there too many items in the view? Do you want to find a specific item in the current view? If so, you can use the **Spotlight** search option to find what you need:

![](spotlight.jpg)

## How does it work?

The Spotlight search option:

- searches only those items that are currently visible in the view that is currently in focus
- matches a string you enter on the **object/node name**, or the **full object/node name**:

![](spotlight2.jpg)

- highlights the objects in the view that you have searched for: select (using the check boxes or mouse click) all the items you want to highlight, then click the search icon in the top right:

![](select.jpg)

![](highlight.jpg)

- retains previous selections in the current view unless you use the **clear all** option:

![](clearall.jpg)

