---
title: "Using the global search"
linkTitle: "Global search"
type: "docs"
weight: 10
no_list: true
---

***

## Overview

CAST Imaging offers a comprehensive and advanced search facility, allowing you to find exactly what you are looking for. The search will return **objects** present in your application (not just those currently visible (see [Spotlight search](../spotlight)) in the view), based on the search string or cypher query you enter.

In addition, there are a number of filters available to help you narrow down what you are looking for.

## How do I access the global search facility?

Use the search bar available at the top of the screen, either:

- enter a **search string**
- or click the **Advanced** button

![](search_bar.jpg)

Or choose the **Custom** option in the [left-panel](../../../interface/viewer/left-panel/):

![](custom.jpg)

In all cases, the global search dialog will be displayed. Entering a **search string** in the search box will return all matching objects, while using the **Advanced** button or **Custom** option will return all objects in the application by default:

![](search.jpg)

## What can I do with the results?

Use the check boxes on the left side of the global search dialog to select the objects you need to investigate further. Click **Display Selected** in the bottom right corner to visualize all the selected objects and any links between them in a dedicated tab ( a new tab is opened for each search display):

![](display.jpg)

Similarly you can download the selected items to `.CSV` file:

![](download.jpg)

{{% alert color="info" %}}The **Download** option can be enabled/disabled using the **Objects export** option in the [Preferences](../../../administer/viewer-prefs/) panel.{{% /alert %}}

## How do I use the filters?

Are your searches returning too many objects? If so, you can use the filters located along the top to narrow down what you are looking for:

![](filters.jpg)

For example:

- **Object type** (filter on specific objects such as Java Field, SQL Server table etc.)
- **Tags** (filter on objects that have been [tagged](../../../interface/viewer/view/actions/#tags-tool))
- **Internal/External** (filter on internal or external (i.e. third-party libraries) object types)
- **Insights** (filter on objects that contain an Insight such as a Structural Flaw, or a CloudMaturity blocker etc.)
- **Post-Its** (filter on objects that have been assigned to an object Post-It - note that view level [Post-Its](../../../interface/viewer/view/actions/#post-its-tool) are not included)
- **Property** (filter on objects with a matching property such as Number of Code Lines, Cyclomatic Complexity etc. - see also [Search Configuration](../../../administer/settings/search-configuration) for more information about changing the properties that are available to users)

{{% alert color="info" %}}Some filter options will not always be visible: for example, if you have not added any [Tags](../../../interface/viewer/view/actions/#tags-tool) or [Post-Its](../../../interface/viewer/view/actions/#post-its-tool) and the application has no Insights, then these filters will not be available.{{% /alert %}}

### Saving filters

If you regularly need to use the same filter options to find what you need, you can save them and return to them in a later session via the Save Filters button:

![](save_filter.jpg)

A saved filter will appear in the left hand panel of the search window and can be re-used whenever required (marked as **"1"**) below. Use the action menu (**"2"**) to **Rename** or **Delete** existing filters:

![](save_filter2.jpg)

## What about AND, OR and NOT conditions?

Some filters (Object types, Post-Its, Properties) provide **AND**, **OR** and **NOT** conditions if you need to narrow down even further:

![](ANDOR.jpg)

{{% alert color="info" %}}By default, all filters are set to **AND**.{{% /alert %}}

## What about other search options?

The Global Search feature provides other search options that are configurable on a **per-user** and **per-session** basis (i.e. they will not persist between logins):

![](options.jpg)

- **Regex pattern**: Enable this option to search via a regular expression, examples below:
    - Digits matching - `"[0-9]", "[0,6]"`
    - Wildcard matching for single character for multiple-characters - `".", ".*"`
    - Alphabet matching - `"[a-z]","[A-Z]"`
    - Matching case-sensitive words - `"keyword"`
    - Matching case-insensitive words - `"[kK]eyword" `
    - Matching Special Characters - `"Code_[0-9]{5}"`
    - Optional Characters and Quanitifiers - `"?", "+"`
    - Matching Repetitions - `"{n}"`
    - Excluding character - `"[^asd]"`
- **Case sensitivity**: Enable this option to do a case sensitive search (disabled by default).
- **Search by**: Name/Fullname (Name by default) - select as appropriate. The search string will be applied to the options you choose.
- **Search by position** - select as appropriate to specify where in the string the match should occur. By default this is set to **Start**.

{{% alert color="info" %}}You can set these options to **persist between sessions** via the [Preferences](../../../administer/viewer-prefs/) menu option:<br><br>![](pref.jpg)<br><br>![](persist2.jpg){{% /alert %}}

## What about cypher search?

Cypher search is a feature that allows you to directly query the Neo4j database (which underpins the CAST Imaging result generation system) using "cypher queries" - a specific functional language. See https://neo4j.com/docs/cypher-manual/current/introduction/ for more information and examples.

Use the **Cypher** option to enter and run your queries:

![](cypher.jpg)

{{% alert color="info" %}}<ul><li>If you do not see the <strong>Cypher</strong> search option in the UI, it may be because your user login does not have appropriate permissions. See <a href="../../../administer/settings/user-permissions/">User Permissions</a> for more information about the <code>Cypher Search Access</code> role.</li><li>If you are attempting to return object properties in a Cypher search, then you should be aware that the <code>return distinct</code> clause will not work and in this specific situation you should use a <code>return *</code> clause instead.</li></ul>{{% /alert %}}

## Tips

### Add new search results to an existing tab

All objects located via a search will always be added to a **new tab** in the view, however, if you want to search for additional objects and add them to a tab that has already been opened following a previous search, use the following action icon in the existing tab:

![](add_objects.jpg)

Using this action icon will open the search popup and any new objects you select for display will be added to the existing tab.
