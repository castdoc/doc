---
title: "Generating built-in report"
linkTitle: "Built-in reports"
type: "docs"
no_list: true
weight: 10
---

***

## Overview

CAST Imaging provides several "built-in" reports which you can download in `.csv`, `.xlsx` and `.json` format:

![](reports.jpg)

## Generating reports

Use the **Download Reports** option available in the top left action menu:

![](action_menu.jpg)

## Available reports

### Data Sources

#### Relation between Objects And Data sources

- Object Name
- Object Full Name
- Object Type
- Object File Name
- Data Source Name
- Data Source Full Name
- Data Source Type
- Access Link Type (Use, Access, Reylon etc.)

#### Relation between DB tables And DB objects

- DB Table Name
- DB Table Full Name
- DB Object Name
- DB Object Full Name
- DB Object Type,
- Access Link Type (Use, Access, Reylon, Refer etc.)

#### Relation between Data sources and Transactions

- Data Source Name
- Data Source Full Name
- Data Source Type
- Transaction Entry Name
- Transaction Entry Full Name
- Transaction Entry Type
- Access Link Types (Use, Access, Reylon, Refer etc.)
- Object File Name

#### Number of Transactions per Data source

Helps identify loosely coupled or highly coupled data sources:

- Data Source Name
- Data Source Full Name
- Data Source Type
- Access Link Type
- Total (links)

### Transactions

#### Relation between Transactions And Objects

- Transaction Entry Name
- Transaction Entry Full Name
- Transaction Entry Type
- Object Name
- Object Full Name
- Object Type
- Object Role in Transaction (startpoint/endpoint)
- Object File Name

#### Relation between Transactions And Data sources

- Transaction Entry Name
- Transaction Entry Full Name
- Transaction Entry Type
- Data Source Name
- Data Source Full Name
- Data Source Type
- Access Links Count (number of times the data source is accessed)
- Access Link Type (Use, Access, Reylon etc.)
- Object File Name

#### Transaction's Complexity

- Transaction Entry Name
- Transaction Entry Full Name
- Transaction Entry Type
- CC (Cyclomatic Complexity) value
- IC (Integration Complexity) value
- EC (Essential Complexity) value
- Total object count value
- LoC (lines of code) value
- Object File Name

See How Complexity metrics are calculated by CAST AIP for more information about the complexity values provided.

### References

#### Most referenced Objects

- Object Name
- Object Full Name
- Object Type
- Object File Name
- Module Name
- References Count (this value corresponds to the number of objects calling the object)

#### Most referenced Data Objects

- Object Name
- Object Full Name
- Object Type
- Number Of Calls (this value corresponds to the number of objects calling the object)

#### Unreferenced Objects

- Object Name
- Object Full Name
- Object Type
- Object File Name
- External (whether the object is categorized as "external")

### Application

#### Relation between Modules

- Source Module Name
- Source Object Name
- Source Object Full Name
- Source Object Type
- Target Module Name
- Target Object Name
- Target Object Full Name
- Target Object Type
- Link Type
- Object File Name

#### Relation between Modules and Transactions

- Module Name
- Transaction Entry Name
- Transaction Entry Full Name
- Transaction Entry Type
- Object File Name

#### Module's Complexity

- Module Name
- CC (Cyclomatic Complexity) value
- IC (Integration Complexity) value
- EC (Essential Complexity) value
- Total object count value
- LoC (lines of code) value
- Object File Name

See How Complexity metrics are calculated by CAST AIP for more information about the complexity values provided.

#### Relation between Modules and Objects

- Module Name
- Object Name
- Object Full Name
- Object Type
- Object File Name

#### Lines of Code Per Technology Artifact

- Object Type
- Technology
- Object Count
- Lines Of Code

#### Added/Modified objects within the application compared to the previous version

- Object Name
- Object Full Name
- Object Type
- Object Status
- Object File Name
- Number of Callers Added
- Number of Callers Unchanged
- Number of Callers Modified
- Number of Callees Added
- Number of Callees Unchanged
- Number of Callees Modified

{{% alert color="info" %}}The count of callers/callees (Modified/Added/Unchanged) is determined by the value of the status property of the caller/callee objects during the result generation process.{{% /alert %}}

## Technical notes

- Reports are generated in a batch of 100,000 records to improve performance.
- For `.xlsx` reports, 1 million records are added per sheet in the Microsoft Excel file.
- For `.json` file reports, if there are more than 100,000 records, then the JSON report will not be structured.
