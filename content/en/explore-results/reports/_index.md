---
title: "Generate reports"
linkTitle: "Generate reports"
type: "docs"
no_list: true
weight: 50
---

***

CAST Imaging provides various methods for generating reports:

{{< cardpanehome >}}
{{% cardhomereference %}}
<i class="fa-solid fa-table-list"></i>&nbsp;[Built-in reports](built-in/)
{{% /cardhomereference %}}
{{% cardhomereference %}}
<img class="no-border" alt="Report Generator" src="com.castsoftware.rg.png">&nbsp;[Report Generator](report-generator/)
{{% /cardhomereference %}}
{{< /cardpanehome >}}

***
