---
title: "Report Generator templates"
linkTitle: "Templates"
type: "docs"
no_list: true
---

***

## Overview

Report Generator is supplied with a set of pre-defined templates which can be used as-is or adapted to your own needs. These templates are located here:

```text
%PROGRAMDATA%\CAST\ReportGenerator\<version>\Templates\Application\Compliance reports
%PROGRAMDATA%\CAST\ReportGenerator\<version>\Templates\Application\Component library
%PROGRAMDATA%\CAST\ReportGenerator\<version>\Templates\Application\Legacy reports
%PROGRAMDATA%\CAST\ReportGenerator\<version>\Templates\Application\Sizing reports
%PROGRAMDATA%\CAST\ReportGenerator\<version>\Templates\Portfolio
%PROGRAMDATA%\CAST\ReportGenerator\<version>\Templates\zh_Hans\Application\Compliance reports
%PROGRAMDATA%\CAST\ReportGenerator\<version>\Templates\zh_Hans\Application\Component library
%PROGRAMDATA%\CAST\ReportGenerator\<version>\Templates\zh_Hans\Application\Legacy reports
%PROGRAMDATA%\CAST\ReportGenerator\<version>\Templates\zh_Hans\Application\Sizing reports
%PROGRAMDATA%\CAST\ReportGenerator\<version>\Templates\zh_Hans\Portfolio
```

{{% alert color="info" %}}<ul><li>The equivalent templates for CAST Report Generator for Dashboards are provided in the <code>Templates</code> folder.</li><li>Templates are taken from <code>\Templates\zh_Hans\...</code> when Chinese (Simplified, PRC) is selected in <strong>Settings > Language</strong>.</li><li>Some Application level templates provided in <code>\Templates\zh_Hans</code> have been translated into Chinese.</li><li>Portfolio level templates provided in <code>\Templates\zh_Hans\Portfolio</code> have NOT been translated into Chinese and are instead provided in English.</li></ul>{{% /alert %}}

## Officially supported templates

CAST provides various templates that are officially supported. These templates include scores, transaction risks, propagated risks, trending, scan-by-scan comparisons, lists of critical violations, and lists of components that have the most impact if remediated. Officially supported templates are stored in the `\Templates` folder.

Templates located in the following locations are not officially supported:

```text
\Templates\Application\Legacy reports
\Templates\zh_Hans\Application\Legacy reports
```

## Creating your own custom templates

CAST provides "base" templates that provide an explanation of how to customize (for Microsoft Word, Excel and PowerPoint) and also contain a large set of placeholders (example data that can be extracted in text, graph and table format) that can be copied and then adapted as necessary. The "base" templates are stored in:

```text
\Templates\Application\Component library
\Templates\zh_CN\Application\Component library
```

You can use these base templates as the basis for creating your own report templates - copy them, make your changes and then save them with a different name. Refresh the Report template section in the UI to view the new template.

## Using the Excel output options

Report Generator supports the generation of reports in Excel format, as explained below. A template called "3 - Excel-components-library.xlsx" is also available as standard for Application reports.

### Reuse of Word and PowerPoint blocks

- All table and text blocks can be re-used in Excel.
- To use any block in Excel, prefix all definitions with `RepGen:TEXT` or `RepGen:TABLE`.
- The data will be put on the exact location where the definition resides.

#### Limitations - Text blocks

- Cells referencing a text block will have their content replaced with the actual value received from the REST APIs.
- Style and format information will be preserved. Report Generator will try and generate numeric-type values when the data provided by the REST API is numeric; otherwise, the data will be inserted as text.
- Formulas referencing "text block" cells will work as expected.

#### Limitations - Table blocks

- Report Generator inserts new lines starting from the line that contains the "RepGen:TABLE;BLOCK_NAME;options…" definition. Existing content below the "RepGen:" will not be overwritten.
- Report Generator will not apply any style or format to inserted cells. However, Report Generator will try and generate numeric-type values when the data provided by the REST API is numeric; otherwise, the data will be inserted as text.
- Formulas referencing cells that are part of the block template will not be updated.
- As a result, it is recommended to reserve one worksheet per table block and insert formulas in one or more consolidation worksheets. Formulas referencing template cells should reference whole columns, e.g.: `=SUM('raw_data_worksheet'!D:D))`, or cell ranges that are large enough to address all data extracted from the RestAPI, e.g.: `=SUM('raw_data_worksheet'!D2:D20000)`.

#### Limitations - Graph blocks

- Graph blocks are not supported in Excel: only table and text blocks.

### Block IFPUG_FUNCTIONS

This block will display the Transaction Functions and the Data Functions as listed below:

- HEADER
    - NO: means there is no header displayed from the data from the REST API
    - YES: means that the header will be taken into account as stored in the REST API
- TYPE
    - TF = Transaction Function display
    - DF = Data function display

Example:

![](https://doc-data.castsoftware.com/DOCCOM/attachments/240347963/240348000.jpg)

Result:

![](https://doc-data.castsoftware.com/DOCCOM/attachments/240347963/240348001.jpg)

Adding data inline with text - Word templates
To add a metric ID (for example) inline with text in a Word template, for example:

![](https://doc-data.castsoftware.com/DOCCOM/attachments/240347963/636059711.jpg)

...proceed as follows:

Enter the text into the Word template
Where you want to display the data, switch to the Developer tab (1) and add in a Rich Text Content Control (2), which will be displayed as below (3):

![](https://doc-data.castsoftware.com/DOCCOM/attachments/240347963/636059710.jpg)

Switch to Design Mode:

![](https://doc-data.castsoftware.com/DOCCOM/attachments/240347963/636059709.jpg)

Right click the Rich Text Content Control and select Properties:

![](https://doc-data.castsoftware.com/DOCCOM/attachments/240347963/636059708.jpg)

Enter the data you want to pull in the TAG field, then click OK to confirm. In this example, the total number of critical violations is being displayed using the following syntax: `TEXT;APPLICATION_METRIC;ID=67011`

![](https://doc-data.castsoftware.com/DOCCOM/attachments/240347963/636059707.jpg)
