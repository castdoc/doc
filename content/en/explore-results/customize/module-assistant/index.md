---
title: "Using the Module Assistant"
linkTitle: "Using the Module Assistant"
type: "docs"
no_list: true
weight: 40
---

***

## Overview

The **Module Assistant** will generate an automated functional breakdown of your application(s) based on the application code's wording, sentences and topics identified and correlated to the adherence of elements between them. Several factors can affect the outcomes and keep in mind that for some applications, the results may not be perfect, especially if the source code lacks understandable wording.

## How does it work?

Technically, the Module Assistant will create a custom aggregation (called **Automated Functional Modules**) for each targeted application. This custom aggregation is generated based on keywords found in your target application: related keywords will be grouped together as modules, and links will be created between the modules based on the underlying objects within the modules. This custom aggregation can be selected by any user in the **Perspective > Aggregated by** section of the [left-panel](../../../interface/viewer/left-panel/) for each targeted application.

{{% alert color="info" %}}This feature is "static", in other words, if you re-analyze your application and the source code has changed then you will need to manually re-generate the Module Assistant results so that they reflect the most recent analysis.{{% /alert %}}

## How do I access the feature?

Use the **Customize the results** option in the [Application Landing page](../../../interface/landing-page/):

![](../customize_results.jpg)

Then choose the **Modules Assistant** tab:

![](module_asst.jpg)

## Are there any prerequisites?

### Microsoft Windows

You must enable the **Beta: Use Unicode UTF-8 for worldwide language support** option as follows on the machine hosting your **Viewer** services:

Settings > Time & Language > Language & Region or Language > Administrative Language Settings > Change system locale

![](utf8.jpg)

{{% alert color="info" %}}You may be prompted to reboot your machine after enabling this option.{{% /alert %}}

### Linux via Docker

No prerequisites.
 
## How do I generate the results?

Use the **Generate Module** option:

![](generate_module.jpg)

Use the queue icon to visualize the progress:

![](queue.jpg)

## How do I view the results?

Use the dedicated **Automated Functional Modules** custom aggregation available in the [left-panel](../../../interface/viewer/left-panel):

![](AFM.jpg)