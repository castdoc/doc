---
title: "Using the bulk Tag/Post-It import feature"
linkTitle: "Using the bulk Tag/Post-It import feature"
type: "docs"
no_list: true
weight: 20
---

***

## Overview

Do you actively use the [Tags](../../../interface/viewer/view/actions/#tags-tool) and [Post-Its](../../../interface/viewer/view/actions/#post-its-tool) tools in CAST Imaging to document and annotate your results? If so, did you know that you can **bulk import** both Tags and Post-Its for **multiple or single objects** at the same time, rather than having to create them all manually in the UI?

## How do I access the feature?

Use the **Customize the results** option in the [Application Landing page](../../../interface/landing-page/):

![](../customize_results.jpg)

Then choose the **Bulk Tag/Annotate** option:

![](bulk.jpg)

## How does it work?

The bulk import process requires a `.csv` file containing the data for your Tags and Post-Its, i.e.:

- a list of objects which you want to associate the Tags/Post-Its to
- the data for the Tags/Post-Its themselves

{{% alert color="info" %}}It is not possible to create [Post-Its](../../../interface/viewer/view/actions/#post-its-tool) at **"view"** level, only Post-Its for objects are supported.{{% /alert %}}

## How do I build the .csv file?

A basic template is available for download [here](Document_Import_Template.csv), or it can be downloaded from the Bulk Import panel:

![](download.jpg)

The syntax of the `.csv` file is as follows:

- Each line in the file corresponds to one Post-It and/or Tag to be imported for each object.
- The line must contain information to identify the object to which the Post-It and/or Tag will be attached, and the title/content of the Post-It and/or the content of the Tag.

The template file contains the following line:

```csv
"Object ID","Object Name","Object Full Name","Object Type","External","Post-it Title","Post-it Description","Tags"
```

Where:

| Item | Description |
|---|---|
| `Object ID, Object Name, Object Full Name, Object Type, External` | These items identify the object that the Post-It and/or Tag will be associated with. To obtain this information, see [below](#obtaining-the-data-for-the-csv-file). |
| `Post-It Title`/`Post-It Description` | Enter the Post-It **title** and **description**, e.g. the text you would enter in the UI as indicated here:<br><br> ![](post-it.jpg) |
| `Tags` | Enter the name of the tag you want to assign to the object (this tag does not need to exist already). You can also enter multiple tags for one single object using a comma separated list surrounded by quote marks, for example `"My_Tag4,My_Tag10"`.<br><br> Note that it is not possible to insert Tags that have the following prefix: `Status -`, `CloudReady -`, `Containerization -`, and `StructuralFlaw -`. |

### Obtaining the data for the .csv file

The information required by the bulk import `.csv` file can be obtained by using the **Download** option in the [global search](../../search/global/) feature. I.e. search for and select the objects you want to add to the bulk import `.csv` file and then download the results:

![](download_button.jpg)

{{% alert color="info" %}}If you do not see the **Download** option you can enable it using the **Preferences > Objects Export** option:<br><br>![](pref_button.jpg){{% /alert %}}

The resulting file will give you the information you need for the following fields:

- `Object ID`
- `Object Name`
- `Object Full Name`
- `Object Type, External`

For example:

```csv
Object ID,Object Name,Object Full Name,Object Type,External
3058,authors,dbo.authors,SQL Server Table,false
4068,authors,com.castsoftware.bookdemo.javabean.NewSaleEjbItf.authors,Java Field,false
```

### Example .csv import file

#### A unique Post-It for 4 objects, no Tags

The position for the `Tag` entry is left empty (the comma at the end of the line denotes this):

```csv
"Object ID","Object Name","Object Full Name","Object Type","External","Post-It Title","Post-It Description","Tags"
2429,author,[D:\CAST\data\deploy\MEUDON1\main_sources\JSP\ListTitles.jsp].author,Java Field,false,My_PostIt1,My_Desc1,
2945,Author,com.castsoftware.bookdemo.javabean.Author,Java Class,false,My_PostIt2,My_Desc2,
2734,Author,com.castsoftware.bookdemo.javabean.Author.Author,Java Method,false,My_PostIt3,My_Desc3,
2233,Author,com.castsoftware.bookdemo.javabean.Author.Author,Java Constructor,true,My_PostIt4,My_Desc4,
```

#### A unique Tag for 4 objects, no Post-Its

The positions for the `Post-It Title`/`Post-It Description` entries are left empty (the separating commas are left):

```csv
"Object ID","Object Name","Object Full Name","Object Type","External","Post-It Title","Post-It Description","Tags"
2429,author,[D:\CAST\data\deploy\MEUDON1\main_sources\JSP\ListTitles.jsp].author,Java Field,false,,,My_Tag1
2945,Author,com.castsoftware.bookdemo.javabean.Author,Java Class,false,,,My_Tag2
2734,Author,com.castsoftware.bookdemo.javabean.Author.Author,Java Method,false,,,My_Tag3
2233,Author,com.castsoftware.bookdemo.javabean.Author.Author,Java Constructor,true,,,My_Tag4
```

#### A unique Post-It for 4 objects all with unique Tags

```csv
"Object ID","Object Name","Object Full Name","Object Type","External","Post-It Title","Post-It Description","Tags"
2429,author,[D:\CAST\data\deploy\MEUDON1\main_sources\JSP\ListTitles.jsp].author,Java Field,false,My_PostIt1,My_Desc1,My_Tag1
2945,Author,com.castsoftware.bookdemo.javabean.Author,Java Class,false,My_PostIt2,My_Desc2,My_Tag2
2734,Author,com.castsoftware.bookdemo.javabean.Author.Author,Java Method,false,My_PostIt3,My_Desc3,My_Tag3
2233,Author,com.castsoftware.bookdemo.javabean.Author.Author,Java Constructor,true,My_PostIt4,My_Desc4,My_Tag4
```

#### A mixture

```csv
"Object ID","Object Name","Object Full Name","Object Type","External","Post-It Title","Post-It Description","Tags"
2429,author,[D:\CAST\data\deploy\MEUDON1\main_sources\JSP\ListTitles.jsp].author,Java Field,false,My_PostIt1,My_Desc1,"My_Tag1,My_Tag2"
2945,Author,com.castsoftware.bookdemo.javabean.Author,Java Class,false,,,My_Tag2
2734,Author,com.castsoftware.bookdemo.javabean.Author.Author,Java Method,false,My_PostIt3,My_Desc3,
2233,Author,com.castsoftware.bookdemo.javabean.Author.Author,Java Constructor,true,My_PostIt4,My_Desc4,My_Tag4
```

## How do I import the .csv file?

Use the **Upload** button:

![](upload.jpg)

## What about the Link Post-It option?

![](link-postit.jpg)

By default, Post-Its are not visible in the [right-panel](../../../interface/viewer/right-panel/) when using the **Aggregated by > Module** option:

![](module.jpg)

Enabling the **Link Post-It** option will ensure that any Post-Its assigned to objects in the **current application** are made available in the [right-panel](../../../interface/viewer/right-panel/):

![](post-it_rightpanel.jpg)
