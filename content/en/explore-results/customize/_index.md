---
title: "Customize your results"
linkTitle: "Customize your results"
type: "docs"
no_list: false
weight: 30
---

***

This section provides information about how to customize the results generated during an analysis. For the most part, they describe the options available in the **Customize your results** option (available in the [landing page](../../interface/landing-page/)):

![](customize_results.jpg)

Other customization options are also described.