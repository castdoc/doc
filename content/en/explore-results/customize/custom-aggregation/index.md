---
title: "Using the custom aggregation feature"
linkTitle: "Using the custom aggregation feature"
type: "docs"
no_list: true
weight: 50
---

***

## Overview

When consulting your [results](../) you can choose how your application is displayed using one of the default "perspectives/aggregation modes" available out of the box (depending on the perimeter/scope you choose) - these default modes are explained in [User Interface Reference](../../../interface/viewer/left-panel/):

![](perspective-dropdown.jpg)

However, you may find that these default modes do not meet your requirements for displaying your application data. If so, you can create your own custom aggregation mode and display your objects as you have designed.

## How does it work?

An aggregation is essentially a method of grouping specific items together, therefore the creation of a custom aggregation involves creating custom nodes that group together specific object types (or a mix of object types). When you choose to display your application using the custom aggregation mode, the items in your application are grouped into the custom nodes defined in your custom aggregation.

For example a custom aggregation has been created containing three custom nodes:

- Java classes
- Java methods
- SQL objects (any type)

This aggregation has then been applied to this example and simple client/server Java/SQL application:

![](example.jpg)

### Dynamic custom aggregations

Custom aggregations are **dynamic** - this means that the objects they display will automatically update when the application is re-analyzed and new objects are identified. For example, if the custom aggregation contains a custom node that captures Java Methods and the application is re-analyzed and new Java Methods are identified, then these objects will be automatically displayed in the custom aggregation. This behaviour was not present in previous releases (i.e. the custom aggregation remained static and was never updated with new objects identified in a new analysis).

## Things to know about custom aggregations

- Custom aggregations are **application** specific. In other words, a custom aggregation will only be available for the specific application it was created in.

- Any user that has permission to view results in the application the custom aggregation was created in will be able to use the custom aggregation by default as soon as it is [published](#step-3---publish-the-custom-aggregation), however, by default only the user that created the custom aggregation will be able to modify it. Edit access can be granted to additional users/groups using the [Share button](#step-4---share-the-custom-aggregation).

## Step-by-step instructions

### Step 1 - Create the custom aggregation

First ensure you are viewing the application you want to create the custom aggregation mode for, then click the dedicated icon in the header bar:

![](icon.jpg)

Then enter a name for the new custom aggregation: this name will be displayed in the left panel and publicly identifies it. When the aggregation has been created, you can then start adding your custom nodes:

![](add_nodes.jpg)

### Step 2 - Adding custom nodes

To add a custom node, click either of the **Custom object** icons:

![](custom_nodes.jpg)

A window will open displaying a list of all the objects in your application: use the filter options at the top of the screen to find the objects that you want to group in the custom node:

![](search_for.jpg)

In this example we want the node to contain only **Java Method** object types, so we can use the **Object type** filter:

![](filter_object_type.jpg)

Click **Save** in the bottom right corner of the window to ensure the choice is applied and choose a name for the custom node:

![](save.jpg)

The new custom node is then displayed:

![](result.jpg)

Repeat this process until you have added all the custom nodes you require. Any links between nodes will be displayed as shown in the example below:

![](result2.jpg)

### Step 3 - Publish the custom aggregation

Before users can use the custom aggregation you will need to publish it:

![](publish.jpg)

This will make it available in the [left-panel](../../interface/viewer/left-panel/) for all users that have permissions to view the application.

![](left_panel.jpg)

{{% alert color="info" %}}When a custom aggregation has been published, it is no longer possible to add new or edit/remove existing nodes within it. To do so, you should "unpublish" it (use the button in the header) first.{{% /alert %}}

### Step 4 - Share the custom aggregation

Initially the custom aggregation can only be edited by the user that created it. Therefore if you would like other users to be able to modify it, use the **Share** button:

![](share.jpg)

### Step 5 - Editing existing custom aggregations

To edit an existing custom aggregation, ensure it is not in "published" mode:

![](unpublish.jpg)

You can:

- rename the custom aggregation
- delete the custom aggregation
- add new or remove/edit existing nodes

![](edit.jpg)