---
title: "Using the Manage Level5 option"
linkTitle: "Manage Level5"
type: "docs"
no_list: true
weight: 10
---

***

## Overview

Do you need to hide specific objects that are irrelevant to your result investigation? If so, this feature allows you to do so by manually hiding items from the results. Note however, that ideally the analysis should be refined to eliminate code that is inconsistent with the application's scope.

## How do I access the feature?

Use the **Customize the results** option in the [Application Landing page](../../../interface/landing-page/):

![](../customize_results.jpg)

Then choose the **Manage Level5** option:

![](managelevel5.jpg)

## How do I hide items?

The table will list all items that are present at Level 5 in the current application's results:

![](items.jpg)

Use the **toggle switches** to hide specific items, or select the items you want to hide and then use the **Hide Selected** button:

![](hide.jpg)

Hidden items will now no longer be visible in the results, for all users.

{{% alert title="note" color="info" %}}<ul><li>Hiding items at level 5 also hides them from levels 1 - 4.</li><li>Use the <strong>toggle switches</strong> or the <strong>Show Selected</strong> button to unhide items that you have already hidden</li></ul>{{% /alert %}}
