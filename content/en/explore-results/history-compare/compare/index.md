---
title: "Working with the compare feature"
linkTitle: "Compare feature"
type: "docs"
no_list: true
---

***

{{% alert color="beta" %}}This feature is currently in beta and we would welcome feedback.{{% /alert %}}

## Overview

Would you like to visualize changes in successive analyses of your application, directly in the view? I.e. whether objects have been **added**, **deleted**, **modified** or **unchanged** or whether links have changed since the last application analysis? If so, you can explore the CAST Imaging "**Compare**" feature:

![](../../../interface/viewer/right-panel/compare/compare.jpg)

{{% alert color="info" %}}The **Compare** feature is closely related to the [History](../history) feature and the two complement each other.{{% /alert %}}

## How can I access this feature?

The Compare feature is available in the [right panel](../../interface/viewer/right-panel/):

![](compare.jpg)

## Are there any requirements?

- the application needs to have been imported into CAST Imaging **≥ 3.2** whether via result import or via an analysis.
- the application needs to have been **imported twice or more** into CAST Imaging whether via result import or via an analysis so that a comparison can be generated for objects and links. If there is only one "import" then a message indicates that the feature is not available:

![](not_available_compare.jpg)

- the feature only functions when working at **Object level**, if this is not the case a message indicates that the feature is not available for this view:

![](not_available2_compare.jpg)

- if you have already selected a previous analysis via the [History](../history/) feature, this will disable the **Compare** feature, since CAST Imaging is already displaying hiitorical information:

![](not_available3_compare.jpg)

## How does it work?

By default, CAST Imaging always displays the most recent information about the objects and links in the view. If **Compare** is available, it is then possible to **enable** the visualization in the view and then to **choose a previous analysis** via the date selector to compare with the current state (CAST Imaging will always compare the current state with the next most recent analysis, unless you choose a different previous analysis):

![](enable_compare.jpg)

When the feature is enabled, the view will update to highlight the change between the two analyses. In the following example, one object is recorded as having been modified, i.e. when comparing the state on the two chosen dates, the object was found to have changed (all other objects and links are unchanged):

![](example.jpg)

Use the toggles to enable/disable the display of specific items in the view, for example, disabling the display of Unchanged objects will leave only objects that have been modified, added or deleted:

![](toggle.jpg)

## What status can objects and links have?

See the same section in [History](../history/#what-status-can-objects-and-links-have).

