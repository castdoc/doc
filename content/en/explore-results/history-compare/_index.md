---
title: "History and Compare"
linkTitle: "Explore history and compare"
type: "docs"
no_list: false
weight: 60
---

***

{{% alert color="beta" %}}These features are currently in beta and we would welcome feedback.{{% /alert %}}

This section provides information about how to explore your results using the **History** and **Compare** features available in the [right panel](../../interface/viewer/right-panel/):

![](historycompare.jpg)

These features allow you to understand what has **changed** with regard to objects and links in successive analyses of your application - therefore they are only available if you have more actioned more than one analysis.
