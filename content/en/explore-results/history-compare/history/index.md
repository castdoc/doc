---
title: "Working with object and link history"
linkTitle: "Object and link history"
type: "docs"
no_list: true
---

***

{{% alert color="beta" %}}This feature is currently in beta and we would welcome feedback.{{% /alert %}}

## Overview

Would you like to understand what has changed in successive analyses of your application, i.e. whether objects have been **added**, **deleted**, **modified** or **unchanged** or whether links have changed since the last application analysis? Look no further than the CAST Imaging "**History**" feature:

![](../../../interface/viewer/right-panel/history/history.jpg)

## How can I access this feature?

The History feature is available in the [right panel](../../interface/viewer/right-panel/):

![](history_location.jpg)

## Are there any requirements?

- the application needs to have been imported into CAST Imaging **≥ 3.2** whether via result import or via an analysis.
- the application needs to have been **imported twice or more** into CAST Imaging whether via result import or via an analysis so that a comparison can be generated for objects and links. If there is only one "import" then a message indicates that the feature is not available:

![](not_available.jpg)

- the feature only functions when working at **Object level**, if this is not the case a message indicates that the feature is not available for this view:

![](not_available2.jpg)

## How does it work?

By default, CAST Imaging always displays the most recent information about the objects and links in the view. If **History** is available, it is then possible to "travel back in time" to a previous date (i.e. the date of a previous analysis/import) and CAST Imaging will display the state of the objects and links in the view at that time. Use the date selector to do so: in this example there are three "versions" of the application - on selecting a date, the screen will update to show the situation at that time:

![](date_selector.jpg)

In addition, **selecting an object or a link** in the view will show the history of that item over time:

![](item_history.jpg)

![](item_history2.jpg)

## What status can objects and links have?

Statuses of objects and links are based on a checksum generated during the original analysis of the source code:

**Objects** will have one of the following statuses:

- **Added**: is new since the previous version (this is also the status in the very first version of an application where multiple versions exist).
- **Unchanged**: has not changed since the previous version.
- **Modified**: the object existed in a previous version, but has been changed in the current displayed version.
- **Deleted**: the object existed in a previous version, but has been deleted in the current displayed version. This status will only be displayed when you have selected a previous date with the date selector (an item cannot be deleted in the most recent version).

**Links**:

- **Added**
- **Unchanged**
- **Deleted**

## How do I return to the current status?

If you have selected previous version data via the date selector, use the highlighted icon to return to the most recent state:

![](switch_back.jpg)

<!-- HIDDEN, NOT YET IMPLEMENTED

## What about enabling the object and link status?

The toggle switch under the date selector will (when enabled) highlight the changes in the view. For example, a user is displaying a given transaction and they have chosen to travel back to the first "import" of their application and enabled the status view:

![](example.jpg)

All objects and all links are highlighted in green since they are considered to have a status of "**Added**".

If the user then switches to the most recent version any changes will be highlighted. In our example, there have been no changes so all items are highlighted in grey with the status "**Unchanged**":

![](unchanged.jpg)

-->