---
title: "Development life-cycle"
linkTitle: "Development life-cycle"
type: "docs"
no_list: true
---

***

## Third party software

### Preservation of intellectual property rights on third-party software
When adding a third-party source code library, we verify that intellectual property rights are preserved. A page in our official documentation describes all uses of third-party source code (See [https://doc.castsoftware.com/legal/open-source/](../open-source)) and which license is specified. We use multiple tools to scan vulnerabilities such as [Docker Scout](https://docs.docker.com/scout/), [OWASP dependency checker](https://owasp.org/www-project-dependency-check/) and [CAST Highlight](https://www.castsoftware.com/highlight).

### Update of third party software

Third-party components used within CAST developments must regularly be updated to new versions to get fixes and improvements. This is done by the team at the occasion of the start of each major version or when required by a new functional need. For most components, testing will be performed during Dev and QA stages without specific study on each component. For some specific component with direct and important impact on security (database access, authentication, etc.), information about the new version included in the product will be given on Change Management request for the production release to allow verification on known security flaw for the component. Upgrade of a third-party component may also be proposed by IT administrators when a known vulnerability is solved by a patch. The request is done through a Change Management request and depending on the severity of the vulnerability may need to be tested and
implemented as soon as possible.

### Third-party source access and integrity

Third-party software is loaded from trusted source using package manager including checksum validation operations.