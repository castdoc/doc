---
title: "EOL policy for delivery of CAST Imaging product versions"
linkTitle: "EOL Policy"
type: "docs"
no_list: true
---

| Delivery Type | Purpose | Release Frequency | Bug Fixes | Audience | New Features | Numbering | Support |
|---|---|---|---|---|---|---|---|
| ALPHA | Add new feature. | 15 days | No | Early stage / Test / Urgent need for this feature | Yes | Alpha_buildnumber_date | No |
| BETA | Stop adding major features, stabilization, bug fixes. | 20 days | Yes | Early stage / Test / Need for feature and / or quick bug fixes. | Yes | beta_buildnumber_date | Yes |
| FUNCTIONAL RELEASE (FuncRel) | Public version with new features. | CAST Analyzers : 90 days<br><br>CAST Imaging Console: 3 Months<br><br>CAST Imaging Viewer: 3 Months | Yes | Can be used by customers in production. | Yes | First or second digit identifies the functional level. Third digit is used when the version contains mostly bug fixes. Version name also containes the FuncRel suffix (2.0.0-funcrel, ...). | Yes |
| LONG TERM SUPPORT (LTS) | Stability and reliability and continuity in analysed results | Minimum of 24 months | Yes, Service Packs | Public version that it has been field tested and can be used by customer in production. | No | The LTS release is identified by the three digits and the absence of suffix (1.1.4 is as LTS, 1.1.4-funcrel is not). The third digit changes with every new service pack. (1.1.0, 1.1.1, ....). | Yes, supported up to 12 month after the release of a new version. |
| MAJOR VERSION | New version containing significant improvements in existing functionality or the addition of new functions. | N/A | Yes, new beta | Can be used by customers in production | Yes | Fist digit ex 8.x | Yes, supported up to 12 month after the release of the new minor version. |
| MINOR VERSION | New version including limited improvements to existing functionality. The changes or improvements will not affect functionality and / or the interoperability of the Software installed | N/A | Yes | Can be used by customers in production | Yes | first 2 digit ex 8.3 | Yes, supported up to 12 month after the GA release of the new minor version. |
| SERVICE PACK | Collection of corrections related to a minor version. | 2 months | Yes | Can be used by customers in production | No | First 3 digit ex 8.3.11 | Yes, as long as the major version is supported. |
| PATCH | Limited to one or several corrections. | depending on the needs. | Yes | Customer who reported the issue. | No | HotFix_<service pack number><build number><patch number> | Yes |