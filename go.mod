module gitlab.com/castdoc/doc

go 1.22

require (
	github.com/FortAwesome/Font-Awesome v0.0.0-20240108205627-a1232e345536 // indirect
	github.com/google/docsy v0.9.0 // indirect
	github.com/twbs/bootstrap v5.2.3+incompatible // indirect
	gitlab.com/castdoc/export v0.0.0-20240425152421-83733cd84543 // indirect
	gitlab.com/castdoc/extensions v0.0.0-20240426105514-e087a5420ff2 // indirect
)
